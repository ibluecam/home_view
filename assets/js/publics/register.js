$( document ).ready(function() {

    $('#frm_register').validate({ 
    	ignore: ':hidden:not(:checkbox)',
        errorPlacement: function (error, element) {
            if (element.is(":checkbox")) {
                $("#error_check").html(error);
                element.closest('.checkbox').append(error);
            } else {
                error.insertAfter(element);
            }
    	},
        rules: {
        	firstname: {
        		required: true
        	},
        	lastname: {
        		required: true
        	},
            email: {
                required: true,
                minlength: 3,
                maxlength: 50,
                email: true,
                remote: {
                    url: "/register/validEmail",
                    type: "post",
                    data: {
                      email: function() {
                        return $("#email").val();
                      }
                    }
                }
            },
            password: {
                required: true,
                minlength: 3,
                maxlength: 20
            },
            agree: {
        		required: true
        	}
        },
        messages: {
        	firstname: "The Firstname field is required.",
        	lastname:  "The Lastname field is required.",
            email: {
                required: "The Email field is required.",
                email: "The Email field must contain a valid email address.",
                remote: jQuery.format("Email already exists.") 
            },
            password: "You must provide a Password.</div>",
            agree: "Please indicate that you accept the Terms of use and Privacy."
        },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });

});

