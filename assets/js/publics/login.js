$( document ).ready(function() {
    $('#login').validate({ 
        rules: {
            email: {
                required: true,
                minlength: 3,
                maxlength: 50,
                email: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                required: "This email is required not empty",
                email: "Please enter a valid email address."
            },
            password:"This password is required not empty."
        },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });

});

