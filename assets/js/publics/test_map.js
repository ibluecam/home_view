
		var map;
		var markers = [];
		var iconBase = '/assets/img/';
		var icons = {House: {icon: iconBase + 'line.png'},Villa: {icon: iconBase + 'viber.png'}};
		var my_path = []; 
        // Draw free hand on map.
		function drawFreeHand(){
		    //the polygon
	        poly = new google.maps.Polyline({
                map:map,
                clickable:false,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
			});
	      
	        //move-listener
	        move=google.maps.event.addListener(map,'mousemove',function(e){
	            poly.getPath().push(e.latLng);
	        });
	      
	        //mouseup-listener
	        google.maps.event.addListenerOnce(map,'mouseup',function(e){
	          removeListener(move);
	          path = poly.getPath();

	          poly_gon = new google.maps.Polygon({
	            map:map,
	            path:path,
	            fillColor: '#FF0000',
	            fillOpacity: 0.35
	          });

	        // Get latlng inside drawing
	        var get_id = "";
	        var my_featurs = "";
	        for (var i = 0, feature; feature = features[i]; i++) {
	            var  position=new google.maps.LatLng(feature.lat, feature.lng);
	              if (google.maps.geometry.poly.containsLocation(position, poly_gon)) {
	                 get_id += feature.id+",";
	                 my_featurs+="{" + "\"id\":" +"\""+ feature.id +"\""+ 
                        ",\"type\":"+"\""+feature.type +"\""+
                        ",\"title\":"+"\""+feature.title+"\""+
                        ",\"lat\":"+"\""+feature.lat+"\""+
                        ",\"lng\":"+"\""+feature.lng +"\""+
                        ",\"price\":"+"\""+feature.price+"\""+
                        ",\"area\":"+"\""+feature.area +"\""+"},"
	            }
	        }
	        var map_id = get_id.split(',');
	        map_id.pop();
	        var max_price = $(".price_val").val();
	        if(get_id == ""){
	        	clearMarker();
	        }else{
		      searchData(features,map_id,max_price,100);
	    	}
	        poly_gon.setMap(null);
			poly.setMap(null);
	        google.maps.event.clearListeners(map.getDiv(), 'mousedown');
	        enable();
	        $("#drawpoly").removeClass('draw');
	        var data_featurs= my_featurs.substring(0, my_featurs.length - 1);
            var json_featurs = '['+data_featurs+']';
            features =JSON.parse(json_featurs);
	      });
		}

		// Disable Map Before  Draw free hand.
        function disable(){
	        map.setOptions({
	          draggable: false, 
	          zoomControl: false, 
	          scrollwheel: false, 
	          disableDoubleClickZoom: false,
	          styles: [
	            {
	                    elementType: "geometry.fill",
	                    stylers: [{
	                        hue: "#91c8f2"
	                    }, {
	                        saturation: -30
	                    }, {
	                        visibility: "off"
	                    }]
	                }, {
	                    elementType: "geometry.stroke",
	                    stylers: [{
	                        hue: "#82AD36"
	                    }, {
	                        saturation: -50
	                    }, {
	                        visibility: "off"
	                    }]
	                }, {
	                    featureType: "administrative.locality",
	                    elementType: "labels.text",
	                    stylers: [{
	                        visibility: "off"
	                    }, {
	                        color: "#252525"
	                    }, {
	                        weight: .1
	                    }]
	                }
	          ],
	          draggableCursor: 'crosshair',
	        });
        }

        // Enable Map After Draw free hand
        function enable(){
	        map.setOptions({
	          draggable: true, 
	          zoomControl: true, 
	          scrollwheel: true, 
	          disableDoubleClickZoom: true,
	          styles: [ { stylers: [ { visibility: 'on' } ] } ],
	          draggableCursor: 'grab',
	        });
        }

        // Create Marker
	    function createMarker(arrayMarker){
			var bounds = new google.maps.LatLngBounds(); 
	      	for (var i = 0, feature; feature = arrayMarker[i]; i++) {
	      		var get_position = new google.maps.LatLng(feature.lat, feature.lng);
	            var marker = new google.maps.Marker({
	                position:  get_position,
	                icon: icons[feature.type].icon,
	                map: map,
	                title: feature.title,
	                class: "show_detail"
	          	});
	               markers.push(marker);
	            (function (marker, feature) {
	                google.maps.event.addListener(marker, "click", function (e) {
	                	get_class = this.class;
    					if(get_class == "show_all" || get_class == "show_detail"){
	                    	$('.map-card').show();
	                    	addClass();
    						marker.set("class", "hide_detail");
    					}else{
    						$('.map-card').hide();
	                		marker.set("class", "show_detail");
    					}
	                	var content_detail = "<div class='wrap_detail' id='id_"+feature.id+"'>"+
	                	"<div class='wrap_image'><img src='/assets/img/home_image2.jpg' alt=''></div>"
	                	+"<div class='wrap_title'>"+feature.title+"</div>"+
	                	"<div class='wrap_description'>"+feature.type+"</div>"
	                	+"</div>";
	                    $('.map-card').html(content_detail);
	                });
	            })(marker, feature);
	            bounds.extend(get_position); 
	        }
	        if(arrayMarker.length > 0){   
	            map.fitBounds(bounds);
	            bounds.getCenter();
			}
            google.maps.event.addListener(map, 'click', function() {
                $('.map-card').hide();
                addClass();
            });
	    }

	    // Clear All Marker
	    function clearMarker(){
	        for (var i = 0; i<= markers.length; i++) {
	           	if(markers[i]!= undefined){
	            	// markers[i].setMap(null);
	            	markers[i].setVisible(false);
	        	}
	    	}
	    }
	    // Initialize Map on load
	    function initialize() {
		    var country="Khum Baray";
			var geocoder = new google.maps.Geocoder();  
		    geocoder.geocode({
		        'address': country
		    }, function(results, status) {
		        if (status == google.maps.GeocoderStatus.OK) {
					var set_style_map = [
					{
	                    elementType: "geometry.fill",
	                    stylers: [{
	                        hue: "#91c8f2"
	                    }, {
	                        saturation: -30
	                    }, {
	                        visibility: "on"
	                    }]
	                }, {
	                    elementType: "geometry.stroke",
	                    stylers: [{
	                        hue: "#82AD36"
	                    }, {
	                        saturation: -50
	                    }, {
	                        visibility: "on"
	                    }]
	                }, {
	                    featureType: "administrative.locality",
	                    elementType: "labels.text",
	                    stylers: [{
	                        visibility: "on"
	                    }, {
	                        color: "#252525"
	                    }, {
	                        weight: .1
	                    }]
	                }, {
	                    featureType: "poi",
	                    elementType: "labels",
	                    stylers: [{
	                        visibility: "off"
	                    }]
	                }, {
	                    featureType: "transit.station.airport",
	                    elementType: "labels",
	                    stylers: [{
	                        visibility: "off"
	                    }]
	                }, {
	                    featureType: "landscape",
	                    elementType: "labels",
	                    stylers: [{
	                        visibility: "off"
	                    }]
	                }],
	                my_style_map = new google.maps.StyledMapType(set_style_map, {
	                    name: "Styled Map"
	                });
		            var myOptions = {
		                zoom: 11,
		                center: results[0].geometry.location,
		                mapTypeId: "styled_maps",
					    zoomControl: false,
					    mapTypeControl: false,
					    streetViewControl: false
		            }

		            map = new google.maps.Map(document.getElementById("map"), myOptions);
		            map.mapTypes.set("styled_maps", my_style_map);
		            createMarker(features);
		        }
            });
	    }

		$(document).ready(function(){

			// Declare Varible.
	        var price_slider = document.getElementById('price_slider');
	        var area_slider = document.getElementById('area_slider');
	        var price_range;
	        var area_range;
	        var get_price_val;
	        var get_area_val;
			var get_id = "";
			var map_id;
			var get_price_silder;
	        if(page_type == "rent"){
	        	price_range = {
	        		"min": [0, 2],
                    "1%": [2, 1],
                    "40%": [10, 1],
                    "70%": [20, 10],
                    "90%": [100, 20],
                    "99%": [200, 999800],
                    "max": [1e6]
                }
	        }else{
	        	price_range = {
	              	"min": [0, 200],
                    "1%": [200, 200],
                    "40%": [2e3, 1e3],
                    "80%": [1e4, 2e3],
                    "99%": [2e4, 98e4],
                    "max": [1e6]
	            }
	        }
	        area_range = {
                        "min": [0, 20],
                        "1%": [20, 5],
                        "35%": [40, 10],
                        "max": [100]
                    }

            // Create Price Slider
	        noUiSlider.create(price_slider, {
	            start: start_price,
                behaviour: "tap",
                connect: "lower",
                direction: "rtl",
                orientation: 'vertical',
	            range: price_range
	        }); 
			price_slider.noUiSlider.on('update', function ( values, handle ) {
	        	show_tooltip_price(values[0]);
	        	get_price_silder = parseInt(values[0]);
	        	if(get_price_silder == 0){
	        		get_price_val = price_range.max[0];
	        	}else{
	        		get_price_val = get_price_silder + "0000";
	        	}
				$(".price_val").val(parseInt(get_price_val));
				for (var i = 0, feature; feature = features[i]; i++) {
	                 	get_id += feature.id+",";
	        	}
		        map_id = get_id.split(',');
		        map_id.pop();
			});
			price_slider.noUiSlider.on('end', function(){
				var get_area_value = $(".area_val").val();
				alert(get_area_value);
				searchData(features,map_id,parseInt(get_price_val),100);
			});
			price_slider.noUiSlider.on('start', function(values){
				show_tooltip_price(values[0]);
			});

			//Create Area Slider
			noUiSlider.create(area_slider, {
	            start: start_area,
                behaviour: "tap",
                connect: "upper",
                direction: "rtl",
                orientation: 'vertical',
	            range: area_range
	        });
	        area_slider.noUiSlider.on('update', function ( values, handle ) {
	        	show_tooltip_area(values[0]);
				get_area_val = parseInt(values[0]);
				$(".area_val").val(parseInt(get_area_val));
				for (var i = 0, feature; feature = features[i]; i++) {
	                 	get_id += feature.id+",";
	        	}
		        map_id = get_id.split(',');
		        map_id.pop();
			});
			area_slider.noUiSlider.on('end', function(){
				var get_price_value = $(".price_val").val();
				searchData(features,map_id,parseInt(get_price_value),parseInt(get_area_val));
			});
			area_slider.noUiSlider.on('start', function(values){
				show_tooltip_area(values[0]);
			}); 
			
			// Block show tooltip price on slider
			function show_tooltip_price(values){

				$(".price_box").appendTo('#price_slider .noUi-handle');
	        	$(".price_box").stop().fadeIn(1,function(){
	        		$(".price_box").stop().fadeOut(1400);
	        	});
				if(parseInt(values) == price_range.max[0]){
					$(".price_box").html("No max");
				}else{
				 	$(".price_box").html("¥ " + parseInt(values) + "0000");
				}

			}

			// Block show tooltip price on slider
			function show_tooltip_area(values){

				$(".area_box").appendTo('#area_slider .noUi-handle');
	        	$(".area_box").stop().fadeIn(1,function(){
	        		$(".area_box").stop().fadeOut(1400);
	        	});
				if(parseInt(values) == area_range.min[0]){
					$(".area_box").html("No min");
				}else{
				 	$(".area_box").html(parseInt(values) + "m²");
				}

			}

			$(document).on("click","#price",function(){
				$(this).toggleClass('show_height');
				if($(this).hasClass('show_height')){
					$('.slider_price').show();
					$('#area').css({"marginTop":"0"});
					$('.slider_area').css({"marginTop":"40px"});
					$('.slider_price').animate({
						"height":"195px"
					},300,function(){
						
					});
				}else{
					$('.slider_price').animate({
						"height":"0px"
					},300,function(){
						$('.slider_price').hide();
						$('#area').css({"marginTop":"70px"});
						$('.slider_area').css({"marginTop":"120px"});
					});
				}
			});
			$(document).on("click","#area",function(){
				$(this).toggleClass('show_height');
				if($(this).hasClass('show_height')){
					$('.slider_area').show();
					$('.slider_area').animate({
						"height":"195px"
					},300,function(){
						
					});
				}else{
					$('.slider_area').animate({
						"height":"0px"
					},300,function(){
						$('.slider_area').hide();
					});
				}
			});
			//draw
			$("#drawpoly").click(function(e) {
				$(this).toggleClass('draw');
				if($(this).hasClass('draw')){
				    e.preventDefault();
				    disable();
				    google.maps.event.addDomListener(map.getDiv(),'mousedown',function(e){
				    	if($("#drawpoly").hasClass('draw')){
				    		var get_all_features = $('.all_featurs').val();
				    		features = JSON.parse(get_all_features);
				    		drawFreeHand();
				    	}
				    });
				}else{
					enable();
				}
			});
			$(".price_box").html("No max");
		});

		function searchData(jsonData,id,price,area){
			var data="";
            for(var i = 0; i < jsonData.length; i++){
              if($.inArray(jsonData[i].id, id) != -1 && parseInt(jsonData[i].price) <= parseInt(price) && parseInt(jsonData[i].area) <= parseInt(area)){
                data+="{" + "\"id\":" +"\""+ jsonData[i].id +"\""+ 
                        ",\"type\":"+"\""+jsonData[i].type +"\""+
                        ",\"title\":"+"\""+jsonData[i].title+"\""+
                        ",\"lat\":"+"\""+jsonData[i].lat+"\""+
                        ",\"lng\":"+"\""+jsonData[i].lng +"\""+
                        ",\"price\":"+"\""+jsonData[i].price+"\""+
                        ",\"area\":"+"\""+jsonData[i].area +"\""+"},"
              }
            }
            var dataString= data.substring(0, data.length - 1);
            var jsontext = '['+dataString+']';
            var datasearch =JSON.parse(jsontext);
		    clearMarker();
            createMarker(datasearch);
		}

		// Remove Event
		function removeListener(event){
			google.maps.event.removeListener(event);
		}

		// Add class show all to map box detail
		function addClass(){
        	for (var i = 0; i<= markers.length; i++) {
	           	if(markers[i]!= undefined){
	            	markers[i].set("class", "show_all");
	        	}
	    	}
		}
