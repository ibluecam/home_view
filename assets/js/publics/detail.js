// Initialize Map on load
  function initialize() {
      var lat=$(".get_latlng").attr("lat");
      var lng=$(".get_latlng").attr("lng");
	     var set_style_map = [
	     {
	            "featureType": "water",
	            "elementType": "all",
	            "stylers": [
	                {
	                    "hue": "#a3ccff"
	                },
	                {
	                    "saturation": 100
	                },
	                {
	                    "lightness": -5
	                },
	                {
	                    "visibility": "on"
	                }
	            ]
	        }
	        ],
	         my_style_map = new google.maps.StyledMapType(set_style_map, {
	             name: "Styled Map"
	         });
	         var latlng = new google.maps.LatLng(lat, lng);
           var myOptions = {
                zoom: 17,
                center: latlng,
                mapTypeId: "styled_maps",
     			      zoomControl: true,
       		      mapTypeControl: false,
       	 	      streetViewControl: false,
                scrollwheel: false,
                navigationControl: false,
                scaleControl: false,
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            map.mapTypes.set("styled_maps", my_style_map);
            var marker = new google.maps.Marker({
			        map: map,
			        position: latlng
		        });
            marker.setAnimation(google.maps.Animation.BOUNCE);
  			    var sunCircle = {
  			        strokeColor: "#000000",
  			        strokeOpacity: 0.8,
  			        strokeWeight: 2,
  			        fillColor: "#d8d8d8",
  			        fillOpacity: 0.35,
  			        map: map,
  			        center: latlng,
  			        radius: 100 // in meters
  			    };
  			    cityCircle = new google.maps.Circle(sunCircle)
  			    cityCircle.bindTo('center', marker, 'position');
            // google.maps.event.addDomListener(window, 'load', initialize);
	}


$(function(){
  $(".fancyboxLauncher").on("click", function () {
    $(".fancyboxDetail").eq(0).trigger("click");
    return false;
  });
    // fancybox
  $(".fancyboxDetail").fancybox({       
    openEffect  : 'elastic',
    closeEffect : 'elastic',
    nextEffect  : 'none',
    prevEffect  : 'none',
    loop    : false
  }); 

  $(document).on('click','#back_link',function(){
     window.history.back();
  });

  $("#show_description").click(function(){
    $(".text_description").toggle();
  });

  $('#frm_inquire').validate({ 
      ignore: ':hidden:not(:checkbox)',
        errorPlacement: function (error, element) {
            if (element.is(":checkbox")) {
                $("#error_check").html(error);
                element.closest('.checkbox').append(error);
            } else {
                error.insertAfter(element);
            }
      },
        rules: {
            name: {
              required: true
            },
            email: {
                required: true,
                minlength: 3,
                maxlength: 50,
                email: true
            },
            phone: {
                required: true,
                minlength: 5,
                maxlength: 20
            },
            message: {
             required: true,
             minlength: 10
           },
           agree: {
            required: true
          }
        },
        messages: {
          name: "The Name field is required.",
          email: {
              required: "The Email field is required.",
              email: "The Email field must contain a valid email address.",
          },
          phone: "The Phone field is required",
          message: "The Message field is required",
          agree: "The Terms field is required."
        },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });


});
