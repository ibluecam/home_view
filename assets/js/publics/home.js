
	$(document).ready(function(){
		$('.link_blog').on('click',function(){
			var get_data_blog = $(this).attr('data_blog');
			var removeClassAllActive = $('.blog_link span a').removeClass('blog_link_active');
			var addClassActive = $(this).addClass('blog_link_active');
			var addClassBlog = $('.blog_link span a').addClass('link_blog');
			var removeClassBlog = $(this).removeClass('link_blog');
			$.ajax({
				method: 'POST',
				url: '/home/ajax_load_blog',
				data: {category: get_data_blog}
			}).done(function(msg){
				$('.blog_data_left').html(msg);
				removeClassAllActive;
				addClassActive;
				addClassBlog;
				removeClassBlog;
			});
		});
	});