
$(document).ready(function(){
	$('.tab-link').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.tab-link').removeClass('current');
		$('.tab-content').removeClass('current');

		$('.content_full_check').addClass('tab-content');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});
	$('.all_tabs').click(function(){
		$('.content_full_check').removeClass('tab-content');
	});
});