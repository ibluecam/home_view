$(document).ready(function(){
    $('#frm_edit_user').validate({ 
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            email: {
                required: true,
                minlength: 3,
                maxlength: 50,
                email: true,
                remote: {
                    url: "/publics/favorite/validEmailEdit",
                    type: "post",
                    data: {
                      email: function() {
                        return $("#email").val();
                      },
                      userId:function(){
                        return $("#user_id").val();
                      }
                    }
                }
            }
        },
        messages: {
            firstname:"Firstname field is required.",
            lastname:"Lastname field is required.",
            email: {
                required: "The Email field is required.",
                email: "The Email field must contain a valid email address.",
                remote: jQuery.format("Email already exists.") 
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "firstname" )
                error.insertAfter("#firstname_error");
            else if  (element.attr("name") == "lastname" )
                error.insertAfter("#lastname_error");
            else if  (element.attr("name") == "email" )
                error.insertAfter("#email_error");
       },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });



	$('#popup_password').fancybox({
			closeBtn: false
	});
	$('#close_pwd').on('click',function(){
		$('.alert-message').html("");
		$('.pwd').val("");
		$.fancybox.close();
	});
	$('#btn_ok_pwd').on('click',function(){ 
		var user_id = $('.hide_user_id').val();
		var password = $('#password').val();
		var conf_pwd = $('#conf_pwd').val();
		$.fancybox.showLoading();
        $.ajax({
        	url: '/favorite/info/edit/'+user_id,
        	method: 'POST',
        	data: {password: password,conf_pwd:conf_pwd}
        }).done(function(msg){
        	$.fancybox.hideLoading();
        	$.fancybox.update();
        	$('.alert-message').html(msg);
        });
    });




});
