
	function add_fav(home_id){
		$(".add_fav_"+home_id).addClass('faa-pulse animated');
		$.ajax({
				method: "POST",
				url: "/lists/add_favorite",
				data: {"home_id":home_id}
			}).done(function(msg){
				$(".add_fav_"+home_id).toggleClass('fa-heart fa-heart-o faa-pulse animated');
				$(".link_add_"+home_id).toggleClass('update_fav add_fav');
			});
	}
	function delete_fav(home_id){
		$(".update_fav_"+home_id).addClass('faa-pulse animated');
		$.ajax({
				method: "POST",
				url: "/lists/delete_favorite",
				data: {"home_id":home_id}
			}).done(function(msg){
				$(".update_fav_"+home_id).toggleClass('fa-heart fa-heart-o faa-pulse animated');
				$(".link_update_"+home_id).toggleClass('update_fav add_fav');
			});
	}

	$(document).ready(function(){
		$('.content_filter').click(function(){
			$('.content_slide_filter').slideToggle("fast");
			$('.bg_sort').toggle();
		});
		$("#btn_go").click(function(){

			var get_link_status = $('.hide_link_status').val();

			var lists_link = get_link_status;

			//Get Type Values
			var get_type_val = '';
			$('.typecheck:checked').each(function(){
				var type_val = $(this).val();
        		get_type_val += type_val + "+";
			});
			if(get_type_val != ""){
				lists_link += "lo=" + get_type_val.slice(0,-1) + "&";
			}

			//Get Maked Values
			var get_maked_val = '';
			$('.makedcheck:checked').each(function(){
				var maked_val = $(this).val();
        		get_maked_val += maked_val + "+";
			});
			if(get_maked_val != ""){
				lists_link += "mk=" + get_maked_val.slice(0,-1) + "&";
			}

			//Get Maked Values
			var get_position_val = '';
			$('.positioncheck:checked').each(function(){
				var position_val = $(this).val();
        		get_position_val += position_val + "+";
			});
			if(get_position_val != ""){
				lists_link += "po=" + get_position_val.slice(0,-1) + "&";
			}

			//Get Min Price Value
			var get_min_price_val = $('.min_price').val();
			if(get_min_price_val != ""){
				lists_link += "mnp=" + get_min_price_val + "&";
			}

			//Get Max Price Value
			var get_max_price_val = $('.max_price').val();
			if(get_max_price_val != ""){
				lists_link += "mxp=" + get_max_price_val + "&";
			}

			//Get Min Breadth Value
			var get_min_breadth_val = $('.min_breadth').val();
			if(get_min_breadth_val != ""){
				lists_link += "mnb=" + get_min_breadth_val + "&";
			}

			//Get Max Breadth Value
			var get_max_breadth_val = $('.max_breadth').val();
			if(get_max_breadth_val != ""){
				lists_link += "mxb=" + get_max_breadth_val + "&";
			}

			//Get Min Breadth Value
			var get_min_age_val = $('.min_age').val();
			if(get_min_age_val != ""){
				lists_link += "mna=" + get_min_age_val + "&";
			}

			//Get Max Breadth Value
			var get_max_age_val = $('.max_age').val();
			if(get_max_age_val != ""){
				lists_link += "mxa=" + get_max_age_val + "&";
			}

			//Get Min Distance to Station Value
			var get_min_distance_to_station = $('.min_distance_to_station').val();
			if(get_min_distance_to_station != ""){
				lists_link += "mnd=" + get_min_distance_to_station + "&";
			}

			//Get Max Distance to Station Value
			var get_max_distance_to_station = $('.max_distance_to_station').val();
			if(get_max_distance_to_station != ""){
				lists_link += "mxd=" + get_max_distance_to_station + "&";
			}

			//Get Other Values
			var get_other_val = '';
			$('.othercheck:checked').each(function(){
				var other_val = $(this).val();
        		get_other_val += other_val + "+";
			});
			if(get_other_val != ""){
				lists_link += "ot=" + get_other_val.slice(0,-1) + "&";
			}

			window.location.href = lists_link.slice(0,-1);
		});
		$("#btn_reset").click(function(){
			window.location.href = "/lists";
		});
	});