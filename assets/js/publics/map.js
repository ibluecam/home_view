	

	var map;
	var icons = "/assets/img/icon/home_icon_map.png";
	var markers = [];
	var get_city_id = $('.pop_city_by_per').attr('city_id');
	var get_perfecture_id = $('#pop_perfecture').attr('perfecture_id');
    var get_min_price = "";
    var get_max_price = "";
    var get_house = "";
    var get_apartment = "";
    var get_buy = "";
    var get_rent = "";
    var get_short = "";
    var sort = "";


	$(document).ready(function(){

		$(document).on('click','#pop_city_by_per',function(){
			var get_per_name_text = $('.my_per_name').html();
			if(get_per_name_text == "select"){
				$('.city_pop').html('Please select perfecture first!');
			}else{
				get_perfecture_id = $('#pop_perfecture').attr('perfecture_id');
				ajax_pop_up();
			}
		});

		$(".get_per_text,#pop_perfecture,#pop_city_by_per").fancybox({
			closeBtn: false
		});
		
		$(document).on('click','.close_popup',function(e){
	        e.preventDefault();
	        $.fancybox.close();
	    });

	    $(document).on('click','.close_detail_map',function(){
	    	$('.content_map_detail').fadeOut(1);
	    });

	    $(document).on('click','.get_per_text',function(){
	    	var get_perfecture_id = $(this).attr('perfecture_id');
			$('#pop_perfecture').attr('perfecture_id',get_perfecture_id);
			$('.my_city_name').html('select');
			$('#pop_city_by_per').attr('city_id','');
			var get_per_name = $(this).find('.get_per_name').html();
			$('.my_per_name').html(get_per_name);
			var get_my_per = $('.my_per_name').html();
			ajax_pop_up();
			do_ajax_get_latlng();
	    });

		$(document).on('click','.city_pop .link_city',function(){
			var get_city_id = $(this).attr('my_city_id');
			$('#pop_city_by_per').attr('city_id',get_city_id);
			var get_link_city = $(this).find('label').attr('id');
			$('.my_city_name').html(get_link_city);
			do_ajax_get_latlng();
	        $.fancybox.close();
		});
		$(document).on('click','#house,#apartment,#buy,#rent,#short-stay',function(){
			do_ajax_get_latlng();
		});
		$(document).on('change','.min_price,.max_price',function(){
			do_ajax_get_latlng();
		});

		$(document).on('click','.pagination a',function(){
			var get_offset = $(this).attr('href');
			var cut_offset = get_offset.split('/');
			var offset = cut_offset[cut_offset.length-1];
			if(offset == "ajax_load_building"){
				offset = 0;
			}
			var building_name = $('#get_building_name').val();
			var get_perfecture_id = $('#pop_perfecture').attr('perfecture_id');
	    	var get_city_id = $('#pop_city_by_per').attr('city_id');
			get_same_val();
			$.ajax({
				method: "POST",
  				url: "/map/ajax_load_building/"+offset+"?building_name="+building_name+"&perfecture_id="+get_perfecture_id+"&city_id="+get_city_id+"&buy="+get_buy+"&rent="+get_rent+"&short="+get_short+"&house="+get_house+"&apartment="+get_apartment+"&min_price="+get_min_price+"&max_price="+get_max_price,
  				async: true
			}).done(function(msg){
				$('.detail_in_map').html(msg);
			});
			return false;
		});


	});

	function get_same_val(){
		get_perfecture_id = $('#pop_perfecture').attr('perfecture_id');
    	get_city_id = $('#pop_city_by_per').attr('city_id');
		($('#house').is(':checked')) ? get_house = $('#house').val() : get_house = "";
		($('#apartment').is(':checked')) ? get_apartment = $('#apartment').val() : get_apartment = "";
		($('#buy').is(':checked')) ? get_buy = $('#buy').val() : get_buy = "";
		($('#rent').is(':checked')) ? get_rent = $('#rent').val() : get_rent = "";
		($('#short-stay').is(':checked')) ? get_short = $('#short-stay').val() : get_short = "";
        get_min_price = $('.min_price').val();
        get_max_price = $('.max_price').val();
	}
	function do_ajax_get_latlng(){
		get_perfecture_id = $('#pop_perfecture').attr('perfecture_id');
    	get_city_id = $('#pop_city_by_per').attr('city_id');
		($('#house').is(':checked')) ? get_house = $('#house').val() : get_house = "";
		($('#apartment').is(':checked')) ? get_apartment = $('#apartment').val() : get_apartment = "";
		($('#buy').is(':checked')) ? get_buy = $('#buy').val() : get_buy = "";
		($('#rent').is(':checked')) ? get_rent = $('#rent').val() : get_rent = "";
		($('#short-stay').is(':checked')) ? get_short = $('#short-stay').val() : get_short = "";
        get_min_price = $('.min_price').val();
        get_max_price = $('.max_price').val();
        ajax_get_latlng(get_perfecture_id,get_city_id,get_house,get_apartment,get_buy,get_rent,get_short,get_min_price,get_max_price);
        link_to_list_condition(get_perfecture_id,get_city_id,get_house,get_apartment,get_buy,get_rent,get_short,get_min_price,get_max_price);
        
	}
	function ajax_get_latlng(get_perfecture_id,get_city_id,get_house,get_apartment,get_buy,get_rent,get_short,get_min_price,get_max_price){
		var get_ajax_link = $('.hide_val_ajax').val();
		var link_ajax_list = get_ajax_link;
		$.ajax({
				method: "POST",
				url: "/map/ajax_load_latlng"+link_ajax_list,
				data: {
					    perfecture_id: get_perfecture_id,
					    city_id: get_city_id,
						house: get_house,
						apartment: get_apartment,
						buy: get_buy,
						rent: get_rent,
						short_stay: get_short,
						min_price: get_min_price,
						max_price: get_max_price,
					  }

			}).done(function(msg){
				// alert(msg);
				var datasearch =JSON.parse(msg);
			    clearMarker();
	            createMarker(datasearch);
			});
	}
	function ajax_pop_up(){
		get_same_val();
		var get_ajax_link = $('.hide_val_ajax').val();
		var link_ajax_list = get_ajax_link;
		$.ajax({
			method: "POST",
			url: "/map/ajax_load_city"+link_ajax_list,
			data: {
					perfecture_id: get_perfecture_id,
					house: get_house,
					apartment: get_apartment,
					buy: get_buy,
					rent: get_rent,
					short_stay: get_short,
					min_price: get_min_price,
					max_price: get_max_price,
				}
		}).done(function(msg){
			$('.city_pop').html(msg);
		});
	}
	function link_to_list_condition(get_perfecture_id,get_city_id,get_house,get_apartment,get_buy,get_rent,get_short,get_min_price,get_max_price){

		var get_lists_link = $('.hide_link_val').val();
		var link_to_list = get_lists_link;

        if(get_perfecture_id != ""){
        	link_to_list += "pf="+get_perfecture_id+"&";
        }
        if(get_city_id != ""){
        	link_to_list += "ct="+get_city_id+"&";
        }
        if(get_buy != ""){
        	link_to_list += "by="+get_buy+"&";
        }
        if(get_rent != ""){
        	link_to_list += "rt="+get_rent+"&";
        }
        if(get_short != ""){
        	link_to_list += "sht="+get_short+"&";
        }
        if(get_house != ""){
        	link_to_list += "hou="+get_house+"&";
        }
        if(get_apartment != ""){
        	link_to_list += "apmt="+get_apartment+"&";
        }
        if(get_min_price != ""){
        	link_to_list += "mnp="+get_min_price+"&";
        }
        if(get_max_price != ""){
        	link_to_list += "mxp="+get_max_price+"&";
        }

        link_to_list = link_to_list.slice(0, -1);
        $('.content_menu_map a.link_map').attr('href',link_to_list);
	}
	// Initialize Map on load
	    window.initialize = function() {
		    var country="TOKYO";
			var geocoder = new google.maps.Geocoder();  
		    geocoder.geocode({
		        'address': country
		    }, function(results, status) {
		        if (status == google.maps.GeocoderStatus.OK) {
					var set_style_map = [
					{
				        "featureType": "water",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#a3ccff"
				            },
				            {
				                "saturation": 100
				            },
				            {
				                "lightness": -5
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    }
				    ],
	                my_style_map = new google.maps.StyledMapType(set_style_map, {
	                    name: "Styled Map"
	                });
		            var myOptions = {
		                zoom: 17,
		                center: results[0].geometry.location,
		                mapTypeId: "styled_maps",
					    zoomControl: true,
					    mapTypeControl: true,
					    streetViewControl: false
		            }

		            map = new google.maps.Map(document.getElementById("map"), myOptions);
		            map.mapTypes.set("styled_maps", my_style_map);
		            createMarker(arrayMarker);		        }
            });
	    }
	    // Create Marker
	    function createMarker(arrayMarker){
			var bounds = new google.maps.LatLngBounds();
			var content = ""; 
			$('.iw-container').html("");
	      	for (var i = 0, feature; feature = arrayMarker[i]; i++) {
	      		var get_position = new google.maps.LatLng(feature.latitude, feature.longitude);
	            var marker = new google.maps.Marker({
	                position:  get_position,
	                icon: icons,
	                map: map,
	                title: feature.building_name,
	                class: "show_detail",
	                zIndex:1000

	          	});

	          	// InfoWindow content
  				content = '<div class="iw-container">' +feature.count_home+ '</div>';
	          	var infowindow = new google.maps.InfoWindow({
				    content: content
				});

				infowindow.open(map, marker);
	            markers.push(marker);
	            (function (marker, feature) {
	                google.maps.event.addListener(marker, "click", function (e) {
	                	setCustomMarker("icon",icons);
                		marker.set("icon", "/assets/img/icon/home_click_icon.png");
	                	get_class = this.class;
    					if(get_class == "show_all" || get_class == "show_detail"){
	                    	setCustomMarker("class", "show_all");
    						marker.set("class", "hide_detail");
    					}else{
	                		marker.set("class", "show_detail");
    					}
    					var get_count_home = parseInt(feature.count_home);
    					if(get_count_home > 1){
							get_same_val();
		          			$.ajax({
		          				method: "get",
		          				url: "/map/ajax_load_building",
		          				data: {
		          					building_name: feature.building_name,
									perfecture_id: get_perfecture_id,
								    city_id: get_city_id,
									house: get_house,
									apartment: get_apartment,
									buy: get_buy,
									rent: get_rent,
									short_stay: get_short,
									min_price: get_min_price,
									max_price: get_max_price,
								}
		          			}).done(function(msg){
		          				$('.detail_in_map').html(msg);
		          				$('.content_map_detail').css("display","inline-block");
		          				$('.content_map_detail').fadeIn(1);
		          			});
	          			}else{
	          				window.location.href = '/detail/'+feature.properties_id;
	          			}
	                });
	            })(marker, feature);
	            bounds.extend(get_position); 
	        }
	        if(arrayMarker.length > 0){ 
	        		map.fitBounds(bounds);
		            bounds.getCenter();
			}
			if (map.getZoom() > 17){
				map.setCenter(get_position);
				map.setZoom(17);
			}

	    }
	    // Add Custom Marker
		function setCustomMarker(key,data){
        	for (var i = 0; i<= markers.length; i++) {
	           	if(markers[i]!= undefined){
	            	markers[i].set(key, data);
	        	}
	    	}
		}
		// Clear All Marker
	    function clearMarker(){
	        for (var i = 0; i<= markers.length; i++) {
	           	if(markers[i]!= undefined){
	            	// markers[i].setMap(null);
	            	markers[i].setVisible(false);
	        	}
	    	}
	    }



		
