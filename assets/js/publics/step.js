
$(document).ready(function(){
	$('.tab-link').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.tab-link').removeClass('active');
		$('.tab-content').removeClass('current');

		$('.content_step_detail').addClass('tab-content');
		$(this).addClass('active');
		$("#"+tab_id).addClass('current');
	});
});