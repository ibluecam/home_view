$(document).ready(function(){
	var count =0;
  $("#selectall").click(function () {
      if ($("#selectall").is(':checked')) {
          $(".data_check").prop("checked", true);
          count= $(".data_check:checked").length;
          $(".count_chk").html(count);
      } else {
          $(".data_check").prop("checked", false);
        	count= $(".data_check:checked").length;
        	$(".count_chk").html(count);
      }
  });
   
  $(".data_check").click(function(){
   		var id =$(this).attr("attr_id");
		  if($(".check_one_"+id).is(':checked')) {
	        $(".count_chk").html(count+=1);
	    } else {
	      	$(".count_chk").html(count-=1);
	    }
  });

  $("#pop_delete").fancybox({
		closeBtn: false,
		'width':400,
    'height':40,
    'autoSize' : false
  });
  $(document).on('click','.popup_cancel',function(e){
        e.preventDefault();
        $.fancybox.close();
 });
  $(document).on('click','#btn_delete',function(e){
     if(count >0 || count!=""){
    	 $(".popup_header").html("Do you can to delete ?");
    	 $(".popup_submit").css('display','block');
 	}else{
 		$(".popup_submit").css('display','none');
 		$(".popup_header").html("Please check properties to delete!");
 	}
 });
  
  $(document).on('click','.popup_submit',function(e){
    $(".btnsubmit").trigger('click');
 });

});