
 function initialize() {
      var lat=$(".get_latlng").attr("lat");
      var lng=$(".get_latlng").attr("lng");
	     var set_style_map = [
	     {
	            "featureType": "water",
	            "elementType": "all",
	            "stylers": [
	                {
	                    "hue": "#a3ccff"
	                },
	                {
	                    "saturation": 100
	                },
	                {
	                    "lightness": -5
	                },
	                {
	                    "visibility": "on"
	                }
	            ]
	        }
	        ],
	         my_style_map = new google.maps.StyledMapType(set_style_map, {
	             name: "Styled Map"
	         });
	         var latlng = new google.maps.LatLng(lat, lng);
           var myOptions = {
                zoom: 17,
                center: latlng,
                mapTypeId: "styled_maps",
     			      zoomControl: true,
       		      mapTypeControl: false,
       	 	      streetViewControl: false,
                scrollwheel: false,
                navigationControl: false,
                scaleControl: false,
                draggable:false
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            map.mapTypes.set("styled_maps", my_style_map);
            var marker = new google.maps.Marker({
			        map: map,
			        position: latlng
		        });
            marker.setAnimation(google.maps.Animation.BOUNCE);
  			    var sunCircle = {
  			        strokeColor: "#000000",
  			        strokeOpacity: 0.8,
  			        strokeWeight: 2,
  			        fillColor: "#d8d8d8",
  			        fillOpacity: 0.35,
  			        map: map,
  			        center: latlng,
  			        radius: 100 // in meters
  			    };
  			    cityCircle = new google.maps.Circle(sunCircle)
  			    cityCircle.bindTo('center', marker, 'position');
            // google.maps.event.addDomListener(window, 'load', initialize);
	}

$(document).ready(function(){
	document.getElementById('links').onclick = function (event) {
	    event = event || window.event;
	    var target = event.target || event.srcElement,
	        link = target.src ? target.parentNode : target,
	        options = {index: link, event: event},
	        links = this.getElementsByTagName('a');
	    blueimp.Gallery(links, options);
	};

  $(".fancyboxLauncher").on("click", function () {
    $(".fancyboxDetail").eq(0).trigger("click");
    return false;
  });

  $("#show_description").click(function(){
    $(".text_description").toggle();
  });
  $(document).on('click','#back_link',function(){
    window.history.back();
  });

});
