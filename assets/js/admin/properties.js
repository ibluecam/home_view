
$(document).ready(function(){

   // CKEDITOR.replace( 'description_rent', {
   //      removePlugins: 'font',
   //      allowedContent: 'p h1 h2 strong em; a[!href]; img[!src,width,height];',
   //  });
   //  CKEDITOR.replace( 'transportation_rent', {
   //      removePlugins: 'font',
   //      allowedContent: 'p h1 h2 strong em; a[!href]; img[!src,width,height];',
   //  });
   //  CKEDITOR.replace( 'description_buy', {
   //      removePlugins: 'font',
   //      allowedContent: 'p h1 h2 strong em; a[!href]; img[!src,width,height];',
   //  });
   //  CKEDITOR.replace( 'transportation_buy', {
   //      removePlugins: 'font',
   //      allowedContent: 'p h1 h2 strong em; a[!href]; img[!src,width,height];',
   //  });
    // CKEDITOR.replace( 'Landmarks', {
    //     removePlugins: 'toolbar'
    // } );

    $("#upload_video_id_buy").on("click",function(e) {
        e.preventDefault();
        var video_id = $("#video_id_buy").val();
        if(video_id != ""){
            var url = "https://www.youtube.com/embed/"+video_id;
            $("#get_video_buy").show();
            $("#get_video_buy").attr("src", url);   
        }else{
            $("#get_video_buy").hide();
            $("#get_video_buy").attr("src", ""); 
        }
    });

    $("#video_id_buy").on("change", function(){
        var video_id = $(this).val();
        if(video_id != ""){
            var url = "https://www.youtube.com/embed/"+video_id;
            $("#get_video_buy").show();
            $("#get_video_buy").attr("src", url);   
        }else{
            $("#get_video_buy").hide();
            $("#get_video_buy").attr("src", ""); 
        }
    });

    $("#upload_video_id_rent").on("click",function(e) {
        e.preventDefault();
        var video_id = $("#video_id_rent").val();
        if(video_id != ""){
            var url = "https://www.youtube.com/embed/"+video_id;
            $("#get_video_rent").show();
            $("#get_video_rent").attr("src", url);   
        }else{
            $("#get_video_rent").hide();
            $("#get_video_rent").attr("src", ""); 
        }
    });

    $("#video_id_rent").on("change", function(){
        var video_id = $(this).val();
        if(video_id != ""){
            var url = "https://www.youtube.com/embed/"+video_id;
            $("#get_video_rent").show();
            $("#get_video_rent").attr("src", url);   
        }else{
            $("#get_video_rent").hide();
            $("#get_video_rent").attr("src", ""); 
        }
    });

    $("#upload_video_id_shortstay").on("click",function(e) {
        e.preventDefault();
        var video_id = $("#video_id_shortstay").val();
        if(video_id != ""){
            var url = "https://www.youtube.com/embed/"+video_id;
            $("#get_video_shortstay").show();
            $("#get_video_shortstay").attr("src", url);   
        }else{
            $("#get_video_shortstay").hide();
            $("#get_video_shortstay").attr("src", ""); 
        }
    });

    $("#video_id_shortstay").on("change", function(){
        var video_id = $(this).val();
        if(video_id != ""){
            var url = "https://www.youtube.com/embed/"+video_id;
            $("#get_video_shortstay").show();
            $("#get_video_shortstay").attr("src", url);   
        }else{
            $("#get_video_shortstay").hide();
            $("#get_video_shortstay").attr("src", ""); 
        }
    });

    $(document).on('click','#save',function(e) {
        var data = $("#form-search").serialize();
        $.ajax({
            data: data,
            type: "post",
            url: "insertmail.php",
            success: function(data){
                alert("Data Save: " + data);
            }
        });
    });

	$('#select-floor,#select-type,#select-Potential,#select-parking,#select-transaction_type,#select-gross_yield').selectize({
	    create: true,
	    sortField: {
	        field: 'text',
	        direction: 'asc'
	    },
	    dropdownParent: 'body'
	});

	var selector = '.list_properties_type_add li';

	$(selector).on('click', function(){
	    $(selector).removeClass('active');
	    $(this).addClass('active');

        var tab_id = $(this).attr('data-tab');
        $('.tab-content').removeClass('current');
        $('.tabs-content-all').addClass('tab-content');
        $("#"+tab_id).addClass('current');
	});

	$("#buy").on('click', function(){
	    $("#properties_type_radio_buy").prop("checked", true);
        // var newUrl = "?status=buy";
        // history.pushState({}, null, newUrl);
	});

	$("#rent").on('click', function(){
	    $("#properties_type_radio_rent").prop("checked", true);
        // var newUrl = "?status=rent";
        // history.pushState({}, null, newUrl);
	});

    $("#short_stay").on('click', function(){
        $("#properties_type_radio_short_stay").prop("checked", true)
    });

    $('.choose_img_floor').click(function(){
        $('.upload_floor_img').trigger('click');
    });

     $('.rent_choose_img_floor').click(function(){
        $('.rent_upload_floor_img').trigger('click');
    });

    $('.short_choose_img_floor').click(function(){
        $('.short_upload_floor_img').trigger('click');
    });




	$( "#next_update_schedule,#date_updated" ).datepicker({
		dateFormat:'yy-mm-dd',
      	changeMonth: true,
      	changeYear: true
    });
    $( "#rent_next_update_schedule,#rent_date_updated" ).datepicker({
        dateFormat:'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });

    $( "#shortstay_next_update_schedule,#shortstay_date_updated" ).datepicker({
        dateFormat:'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });

	$(".btn_hide").on("click", function(){
		$('.additional_details_tool').slideToggle("fast");
        $(this).find('img').toggle();
	});

	$(document).on("click",".choose_image",function(event){
         event.preventDefault();
		$("#tabs-buy .jFiler").find('.file-image:last').trigger('click');
	});

    $(document).on("click",".rent_choose_image",function(event){
         event.preventDefault();
        $("#tabs-rent .jFiler").find('.file-image:last').trigger('click');
    });

    $(document).on("click",".short_choose_image",function(event){
        event.preventDefault();
        $("#tabs-shortstay .jFiler").find('.file-image:last').trigger('click');
    });

    // $(document).on("click",".short_choose_image",function(event){
    //      event.preventDefault();
    //     $('#short_file_image1').trigger('click');
    // });

    $(document).on('change','input[name="check_all_image"]',function() {
        $('.check_img_upload').prop("checked" , this.checked);
    });

    var check_multi_image = new Array();
    $(document).on("click",".delete_image", function(event){
        event.preventDefault();
        var i=0;
        $('[name="check[]"]:checked').map(function(){
            var id = $(this).attr("id");
            //$(".custom-jFiler-item-"+id).remove();
            $(".filer-delete-item-"+id).trigger("click");
        }).get(); 

        

        // $.ajax({
        //       type: "POST",
        //       url: "/admin/properties/remove_multi_file_insert", // preview.php
        //       dataType: "json",
        //       data: 'check_multi_image='+check_multi_image, // all form fields
        //       success: function (data) {

        //       }
        // });
    });

    $(document).on("click","btn_trigger",function(evt){
        evt.preventDefault();
        $(".jFiler-items").find(".primary").first().trigger("click");
        $(".jFiler-items").find('.primary').first().prop('checked','checked').val(1);
    });


	// $('.file-image').change(function(event){
	// var count_images = $(this).get(0).files.length;
	// for(var i = 0; i < count_images; i++){
	// 	$("#sortable").append(
	// 		'<li class="list_image_upload list_image_num_'+i+'">'+
	// 			'<div class="img_list">'+
	// 				'<input type="checkbox" name="check" class="check_img_upload">'+
	// 				'<img src="'+URL.createObjectURL(event.target.files[i])+'">'+
	// 				'<input id="import_file_'+i+'" value="'+event.target.files[i].name+'">'+
	// 			'</div>'+
	// 			'<input type="radio" name="primary" value="primary" class="primary radio-field"/><label class="lbl_primary">Primary</label>'+
	// 			'<input type="button" id="'+i+'" name="delete" class="delete" value="Delete">'+
	// 			'<a class="MultiFile-remove" href="#file_image"> remove</a>'+
	// 		'</li>');
	// }

	// $("#sortable").find('.primary').first().prop('checked','checked');
	
	// });

	$( "#sortable" ).sortable({
	stop: function(){
    		$("#sortable").find('.primary').first().prop('checked','checked');
	    }
    });


	$("#location_city").chained("#location_perfecture");
    $("#rent_location_city").chained("#rent_location_perfecture");
    $("#short_location_city").chained("#short_location_perfecture");

		     //Example 1
     $("#file_image").filer({
        limit: null,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                	<input type="checkbox" name="check[]" class="check_img_upload" id="{{fi-id}}" value="{{fi-name}}">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <input type="radio" id="{{fi-id}}" name="primary" value="primary" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                        <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        <input type="hidden" name="file_images[]" value="{{fi-name}}" class="primary"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                    	<input type="checkbox" name="check" class="check_img_upload">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        // uploadFile: {
        //     //url: "/admin/properties/upload_file",
        //     data: null,
        //     type: 'POST',
        //     dataType: 'json',
        //     enctype: 'multipart/form-data',
        //     //beforeSend: function(){},
        //     success: function(data, el){
        //     	//console.log(data.img);
        //         var parent = el.find(".jFiler-jProgressBar").parent();
                
        //         // $(".jFiler-items").find(".primary").first().trigger("click");
        //         // $(".jFiler-items").find('.primary').first().prop('checked','checked').val(1);

        //         el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
        //             $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");    
        //         });
        //     },
        //     error: function(el){
        //         var parent = el.find(".jFiler-jProgressBar").parent();
        //         el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
        //             $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");    
        //         });
        //     },
        //     statusCode: null,
        //     onProgress: null,
        //     onComplete: null
        // },
        files: null,
        addMore: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
           // var file = file.name;
            //console.log(file);
            //$.post('/admin/properties/remove_file_insert', {file: file}).done(function() {
                        $("#primary_image").val("");
                        $(".jFiler-items").find('.primary').first().prop('checked', false).val("");
                    //});
        },
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

    $("#file_image1").filer({
        limit: null,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                	<input type="checkbox" name="check[]" id="{{fi-id}}" class="check_img_upload" value="{{fi-name}}">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <input type="radio" id="{{fi-id}}" name="primary" value="" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                        <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        <input type="hidden" name="file_images[]" value="{{fi-name}}" class="primary"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                    	<input type="checkbox" name="check[]" class="check_img_upload" id="{{fi-id}}" value="{{fi-name}}">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                           	<input type="radio" id="{{fi-id}}" name="primary" value="" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                        	<input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: img(),
        addMore: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            itemEl[0].remove();
        	var id = file.img_id;
            var file = file.name;
            $.post('/admin/properties/remove_file_update', {file: file,img_id: id},function() {
                        $("#primary_image").val("");
                        $(".btn_trigger").trigger("click");
                    });
        },
        onEmpty: null,
        options: true,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            //dddremoveConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });


     //rent image
     $("#rent_file_image").filer({
        limit: null,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <input type="checkbox" name="check[]" class="check_img_upload" id="{{fi-id}}" value="{{fi-name}}">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <input type="radio" id="{{fi-id}}" name="primary" value="primary" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                        <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        <input type="hidden" name="file_images[]" value="{{fi-name}}" class="primary"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <input type="checkbox" name="check" class="check_img_upload">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: null,
        addMore: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            $("#rent_primary_image").val("");
            $(".jFiler-items").find('.primary').first().prop('checked', false).val("");
        },
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

    $("#rent_file_image1").filer({
        limit: null,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <input type="checkbox" name="check[]" id="{{fi-id}}" class="check_img_upload" value="{{fi-name}}">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <input type="radio" id="{{fi-id}}" name="primary" value="" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                        <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        <input type="hidden" name="file_images[]" value="{{fi-name}}" class="primary"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <input type="checkbox" name="check[]" class="check_img_upload" id="{{fi-id}}" value="{{fi-name}}">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <input type="radio" id="{{fi-id}}" name="primary" value="" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                            <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: img(),
        addMore: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            itemEl[0].remove();
            var id = file.img_id;
            var file = file.name;
            
            $.post('/admin/properties/remove_file_update', {file: file,img_id: id},function() {
                        $("#rent_primary_image").val("");
                        $(".btn_trigger").trigger("click");
                    });

           
           

        },
        onEmpty: null,
        options: true,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            //dddremoveConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

    //short stay image
     $("#short_file_image").filer({
        limit: null,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <input type="checkbox" name="check[]" class="check_img_upload" id="{{fi-id}}" value="{{fi-name}}">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <input type="radio" id="{{fi-id}}" name="primary" value="primary" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                        <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        <input type="hidden" name="file_images[]" value="{{fi-name}}" class="primary"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <input type="checkbox" name="check" class="check_img_upload">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: null,
        addMore: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            $("#short_primary_image").val("");
            $(".jFiler-items").find('.primary').first().prop('checked', false).val("");
        },
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });
    
        //short stay image
    $("#short_file_image1").filer({
        limit: null,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <input type="checkbox" name="check[]" id="{{fi-id}}" class="check_img_upload" value="{{fi-name}}">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <input type="radio" id="{{fi-id}}" name="primary" value="" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                        <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        <input type="hidden" name="file_images[]" value="{{fi-name}}" class="primary"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item custom-jFiler-item-{{fi-id}}">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <input type="checkbox" name="check[]" class="check_img_upload" id="{{fi-id}}" value="{{fi-name}}">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <input type="radio" id="{{fi-id}}" name="primary" value="" class="primary radio-field"/><label class="lbl_primary">Primary</label>\
                                            <input type="hidden" name="file_images_primary[]" value="{{fi-name}}" class="set_image_primary_{{fi-id}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="jFiler-item-trash-action MultiFile-remove filer-delete-item-{{fi-id}}">Delete</a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: img(),
        addMore: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            console.log(itemEl);
            itemEl[0].remove();
            var id = file.img_id;
            var file = file.name;
            $.post('/admin/properties/remove_file_update', {file: file,img_id: id},function() {
                $("#short_primary_image").val("");
                $(".jFiler-items").find('.primary').first().prop('checked', false).val("");
            });
        },
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });




    $(".jFiler-items").find('.primary').first().prop('checked','checked').val(1);

    $(document).on("change",".primary",function() {   
    	$(this).attr('checked',true);
    	if($(this).is(":checked") == true) {   
    	 	var id = $(this).attr("id"); 
    	 	var set_image_primary = $(".set_image_primary_"+id).val();
    	 	$(".primary_image").val(set_image_primary);
		}else{
			$(".primary").val(0);
		}
		
	});



    
	$('#upload_properties_buy').validate({ 
       // ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
       ignore: ':hidden:not(#select-layout,#building_year)',
        rules: {
            // building_name: {
            //     required: true
            // },
            // unit_number: {
            //     required: true
            // },
            // size: {
            //     required: true
            // },
            location_perfecture: {
                required: true
            },
            location_city: {
                required: true
            },
            // land_right: {
            //     required: true
            // },
            // occupancy: {
            //     required: true
            // },
            // nearest_station: {
            //     required: true
            // },
            distance_to_station: {
                required: true,
                number: true
            },
            maked: {
                required: true,
            },
            // position: {
            //     required: true,
            // },
            price: {
                required: true,
                number: true
            },
            layout: {
                required: true
            },
            year_build: {
                required: true
            }
            // zoning: {
            //     required: true
            // },
            
            // structure: {
            //     required: true
            // },
            // maintainance_fee: {
            //     required: true
            // },
            // available_from: {
            //     required: true
            // },
            // unit_summary: {
            //     required: true
            // },
            // floor_area_ratio: {
            //     required: true
            // },
            // date_updated: {
            //     required: true
            // },
            // building_area_ratio: {
            //     required: true
            // },
            // next_update_schedule: {
            //     required: true
            // }
        },
        messages: {
           // building_name:"building name field is required.",
        //    unit_number:"unit number field is required.",
            //size:"size field is required.",
            location_perfecture: "location perfecture field is required.",
            location_city: "location city perfecture field is required.",
            //land_right:"land right field is required.",
            //occupancy:"occupancy field is required.",
            //nearest_station:"nearest station field is required.",
            maked: "maked field is required.",
          //  position: "position field is required.",
            layout: "layout field is required.",
            price:{
                required: "price field is required.",
                number: "price field is decimal numbers only"
            },
            distance_to_station:{
                required: "distance to station field is required.",
                number: "distance to station field is decimal numbers only"
            },
            year_build:"building year field is required.",
            // zoning:"floor field is required.",
            
            // structure:"floor field is required.",
            // maintainance_fee:"floor field is required.",
            // available_from:"floor field is required.",
            // unit_summary:"floor field is required.",
            // floor_area_ratio:"floor field is required.",
            // date_updated:"floor field is required.",
            // building_area_ratio:"floor field is required.",
            // next_update_schedule:"floor field is required."
        },
        errorPlacement: function(error, element) {

        	// if (element.attr("name") == "building_name" )
		       //  error.insertAfter("#building_name_error");
		    // else if  (element.attr("name") == "size" )
		    // 	error.insertAfter("#size_error");
		    // else if  (element.attr("name") == "unit_number" )
		    //     error.insertAfter("#unit_number_error");
		    // else if  (element.attr("name") == "land_right" )
		    //     error.insertAfter("#land_right_error");
		    // else if  (element.attr("name") == "occupancy" )
		    //     error.insertAfter("#occupancy_error");
		    // else if  (element.attr("name") == "nearest_station" )
		    //     error.insertAfter("#nearest_station_error");
             if  (element.attr("name") == "maked" )
                error.insertAfter("#maked_error");
            // else if  (element.attr("name") == "position" )
            //     error.insertAfter("#position_error");
            else if  (element.attr("name") == "distance_to_station" )
                error.insertAfter("#distance_to_station_error");
		    else if  (element.attr("name") == "price" )
		        error.insertAfter("#price_error");
             else if  (element.attr("name") == "location_perfecture" )
                error.insertAfter("#location_perfecture_error");
             else if  (element.attr("name") == "location_city" )
                error.insertAfter("#location_city_error");
		    else if  (element.attr("name") == "layout")
		        error.insertAfter("#layout_error");
            else if  (element.attr("name") == "year_build" )
                error.insertAfter("#year_build_error");
		    // else if  (element.attr("name") == "zoning" )
		    //     error.insertAfter("#zoning_error");
		    
		    // else if  (element.attr("name") == "structure" )
		    //     error.insertAfter("#structure_error");
		    // else if  (element.attr("name") == "maintainance_fee" )
		    //     error.insertAfter("#maintainance_fee_error");
		    // else if  (element.attr("name") == "available_from" )
		    //     error.insertAfter("#available_from_error");
		    // else if  (element.attr("name") == "unit_summary" )
		    //     error.insertAfter("#unit_summary_error");
		    // else if  (element.attr("name") == "floor_area_ratio" )
		    //     error.insertAfter("#floor_area_ratio_error");
		    // else if  (element.attr("name") == "date_updated" )
		    //     error.insertAfter("#date_updated_error");
		    // else if  (element.attr("name") == "building_area_ratio" )
		    //     error.insertAfter("#building_area_ratio_error");
		    // else if  (element.attr("name") == "next_update_schedule" )
		    //     error.insertAfter("#next_update_schedule_error");
		    // else
		    //     error.insertAfter(element);
	   },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });

    $('#upload_properties_rent').validate({ 
        // ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        ignore: ':hidden:not(#select-layout,#building_year)',
        rules: {
            // building_name: {
            //     required: true
            // },
            // unit_number: {
            //     required: true
            // },
            // size: {
            //     required: true
            // },
            location_perfecture: {
                required: true
            },
            location_city: {
                required: true
            },
            // land_right: {
            //     required: true
            // },
            // occupancy: {
            //     required: true
            // },
            // nearest_station: {
            //     required: true
            // },
            distance_to_station: {
                required: true,
                number: true
            },
            maked: {
                required: true,
            },
            // position: {
            //     required: true,
            // },
            price: {
                required: true,
                number: true
            },
            layout: {
                required: true,
            },
            year_build: {
                required: true
            }
            // layout: {
            //     required: true
            // },
            // zoning: {
            //     required: true
            // },
            
            // structure: {
            //     required: true
            // },
            // maintainance_fee: {
            //     required: true
            // },
            // available_from: {
            //     required: true
            // },
            // unit_summary: {
            //     required: true
            // },
            // floor_area_ratio: {
            //     required: true
            // },
            // date_updated: {
            //     required: true
            // },
            // building_area_ratio: {
            //     required: true
            // },
            // next_update_schedule: {
            //     required: true
            // }
        },
        messages: {
            // building_name:"building name field is required.",
            // unit_number:"unit number field is required.",
            //size:"size field is required.",
            location_perfecture: "location perfecture field is required.",
            location_city: "location city perfecture field is required.",
            //land_right:"land right field is required.",
            //occupancy:"occupancy field is required.",
            //nearest_station:"nearest station field is required.",
            maked: "maked field is required.",
           // position: "position field is required.",
            layout: "layout field is required.",
            price:{
                required: "price field is required.",
                number: "price field is decimal numbers only"
            },
            distance_to_station:{
                required: "distance to station field is required.",
                number: "distance to station field is decimal numbers only"
            },
            year_build:"building year field is required.",
            // layout:"floor field is required.",
            // zoning:"floor field is required.",
           
            // structure:"floor field is required.",
            // maintainance_fee:"floor field is required.",
            // available_from:"floor field is required.",
            // unit_summary:"floor field is required.",
            // floor_area_ratio:"floor field is required.",
            // date_updated:"floor field is required.",
            // building_area_ratio:"floor field is required.",
            // next_update_schedule:"floor field is required."
        },
        errorPlacement: function(error, element) {

          

            // if (element.attr("name") == "building_name" )
            //     error.insertAfter("#building_name_error_rent");
            // else if  (element.attr("name") == "size" )
            //  error.insertAfter("#size_error");
            // else if  (element.attr("name") == "unit_number" )
            //     error.insertAfter("#unit_number_error_rent");
            // else if  (element.attr("name") == "land_right" )
            //     error.insertAfter("#land_right_error");
            // else if  (element.attr("name") == "occupancy" )
            //     error.insertAfter("#occupancy_error");
            // else if  (element.attr("name") == "nearest_station" )
            //     error.insertAfter("#nearest_station_error");
             if  (element.attr("name") == "maked" )
                error.insertAfter("#maked_error_rent");
            // else if  (element.attr("name") == "position" )
            //     error.insertAfter("#position_error_rent");
            else if  (element.attr("name") == "distance_to_station" )
                error.insertAfter("#distance_to_station_error_rent");
            else if  (element.attr("name") == "price" )
                error.insertAfter("#price_error_rent");
             else if  (element.attr("name") == "location_perfecture" )
                error.insertAfter("#location_perfecture_error_rent");
             else if  (element.attr("name") == "location_city" )
                error.insertAfter("#location_city_error_rent");
            else if  (element.attr("name") == "layout" )
                error.insertAfter("#layout_error_rent");
            else if  (element.attr("name") == "year_build" )
                error.insertAfter("#year_build_error_rent");
            // else if  (element.attr("name") == "zoning" )
            //     error.insertAfter("#zoning_error");
            
            // else if  (element.attr("name") == "structure" )
            //     error.insertAfter("#structure_error");
            // else if  (element.attr("name") == "maintainance_fee" )
            //     error.insertAfter("#maintainance_fee_error");
            // else if  (element.attr("name") == "available_from" )
            //     error.insertAfter("#available_from_error");
            // else if  (element.attr("name") == "unit_summary" )
            //     error.insertAfter("#unit_summary_error");
            // else if  (element.attr("name") == "floor_area_ratio" )
            //     error.insertAfter("#floor_area_ratio_error");
            // else if  (element.attr("name") == "date_updated" )
            //     error.insertAfter("#date_updated_error");
            // else if  (element.attr("name") == "building_area_ratio" )
            //     error.insertAfter("#building_area_ratio_error");
            // else if  (element.attr("name") == "next_update_schedule" )
            //     error.insertAfter("#next_update_schedule_error");
            // else
            //     error.insertAfter(element);
       },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });

    $('#upload_properties_shortstay').validate({ 
       // ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        ignore: ':hidden:not(#select-layout,#building_year)',
        rules: {
            // building_name: {
            //     required: true
            // },
            // unit_number: {
            //     required: true
            // },
            // size: {
            //     required: true
            // },
            location_perfecture: {
                required: true
            },
            location_city: {
                required: true
            },
            // land_right: {
            //     required: true
            // },
            // occupancy: {
            //     required: true
            // },
            // nearest_station: {
            //     required: true
            // },
            distance_to_station: {
                required: true,
                number: true
            },
            maked: {
                required: true,
            },
            // position: {
            //     required: true,
            // },
            price: {
                required: true,
                number: true
            },
            layout: {
                required: true,
            },
            year_build: {
                required: true
            }

            // zoning: {
            //     required: true
            // },
           
            // structure: {
            //     required: true
            // },
            // maintainance_fee: {
            //     required: true
            // },
            // available_from: {
            //     required: true
            // },
            // unit_summary: {
            //     required: true
            // },
            // floor_area_ratio: {
            //     required: true
            // },
            // date_updated: {
            //     required: true
            // },
            // building_area_ratio: {
            //     required: true
            // },
            // next_update_schedule: {
            //     required: true
            // }
        },
        messages: {
            // building_name:"building name field is required.",
            // unit_number:"unit number field is required.",
            //size:"size field is required.",
            location_perfecture: "location perfecture field is required.",
            location_city: "location city perfecture field is required.",
            //land_right:"land right field is required.",
            //occupancy:"occupancy field is required.",
            //nearest_station:"nearest station field is required.",
            maked: "maked field is required.",
          //  position: "position field is required.",
            layout: "layout field is required.",
            price:{
                required: "price field is required.",
                number: "price field is decimal numbers only"
            },
            distance_to_station:{
                required: "distance to station field is required.",
                number: "distance to station field is decimal numbers only"
            },
            year_build:"building year field is required.",
            // layout:"floor field is required.",
            // zoning:"floor field is required.",
            // year_build:"floor field is required.",
            // structure:"floor field is required.",
            // maintainance_fee:"floor field is required.",
            // available_from:"floor field is required.",
            // unit_summary:"floor field is required.",
            // floor_area_ratio:"floor field is required.",
            // date_updated:"floor field is required.",
            // building_area_ratio:"floor field is required.",
            // next_update_schedule:"floor field is required."
        },
        errorPlacement: function(error, element) {

          
            // if (element.attr("name") == "building_name" )
            //     error.insertAfter("#building_name_error_short_stay");
            // else if  (element.attr("name") == "size" )
            //  error.insertAfter("#size_error");
            // else if  (element.attr("name") == "unit_number" )
            //     error.insertAfter("#unit_number_error_short_stay");
            // else if  (element.attr("name") == "land_right" )
            //     error.insertAfter("#land_right_error");
            // else if  (element.attr("name") == "occupancy" )
            //     error.insertAfter("#occupancy_error");
            // else if  (element.attr("name") == "nearest_station" )
            //     error.insertAfter("#nearest_station_error");
             if  (element.attr("name") == "maked" )
                error.insertAfter("#maked_error_short_stay");
            // else if  (element.attr("name") == "position" )
            //     error.insertAfter("#position_error_short_stay");
            else if  (element.attr("name") == "distance_to_station" )
                error.insertAfter("#distance_to_station_error_short_stay");
            else if  (element.attr("name") == "price" )
                error.insertAfter("#price_error_short_stay");
             else if  (element.attr("name") == "location_perfecture" )
                error.insertAfter("#location_perfecture_error_short_stay");
             else if  (element.attr("name") == "location_city" )
                error.insertAfter("#location_city_error_short_stay");
            else if  (element.attr("name") == "layout" )
                error.insertAfter("#layout_error_short_stay");
            else if  (element.attr("name") == "year_build" )
                error.insertAfter("#year_build_error_short_stay")
            // else if  (element.attr("name") == "zoning" )
            //     error.insertAfter("#zoning_error");
            // else if  (element.attr("name") == "structure" )
            //     error.insertAfter("#structure_error");
            // else if  (element.attr("name") == "maintainance_fee" )
            //     error.insertAfter("#maintainance_fee_error");
            // else if  (element.attr("name") == "available_from" )
            //     error.insertAfter("#available_from_error");
            // else if  (element.attr("name") == "unit_summary" )
            //     error.insertAfter("#unit_summary_error");
            // else if  (element.attr("name") == "floor_area_ratio" )
            //     error.insertAfter("#floor_area_ratio_error");
            // else if  (element.attr("name") == "date_updated" )
            //     error.insertAfter("#date_updated_error");
            // else if  (element.attr("name") == "building_area_ratio" )
            //     error.insertAfter("#building_area_ratio_error");
            // else if  (element.attr("name") == "next_update_schedule" )
            //     error.insertAfter("#next_update_schedule_error");
            // else
            //     error.insertAfter(element);
       },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });

    $("#upload_floor_img").filer({
        limit: 1,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        <input type="hidden" name="floor_plan_image" value="{{fi-name}}"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            <input type="hidden" name="floor_plan_image" value="{{fi-name}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                        <li></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: floorplan_img(),
        addMore: false,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded. Please delete images when upload.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

    //rent upload phane floor
    $("#rent_upload_floor_img").filer({
        limit: 1,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        <input type="hidden" name="floor_plan_image" value="{{fi-name}}"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            <input type="hidden" name="floor_plan_image" value="{{fi-name}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                        <li></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: floorplan_img(),
        addMore: false,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded. Please delete images when upload.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

    //rent upload phane floor
    $("#short_upload_floor_img").filer({
        limit: 1,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        <input type="hidden" name="floor_plan_image" value="{{fi-name}}"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            <input type="hidden" name="floor_plan_image" value="{{fi-name}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                        <li></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        files: floorplan_img(),
        addMore: false,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded. Please delete images when upload.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });



});

function img(){
	var images =[];
	var inp = $("#image_name_lists").val(); 

	if(inp != undefined){
        if(inp != ''){
            inp = inp.split(',');
            var url= '/assets/img/';
            $.each( inp, function( key, value ) {
                value = value.split("#_#");
                inp='{"img_id": "'+value[0]+'","name": "'+value[1]+'","size": 5453,"type": "image/jpg","file": "'+url+'uploads/properties/original/'+value[1]+'"}';
                imag = $.parseJSON(inp);
                images.push(imag);
            });
            console.log(images);
            console.log(images[0].img_id);    
        }
	}
	return images;
}


function floorplan_img(){
    var images = [];
    var inp = $("#planfloor_image").val();  
    if(inp != undefined){
        if(inp != ''){
            var url= '/assets/img/';
            inp='{"name": "'+inp+'","size": 5453,"type": "image/jpg","file": "'+url+'uploads/properties/original/'+inp+'"}';
            imag = $.parseJSON(inp);
            images.push(imag);
            console.log(images);
              
        }
    }
    return images;
}


