
$(document).ready(function(){
    $('.choose_image').click(function(){
        $('.upload_logo').trigger('click');
    });

    $(document).on("click","#change_password", function(){
        $.fancybox(
            $('#form_update_password').html(),
            {   
                closeBtn            : false,
                'width'             : 400,
                'height'            : 160,
                'autoSize'          : true,
                'autoScale'         : false,
                'transitionIn'      : 'none',
                'transitionOut'     : 'none',
                'hideOnContentClick': false
             }
        );
    });
    $(document).on("click","#btn_cancel", function(){
        $(".alert_message").html(""); 
        $.fancybox.close();
    });    

    $(document).on("submit","#form_change_password", function(e){
        e.preventDefault(); // avoids calling preview.php
            $.ajax({
                  type: "POST",
                  url: "/admin/agent/change_password", // preview.php
                  dataType: "json",
                  data: $(this).serializeArray(), // all form fields
                  success: function (data) {
                    $.fancybox.toggle();
                    var message = message_update_password(data);
                    $(".alert_message").html(message);          
                  }
            });
    });  


    

$("#upload_logo").filer({
        limit: 1,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        <input type="hidden" name="file_company_logo" value="{{fi-name}}"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            <input type="hidden" name="file_company_logo" value="{{fi-name}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                        <li></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        // uploadFile: {
        //     url: "/admin/agent/upload_logo",
        //     data: null,
        //     type: 'POST',
        //     enctype: 'multipart/form-data',
        //     beforeSend: function(){
        //     },
        //     success: function(data, el){
        //         console.log(data.length);
        //         var parent = el.find(".jFiler-jProgressBar").parent();

        //         el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
        //             $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");    
        //         });
        //     },
        //     error: function(el){
        //         var parent = el.find(".jFiler-jProgressBar").parent();
        //         el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
        //             $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");    
        //         });
        //     },
        //     statusCode: null,
        //     onProgress: null,
        //     onComplete: null
        // },
        files: img(),
        addMore: false,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            var file = file.name;
            $.post('/admin/agent/remove_file_insert', {file: file});
        },
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded. Please delete images when upload.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

    $('#form_update_agent').validate({ 
        rules: {
            company_name: {
                required: true
            },
            email: {
                required: true,
                minlength: 3,
                maxlength: 50,
                email: true,
                remote: {
                    url: "/admin/agent/validEmailEdit",
                    type: "post",
                    data: {
                      email: function() {
                        return $("#email").val();
                      },
                      userId:function(){
                        return $("#user_id").val();
                      }
                    }
                }
            }
        },
        messages: {
            company_name:"company name field is required.",
            email: {
                required: "The Email field is required.",
                email: "The Email field must contain a valid email address.",
                remote: jQuery.format("Email already exists.") 
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "company_name" )
                error.insertAfter("#company_name_error");
            else if  (element.attr("name") == "email" )
                error.insertAfter("#email_error");
       },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });


});

function img(){
    var images =[];
    var inp = $("#get_image_edit").val();  
    if(inp != undefined){
        inp = inp.split(',');
        var url= '/assets/img/';
        $.each( inp, function( key, value ) {
            value = value.split("#_#");
            inp='{"img_id": "'+value[0]+'","name": "'+value[1]+'","size": 5453,"type": "image/jpg","file": "'+url+'uploads/company/resize/'+value[1]+'"}';
            imag = $.parseJSON(inp);
            images.push(imag);
        });
        console.log(images);
        console.log(images[0].img_id);
    }
    return images;
}

function message_update_password(message){
    if(message.msg){
        return '<div class="alert alert-success-update-password alert-success"><strong>Success!</strong> '+message.msg+'</div>';                
    }else{
        return '<div class="alert alert-error-update-password alert-error"><strong>Warning!</strong> '+message.msg_error+'</div>';
    } 
}