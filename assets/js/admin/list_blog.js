$(document).ready(function(){
	var id;
	$(document).on("click",".btn_action",function(){
		  id=$(this).attr("id");
		  if($('.ul_data').hasClass('active')){
		  	$('.ul_data').hide();
		  	$('.ul_data').removeClass('active');
		  }else{
			$('.show_'+id).toggleClass('active');
			$('.show_'+id).show();
			return false;
		  }
	});
	function messagebox(id,img_id) {
    	$('#popup_delete').html('<div class="popup_header">Do you want delete this article?</div>'+
	   		'<div class="popup_footer">'+
	   			'<form method="post">'+
	   				'<input type="hidden" value="'+id+'" name="article_id" class="post_user_id"/>'+
	   				'<input type="hidden" value="'+img_id+'" name="image_name"/>'+
		   			'<input class="popup_btn popup_submit" type="submit" value="DELETE"  name ="btndelete"/>'+
		   			'<input class="popup_btn popup_cancel" type="submit" value="CANCEL"  name ="btncancel"/>'+
	   			'</form>'+
	   	'</div>');
	}


   $(".delete_id").each(function (i) {
        $(this).on("click", function () {
        	var article_id=$(this).attr("article_id");
        	var img_id=$(this).attr("img_id");
        	messagebox(article_id,img_id);
            $.fancybox(
	            $('#popup_delete').html(),{   
	                closeBtn: false,
			        'width':400,
			        'height':40,
			        'autoSize' : false
	             }
             );
        }); // on click
    }); // 
    
    $(document).on('click','.popup_cancel',function(e){
        e.preventDefault();
        $.fancybox.close();
        $('.ul_data').hide();
		$('.ul_data').removeClass('active');
    });
});
