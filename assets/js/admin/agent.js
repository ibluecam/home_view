$(document).ready(function(){
	var id;
	$(document).on("click",".btn_action",function(){
		  id=$(this).attr("id");
		  if($('.ul_data').hasClass('active')){
		  	$('.ul_data').hide();
		  	$('.ul_data').removeClass('active');
		  }else{
			$('.show_'+id).toggleClass('active');
			$('.show_'+id).show();
			return false;
		  }
	});

	function messagebox(id,logo) {
    $('#popup_delete').html('<div class="popup_header">Do you want delete this agent?</div>'+
	   		'<div class="popup_footer">'+
	   			'<form method="post">'+
	   				'<input type="hidden" value="'+id+'" name="user_id"/>'+
	   				'<input type="hidden" value="'+logo+'" name="logo"/>'+
		   			'<input class="popup_btn popup_submit" type="submit" value="DELETE"  name ="btndelete"/>'+
		   			'<input class="popup_btn popup_cancel" type="submit" value="CANCEL"  name ="btncancel"/>'+
	   			'</form>'+
	   	'</div>');
	}


   $(".delete_id").each(function (i) {
        $(this).on("click", function () {
        	var user_id=$(this).attr("user_id");
        	var logo=$(this).attr("logo");
        	messagebox(user_id,logo);
            $.fancybox(
	            $('#popup_delete').html(),{   
	                closeBtn: false,
			        'width':400,
			        'height':40,
			        'autoSize' : false
	             }
             );
        }); // on click
    }); // 
    
    $(document).on('click','.popup_cancel',function(e){
        e.preventDefault();
        $.fancybox.close();
        $('.ul_data').hide();
		$('.ul_data').removeClass('active');
    });


 });