$(document).ready(function(){
    var count =0;
    $("#selectall").on("click", function () {
        if ($("#selectall").is(':checked')) {
            $(".select_items").prop("checked", true);
            count= $(".select_items:checked").length;
            $(".count_chk").html(count);
        } else {
            $(".select_items").prop("checked", false);
                count= $(".select_items:checked").length;
            $(".select_items").html(count);
            $(".count_chk").html(0);
        }
    });
   
    $(".select_items").on("click",function(){
        var id =$(this).attr("attr_id");
          if($(".check_one_"+id).is(':checked')) {
            $(".count_chk").html(count+=1);
        } else {
            $(".count_chk").html(count-=1);
        }
    });

    $("#pop_delete").fancybox({
        closeBtn: false,
        'width':400,
        'height':40,
        'autoSize' : false
    });
    $(document).on('click','.popup_cancel',function(e){
        e.preventDefault();
        $.fancybox.close();
        $(".select_items").prop("checked", false);
        count=0;
        $(".count_chk").html(count);
    });
    $("#btn_delete").on('click',function(e){
        if(count >0 || count!=""){
             $(".popup_header").html("Do you can to delete ?");
             $(".popup_submit").css('display','block');
        }else{
            $(".popup_submit").css('display','none');
            $(".popup_header").html("Please check your properties for delete!");
        }
    });
    
    $(".tools_delete").on("click",function(){
        var id =$(this).attr("attrid");
        $(".check_one_"+id).prop("checked", true);
        count=1;
        $(".count_chk").html(count);
        $("#btn_delete").trigger('click');
    });

    $(document).on('click','.popup_submit',function(e){
        $(".btnsubmit").trigger('click');
    });

});