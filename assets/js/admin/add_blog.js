$(document).ready(function(){

    $('.choose_image').click(function(){
        $('.upload_logo').trigger('click');
    });


	$("#upload_logo").filer({
        limit: 1,
        maxSize: null,
        extensions: ['jpg', 'jpeg', 'png', 'gif'],
        changeInput: '',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <input type="hidden" name="file_company_logo" value="{{fi-name}}"/>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <input type="hidden" name="file_company_logo" value="{{fi-name}}"/>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                        <li></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        // uploadFile: {
        //     url: "/admin/agent/upload_logo",
        //     data: null,
        //     type: 'POST',
        //     enctype: 'multipart/form-data',
        //     beforeSend: function(){
        //     },
        //     success: function(data, el){
        //         console.log(data.length);
        //         var parent = el.find(".jFiler-jProgressBar").parent();

        //         el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
        //             $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");    
        //         });
        //     },
        //     error: function(el){
        //         var parent = el.find(".jFiler-jProgressBar").parent();
        //         el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
        //             $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");    
        //         });
        //     },
        //     statusCode: null,
        //     onProgress: null,
        //     onComplete: null
        // },
        files: [
                {
                name: "default_feature_image.jpg",
                size: 5453,
                type: "image/jpg",
                file: "/assets/img/uploads/blog/resize/default_feature_image.jpg"
            }],
        addMore: false,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            var file = file.name;
            $.post('/admin/agent/remove_file_insert', {file: file});
        },
        onEmpty: null,
        options: null,
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded. Please delete images when upload.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

    $('#add_blog').validate({ 
        rules: {
            title: {
                required: true
            },
            source: {
                required: true
            }
        },
        messages: {
            title:"title field is required.",
            source:"source field is required.",
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "title" )
                error.insertAfter("#error_title");
            else if  (element.attr("name") == "source" )
                error.insertAfter("#error_source");
        },
        submitHandler: function (form) { 
          if ($(form).valid()) 
                form.submit(); 
            return false; 
        }
    });


    


});