$(document).ready(function(){
    $('#location_city,#location_perfecture,#rent_location_city,#rent_location_perfecture,#short_location_city,#short_location_perfecture').on('change', function() {  
    $('#edit_id').val(""); 
      initialize();
    });

    $('.list_properties_type li').on('click', function(){
       var get_hide_status = $(this).attr('id');
       $('.hide_status').val(get_hide_status); 
       initialize();
    }); 
  
});
// Initialize Map on load
      var myLatlng="";
      var marker=false;
      var getlat,getlang=null;
  function initialize() {

        var get_hide_status = $('.hide_status').val();
        if(get_hide_status == 'buy'){
          country_perf = $('#location_perfecture option:selected').text();
          country_city = $("#location_city option:selected").text();
        }else if(get_hide_status == 'rent'){
          country_perf = $('#rent_location_perfecture option:selected').text();
          country_city = $("#rent_location_city option:selected").text();
        }else{
          country_perf = $('#short_location_perfecture option:selected').text();
          country_city = $("#short_location_city option:selected").text();
        }
        if(country_perf == "Perfecture"){
            country = "TOKYO";
        }else{
            country = country_perf;
        }
        if(country_city == "City"){
            if(country_perf == "Perfecture"){
              country = "TOKYO";
            }else{
              country = country_perf;
            }
        }else{
          country = country_city;
        }

     var geocoder = new google.maps.Geocoder();  
      geocoder.geocode({
          'address': country
      }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
             var set_style_map = [
             {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#a3ccff"
                        },
                        {
                            "saturation": 100
                        },
                        {
                            "lightness": -5
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
                ],
                my_style_map = new google.maps.StyledMapType(set_style_map, {
                       name: "Styled Map"
                });

                var edit_id = $('#edit_id').val();
                var edit_lat = $('#edit_lat').val();
                var edit_lng = $('#edit_lng').val();
                if(edit_id != undefined && edit_id != ""){
                  getlat=parseFloat(edit_lat);
                  getlng=parseFloat(edit_lng);
                }else{
                  getlat=parseFloat(results[0].geometry.location.lat());
                  getlng=parseFloat(results[0].geometry.location.lng());
                }

                if(get_hide_status == 'buy'){
                  $("#getlat").val(getlat);
                  $("#getlng").val(getlng);
                  $("#label_lat").html(getlat);
                  $("#label_lng").html(getlng);
                }else if(get_hide_status == 'rent'){
                  $("#rent_getlat").val(getlat);
                  $("#rent_getlng").val(getlng);
                  $("#rent_label_lat").html(getlat);
                  $("#rent_label_lng").html(getlng);
                }else{
                  $("#short_getlat").val(getlat);
                  $("#short_getlng").val(getlng);
                  $("#short_label_lat").html(getlat);
                  $("#short_label_lng").html(getlng);
                }

                myLatlng = new google.maps.LatLng(getlat,getlng);
                var myOptions = {
                    zoom: 17,
                    center:myLatlng ,
                    mapTypeId: "styled_maps",
                    zoomControl: true,
                    mapTypeControl: true,
                    streetViewControl: false
                }
                if(get_hide_status == 'buy'){
                  map = new google.maps.Map(document.getElementById("map"), myOptions);
                }else if(get_hide_status == 'rent'){
                  map = new google.maps.Map(document.getElementById("rent_map"), myOptions);
                }else{
                  map = new google.maps.Map(document.getElementById("short_map"), myOptions);
                }
                map.mapTypes.set("styled_maps", my_style_map);
          }

          marker = new google.maps.Marker({
             position: myLatlng, 
             map: map
          });
          marker.setAnimation(google.maps.Animation.BOUNCE);
          google.maps.event.addListener(map, 'center_changed', function() {
            var location = map.getCenter();
            if(get_hide_status == 'buy'){
              document.getElementById("getlat").value = location.lat();
              document.getElementById("getlng").value = location.lng();
              document.getElementById("label_lat").innerHTML = location.lat();
              document.getElementById("label_lng").innerHTML = location.lng();
            }else if(get_hide_status == 'rent'){
              document.getElementById("rent_getlat").value = location.lat();
              document.getElementById("rent_getlng").value = location.lng();
              document.getElementById("rent_label_lat").innerHTML = location.lat();
              document.getElementById("rent_label_lng").innerHTML = location.lng();
            }else{
              document.getElementById("short_getlat").value = location.lat();
              document.getElementById("short_getlng").value = location.lng();
              document.getElementById("short_label_lat").innerHTML = location.lat();
              document.getElementById("short_label_lng").innerHTML = location.lng();
            }
             placeMarker(location.lat(),location.lng());
          });
          google.maps.event.addListener(map, 'zoom_changed', function() {
             zoomLevel = map.getZoom();
          });
          google.maps.event.addListener(marker, 'dblclick', function() {
             zoomLevel = map.getZoom()+1;
             if (zoomLevel == 20) {
              zoomLevel = 10;
             }    
             map.setZoom(zoomLevel); 
          });

        });
    }

     function placeMarker(lat,lng) {
        var clickedLocation = new google.maps.LatLng(lat,lng);
        marker.setPosition(clickedLocation);
     }


