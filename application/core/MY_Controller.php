<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	// declare variable	
	public $data = array();
	var $array_css = array();
	var $array_js = array();

	var $me = array();
	var $is_login=array();
	var $is_logged_in = false;
	var $map_js=false;
	var $is_mobile = false;

	var $page_title = "Home View";
	var $header_page_title = "";
	var $header_icon = "";
	var $chk_group_user = "";
	var $chk_user_group_name = "";
	var $get_user_group_name = "";
	var $load_view = "";
	var $count_agent = 0;
	var $count_properties = 0;
	var $user_id = "";

	public function __construct()
	{
		parent::__construct();	
		date_default_timezone_set('Asia/Jakarta');
		$this->load->helper('url');
		$this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('cookie');	
        $this->load->model('User','mo_user');
        $this->load->model('Mo_users_groups','mo_users_groups');
        $this->load->model('Mo_properties','mo_properties');
        $this->load->model('Mo_favorite','mo_favorite');
        $this->init_variable();
        $this->load->library('Mobile_Detect');
        $this->detect_mobile();
        //$this->check_user_group();
	}


    function detect_mobile(){
    	$detect = new Mobile_Detect();
        if ($detect->isMobile() && !$detect->isTablet()){
           $this->is_mobile = true;
        }
    }
	function init_variable(){
		$user_session = $this->session->all_userdata();

		$this->is_login = $user_session;
		$this->me = $this->mo_user->getMyDetail();
		//var_dump($this->me);
		if($this->me){
			$this->is_logged_in = true;
			$this->user_id = $this->me->user_id;
		}
		$this->data['me'] = $this->me; 
		$this->data['is_logged_in'] = $this->is_logged_in;
		$this->chk_group_user = $this->check_user_group();
		$this->data['chk_group_user'] = $this->chk_group_user;
		$this->chk_user_group_name = $this->check_user_group_name();
		$this->data['chk_user_group_name'] = $this->chk_user_group_name;
		$this->get_user_group_name = $this->get_user_group_name();
		$this->data['get_user_group_name'] = $this->get_user_group_name;
		$this->count_agent = $this->mo_user->select_all_agent();
		$this->data['count_agent'] = $this->count_agent->count_agent;
		$this->count_properties = $this->mo_properties->admin_count_properties_lists(array('user_id_fk' => $this->user_id),"");
		$this->data['count_properties'] = $this->count_properties->count_properties;
		$this->count_favorite = $this->mo_favorite->select_count_favorite($this->user_id,"");
		$this->data['count_favorite'] = count($this->count_favorite);
	}

	public function get_session_user_id(){
		return $this->me->user_id;
	}
	
	public function load_view($view_file){
		$this->data['array_css'] = $this->array_css;
		$this->data['array_js'] = $this->array_js;
		$this->data['page_title'] = $this->page_title;
		$this->data['header_page_title'] = $this->header_page_title;
		$this->data['header_icon'] = $this->header_icon;
		$this->load->view('/templates/header', $this->data);
		$this->load->view($view_file, $this->data);
		$this->load->view('/templates/footer', $this->data);
	}

	public function load_view_mobile($view_file){
		$this->data['array_css'] = $this->array_css;
		$this->data['array_js'] = $this->array_js;
		$this->data['page_title'] = $this->page_title;
		$this->data['header_page_title'] = $this->header_page_title;
		$this->data['header_icon'] = $this->header_icon;
		if($this->is_logged_in==true){
			$this->data['chk_group_user'] = $this->chk_group_user;
			$this->data['user_id'] = $this->get_session_user_id();
		}
		$this->load->view('/template_mobile/header', $this->data);
		$this->load->view($view_file, $this->data);
		$this->load->view('/template_mobile/footer', $this->data);
	}

	public function load_auth_view($view_file){
		$this->data['array_css'] = $this->array_css;
		$this->data['array_js'] = $this->array_js;
		$this->data['page_title'] = $this->page_title;
		$this->data['header_page_title'] = $this->header_page_title;
		$this->data['header_icon'] = $this->header_icon;
		$this->data['chk_group_user'] = $this->chk_group_user;
		$this->data['user_id'] = $this->get_session_user_id();
		$this->load->view('/templates/header', $this->data);
		$this->load->view($view_file, $this->data);
		$this->load->view('/templates/footer_admin', $this->data);
	}

	public function canDo($key){ 
		$user_id = $this->get_session_user_id();
		$user_group_id = $this->mo_user->group_by_user_id($user_id);
		$group_data=$this->mo_users_groups->groupdata_by_id($user_group_id);
		
		if(!empty($group_data)) { /* if group exists */
			if($group_data->status==1) { /* if group is active */
				$g_permissions=json_decode($group_data->permission);
				if(!empty($g_permissions)) { /* if group has permissions */ 
					if(isset($g_permissions->$key) && $g_permissions->$key==1) { /* check permission with key  */ 
						return TRUE;
					} else {
						return FALSE;
					}
				} else {  /* if group permission is empty  */ 
					return FALSE;
				}
			} else { /* if group is inactive */ 
				return FALSE;
			}	
		} else { /* if group not exists */ return FALSE; }
	}

	public function superadmin_permission(){
		if($this->is_logged_in){
			if($this->canDo("access_backend") && $this->canDo("view_agent") && $this->canDo("add_agent") && $this->canDo("delete_agent") && $this->canDo("login_to_frontend") && $this->canDo("view_info")){
				return true;
			}else{
				return false;
			}	
		}
	}

	public function agent_permission(){
		if($this->is_logged_in){
			if($this->canDo("access_backend") && $this->canDo("view_properties") && $this->canDo("add_properties") && $this->canDo("delete_properties") && $this->canDo("login_to_frontend") && $this->canDo("view_info")){
				return true;
			}else{
				return false;
			}
		}
	}

	public function user_permission(){
		if($this->is_logged_in){
			if($this->canDo("access_backend") && $this->canDo("view_favorite") && $this->canDo("add_favorite") && $this->canDo("delete_favorite") && $this->canDo("login_to_frontend") && $this->canDo("view_info")){
				return true;
			}else{
				return false;
			}
		}
	}

	public function check_user_group(){
		if($this->is_logged_in){
			if($this->me->user_group == 1){
				return "admin/admin_info/info/";
			}else if($this->me->user_group == 2){
				return "admin/info/";
			}else{
				return "favorite/info/";
			}
		}
	}

	public function get_user_group_name(){
		if($this->is_logged_in){
			if($this->me->user_group == 1){
				return "admin";
			}else if($this->me->user_group == 2){
				return "agent";
			}else{
				return "favorite";
			}
		}
	}

	public function check_user_group_name(){
		if($this->is_logged_in){
			if($this->me->user_group == 1){
				return "Admin";
			}else if($this->me->user_group == 2){
				if(!empty($this->me->company_name)){
					return $this->me->company_name;
				}else{
					return $this->me->last_name." ".$this->me->first_name;
				}
			}else{
				return $this->me->last_name." ".$this->me->first_name;
			}
		}
	}
}

?>