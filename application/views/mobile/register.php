<div class="content_login">
	<div class="block_button">
		<span class="text_login"><a href="/login">LOGIN</a></span>
		<span class="text_register">REGISTER</span>
	</div>
	<div class="clear"></div>
	<div class="content_frm_register">
		<div class="frm_content">
			<?php echo form_open('register',array('id' => 'frm_register')); ?>
			<p class="text_create">CREATE YOUR ACCOUNT</p>
				<div class="content_form">
					<div class="first_name form_controll">
						<input type="text" name="firstname" class="text_field" value="<?php echo set_value('firstname') ?>" placeholder="First Name" required/>
					</div>
					<div class="error"><?php echo form_error('firstname'); ?></div>
					<div class="last_name form_controll">
						<input type="text" name="lastname" class="text_field" value="<?php echo set_value('lastname') ?>" placeholder="Last Name" required/>
					</div>
					<div class="error"><?php echo form_error('lastname'); ?></div>
					<div class="clear"></div>
				</div>
				<div class="content_form form_controll">
					<input type="text" name="email" id="email" class="text_field" value="<?php echo set_value('email') ?>" placeholder="Email Address" required/>
					<div class="error">
						<?php echo form_error('email'); ?>
					</div>
				</div>
				<div class="content_form form_controll">
					<input type="password" name="password" class="text_field" value="<?php echo set_value('password') ?>" placeholder="Password" required/>
					<div class="error">
						<?php echo form_error('password'); ?>
					</div>
				</div>
				<div class="content_btn_reg">
					<input type="submit" name="btn_register" class="btn_register btn_go" value="Register" />
				</div>
				<div class="agree_privacy">
					<input type="checkbox" name="agree" id="ch_agree" required/>&nbsp;
					<label for="ch_agree" class="text_agree">I Agree to the Terms of Use and Privacy</label>
					<div class="error_prefix_check" id="error_check">
						<?php echo form_error('agree'); ?>
					</div>
				</div>
				<div class="already_acc">
					<span class="text_agree">Already Have An Account?</span>
					<a href="/login" class="go_login">LOGIN</a>
				</div>
			</form>
		</div>
	</div>
</div>