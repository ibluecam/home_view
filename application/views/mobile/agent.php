	
	<div class="content_agent">
		<div class="info_agent">
			<div class="info_detail">
				<?php if(count($agent_info) > 0){ ?>
				<div class="logo_agent">
					<?php if($agent_info->logo != ""){ ?>
						<img src="/assets/img/uploads/company/resize/<?php echo $agent_info->logo; ?>" alt="Logo Agent">
					<?php }else{ ?>
						<img src="/assets/img/uploads/company/resize/company_logo_default.jpg" alt="Logo Agent">
					<?php } ?>
				</div>
				<div class="agent_name">
					<?php if(!empty($agent_info->company_name)){
							echo $agent_info->company_name;
						}else{
							echo $agent_info->last_name." ".$agent_info->first_name;
						}
					?>
				</div>
				<div class="agent_social_media">
					<?php if($agent_info->skype_id != ""){ ?>
						<span class="skype">
							<img src="/assets/img/icon/skype.png" alt="Skype">
							<span><a><?php echo $agent_info->skype_id; ?></a></span>
						</span>
					<?php } ?>
					<?php if($agent_info->viber_id != ""){ ?>
					<span class="viber">
						<img src="/assets/img/icon/viber.png" alt="Viber">
						<span><a><?php echo $agent_info->viber_id; ?></a></span>
					</span>
					<?php } ?>
					<?php if($agent_info->line_id != ""){ ?>
					<span class="line">
						<img src="/assets/img/icon/line.png" alt="Line">
						<span><a><?php echo $agent_info->line_id; ?></a></span>
					</span>
					<?php } ?>
				</div>
				<?php if($agent_info->email != ""){ ?>
					<div class="agent_email">
						<img class="arrow_img" src="/assets/img/icon/message.png"/>
						<span class="contact_text"><?php echo $agent_info->email; ?></span>
					</div>
				<?php } ?>
				<div class="agent_phone">
					<?php if($agent_info->phone1 != ""){ ?>
						<span class="phone1">
							<img class="arrow_img" src="/assets/img/icon/phone.png"/>
							<span class="contact_text"><?php echo $agent_info->phone1; ?></span>
						</span>
					<?php } ?>
					<?php if($agent_info->phone2 != ""){ ?>	
						<span class="phone2">
							<img class="arrow_img" src="/assets/img/icon/tel.png"/>
							<span class="contact_text"><?php echo $agent_info->phone2; ?></span>
						</span>
					<?php } ?>
				</div>
				<?php if($agent_info->office_location != ""){ ?>
					<div class="agent_office_location">
						<p class="title_red">Office Location</p>
						<p class="office_name"><?php echo $agent_info->office_location; ?></p>
					</div>
				<?php } ?>
				<?php if($agent_info->description != ""){ ?>
					<div class="agent_description">
						<p class="title_red">Description</p>
						<p class="description"><?php echo $agent_info->description; ?></p>
					</div>
				<?php } ?>
				<?php }else{ ?>
					<div class="not_found">
						No agent were found!
					</div>
				<?php } ?>
			</div>
			<div class="red_ruler"></div>
		</div>
		<div class="properties_agent">
			<div class="text_count">
				Product List (<span><?php echo $count_agent_properties; ?></span>)
			</div>

			<div class="content_data_properties">
			<?php if($agent_properties && count($agent_properties) >0 ){
				foreach ($agent_properties as $row) {
			?>
			<a href="/detail/<?php echo $row->id; ?>">
				<div class="data_properties">
					<div class="properties_image">
						<?php if($row->images_path != ""){ ?>
							<img src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>" alt="Image of <?php echo $row->building_name; ?>"/>
						<?php }else{ ?>
							<img src="/assets/img/uploads/default_image_properties.jpg" alt="No Image"/>
						<?php } ?>
					</div>
					<div class="properties_img_status">
						<img src="/assets/img/mobile/icon/status_buy.png" alt="Status" />
					</div>
					<div class="properties_description">
						<div class="properties_name properties_font">
							<?php echo $row->building_name; ?>
						</div>
						<div class="properties_location properties_font">
							<td><?php echo $row->perfecture_name." ".$row->city_name; ?></td>
						</div>
						<div class="properties_price properties_font">
							<?php echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price); ?>
						</div>
						<div class="properties_view properties_font">
							<span><?php echo number_format($row->views); ?></span> Views
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</a>
			<?php }}else{ ?>
				<div class="not_found">
					<h2 style="margin-bottom: 10px;">Page Not Found</h2>
					<p>The page you're looking for cannot be found.</p>
				</div>
			<?php } ?>

			<div class="pagination">
				<?php echo $pagination; ?>
			</div>

		</div>

	</div>