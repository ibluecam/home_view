<div class="container_market_price">
	<div class="market_search">
		<p class="normal_p">TYPE</p>
		<div class="data_radio">
			<input type="radio" class="rdo_search" id="Apartment" name="type" />
			<label for="Apartment" class="text_radio">Apartment</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="House" name="type" />
			<label for="House" class="text_radio">House</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="Short" name="type" />
			<label for="Short" class="text_radio">Short term</label>
		</div>
		<p class="p_top">FLOOR PLAN</p>
		<div class="data_radio">
			<input type="radio" class="rdo_search" id="flat" name="floor" />
			<label for="flat" class="text_radio">Flat</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="1K" name="floor" />
			<label for="1K" class="text_radio">1K/1DK</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="2K" name="floor" />
			<label for="2K" class="text_radio">1LDK/2K/2DK</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="3K" name="floor" />
			<label for="3K" class="text_radio">1LDK/3K/3DK</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="4K" name="floor" />
			<label for="4K" class="text_radio">3LDK/4K ~</label>
		</div>
	</div>
	
	<div class="table_market_price">
		<table class="table_market">
			<tr>
				<th class="market_title">CITY</th>
				<th class="market_title" colspan="3">MARKET PRICE</th>
			</tr>
			<tr>
				<td class="name">Yokohama</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Kawasaki</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 60px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Yokosuka</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Odawara</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 70px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Atsugi</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 55px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Ayase</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 40px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Chigasaki</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 35px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Ebina</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 77px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Fujisawa</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Hadano</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Hiratsuka</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Isehara</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Kamakura</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Kawasaki</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Minamiashigara</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Miura</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Odawara</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Sagamihara</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Yamato</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Yokohama</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Yokosuka</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Zama</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
			<tr>
				<td class="name">Zushi</td>
				<td class="price">7.9 万円</td>
				<td class="progress">
					<div style=" width: 50px;" class="market_progress"></div>
				</td>
				<td class="search">
					<input type="submit" name="search" value="SEARCH" class="btnsearch" id="btnsearch">
				</td>
			</tr>
		</table>
	</div>
</div>