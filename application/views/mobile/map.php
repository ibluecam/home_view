
	<div class="content_map">
		<input type="hidden" class="hide_val_ajax" value="<?php if(isset($link_lists_ajax)) echo $link_lists_ajax; ?>">
		<input type="hidden" class="hide_link_val" value="<?php if(isset($link_status)) echo $link_status; ?>">
		<div class="content_menu_map">
			<div class="top_link_map"><a href="<?php if(isset($link_to_lists)) echo $link_to_lists; ?>" class="link_map">LIST SEARCH</a></div>
			<div class="map_active">SEARCH BY MAP</div>
		</div>
		<div class="wrap_map">
			<div class="content_map_right">
				<div class="content_map_detail">
					<div class="in_wrap_detail">
						<div class="detail_map">
							<div class="detail_in_map"></div>
							<div class="close_detail_map">
								<img src="/assets/img/icon/close_map.png" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="map"></div>
		</div>
		<div class="content_map_search">
			<div class="content_search_area">
				<div class="text_area">AREA</div>
				<div class="search_area">
					<p>PERFECTURE</p>
					<a href="#popup_perfecture" id="pop_perfecture" perfecture_id="<?php if(isset($perfecture_id_get)) echo $perfecture_id_get; ?>">
						<div class="btn_search_area">
							<span class="text_search_area my_per_name"><?php echo (isset($get_perfecture_name)) ? $get_perfecture_name : "select" ?></span>
							&nbsp;&nbsp;<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
						</div>
					</a>
				</div>
				<div class="search_area">
					<p>CITY</p>
					<a href="#popup_all_city" id="pop_city_by_per" city_id="<?php if(isset($city_id_get)) echo $city_id_get; ?>">
						<div class="btn_search_area">
							<span class="text_search_area my_city_name"><?php echo (isset($get_city_name)) ? $get_city_name : "select" ?></span>&nbsp;&nbsp;<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
						</div>
					</a>
				</div>
			</div>
			<div class="content_search_filter">
				<div class="text_filter">FILTER</div>
				<div class="content_type_search">
					<p class="type_text">Type</p>
					<div class="type_check">
						<input type="checkbox" name="typecheck[]" value="apartment" id="apartment" <?php if(isset($get_apartment)) echo "checked" ?>><label for="apartment"> Apartment</label><br/>
						<input type="checkbox" name="typecheck[]" value="house" id="house" <?php if(isset($get_house)) echo "checked" ?>><label for="house"> House</label><br/>
					</div>
					<p class="type_text">Status</p>
					<div class="status_check">
						<input type="checkbox" name="typecheck[]" value="buy" id="buy" <?php if(isset($get_buy)) echo "checked" ?>><label for="buy"> Buy</label><br/>
						<input type="checkbox" name="typecheck[]" value="rent" id="rent" <?php if(isset($get_rent)) echo "checked" ?>><label for="rent"> Rent</label><br/>
						<input type="checkbox" name="typecheck[]" value="short-stay" id="short-stay" <?php if(isset($get_short)) echo "checked" ?>><label for="short-stay"> Short Stay</label><br/>
					</div>
					<p class="type_text">Price</p>
					<div class="price_select">
						<select class="min_price">
						    <option value="">MIN</option>
						    <?php for($i = 0; $i < count($get_price); $i++) { ?>
								<option value="<?php echo $get_price[$i] ?>" <?php if(isset($get_min_price) && $get_min_price == $get_price[$i]) echo "selected"; ?>><?php echo number_format($get_price[$i]); ?></option>
							<?php } ?>
						</select>
						<span class="space">~</span>
						<select class="max_price">
						    <option value="">MAX</option>
						    <?php for($i = 0; $i < count($get_price); $i++) { ?>
								<option value="<?php echo $get_price[$i] ?>" <?php if(isset($get_max_price) && $get_max_price == $get_price[$i]) echo "selected"; ?>><?php echo number_format($get_price[$i]); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
	</div>
	</div>
	<!-- Perfecture popup -->
<div class="content_perfecture_pop">
	<div id="popup_perfecture">
		<p class="text_perfecture">PERFECTURE</p>
		<div class="per_pop">
			<table>
				<tr>
				<?php if($get_perfectures && $get_perfectures > 0){
					$i = 0;
					foreach ($get_perfectures as $perfecture) {
					$i++;
				?>
					<td>
						<a href="#popup_all_city" class="get_per_text" perfecture_id="<?php echo $perfecture->id; ?>" perfecture_data="<?php echo $perfecture->perfecture_name; ?>" >
							<img src="<?php echo $perfecture->perfecture_icon; ?>" alt="" />
							<p class="get_per_name"><?php echo $perfecture->perfecture_name; ?></p>
							<p>Perfecture</p>
						</a>
					</td>
					<?php if($i%2 == 0){ ?>
							</tr>
			            </table>
			        </div>
		            <div class="per_pop">
						<table>
							<tr>
					<?php }}} ?>
				</tr>
            </table>
        </div>
		<div class="content_close_popup">
			<img src="/assets/img/icon/close_popup.jpg" alt="CLOSE" class="close_popup">
		</div>
	</div>
</div>

<!-- City popup -->
<div class="content_city_popup">
	<div id="popup_all_city" class="popup_city">
		<p class="text_city">CITY</p>
		<div class="city_pop"></div>
		<div class="content_close_popup">
			<img src="/assets/img/icon/close_popup.jpg" alt="CLOSE" class="close_popup">
		</div>
	</div>
</div>
	<script type="text/javascript" >
		var arrayMarker = <?php echo $marker; ?>
	</script>