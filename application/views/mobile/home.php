<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta http-equiv="Expires" content="-1" />
	    <meta http-equiv="Pragma" content="no-cache" />
	    <meta http-equiv="Cache-Control" content="no-cache" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
			
		<title>Home View</title>
		
		<link rel="shortcut icon" type="image/png" href="/assets/img/icon/favo16.png"/>

		<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
		<link href="/assets/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="/assets/css/mobile/home.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
	</head>
	<body>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1030846260272871";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
		<div class="wrapper" id="wrapper">
			<?php if (isset($mobile_login_msg)) {
				echo $mobile_login_msg;
			} ?>
			<!-- Block Header -->
			<div class="content_header">
				<div class="content_logo">
					<a href="/"><img src="/assets/img/mobile/icon/home_logo.svg" alt="Home View Logo"></a>
				</div>
				<div class="content_menu">
					<div class="menu_icon" id="menu_icon">
						<img src="/assets/img/mobile/icon/menu_icon.png" alt="MENU">
						<img src="/assets/img/mobile/icon/menu_icon_change.png" style="display:none;" alt="MENU">
					</div>
					<div class="login_register">
						<?php if(!$is_logged_in){ ?>
							<a href="/login">LOGIN</a>
							<span>|</span>
							<a href="/register">REGISTER</a>
						<?php }else{ ?>
							<a href="/<?php echo $chk_group_user; ?><?php echo $me->user_id;  ?>" class="list_menu account_name">
								<i class="fa fa-user account_icon" aria-hidden="true"></i> 
								Hello! <?php echo $chk_user_group_name;  ?>
							</a>
							<span>|</span>
							<a href="/login/logout" class="list_menu">Log out <i class="fa fa-power-off" aria-hidden="true"></i></a>
						<?php } ?>
					</div>
					<div class="clear"></div>
				</div>
				<div class="content_slide_menu">
					<div class="slide_menu" id="slide_menu">
						<div class="wrap_slide_menu">
							<div><a href="/">HOME</a></div>
							<div><a href="/lists/buy">BUY</a></div>
							<div><a href="/lists/rent">RENT</a></div>
							<div><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" target="blank">FACE BOOK</a></div>
							<div><a href="/videos">360<sup>&deg;</sup> Images</a></div>
						</div>
					</div>
				</div>
				<div class="text_find_home">
					FIND A HOME IN JAPAN
				</div>
				<div class="content_menu_status">
					<div class="menu_status">
						<div><a href="/pick-city/buy">BUY</a></div>
						<div><a href="/pick-city/rent">RENT</a></div>
						<div><a href="/pick-city/short-stay">SHORT STAY</a></div>
					</div>
				</div>
			</div>
			<!-- Close block header -->
			<!-- Block body -->
			<div class="content_body">
				<div class="text_popular_home">
					MOST POPULAR HOME
				</div>
				<div class="content_data_properties">
					<?php if($get_home && count($get_home) > 0){
						foreach ($get_home as $row) {
					?>
					<a href="/detail/<?php echo $row->id; ?>">
						<div class="data_properties">
							<div class="properties_image">
								<?php if($row->images_path != ""){ ?>
									<img src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>" alt="Image of <?php echo $row->building_name; ?>">
								<?php }else{ ?>
									<img src="/assets/img/uploads/default_image_properties.jpg" alt="No Image">
								<?php } ?>
							</div>
							<div class="properties_img_status">
								<?php if($row->status == 1){ ?>
									<img src="/assets/img/mobile/icon/status_buy.png" alt="Status" />
								<?php } ?>
								<?php if($row->status == 2){ ?>
									<img src="/assets/img/mobile/icon/status_rent.png" alt="Status" />
								<?php } ?>
								<?php if($row->status == 3){ ?>
									<img src="/assets/img/mobile/icon/status_short_stay.png" alt="Status" />
								<?php } ?>
							</div>
							<div class="properties_description">
								<div class="properties_name properties_font">
									<?php echo $row->building_name; ?>
								</div>
								<div class="properties_location properties_font">
									<?php echo $row->city_name." ".$row->perfecture_name; ?>
								</div>
								<div class="properties_price properties_font">
									<?php echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price); ?>
								</div>

								<?php if($row->views != ""){ ?>
								<div class="properties_view properties_font">
									<span><?php echo $row->views; ?></span> Views
								</div>
								<?php } ?>
							</div>
							<div class="clear"></div>
						</div>
					</a>
					<?php }}else{ ?>
						<div class="not_found">
							This property could not be found
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="content_step">
				<div class="text_how">
					HOW TO GET A PLACE IN JAPAN
				</div>
				<div class="link_detail_how">
					<a href="/step">DETAILS</a>
				</div>
			</div>
			<div class="content_blog">
				<!-- <div class="blog_text">
					BLOG
				</div>
				<div class="blog_link">
					<span><a data_blog="all" class="blog_link_active link_blog">ALL</a></span>
					<span><a data_blog="news" class="link_blog">NEWS</a></span>
					<span><a data_blog="buy" class="link_blog">BUY</a></span>
					<span><a data_blog="rent" class="link_blog">RENT</a></span>
					<span><a data_blog="living" class="link_blog">LIVING</a></span>
				</div> -->
				<!-- <div class="blog_data_left">
				<?php if(isset($get_blogs) && count($get_blogs) > 0){
					foreach ($get_blogs as $row) {
				?>
				<a href="/blog-detail/<?php echo $row->id; ?>" class="link_blog_all">
					<div class="blog_data">
						<div class="blog_title">
							<?php 
								$count_str_title = strlen($row->title);
								if($count_str_title > 33){
									echo substr($row->title, 0,33).'...';
								}else{
									echo $row->title;
								}
							?>
						</div>
						<div class="blog_date">
							<?php 
								$time = strtotime($row->created_dt);
								$newformat = date('Y m d',$time);
								echo $newformat;
							?>
						</div>
						<div class="blog_description">
							<?php 
								$count_str_body = strlen(strip_tags($row->body));
								if($count_str_body > 100){
									echo substr(strip_tags($row->body), 0,100)."...";
								?>
								<?php }else{
									echo strip_tags($row->body);
								}
							?>
						</div>
					</div>
				</a>
				<?php } ?>
				<?php if(count($get_blogs) >= 4){ ?>
					<div class="more_blog">
						<a href="/blog">MORE</a>
					</div>
			    <?php }}else{ ?>
					<div class="not_found">
						<h3 style="margin-bottom: 10px;">Page Not Found</h3>
						<p>The page you're looking for cannot be found.</p>
					</div>
				<?php } ?>
				</div> -->
				<div class="fb-page" data-href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" data-tabs="timeline" data-width="500" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
			    <blockquote cite="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts">HOME VIEW</a></blockquote></div>
			</div>

			<!-- Block Footer -->
			<div class="content_footer">
				<div class="footer_contact">
					<div class="wrap_contact">
						<div class="footer_phone">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<span> &nbsp; +82-32-264-5500</span>
						</div>
						<div class="footer_email">
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<span> &nbsp; INFO@HOMEVIEW.JP</span>
						</div>
					</div>
				</div>
				<div class="footer_copy_right">
					<div class="copy_top">
						©2016 Copyright Home View. All Rights Reserved. 
					</div>
					<div class="copy_bottom">
						Designed by Softbloom Co., LTD.
					</div>
				</div>
			</div>

		</div>
		<script type="text/javascript" src="/assets/js/jquery.js"></script>
		<script type="text/javascript" src="/assets/js/mobile/home.js?v=<?php echo time(); ?>"></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-88691267-1', 'auto');
		  ga('send', 'pageview');
		</script>
	</body>
</html>