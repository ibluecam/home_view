<div class="content_favorite">
	<?php $this->load->view('/template_mobile/sidebar');?>
	<form  method="get">
		<div class="top_header_form">
			<?php
			    $success=$this->session->flashdata('success');
			    if(isset($success)){
					echo $success;
				}	
		    ?> 
			<input class="vertical_middle" id="selectall" type="checkbox" name="checkall"/>
			<label for="selectall">Selecte All</label>
			<span class="count_chk">0</span> <span>Files Selected</span>
			<a href="#popup_delete" id="pop_delete">
				<button type="button" value="1" id="btn_delete" class="btn_delete">
					<i class="fa fa-trash" aria-hidden="true"></i>
			    </button>
			</a>
			<input class="btnsubmit" type="submit" value="DELETE"  name ="btndelete" style="display:none"/>
			<div class="btn_search">
		    	<input class="text_keyword" type="text" value="<?php echo (isset($_GET['keyword'])) ? $_GET["keyword"] : set_value('keyword'); ?>" placeholder="Keyword" name="keyword"/>
		    	<input type="submit" name="btnsearch" class="search" value="search"/>
			</div>
		</div>
		<div class="block_data_properites">
			<?php if(count($favorites)>0){ ?>
			<?php foreach ($favorites as $row) { ?>
			<div class="data_properties">
				<div class="properties_image">
					 <input class="data_check check_one_<?php echo $row->properties_id_fk; ?>" 
				        value="<?php echo  $row->properties_id_fk .','. $me->user_id;  ?>"
				        attr_id="<?php echo $row->properties_id_fk; ?>" type="checkbox" name="chk_favordite[]"/>

						<?php  if($row->images_path !=""){ ?>
							<a href="/detail/<?php echo $row->properties_id; ?>"><img class="thumb_image" alt="<?php echo strtoupper($row->building_name); ?>" src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>"/></a>
						<?php }else{  ?>
							<a href="/detail/<?php echo $row->properties_id; ?>"><img class="thumb_image" src="/assets/img/uploads/properties/resize/default_image_properties.jpg"/></a>
						<?php } ?>
				</div>
				<div class="properties_img_status">
					<?php 
						if($row->properties_status==1){
								echo "<img alt='Status' src='/assets/img/mobile/icon/status_buy.png'>";
							}else if($row->properties_status==2){
								echo "<img alt='Status' src='/assets/img/mobile/icon/status_rent.png'>";
							}else{
								echo "<img alt='Status' src='/assets/img/mobile/icon/status_short_stay.png'>";
					    }
					?>
				</div>
				<a href="/detail/<?php echo $row->properties_id; ?>">
					<div class="properties_description">
						<div class="properties_name properties_font">
						<?php 	echo strtoupper($row->building_name); ?>
						</div>
						<div class="properties_location properties_font">
							<?php echo $row->city_name." ".$row->perfecture_name; ?>	
						</div>
						<div class="properties_price properties_font">
							<?php echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price); ?>
						</div>
						<div class="properties_view properties_font">
							<span><?php echo $row->views; ?></span> Views
						</div>
					</div>
			   </a>
				<div class="clear"></div>
			</div>
		<?php } ?>
		<?php }else{ ?>
		<div class="not_found">
			<h2 style="margin-bottom: 10px;">Page Not Found</h2>
			<p>The page you're looking for cannot be found.</p>
		</div>
		<?php } ?>
		</div>
		<div class="clear"></div>
	    <div class="pagination">
			<?php echo $pagination; ?>
	    </div>
	     <!-- popup confrim message -->
	   <div id="popup_delete">
	   		<div class="popup_header"></div>
	   		<div class="popup_footer">
	   			<input class="popup_btn popup_submit" type="button" value="DELETE"  name ="btndelete"/>
	   			<input class="popup_btn popup_cancel" type="submit" value="CANCEL"  name ="btncancel"/>
	   		</div>
	   </div>
	   <!-- end popup confrim message -->

	</form>
</div>