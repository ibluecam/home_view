<div class="video_list">
	<?php if(count($get_video_list) > 0){
		$i = 0;
			foreach ($get_video_list as $row) {
	?>
	<div class="video_item">
		<div class="video_image_title">
			<a href="#popup_vdo<?php echo $i ?>" class="popup_vdo">
				<?php if($row->images_path != ""){ ?>
					<img class="video_image" src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>" alt="">
				<?php }else{ ?>
					<img class="video_image" src="/assets/img/uploads/properties/resize/default_image_properties.jpg" alt="">
				<?php } ?>
			</a>
			<a href="#popup_vdo<?php echo $i ?>" class="popup_vdo"><h3 class="video_title">360°  渋谷常磐松ハウス</h3></a>
		</div>
	</div>
	<div class="content_popup<?php echo $i ?>" style="display:none;">
		<div id="popup_vdo<?php echo $i ?>">
				<div class="content_video">
					<iframe class="video_360_degree" src="https://www.youtube.com/embed/<?php echo $row->video_id; ?>" frameborder="0" allowfullscreen></iframe>	
				</div>
		</div>
	</div>
	<?php $i++; }}else{ ?>
		<div class="not_found">
			<h2 style="margin-bottom: 10px;">Page Not Found</h2>
			<p>The page you're looking for cannot be found.</p>
		</div>
	<?php } ?>
	<div class="pagination">
		<?php echo $pagination; ?>
	</div>
</div>