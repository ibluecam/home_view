			<?php if(isset($get_blogs) && count($get_blogs) > 0){
					foreach ($get_blogs as $row) {
				?>
				<a href="/blog-detail/<?php echo $row->id; ?>" class="link_blog_all">
					<div class="blog_data">
						<div class="blog_title">
							<?php 
								$count_str_title = strlen($row->title);
								if($count_str_title > 33){
									echo substr($row->title, 0,33).'...';
								}else{
									echo $row->title;
								}
							?>
						</div>
						<div class="blog_date">
							<?php 
								$time = strtotime($row->created_dt);
								$newformat = date('Y m d',$time);
								echo $newformat;
							?>
						</div>
						<div class="blog_description">
							<?php 
								$count_str_body = strlen(strip_tags($row->body));
								if($count_str_body > 100){
									echo substr(strip_tags($row->body), 0,100)."...";
								?>
								<?php }else{
									echo strip_tags($row->body);
								}
							?>
						</div>
					</div>
				</a>
			<?php } ?>
			<?php if(count($get_blogs) >= 4){ ?>
				<div class="more_blog">
					<a href="/blog">MORE</a>
				</div>
		    <?php }}else{ ?>
				<div class="not_found">
					<h3 style="margin-bottom: 10px;">Page Not Found</h3>
					<p>The page you're looking for cannot be found.</p>
				</div>
			<?php } ?>