<div class="content_favorite">
	<?php $this->load->view('/template_mobile/sidebar');?>
	<div class="title_info">
		<span>MY INFO</span>
	</div>
	<?php if(count($user_info) > 0){ ?>
	<div class="content_form">
		<form action="/favorite/info/edit/<?php echo $user_id; ?>" method="POST">
			<table class="table_info">
				<tr>
					<td class="td_left">First Name</td>
					<td class="td_right"><?php echo $user_info->first_name; ?></td>
				</tr>
				<tr>
					<td class="td_left">Last Name</td>
					<td class="td_right"><?php echo $user_info->last_name; ?></td>
				</tr>
				<tr>
					<td class="td_left">Email</td>
					<td class="td_right"><?php echo $user_info->email; ?></td>
				</tr>
				<tr>
					<td class="td_left">Passwords</td>
					<td class="td_right">******</td>
				</tr>
				<tr class="tr_center">
					<td colspan="2">
						<input type="submit" name="edit" class="button_edit_info" value="Edit Info">
					</td>
				</tr>
			</table>
		</form>		
	</div>
	<?php } ?>
</div>