
<div class="blog_list">
	<div class="blog_link">
		<span><a href="/blog/all" class="<?php if($blog_link_active == 'all'){echo 'blog_link_active';} ?>">ALL</a></span>
		<span><a href="/blog/news" class="<?php if($blog_link_active == 'news'){echo 'blog_link_active';} ?>">NEWS</a></span>
		<span><a href="/blog/buy" class="<?php if($blog_link_active == 'buy'){echo 'blog_link_active';} ?>">BUY</a></span>
		<span><a href="/blog/rent" class="<?php if($blog_link_active == 'rent'){echo 'blog_link_active';} ?>">RENT</a></span>
		<span><a href="/blog/living" class="<?php if($blog_link_active == 'living'){echo 'blog_link_active';} ?>">LIVING</a></span>
	</div>
	<?php 
		if(isset($get_blogs) && count($get_blogs) > 0){
			foreach ($get_blogs as $row) {
	?>
		<div class="blog_item">
			<h2 class="blog_title"><a href="/blog-detail/<?php echo $row->id; ?>">
				<?php 
					$count_str_title = strlen($row->title);
					if($count_str_title > 58){
						echo substr($row->title, 0,58)."...";
					}else{
						echo $row->title;
					}
				?>
			</a></h2>
			<span class="blog_date">
				<?php 
					$time = strtotime($row->created_dt);
					$newformat = date('Y m d',$time);
					echo $newformat;
				?>
			</span>
			<p class="blog_text">
				<?php 
					$count_str_body = strlen(strip_tags($row->body));
					if($count_str_body > 150){
						echo substr(strip_tags($row->body), 0,150);
					?>
					<span><a class="readmore" href="/blog-detail/<?php echo $row->id; ?>">learn more...</a></span>
					<?php }else{
						echo strip_tags($row->body);
					}
				?>
			</p>
		</div>
	<?php }}else{ ?>
		<div class="not_found">
			<h2 style="margin-bottom: 10px;">Page Not Found</h2>
			<p>The page you're looking for cannot be found.</p>
		</div>
	<?php } ?>
	<div class="pagination">
		<?php echo $pagination; ?>
	</div>
</div>