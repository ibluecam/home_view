<div class="content_login">
	<div class="block_button">
		<span class="text_login">LOGIN</span>
		<a href="/register"><span class="text_register">REGISTER</span></a>
	</div>
	<div class="clear"></div>
	<div class="block_form">
	  <form id="login" action="" method="post">
		<div class="title_sigin">
			<span>SING IN</span>
		</div>
			<?php if(!empty($error_message)){ ?>
				<div class="isa_error">
				   <i class="fa fa-times-circle"></i>
				   <?php echo $error_message; ?>
				</div>
			<?php }	?> 
			<div class="form_controll">
				<input class="text_field" type="text" name="email" value="<?php echo set_value('email') ?>" placeholder="Email Address"/>
			</div>
			<div class="php_error"><?php echo form_error('email'); ?></div>
			<div class="form_controll">
				<input class="text_field" type="password" value="<?php echo set_value('password') ?>" name="password" placeholder="Password"/>
			</div>
			<div class="php_error"><?php echo form_error('password'); ?></div>
			<div class="clear"></div>
			<div class="form_controll">
				<input class="btn_go" type="submit" name="btngo" value="Go"/>
			</div>
		<div class="block_forget">
			<p class="forget">Forgot your password?</p>
			<p class="dont">Don't Have An Account Yet?</p>
			<a href="/register"><span class="yet">Register Here</span></a>
		</div>
	 </form>
	</div>
</div>