

<div class="content_step">
	<div class="content_step_detail" id="tab-1">
		<p class="text_title">1: Cash</p>
		<p class="text_description">When you try rent Japanese apartments, you have to get Japanese cash,</p><br/>
		<p class="text_description">Basic fees to consider are the deposit, the real estate agency commission fee, plus the first month rent, as well as a small fee for property insurance, an annual maintenance fee and a key exchange fee.</p><br/>
		<p class="text_description">You might have to pay key money too, though not all landlords will ask for it and some agents may waive the fee</p>
		<p class="text_description">So for a standard apartment for 80,000 yen a month, you can expect to pay around 400,000 yen in upfront costs before any moving services and the initial costs for setting up your utilities.</p><br/>
		<p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Deposit (one month’s rent) = 80,000</p>
		<p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Key money (one month’s rent) = 80,000</p>
		<p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Agent fee (up to one and a half months rent) = 120,000</p>
		<p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;First month’s rent = 80,000</p>
		<p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Property insurance = 15,000</p>
		<p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Maintenance fee = 10,000</p>
		<p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Key exchange fee = 12,000</p><br/>
		<p class="text_description">Total: 397,000 yen</p><br/>
		<p class="text_title">2: Passport and Visa</p>
		<p class="text_description">If apply for an apartment or housing contract in Japan you need to provide two forms of official identification:</p><br/>
		<p class="text_description">1. Your passport</p>
		<p class="text_description">2. visa, residence card or student ID.</p><br/>
		<p class="text_description">If you’re on a tourist visa, you’ll only be able to rent short-term contracts that specifically don’t require a guarantor. For long-term rentals, a 90-day tourist visa won’t be accepted.</p><br/>
		<p class="text_title">3: Japanese phone number</p>
		<div class="step_img">
			<img src="/assets/img/step_phone_img.jpg">
		</div>
		<div class="step_block">
			<p class="text_description">You need to have a working phone number where agent can contact you</p>
			<p class="text_description">directly If applying from overseas, an international number is fine.</p>
		</div>
		<div class="clear"></div><br/>
		<p class="text_title">4: Japanese Bank Account</p>
		<p class="text_description">When you start look at Apartment, you don't need to have Japanese bank account, you’ll need one eventually to pay the rent via bank transfer. For the upfront costs like the deposit you can wire transfer 
			from an international bank and some agents will accept credit card. Cash payment is rare, though possible at some places. It’s best to check with the agent which method they prefer. For overseas applications, your home account will 
			work but you’ll have to cover any transfer costs.
		</p><br/>
		<p class="text_title">5: Employer letter or certficate of eligibility (If you’re a student)</p>
		<p class="text_description">
			These are the same documents you likely used in your visa application; any papers that demonstrate your activities in Japan such as a letter of employment, invitation letter or certificate of eligibility from the immigration bureau. Often your letter of employment will show your salary information but you should also prepare number 7 below…
		</p><br/>
		<p class="text_title">6: A copy of recent pay slips or bank statement</p>
		<p class="text_description">You’ll need to prove that you can pay the rent each month so agents will ask for a copy of the past few months’ pay slips (usually 3 months), your yearly income slip or a copy of your latest bank statement or bank book if you’re unemployed.</p><br/>
		<p class="text_description">Japanese agents will set the rent at around 30% of your income so you need to prove that you consistently make or will make more than 3 times the rent. Some agents may consider only the income you earn from within the country so you won’t be allowed to rent a place that’s more than a third of your Japan income, even if you plan to subsidize that with earnings from back home.</p><br/>
		<p class="text_title">7: Domestic Emergency Contact</p>
		<p class="text_description">In case you suddenly abandon ship and leave the country, the emergency contact deals with the hot mess you left behind so it’s sometimes difficult to find a Japanese person who is willing. Your best bet is to nominate your employer if you can; culturally they view you as their responsibility and as an organization they are better prepared to deal with any problems that might arise. Nevertheless, some agents will let you nominate a non-Japanese resident as an emergency contact.</p><br/>
		<p class="text_title">8: Guarantor</p>
		<p class="text_description">Even if you can show that you are employed and earning enough salary, you will still need a guarantor who will be liable for the rent if you can’t make the payments. Some Japanese people who need a guarantor will ask their parents, some companies might also cover their employees. If you can find someone to be your guarantor they’ll need to prepare several documents including proof of residence and an income statement which they will have to get from the local government office. They will also have to prove that the rent is around 30% of what they earn or else they cannot be your guarantor.</p><br/>
		<p class="text_description">If you don’t have somebody you can ask, you can use a guarantor company which the agent will recommend to you. They act as a kind of third party insurance company – you won’t have to deal with them directly but you will have to pay them a month’s rent or more for the service, plus an annual renewal fee of around 10,000 yen. In fact, it’s becoming more and more common for Japanese people to use a guarantor company either at the specific request of the agent or by choice (to avoid burdening someone with the responsibility of being a guarantor).</p><br/>
		<p class="text_description">Finally, make sure to have several copies of all of the documents and keep everything in one place as you’ll need it all again if you renew your contract or move elsewhere. You should start to look for apartments around 1 month before you intend to move and processing the contract should take about two weeks. Happy hunting!</p><br/>
		<p class="text_title">Useful words to know:</p>
		<div class="content_table_step">
			<table>
				<tr>
					<td>Passport: <span>パスポート</span> pasupo-to</td>
					<!-- <td><span>パスポート</span> pasupo-to</td> -->
				</tr>
				<tr>
					<td>Visa: <span>ビザ</span> bi-zah</td>
					<!-- <td><span>ビザ</span> bi-zah</td> -->
				</tr>
				<tr>
					<td>Residence Card: <span>在留</span> zairyu card</td>
					<!-- <td><span>在留</span> zairyu card</td> -->
				</tr>
				<tr>
					<td>Letter of employment (with salary information): <span>在籍証明書</span> zaisekishomeisho</td>
					<!-- <td><span>在籍証明書</span> zaisekishomeisho</td> -->
				</tr>
				<tr>
					<td>Pay slip/Tax withholding slip: <span>源泉徴収票</span> gensenchoshuhyo</td>
					<!-- <td><span>源泉徴収票</span> gensenchoshuhyo</td> -->
				</tr>
				<tr>
					<td>Certificate of eligibility: <span>在留資格認定証明書</span> zairyushikaku nintei shomeisho</td>
					<!-- <td><span>在留資格認定証明書</span> zairyushikaku nintei shomeisho</td> -->
				</tr>
				<tr>
					<td>Guarantor: <span>保証人</span> hoshonin</td>
					<!-- <td><span>保証人</span> hoshonin</td> -->
				</tr>
			</table>
		</div>
	</div>
</div>