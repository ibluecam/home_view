<div class="blog_detail">
<?php if(count($get_blog_detail) > 0) {?>
	<h2 class="blog_title"><?php echo $get_blog_detail->title; ?></h2>
	<span class="blog_date">
		<?php 
			$time = strtotime($get_blog_detail->created_dt);
			$newformat = date('Y m d',$time);
			echo $newformat;
		?>
	</span>
	<div class="feature_image">
		<?php if($get_blog_detail->feature_image !== ""){ ?>
			<img src="/assets/img/uploads/blog/resize/<?php echo $get_blog_detail->feature_image; ?>">
		<?php }else{ ?>
			<img src="/assets/img/uploads/blog/resize/default_feature_image.jpg">
		<?php } ?>
	</div>
	<div class="blog_text">
		 <p><?php echo $get_blog_detail->body; ?></p>
	</div>

	<div class="show_category">
		<a href="/blog/<?php echo $get_blog_detail->category; ?>"><?php echo $get_blog_detail->category; ?></a>
	</div>
	<div class="resource">
		<span class="title_source">Source:</span> <?php echo $get_blog_detail->source; ?>	
	</div>
<?php }else{ ?>
	<div class="blog_link">
		<span><a href="/blog/all">ALL</a></span>
		<span><a href="/blog/news">NEWS</a></span>
		<span><a href="/blog/buy">BUY</a></span>
		<span><a href="/blog/rent">RENT</a></span>
		<span><a href="/blog/living">LIVING</a></span>
	</div>
	<div class="not_found">
		<h2 style="margin-bottom: 10px;">Page Not Found</h2>
		<p>The page you're looking for cannot be found.</p>
	</div>
<?php } ?>
</div>