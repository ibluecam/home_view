<div class="content_pick_city">
	<div class="pick_search">
		<span>
			<i class="fa fa-search" aria-hidden="true"></i>
			FIND FROM AREA
		</span>
	</div>
	<div class="count_list">
		<span>
			<?php echo number_format($count_properties[0]->count_properties); ?>
			<span>List</span>
			<img src="/assets/img/icon/wifi.png"/>
		</span>
	</div>
	<?php 
		if($link_status != ""){ 
			$status = "?sta=".$link_status;
		}else{
			$status = "";
		} 
	?>
	<div class="content_img_map">
		<div class="link_tochigi">
			<a href="/city/tochigi<?php echo $status; ?>"></a>
		</div>
		<div class="link_gunma">
			<a href="/city/gunma<?php echo $status; ?>"></a>
		</div>
		<div class="link_ibaraki">
			<a href="/city/ibaraki<?php echo $status; ?>"></a>
		</div>
		<div class="link_saitama">
			<a href="/city/saitama<?php echo $status; ?>"></a>
		</div>
		<div class="link_tokyo">
			<a href="/city/tokyo<?php echo $status; ?>"></a>
		</div>
		<div class="link_kanagawa">
			<a href="/city/kanagawa<?php echo $status; ?>"></a>
		</div>
		<div class="link_chiba">
			<a href="/city/chiba<?php echo $status; ?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<div class="content_pick_mms">
		<a href="/map">
			 <img class="img" src="/assets/img/icon/map.png"/>
			 MAP
		</a>
	</div>
	<div class="content_pick_mms">
		<a href="/market-price">	
			<img class="img" src="/assets/img/icon/market_price.png"/>
			MARKET PRICE
		</a>
	</div>
	<div class="content_pick_mms">
		<img class="img" src="/assets/img/icon/short_term.png"/>
		SHORT TERM
	</div>
	<div class="content_couple">
		<table>
			<tr class="tr_border">
				<td>
					<img src="/assets/img/icon/couple.png"/>
					<p class="text_couple">Couple</p>
				</td>
				<td>
					<img src="/assets/img/icon/school.png"/>
					<p class="text_school">Close to </br> school</p>
				</td>
			</tr>
			<tr>
				<td>
					<img src="/assets/img/icon/new_constr.png"/>
					<p class="text_new_constr">New </br> Construction</p>
				</td>
				<td>
					<img src="/assets/img/icon/company.png"/>
				<p class="text_company">Close to </br> Company</p>
				</td>
			</tr>
		</table>
	</div>
	<div class="company_info">
		<div class="data_info">
			<img class="img" src="/assets/img/icon/phone.png"/>
			<span>+82-32-264-5500</span>
		</div>
		<div class="data_info">
			<img class="img" src="/assets/img/icon/message.png"/>
			<span>INFO@HOMEVIEW.JP</span>
		</div>
		<div class="data_info skype">
			<img class="img" src="/assets/img/icon/skype.png"/>
			<span><a href="skype://live:shimanekennow?call">live:shimanekennow</a></span>
		</div>
		<div class="data_info viber">
			<img class="img" src="/assets/img/icon/viber.png"/>
			<span><a href="viber://+818064501135">+818064501135</a></span>
		</div>
		<div class="data_info line">
			<img class="img" src="/assets/img/icon/line.png"/>
			<span><a line-id="home-view">home-view</a></span>
		</div>
	</div>
</div>