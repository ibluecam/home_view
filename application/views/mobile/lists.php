
<div class="content_list">
	<input type="hidden" value="<?php echo $link_status; ?>" class="hide_link_status"/>
	<div class="content_list_menu">
		<div class="list_active">LIST SEARCH</div>
		<div><a href="<?php if($link_to_map) echo $link_to_map; ?>">SEARCH BY MAP</a></div>
	</div>
	<?php if(isset($get_perfecture_name)){ ?>
	<div class="content_location">
		<div class="content_perfecture">
			<i class="fa fa-map-marker map_icon" aria-hidden="true"></i>
			<span class="name_perfecture"><?php echo $get_perfecture_name; ?> PERFECTURE <?php if(!isset($get_city_name)){ ?>(<span class="count_city"><?php echo $count_city; ?></span>)<?php }else{ ?>: <?php } ?></span>
		</div>
		<div class="content_city">
			<?php if(isset($get_city_name)){ echo $get_city_name; ?> (<span class="count_city"><?php echo $count_city; ?></span>)<?php } ?>
		</div>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<div class="content_link_sort">
	<div class="text_sort">SORT BY:</div>
		<div class="sort_top">
		    <span class="span_sort">
		    	<span>Price</span>
			    <a href="<?php echo $link_to_list; ?>stp&ced=desc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
			    <a href="<?php echo $link_to_list; ?>stp&ced=asc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
		    </span>
		    <span class="span_sort">
		    	<span>Size</span>
			    <a href="<?php echo $link_to_list; ?>stb&ced=desc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
			    <a href="<?php echo $link_to_list; ?>stb&ced=asc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
		    </span>
	    </div>
	    <div class="bottom_sort">
		    <span class="span_sort">
		    	<span>Age</span>
			    <a href="<?php echo $link_to_list; ?>sta&ced=asc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
			    <a href="<?php echo $link_to_list; ?>sta&ced=desc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
		    </span>
		    <span class="span_sort">
		    	<span>Distance to Station</span>
			    <a href="<?php echo $link_to_list; ?>std&ced=desc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
			    <a href="<?php echo $link_to_list; ?>std&ced=asc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
		    </span>
	    </div>
	</div>
	<div class="content_filter">
		FILTER
			<span class="bg_sort sort_down"></span>
			<span class="bg_sort sort_up"></span>
		</span>
	</div>
	<div class="content_slide_filter">
	<form action="/lists" method="get">
		<div class="title_text">Type</div>
		<div class="frm_filter">
			<table>
				<tr>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="1k" id="1k" <?php if(isset($typecheck) && in_array('1k', $typecheck)){ echo "checked";} ?>><label for="1k"> 1K</label></td>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="1dk" id="1dk" <?php if(isset($typecheck) && in_array('1dk', $typecheck)){ echo "checked";} ?> ><label for="1dk"> 1DK</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="1ldk" id="1ldk" <?php if(isset($typecheck) && in_array('1ldk', $typecheck)){ echo "checked";} ?> ><label for="1ldk"> 1LDK</label></td>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="2k" id="2k" <?php if(isset($typecheck) && in_array('2k', $typecheck)){ echo "checked";} ?> ><label for="2k"> 2K</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="2dk" id="2dk" <?php if(isset($typecheck) && in_array('2dk', $typecheck)){ echo "checked";} ?> ><label for="2dk"> 2DK</label></td>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="2ldk" id="2ldk" <?php if(isset($typecheck) && in_array('2ldk', $typecheck)){ echo "checked";} ?> ><label for="2ldk"> 2LDK</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="3k" id="3k" <?php if(isset($typecheck) && in_array('3k', $typecheck)){ echo "checked";} ?> ><label for="3k"> 3K</label></td>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="3dk" id="3dk" <?php if(isset($typecheck) && in_array('3dk', $typecheck)){ echo "checked";} ?> ><label for="3dk"> 3DK</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="3ldk" id="3ldk" <?php if(isset($typecheck) && in_array('3ldk', $typecheck)){ echo "checked";} ?> ><label for="3ldk"> 3LDK</label></td>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="4k" id="4k" <?php if(isset($typecheck) && in_array('4k', $typecheck)){ echo "checked";} ?> ><label for="4k"> 4K</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="4dk" id="4dk" <?php if(isset($typecheck) && in_array('4dk', $typecheck)){ echo "checked";} ?> ><label for="4dk"> 4DK</label></td>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="4ldk" id="4ldk" <?php if(isset($typecheck) && in_array('4ldk', $typecheck)){ echo "checked";} ?> ><label for="4ldk"> 4LDK</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="1room" id="1room" <?php if(isset($typecheck) && in_array('1room', $typecheck)){ echo "checked";} ?> ><label for="1room"> 1 ROOM</label></td>
					<td><input type="checkbox" class="typecheck" name="typecheck[]" value="more5k" id="more5k" <?php if(isset($typecheck) && in_array('more5k', $typecheck)){ echo "checked";} ?> ><label for="more5k"> More 5K</label></td>
				</tr>
			</table>
		</div>
		<div class="title_text">Maked</div>
		<div class="frm_filter">
			<span class="maked"><input type="checkbox" class="makedcheck" name="makedcheck[]" value="iron" id="iron" <?php if(isset($makedcheck) && in_array('iron', $makedcheck)){ echo "checked";} ?> ><label for="iron">Iron</label></span>
			<span class="maked1"><input type="checkbox" class="makedcheck" name="makedcheck[]" value="wood" id="wood" <?php if(isset($makedcheck) && in_array('wood', $makedcheck)){ echo "checked";} ?> ><label for="wood">Wood</label></span>
			<span class="maked2"><input type="checkbox" class="makedcheck" name="makedcheck[]" value="other" id="other" <?php if(isset($makedcheck) && in_array('other', $makedcheck)){ echo "checked";} ?> ><label for="other">Other</label></span>
		</div>
		<div class="title_text">Position</div>
		<div class="frm_filter">
			<table class="position_td">
				<tr>
					<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="1f" id="1f" <?php if(isset($positioncheck) && in_array('1f', $positioncheck)){ echo "checked";} ?> ><label for="1f"> 1F</label></td>
					<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="more2f" id="more2f" <?php if(isset($positioncheck) && in_array('more2f', $positioncheck)){ echo "checked";} ?> ><label for="more2f"> More 2F</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="topfloor" id="topfloor" <?php if(isset($positioncheck) && in_array('topfloor', $positioncheck)){ echo "checked";} ?> ><label for="topfloor"> Top Floor</label></td>
					<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="cornorroom" id="cornorroom" <?php if(isset($positioncheck) && in_array('cornorroom', $positioncheck)){ echo "checked";} ?> ><label for="cornorroom"> Cornor Room</label></td>
				</tr>
			</table>
		</div>
		<div class="title_text">Price</div>
		<div class="frm_filter center">
			<select class="min_price"  name="min_price">
			    <option value="">MIN</option>
			    <?php for($i = 0; $i < count($get_price); $i++) { ?>
					<option value="<?php echo $get_price[$i] ?>" <?php if(isset($get_min_price) && $get_min_price == $get_price[$i]) echo "selected"; ?>><?php echo number_format($get_price[$i]); ?></option>
				<?php } ?>
			</select>
			<span class="space">~</span>
			<select class="max_price" name="min_price">
			    <option value="">MAX</option>
			    <?php for($i = 0; $i < count($get_price); $i++) { ?>
					<option value="<?php echo $get_price[$i] ?>" <?php if(isset($get_max_price) && $get_max_price == $get_price[$i]) echo "selected"; ?>><?php echo number_format($get_price[$i]); ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="title_text">Size</div>
		<div class="frm_filter center">
			<select name="min_breadth" class="min_breadth">
				<option value="">MIN</option>
				<?php for($i = 0; $i < count($get_breadth); $i++) { ?>
					<option value="<?php echo $get_breadth[$i] ?>" <?php if(isset($get_min_breadth) && $get_min_breadth == $get_breadth[$i]) echo "selected"; ?>><?php echo $get_breadth[$i]; ?></option>
				<?php } ?>
			</select>
			<span class="space">~</span>
			<select name="max_breadth" class="max_breadth">
				<option value="">MAX</option>
				<?php for($i = 0; $i < count($get_breadth); $i++) { ?>
					<option value="<?php echo $get_breadth[$i] ?>" <?php if(isset($get_max_breadth) && $get_max_breadth == $get_breadth[$i]) echo "selected"; ?>><?php echo $get_breadth[$i]; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="title_text">Age</div>
		<div class="frm_filter center">
			<select name="min_age" class="min_age">
				<option value="">MIN</option>
				<?php for($i = 0; $i < count($get_age); $i++) { ?>
					<option value="<?php echo $get_age[$i] ?>" <?php if(isset($get_min_age) && $get_min_age == $get_age[$i]) echo "selected"; ?>><?php if($get_age[$i] == 0){echo "New";}else if($get_age[$i] == 1){echo $get_age[$i]." Year";}else{echo $get_age[$i]." Years";} ?></option>
				<?php } ?>
			</select>
			<span class="space">~</span>
			<select name="max_age" class="max_age">
				<option value="">MAX</option>
				<?php for($i = 0; $i < count($get_age); $i++) { ?>
					<option value="<?php echo $get_age[$i] ?>" <?php if(isset($get_max_age) && $get_max_age == $get_age[$i]) echo "selected"; ?>><?php if($get_age[$i] == 0){echo "New";}else if($get_age[$i] == 1){echo $get_age[$i]." Year";}else{echo $get_age[$i]." Years";} ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="title_text">Distance to Station</div>
		<div class="frm_filter center">
			<select name="min_distance_to_station" class="min_distance_to_station">
				<option value="">MIN</option>
				<?php for($i = 0; $i < count($get_distance); $i++) { ?>
					<option value="<?php echo $get_distance[$i] ?>" <?php if(isset($get_min_distance_to_station) && $get_min_distance_to_station == $get_distance[$i]) echo "selected"; ?>><?php echo $get_distance[$i]." minutes"; ?></option>
				<?php } ?>
			</select>
			<span class="space">~</span>
			<select name="max_distance_to_station" class="max_distance_to_station">
				<option value="">MAX</option>
				<?php for($i = 0; $i < count($get_distance); $i++) { ?>
					<option value="<?php echo $get_distance[$i] ?>" <?php if(isset($get_max_distance_to_station) && $get_max_distance_to_station == $get_distance[$i]) echo "selected"; ?>><?php echo $get_distance[$i]." minutes"; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="title_text">Other</div>
		<div class="frm_filter">
			<table class="content_other">
				<tr>
					<td><input type="checkbox" class="othercheck" name="othercheck[]" value="separate_toilet" id="separate_toilet" <?php if(isset($othercheck) && in_array('separate_toilet', $othercheck)){ echo "checked";} ?> ><label for="separate_toilet">Separate Toilet</label></td>
					<td><input type="checkbox" class="othercheck" name="othercheck[]" value="flooring" id="flooring" <?php if(isset($othercheck) && in_array('flooring', $othercheck)){ echo "checked";} ?> ><label for="flooring">Flooring</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="othercheck" name="othercheck[]" value="elevator" id="elevator" <?php if(isset($othercheck) && in_array('elevator', $othercheck)){ echo "checked";} ?> ><label for="elevator">Elevator</label></td>
					<td><input type="checkbox" class="othercheck" name="othercheck[]" value="air_conditioner" id="air_conditioner" <?php if(isset($othercheck) && in_array('air_conditioner', $othercheck)){ echo "checked";} ?> ><label for="air_conditioner">Air Conditioner</label></td>
				</tr>
				<tr>
					<td><input type="checkbox" class="othercheck" name="othercheck[]" value="parking" id="parking" <?php if(isset($othercheck) && in_array('parking', $othercheck)){ echo "checked";} ?> ><label for="parking">Parking</label></td>
					<td><input type="checkbox" class="othercheck" name="othercheck[]" value="infrared_heater" id="infrared_heater" <?php if(isset($othercheck) && in_array('infrared_heater', $othercheck)){ echo "checked";} ?> ><label for="infrared_heater">Infrared Heater</label></td>
				</tr>
			</table>
		</div>
		<div class="frm_filter center_submit">
			<input type="button" value="GO" name="btn_go" class="btn_go" id="btn_go">
			<input type="button" value="Reset" class="btn_reset" id="btn_reset">
		</div>
	</form>
	</div>
	<div class="content_data_properties">
		<?php if($get_lists && count($get_lists) >0 ){
			foreach ($get_lists as $row) {
		?>
		<div class="data_properties">
			<div class="properties_image">
				<?php if($row->images_path != ""){ ?>
					<a href="/detail/<?php echo $row->id; ?>"><img src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>" alt="Image of <?php echo $row->building_name; ?>"/></a>
				<?php }else{ ?>
					<a href="/detail/<?php echo $row->id; ?>"><img src="/assets/img/uploads/default_image_properties.jpg" alt="No Image"/></a>
				<?php } ?>
			</div>
			<div class="properties_img_status">
				<img src="/assets/img/mobile/icon/status_buy.png" alt="Status" />
			</div>
			<div class="properties_description">
				<div class="properties_name properties_font">
					<a href="/detail/<?php echo $row->id; ?>"><?php echo $row->building_name; ?></a>
				</div>
				<div class="properties_location properties_font">
					<td><?php echo $row->perfecture_name." ".$row->city_name; ?></td>
				</div>
				<div class="properties_price properties_font">
					<?php if($row->price > 0){ echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price); }else{ echo "Ask";} ?>
				</div>
				<div class="properties_view properties_font">
					<span><?php echo number_format($row->views); ?></span> Views
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<?php if($is_logged_in){
			if($me->user_group != 3){/* Not Show Favorite Link */}
			else{ if(in_array($row->id, $fav_pro_id)){
		?>
			<a home_id='<?php echo $row->id; ?>' class='link_fav update_fav link_update<?php echo "_".$row->id; ?>' onclick="delete_fav('<?php echo $row->id; ?>')"><i class="fa fa-heart heart_icon update_fav<?php echo "_".$row->id; ?>" aria-hidden="true"></i></a>
		<?php }else{ ?>
			<a home_id="<?php echo $row->id; ?>" class="link_fav add_fav link_add<?php echo "_".$row->id; ?>" onclick="add_fav('<?php echo $row->id; ?>')"><i class="fa fa-heart-o heart_icon add_fav<?php echo "_".$row->id; ?>" aria-hidden="true"></i></a>
		<?php }}}else{ ?>
			<a href="/login" class="link_fav"><i class="fa fa-heart-o heart_icon" aria-hidden="true"></i></a>
		<?php } ?>
		<?php }}else{ ?>
			<div class="not_found">
				<h2 style="margin-bottom: 10px;">Page Not Found</h2>
				<p>The page you're looking for cannot be found.</p>
			</div>
		<?php } ?>

		<div class="pagination">
			<?php echo $pagination; ?>
		</div>
	</div>
</div>