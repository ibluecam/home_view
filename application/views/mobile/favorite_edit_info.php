<div class="content_favorite">
	<?php $this->load->view('/template_mobile/sidebar');?>
	<div class="title_info">
		<span>EDIT INFO</span>
	</div>
	<?php  if(count($user_info) > 0){ ?>
	<div class="content_form">
		<form id="frm_edit_user" action="/favorite/info/edit/<?php echo $user_id; ?>" method="POST">
			<table class="table_info">
				<tr>
					<td class="td_left">First Name</td>
					<td class="td_right">
						<input type="text" value="<?php echo $user_info->first_name; ?>" class="text_filed" name="firstname">
						<div class="error" id="firstname_error"></div>
					</td>
				</tr>
				<tr>
					<td class="td_left">Last Name</td>
					<td class="td_right">
						<input type="text" value="<?php echo $user_info->last_name; ?>" class="text_filed" name="lastname">
						<div class="error" id="lastname_error"></div>
					</td>
				</tr>
				<tr>
					<td class="td_left">Email</td>
					<td class="td_right">
						<input type="text" id="email" value="<?php echo $user_info->email; ?>" class="text_filed" name="email">
						<div class="error" id="email_error"></div>
						<input type="hidden" id="user_id" value="<?php echo  $user_id ?>" name="user_id">
					</td>
				</tr>
				<tr>
					<td class="td_left">Passwords</td>
					<td class="td_right">
						<a href="#pop_password" id="popup_password" class="change_password">Change Password</a>
					</td>
				</tr>
				<tr class="tr_center">
					<td colspan="2">
						<input type="submit" name="btn_edit" class="button_edit_info" value="OK">
					</td>
				</tr>
			</table>
		</form>		
	</div>
	<div id="pop_password" class="content_change_pwd">
		<input type="hidden" value="<?php echo $user_id; ?>" class="hide_user_id">
		<table>
			<tr>
				<td class="lb_pwd">Password</td>
				<td><input type="password" name="password" class="text_filed pwd" id="password"></td>
			</tr>
			<tr>
				<td class="lb_pwd">Confirm Password</td>
				<td><input type="password" name="confirm_password" class="text_filed pwd" id="conf_pwd" ></td>
			</tr>
			<tr class="tr_left">
				<td colspan="2">
					<input type="submit" name="btn_change_pwd" value="OK" class="btn_pwd" id="btn_ok_pwd">
					<input type="button" name="btn_close" value="Close" class="btn_pwd" id="close_pwd">
				</td>
			</tr>
		</table>
		<div class="alert-message"></div>
	</div>
	<?php } ?>
</div>