<div class="content_left">
	<div class="nav_list">
		<div class="title">
			<span>MENU</span>
		</div>
		<?php if($get_user_group_name == "agent"){ ?>
		<div class="nav_agent">
			<a href="/admin/properties/add">
				<i class="fa fa-plus-circle custom_icon" aria-hidden="true"></i>
				<span>Post</span>
			</a>
		</div>
		<div class="nav_agent">
			<a href="/admin/properties">
				<i class="fa fa-home custom_icon" aria-hidden="true"></i>
				<span>List Properties</span>
			</a>
		</div>

		<div class="nav_agent">
			<a href="/<?php echo $chk_group_user; ?><?php echo $me->user_id;  ?>">
				<i class="fa fa-info custom_icon" aria-hidden="true"></i>
				<span>Info</span>
			</a>
		</div>
		<div class="agent_total">
			<span>YOUR TOTAL PROPERTIES</span>
		</div>
		<p><?php echo $count_properties; ?></p>
		<?php }else if($get_user_group_name == "admin"){ ?>

		<div class="nav_agent">
			<a href="/admin/agent/add">
				<i class="fa fa-plus-circle custom_icon" aria-hidden="true"></i>
				<span>Add Agent</span>
			</a>
		</div>
		<div class="nav_agent">
			<a href="/admin/agent">
				<i class="fa fa-users custom_icon" aria-hidden="true"></i>
				<span>List Agent</span>
			</a>
		</div>
		<!-- <div class="nav_agent">
			<a href="/admin/blog/add">
				<i class="fa fa-book custom_icon" aria-hidden="true"></i>
				<span>Add Blog</span>
			</a>
		</div>
		<div class="nav_agent">
			<a href="/admin/blog">
				<i class="fa fa-list-alt custom_icon" aria-hidden="true"></i>
				<span>List Blog</span>
			</a>
		</div> -->
		<div class="nav_agent">
			<a href="/<?php echo $chk_group_user; ?><?php echo $me->user_id;  ?>">
				<i class="fa fa-info custom_icon" aria-hidden="true"></i>
				<span>My Info</span>
			</a>
		</div>
		<div class="nav_agent">
			<a href="#">
				<i class="fa fa-cog custom_icon" aria-hidden="true"></i>
				<span>Setting</span>
			</a>
		</div>
		<div class="agent_total">
			<span>YOUR TOTAL AGENT</span>
		</div>
		<p><?php echo $count_agent; ?></p>
		<?php }else{ ?>
		<div class="nav_agent">
			<a href="/favorite">
				<i class="fa fa-heart custom_icon" aria-hidden="true"></i>
				<span>Favorite</span>
			</a>
		</div>

		<div class="nav_agent">
			<a href="/<?php echo $chk_group_user; ?><?php echo $me->user_id;  ?>">
				<i class="fa fa-info custom_icon" aria-hidden="true"></i>
				<span>Info</span>
			</a>
		</div>

		<div class="agent_total">
			<span>YOUR TOTAL FAVORITE</span>
		</div>
		<p><?php echo $count_favorite; ?></p>

		<?php } ?>

		
	</div>
</div>