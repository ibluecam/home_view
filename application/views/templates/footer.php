<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



			</div>

			<!-- End Block Body -->



			<!-- Block Footer -->

		

			<div class="content_footer">

				<div class="content_contact">

					<div class="page_size contac_info">

						<div class="contact">

							<div class="contact_left">

								<span class="phone">

									<img src="/assets/img/icon/phone_b.png" alt="Phone">

									<span>+81 (0)3 6277 7223</span>

								</span>

								<span class="email">

									<img src="/assets/img/icon/email.png" alt="Email">

									<span>INFO@HOMEVIEW.JP</span>

								</span>

							</div>

							<div class="contact_right">

								<span class="facebook">

									<img src="/assets/img/icon/face.png" alt="Skype">

									<span><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" target="blank">HOME VIEW</a></span>

								</span>

								<span class="skype">

									<img src="/assets/img/icon/skype.png" alt="Skype">

									<span><a href="skype://live:shimanekennow?call">HONE VIEW</a></span>

								</span>

								<span class="viber">

									<img src="/assets/img/icon/viber.png" alt="Viber">

									<span><a href="viber://+8108094462100">+81 080 9446 2100</a></span>

								</span>

								<span class="line">

									<img src="/assets/img/icon/line.png" alt="Line">

									<span><a line-id="home-view">home-view</a></span>

								</span>

							</div>

							<div class="clear"></div>

						</div>

						<div class="text_contact">

							CONTACT INFORMATION

						</div>

						<div class="txet_des_contact">

							Get the lastest news and properties in your inbox

						</div>

						<div class="form_email">

							<form>

								<input type="text" class="con_email" name="email" placeholder="Your Email">

								<br/>

								<input type="submit" name="subscribe" value="SUBSCRIBE" class="btn_sub">

							</form>

						</div>

					</div>

				</div>

				<div class="content_footer_bottom">

					<div class="page_size">

						<div class="footer_left">&copy;2016 Copyright Home View. All Rights Reserved. </div>

						<div class="footer_right">Designed by Softbloom Co., LTD.</div>

						<div class="clear"></div>

					</div>

				</div>

			</div>

			<!-- End Block Footer -->

		</div>

		<!-- Import JS -->

		<script type="text/javascript" src="/assets/js/jquery.js"></script>

		<script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>

		<script type="text/javascript" src="/assets/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>

		<?php if(count($array_js)) { foreach ($array_js as $js) { ?>

			<script type="text/javascript" src="/assets/js/<?php echo $js ?>.js?v=<?php echo time(); ?>"></script>

		<?php }} ?>

		<?php if( isset($map_js) && $map_js == true){ ?>

			<script type="text/javascript" async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlbmxCW7W-62OvGVyyzQ-2_BJXs1KLmPk&callback=initialize"> </script>

		<?php } ?>
		<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-88691267-1', 'auto');
			  ga('send', 'pageview');
		</script>
	</body>

</html>