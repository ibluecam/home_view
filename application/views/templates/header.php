<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
		
	<title><?php echo $page_title ?></title>
	
	<link rel="shortcut icon" type="image/png" href="/assets/img/icon/favo16.png"/>

	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<link href="/assets/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="/assets/fancyBox/source/jquery.fancybox.css?v=2.1.5">
	<link rel="stylesheet" type="text/css" href="/assets/css/jquery.mmenu.all.css"/>
	<link href="/assets/css/global.css" rel="stylesheet" type="text/css"/>
	<?php if(count($array_css) > 0 ) { foreach ($array_css as $css) {  ?>
		<link href="/assets/css/<?php echo $css; ?>.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
	<?php }} ?>

</head>
<body>

<div class="wrapper">

	<!-- Block Header -->

	<div class="contain_header">
		<div class="content_logo page_size">
			<div class="logo"><a href="/"><img src="/assets/img/icon/home_logo.svg" alt="Home View Logo"></a></div>
		</div>
		<div class="content_menu page_size">
			<div class="left_bg"></div>
			<div class="nav_menu">
				<div class="menu">
					<ul>
						<li><a href="/" class="list_menu">HOME</a></li>
						<li><a href="/lists/buy" class="list_menu">BUY</a></li>
						<li><a href="/lists/rent" class="list_menu">RENT</a></li>
						<!-- <li><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" class="list_menu" target="blank">FACE BOOK</a></li> -->
						<li><a href="/videos" class="list_menu">360<sup>&deg;</sup> IMAGES</a></li>
					</ul>
				</div>
				<div class="login_register">
					<ul>
						<?php if(!$is_logged_in){ ?>
						<li><a href="/login" class="list_menu">LOGIN</a></li>
						<li>|</li>
						<li><a href="/register" class="list_menu">REGISTER</a></li>
						<?php }else{ ?>
						<li>
							<a href="/<?php echo $chk_group_user; ?><?php echo $me->user_id;  ?>" class="list_menu account_name">
								<i class="fa fa-user account_icon" aria-hidden="true"></i>
							    Hello! <?php echo $chk_user_group_name;  ?>
							</a>
						</li>
						<li><a href="/login/logout" class="list_menu logout">Log out <i class="fa fa-power-off" aria-hidden="true"></i></a></li>
						<?php } ?>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<div class="right_bg"></div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="header_title">
		<div class="header_title_text page_size">
			<i class="fa <?php echo $header_icon; ?>" aria-hidden="true"></i> <?php echo $header_page_title; ?>
		</div>
	</div>
	<!-- <div class="clear"></div> -->

	<!-- End Block Header -->

	<!-- Block Body -->
	<div class="container_body page_size">
	<?php if(isset($success)){ ?>
		<?php echo $success; ?>
	<?php } ?>

	