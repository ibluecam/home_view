      <div class="contain_footer">
      		<div class="block_contact">
      			<p class="contact">
      				<i class="fa fa-phone" aria-hidden="true"></i>
      				+82-32-264-5500
      			</p>
      			<p class="contact">
      				<i class="fa fa-envelope-o" aria-hidden="true"></i>
      				INFO@HOMEVIEW.JP
      			</p>
      		</div>
      </div>
      <div class="block_copyright">
			<p>©2016 Copyright Home View. All Rights Reserved.</p>
			<p>Designed by Softbloom Co., LTD.</p>
      </div>

      </div>
		<script type="text/javascript" src="/assets/js/jquery.js"></script>
            <script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="/assets/js/mobile/global.js?v=<?php echo time(); ?>"></script>
            <script type="text/javascript" src="/assets/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
            <script type="text/javascript" src="/assets/blueimpGallery/js/blueimp-gallery.min.js?v=<?php echo time(); ?>"></script>
		<?php if(count($array_js)) { foreach ($array_js as $js) { ?>
			<script type="text/javascript" src="/assets/js/<?php echo $js ?>.js?v=<?php echo time(); ?>"></script>
		<?php }} ?>
            <?php if( isset($map_js) && $map_js == true){ ?>
                  <script type="text/javascript" async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlbmxCW7W-62OvGVyyzQ-2_BJXs1KLmPk&callback=initialize"> </script>
            <?php } ?>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-88691267-1', 'auto');
              ga('send', 'pageview');
            </script>
	</body>
</html>