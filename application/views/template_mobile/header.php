<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta http-equiv="Expires" content="-1" />
	    <meta http-equiv="Pragma" content="no-cache" />
	    <meta http-equiv="Cache-Control" content="no-cache" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
			
		<title>Home View</title>
		
		<link rel="shortcut icon" type="image/png" href="/assets/img/icon/favo16.png"/>

		<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
		<link href="/assets/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="/assets/css/mobile/global.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="/assets/fancyBox/source/jquery.fancybox.css?v=2.1.5">
		<link rel="stylesheet" href="/assets/blueimpGallery/css/blueimp-gallery.min.css">
		<?php if(count($array_css) > 0 ) { foreach ($array_css as $css) {  ?>
		  <link href="/assets/css/<?php echo $css; ?>.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
	    <?php }} ?>
	</head>
	<body>
		<div class="wrapper" id="wrapper">
			<!-- Block Header -->
			<div class="content_header">
				<div class="content_logo">
					<a href="/"><img src="/assets/img/mobile/icon/home_logo.svg" alt="Home View Logo"></a>
				</div>
				<div class="content_menu">
					<div class="menu_icon">
						<img src="/assets/img/mobile/icon/menu_icoin_1.png" alt="MENU">
						<img src="/assets/img/mobile/icon/menu_icon_change_1.png" style="display:none;" alt="MENU">
					</div>
					<div class="login_register">
						<?php if(!$is_logged_in){ ?>
						<a href="/login">LOGIN</a>
						<span id="ruler_heri">|</span>
						<a href="/register">REGISTER</a>
						<?php }else{ ?>
							<a href="/<?php echo $chk_group_user; ?><?php echo $me->user_id;  ?>" class="list_menu account_name">
								<i class="fa fa-user account_icon" aria-hidden="true"></i>
							   Hello! <?php echo $chk_user_group_name;  ?>
							</a>
							<span id="ruler_heri">|</span>
							<a href="/login/logout" class="list_menu logout">Log out <i class="fa fa-power-off" aria-hidden="true"></i></a>
						<?php } ?>
					</div>
					<div class="clear"></div>
				</div>
				<div class="content_slide_menu">
					<div class="slide_menu">
						<div class="wrap_slide_menu">
							<div><a href="/">HOME</a></div>
							<div><a href="/lists/buy">BUY</a></div>
							<div><a href="/lists/rent">RENT</a></div>
							<div><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" target="blank">FACE BOOK</a></div>
							<div><a href="/videos">360<sup>&deg;</sup> Images</a></div>
						</div>
					</div>
				</div>
				<div class="banner_header">
					<div class="title_page"><?php echo $header_page_title; ?></div>
				</div>
			</div>