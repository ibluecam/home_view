

	<div id="wrap_content">
		<?php $this->load->view('/templates/sidebar_admin');?>
		<div class="content">
			<div class="tool_search_select">
				<div class="text_myinfo">
					MY INFO
				</div>
			</div>	

			<div class="show_content_info">
				<div class="content_info">

					<table class="table_info">
						<tr>
							<td>Logo</td>
							<td class="logo_info">
								<img width="135" height="101" src="/assets/img/uploads/company/resize/<?php echo $user_info->logo; ?>" alt="">
								<div class="content_link_edit"><a href="/admin/info/edit/<?php echo $user_info->user_id; ?>">Edit Info</a></div>
							</td>
						</tr>
						<tr>
							<td>Company Name</td>
							<td><?php echo $user_info->company_name; ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?php echo $user_info->email; ?></td>
						</tr>
						<tr>
							<td>Passwords</td>
							<td>****************</td>
						</tr>
						<tr>
							<td>Social Media</td>
							<td>
								<div class="social_media">
									<span class="skype">
										<img src="/assets/img/icon/skype.png" alt="Phone">
										<span><a href="/"><?php echo $user_info->skype_id; ?></a></span>
									</span>
									<span class="viber">
										<img src="/assets/img/icon/viber.png" alt="Email">
										<span><a href="/"><?php echo $user_info->viber_id; ?></a></span>
									</span>
									<span class="line">
										<img src="/assets/img/icon/line.png" alt="Email">
										<span><a href="/"><?php echo $user_info->line_id; ?></a></span>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>Phone Number</td>
							<td><?php echo $user_info->phone1; ?> | <?php echo $user_info->phone2; ?></td>
						</tr>
						<tr>
							<td>Office Location</td>
							<td><?php echo $user_info->office_location; ?></td>
						</tr>
						<tr>
							<td class="text_des">Description</td>
							<td class="detail_des">
								<?php echo $user_info->description; ?>
							</td>
						</tr>
					</table>		
				</div>

			</div>


		</div><div class="clear"></div>
	</div>
