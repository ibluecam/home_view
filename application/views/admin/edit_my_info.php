

	<div class="container_agent" id="wrap_content">
		<?php $this->load->view('/templates/sidebar_admin');?>
		<div class="content">
			<div class="tool_search_select">
				<div class="text_myinfo">
					EDIT INFO
				</div>
			</div>	

			<div class="show_content_info">
				<div class="content_info">
				<?php echo form_open_multipart($action_form,array('class' => 'form_add_agent','id'=>'frm_edit_info')); ?>
					<table class="table_info">
						<tr>
							<td>Logo</td>
							<td class="logo_info">
								<input type="file" name="file_image" class="upload_logo" id="upload_logo">
								<input type="button" class="choose_image" name="choose" value="choose logo upload">
								<input type="hidden" class="get_image_edit" id="get_image_edit" value="<?php echo $id."#_#".$logo; ?>">
							</td>
						</tr>
						<tr>
							<td>Company Name<span class="require">*</span></td>
							<td>
								<input type="text" class="company text-field" name="company_name" placeholder="Company name" value="<?php echo $company_name; ?>">			
								<?php echo form_error('company_name', '<div class="error">', '</div>'); ?>
								<div class="error" id="company_error"></div>
								<input type="hidden" name="user_id" id="user_id" value="<?php echo $id; ?>"/>
							</td>
						</tr>
						<tr>
							<td>Email<span class="require">*</span></td>
							<td>
								<input type="email" class="email text-field" id="email" name="email" placeholder="Email" value="<?php echo $email; ?>">
								<?php echo form_error('email', '<div class="error">', '</div>'); ?>
								<div class="error" id="email_error"></div>
							</td>
						</tr>
						<tr>
							<td>Password</td>
							<td>
								<input type="button" id="change_password" class="change_password" value="Change Password">
								<!-- <input type="text" class="password text-field" name="password" value="<?php echo $password; ?>">
								<?php echo form_error('password', '<div class="error">', '</div>'); ?> -->
							</td>
						</tr>
						<!-- <tr>
							<td>Comfirm Password<span class="require">*</span></td>
							<td>
								<input type="text" class="confirm_password text-field" name="confirm_password" value="<?php echo $confirm_password; ?>">
								<?php echo form_error('confirm_password', '<div class="error">', '</div>'); ?>
							</td>
						</tr> -->
						<tr>
							<td>Social Media</td>
							<td>
								<div class="social_media">
									<span class="add_sociel">
										<!-- <img src="/assets/img/icon/skype.png" alt="Phone"> -->
										
										<input type="text" class="skype_id text-field" name="skype_id" placeholder="Skype id" value="<?php echo $skype_id; ?>">
									</span>
									<span class="add_sociel">
										<!-- <img src="/assets/img/icon/viber.png" alt="Email"> -->
										<input type="text" class="viber_id text-field" name="viber_id" placeholder="Viber id" value="<?php echo $viber_id; ?>">
									</span>
									<span class="add_sociel">
										<!-- <img src="/assets/img/icon/line.png" alt="Email"> -->
										<input type="text" class="line_id text-field" name="line_id" placeholder="Line id" value="<?php echo $line_id; ?>">
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>Phone Number</td>
							<td><input type="text" class="phone1 text-field" name="phone1" placeholder="Phone number 1" value="<?php echo $phone1; ?>"> 
							<input type="text" class="phone2 text-field" name="phone2"  placeholder="Phone number 2" value="<?php echo $phone2; ?>"></td>
						</tr>
						<tr>
							<td>Office Location</td>
							<td>
								<input type="text" class="location text-field" name="office_location" placeholder="Office location" value="<?php echo $office_location; ?>">
								
							</td>
						</tr>
						<tr>
							<td class="text_des">Description</td>
							<td class="detail_des">
								<textarea cols="58" rows="5" name="description" class="description"><?php echo $description; ?></textarea>
								
							</td>
						</tr>
						<tr>
							<td class="text_des">
								
							</td>
							<td class="detail_des">
								<input type="hidden" name="id" value="<?php echo $id; ?>">
								<input type="submit" name="save_update" class="add_agent" id="add_agent" value="OK">

							</td>
						</tr>
					</table>	
					</form>
				</div>

			</div>


		</div><div class="clear"></div>
	</div>



<div id="form_update_password" style="display:none;">
	<form id="form_change_password">
		<table class="table_change_password">
			<tr>
				<td class="label_password">Password</td>
				<td><input type="password" class="password text-field" name="password" value=""></td>
			</tr>
			<tr>
				<td class="label_confirm_password">Conform Password</td>
				<td><input type="password" class="password text-field" name="confirm_password" value=""></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="btn_change_password" class="btn_change_password" id="btn_change_password" value="OK">
					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<input type="button" class="btn_cancel" id="btn_cancel" value="Close">
				</td>
			</tr>
		</table>
	</form>
	<div class="alert_message"></div>
</div>