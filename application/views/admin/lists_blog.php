<div class="container_blog">

<?php $this->load->view('/templates/sidebar_admin');?>

	<div class="content_right">
		<table class="table_data" cellpadding=5 cellspacing=0>
			<tr class="contain_th">
				<th class="th_name">LIST BLOG TITLES</th>
				<th class="th_upload">CATEGORY</th>
				<th class="th_date">ADDED DATE</th>
				<th class="th_action"></th>
			</tr>
			<?php 
				if(count($select_blog_admins)){ 
					foreach($select_blog_admins as $select_blog_admin){
			?>
			<tr class="tr_data">
				<td class="td_name"><a class="blog_link" target="_blank" href="/blog-detail/<?php echo $select_blog_admin->id; ?>"><?php echo $select_blog_admin->title; ?></a></td>
				<td class="td_upload"><?php echo $select_blog_admin->category; ?></td>
				<td class="td_date"><?php echo $select_blog_admin->created_dt; ?></td>
				<td class="td_action">
					<button class="btn_action" id="<?php echo $select_blog_admin->id; ?>">ACTION<img class="icon" src="/assets/img/icon/wirte_arrow_down.png"/></button>
					<ul class="ul_data show_<?php echo $select_blog_admin->id; ?>">
						<li class="li_data"><a href="/admin/blog/edit/<?php echo $select_blog_admin->id; ?>">Edit</a></li>
						<li class="li_data"><a class="delete_id" img_id="<?php echo $select_blog_admin->feature_image; ?>" article_id="<?php echo $select_blog_admin->id; ?>">Delete</a></li>
					</ul>
				</td>
			</tr>
			<?php } }else{ ?>
			<tr class="tr_data">
				<td colspan="4" class="td_name data_agent_empty">
					<span class="text_data_agent_empty">empty data</span>
				</td>
			</tr> 
			<?php } ?>
		</table>
		<div class="pagination">
			<?php echo $pagination; ?>
		</div>
		<!-- popup confrim message -->
	   <div id="popup_delete">
	   </div>
	   <!-- end popup confrim message -->
	</div>
	<div class="clear"></div>
</div>
