
	<div id="wrap_content">
		<form method="get">
		<?php $this->load->view('/templates/sidebar_admin');?>
		<div class="content">
			<!-- <?php 
			$success=$this->session->flashdata('success');
			if(isset($success)){ ?>
				 <?php echo $success; ?>
			<?php  } ?> -->
			<div class="tool_search_select">
				<div class="select_delete">
					<div class="select_all_tools">
						<input type="checkbox" id="selectall" name="select" /><label for="selectall" class="select_all">Select All</label>	
					</div>
					<div class="select_number">
						<span class="count_chk">0</span> <span> Files Selected</span>	
					</div>
					<div class="button_delete">
						<a href="#popup_delete" id="pop_delete">
						<button type="button" value="1" class="delete" id="btn_delete" name="delete">
							<i class="fa fa-trash-o custom_icon_delete" aria-hidden="true"></i> Delete</button>
						</a>
					</div><div class="clear"></div>
					<input class="btnsubmit" type="submit" value="DELETE"  name ="btndelete" style="display:none"/>
				</div>

				<div class="select_search">
					<div class="block_search">
						<input type="text" name="keyword" class="keyword" value="<?php echo (isset($_GET['keyword'])) ? $_GET['keyword'] : ""; ?>" placeholder="Keyword">
						<button class="button_search" value="1" name="submit">Search</button>
					</div>
				</div><div class="clear"></div>
			</div>	

			<div class="conetent_items_lists">

			<?php 
				if(count($admin_get_properties_lists) > 0){
					foreach($admin_get_properties_lists as $admin_get_properties_list){ 
						$image_path = "default_image_properties.jpg";
						if(!empty($admin_get_properties_list->images_path)){
							$image_path = $admin_get_properties_list->images_path;
						}
			?>
				<div class="wrap_data">
					<div class="content_data">

						<input class="select_items check_one_<?php echo $admin_get_properties_list->properties_id; ?>" 
				        value="<?php echo  $admin_get_properties_list->properties_id;  ?>"
				        attr_id="<?php echo $admin_get_properties_list->properties_id; ?>" type="checkbox" name="chk_properties[]"/>
						<div class="data_image">
							<a target="_blank" href="/detail/<?php echo $admin_get_properties_list->properties_id; ?>"><img width="165" height="124" title="<?php echo $admin_get_properties_list->building_name; ?>" alt="<?php echo $admin_get_properties_list->building_name; ?>" src="/assets/img/uploads/properties/thumb/<?php echo $image_path; ?>"></a>
						</div>
						<div class="data_currency">
							<?php if($admin_get_properties_list->status == 1){ ?>
								&yen;
							<?php } ?>
							<?php if($admin_get_properties_list->status == 2){ ?>
								R
							<?php } ?>
							<?php if($admin_get_properties_list->status == 3){ ?>
								S
							<?php } ?>
						</div>
						<div class="data_title">
							<a target="_blank" title="<?php echo $admin_get_properties_list->building_name; ?>" href="/detail/<?php echo $admin_get_properties_list->properties_id; ?>"><?php echo $admin_get_properties_list->building_name; ?></a>
						</div>
						<div class="items_tools">
							<a class="tools_edit" title="Edit Properties" href="/admin/properties/edit/<?php echo $admin_get_properties_list->properties_id; ?>"><i class="fa fa-pencil-square-o custom_icon_tools custom_icon_edit" aria-hidden="true"></i> EDIT</a>
							<a href="#popup_delete" id="pop_delete">
								<a class="tools_delete delete_id_<?php echo $admin_get_properties_list->properties_id; ?>" attrid="<?php echo $admin_get_properties_list->properties_id; ?>"  title="Delete Properties">DELETE <i class="fa fa-trash-o custom_icon_tools custom_icon_delete" aria-hidden="true"></i></a>
							</a>
						</div>
					</div>
					<div class="ruler_bottom"></div>
				</div>
			<?php } }else{ ?>
				<div class="not_found_data">Data is not found</div>
			<?php } ?>

				<div class="clear"></div>

			</div>

			<div class="pagination">
				<?php echo $pagination; ?>
			</div>
			 <!-- popup confrim message -->
		   <div id="popup_delete">
		   		<div class="popup_header"></div>
		   		<div class="popup_footer">
		   			<input class="popup_btn popup_submit" type="button" value="DELETE"  name ="btndelete"/>
		   			<input class="popup_btn popup_cancel"  type="submit" value="CANCEL"  name ="btncancel"/>
		   		</div>
		   </div>
		   <!-- end popup confrim message -->
		</div><div class="clear"></div>

		</form>
	</div>
