<div class="container">

<?php $this->load->view('/templates/sidebar_admin');?>
	<div class="content_right">
		<div class="blog_head_content">
			<h3>ADD BLOG</h3>
		</div>
		<?php echo form_open_multipart($action_form,array('id' => 'add_blog')); ?>
			<div class="blog_body_content">
				<div class="blog_content">
					<div class="title_cate_source">
						<div class="d_field_text">
							<label class="lb_title">Titles<span class="require_field">*</span></label>
							<input type="text" class="field_text title" name="title" placeholder="Home Online Co.,LTD" value="<?php echo set_value('title'); ?>">
							<div class="error error_title" id="error_title"></div>
							<?php echo form_error('title', '<div class="error error_title">', '</div>'); ?>
						</div>
						<div class="d_field_dropdown">
							<div class="part_category">
								<label class="lb_category">Category<span class="require_field">*</span></label> 
								<select class="category" name="category">
									<option value="living">Living</option>
									<option value="news">News</option>
									<option value="buy">Buy</option>
									<option value="rent">Rent</option>	
								</select>
								<?php echo form_error('category', '<div class="error error_category">', '</div>'); ?>
							</div>
							<div class="part_Source">
								<label class="lb_Source">Source<span class="require_field">*</span></label> <input type="text" class="field_text source" name="source" value="<?php echo set_value('source'); ?>">
								<div class="error error_source" id="error_source"></div>
								<?php echo form_error('source', '<div class="error error_source">', '</div>'); ?>
							</div>
							
						</div>

					</div>
					<div class="image_feature">
						<label class="lb_feature_image">Feature Image</label>
						<input type="file" name="file_image" class="upload_logo" id="upload_logo">
						
					</div><div class="clear"></div>
					<div class="text_body_content">
						<label class="lb_article">Article</label>
						<div class="d_image_change"><input type="button" class="choose_image" name="choose" value="Change"></div>
						<div class="clear"></div>
						<textarea class="ckeditor" name="body"></textarea>
					</div>	
				</div>
			</div>
			<div class="d_button">
				<input type="submit" class="save_blog" id="save_blog" name="save" value="OK">
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>