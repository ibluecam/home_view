<div class="container_agent">
	<?php $this->load->view('/templates/sidebar_admin');?>
	<div class="content_right">
		<table class="table_data" cellpadding=5 cellspacing=0>
			<tr class="contain_th">
				<th class="th_name">AGENT'S NAME</th>
				<th class="th_upload">HOME UPLOADS</th>
				<th class="th_date">REGISTER DATE</th>
				<th class="th_action"></th>
			</tr>
			<?php 
				if(count($select_agent_lists) > 0){
					$company_logo = "/assets/img/uploads/company/resize/company_logo_default.jpg";
					foreach($select_agent_lists as $select_agent_list){
						if(!empty($select_agent_list->logo)){
							$company_logo = "/assets/img/uploads/company/resize/".$select_agent_list->logo;
						}
			?>
			<tr class="tr_data">
				<td class="td_name"><a target="_blank" class="agent_link" href="/agent/<?php echo  $select_agent_list->user_id; ?>"><img class="icon_comapany" src="<?php echo $company_logo; ?>"/><?php echo $select_agent_list->company_name; ?></a></td>
				<td class="td_upload">(<span class="span"><?php echo $select_agent_list->count_properties; ?></span>) Homes</td>
				<td class="td_date"><?php echo $select_agent_list->created_dt; ?></td>
				<td class="td_action">
					<button class="btn_action" id="<?php echo $select_agent_list->user_id; ?>">ACTION<img class="icon" src="/assets/img/icon/wirte_arrow_down.png"/></button>
					<ul class="ul_data show_<?php echo $select_agent_list->user_id; ?>">
						<li class="li_data"><a href="/admin/agent/edit/<?php echo $select_agent_list->user_id; ?>">Edit</a></li>
						<li class="li_data"><a class="delete_id" logo="<?php echo $select_agent_list->logo; ?>" user_id="<?php echo $select_agent_list->user_id; ?>">Delete</a></li>
					</ul>
				</td>
			</tr>
			<?php } }else{ ?>
			<tr class="tr_data">
				<td colspan="4" class="td_name data_agent_empty">
					<span class="text_data_agent_empty">empty data</span>
				</td>
			</tr>
			<?php } ?>
		</table>
	   <!-- popup confrim message -->
	   <div id="popup_delete">
	   </div>
	   <!-- end popup confrim message -->
	</div>
	<div class="clear"></div>
</div>
