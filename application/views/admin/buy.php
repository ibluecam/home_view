


			<table class="table_properties">
				<tr>
					<td class="td_label">Building Name <span class="required_field">*</span></td>
					<td class="tb_input">
						
						<input type="text" name="building_name" value="<?php echo $building_name; ?>" class="building_name text-field" required/>
						<?php echo form_error('building_name', '<div class="error">', '</div>'); ?>
						<div class="error" id="building_name_error"></div>
					</td>
					<td class="td_label">Floor</td>
					<td>
						<select name="floor" class="select-field floor" id="select-floor" placeholder="Select or type...">
							<option value="">Floor</option>
							<?php foreach($select_option_floor_tool_properties as $select_option_floor_tool_propertie){ ?>
							<option <?php echo ($floor == $select_option_floor_tool_propertie->floor ? "selected " : ""); ?> value="<?php echo $select_option_floor_tool_propertie->floor; ?>"><?php echo $select_option_floor_tool_propertie->floor; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('floor', '<div class="error">', '</div>'); ?>
						<div class="error" id="floor_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Unit Number <span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="text" name="unit_number" value="<?php echo $unit_number; ?>" class="unit_number text-field" required/>
						<?php echo form_error('unit_number', '<div class="error">', '</div>'); ?>
						<div class="error" id="unit_number_error"></div>
					</td>
					<td class="td_label">Size</td>
					<td>
						<input type="text" name="size" value="<?php echo $size; ?>" class="size text-field"/><label class="lbl_size">m&sup2;</label>
						<?php echo form_error('size', '<div class="error">', '</div>'); ?>
						<div class="error" id="size_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Type</td>
					<td class="tb_input">
						<input type="radio" name="type" value="house" <?php echo ($type == "house" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">House</label>
						<input type="radio" name="type" value="apartment" <?php echo ($type == "apartment" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">Apartment</label>
					</td>
					<td class="td_label">Land Rights</td>
					<td>
						<input type="text" value="<?php echo $land_right; ?>" name="land_right" class="land_rights text-field"/>
						<?php echo form_error('land_right', '<div class="error">', '</div>'); ?>
						<div class="error" id="land_right_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Maked<span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="radio"  name="maked" value="iron" <?php echo (($maked=="iron") ? "checked" : set_radio('maked', 'iron', FALSE)); ?> class="status radio-field"/><label for="iron" class="lbl_status">Iron</label>
						<input type="radio" name="maked" value="Wood" <?php echo (($maked=="Wood") ? "checked" : set_radio('maked', 'Wood', FALSE)); ?> class="status radio-field"/><label for="Wood" class="lbl_status">Wood</label>
						<input type="radio" name="maked" value="Other" <?php echo (($maked=="Other") ? "checked" : set_radio('maked', 'Other', FALSE)); ?> class="status radio-field"/><label for="Other" class="lbl_status">Other</label>
						<?php echo form_error('maked', '<div class="error">', '</div>'); ?>
						<div class="error" id="maked_error"></div>
					</td>
					<td class="td_label">Position<span class="required_field">*</span></td>
					<td>
						<input type="radio"  name="position" value="1f" <?php echo (($position=="1f") ? "checked" : set_radio('position', '1f', FALSE)); ?> class="status radio-field"/><label for="iron" class="lbl_status">1F</label>
						<input type="radio"  name="position" value="more2f" <?php echo (($position=="more2f") ? "checked" : set_radio('position', 'more2f', FALSE)); ?> class="status radio-field"/><label for="iron" class="lbl_status">More 2F</label>
						<input type="radio"  name="position" value="topfloor" <?php echo (($position=="topfloor") ? "checked" : set_radio('position', 'topfloor', FALSE)); ?> class="status radio-field"/><label for="iron" class="lbl_status">Top Floor</label>
						<input type="radio"  name="position" value="cornorroom" <?php echo (($position=="cornorroom") ? "checked" : set_radio('position', 'cornorroom', FALSE)); ?> class="status radio-field"/><label for="iron" class="lbl_status">Cornor Room</label>
						<?php echo form_error('position', '<div class="error">', '</div>'); ?>
						<div class="error" id="position_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Location <span class="required_field">*</span></td>
					<td class="tb_input">

						<select id="location_perfecture" name="location_perfecture" class="select-field perfecture">
							<option value="">Perfecture</option>
							<?php foreach($select_perfectures as $select_perfecture){ ?>
								<option <?php echo ($location_perfecture == $select_perfecture->id ? "selected" : ""); ?> value="<?php echo $select_perfecture->id?>"><?php echo $select_perfecture->perfecture_name; ?></option>
							<?php } ?>
						</select>

						<select id="location_city" name="location_city" class="select-field city">
							<option value="">City</option>
							<?php foreach($select_perfectures_citys as $select_perfectures_city){ ?>
								<option <?php echo ($location_city == $select_perfectures_city->id ? "selected" : ""); ?> class="<?php echo $select_perfectures_city->perfecture_id_fk; ?>" value="<?php echo $select_perfectures_city->id?>"><?php echo $select_perfectures_city->city_name; ?></option>
							<?php } ?>
						</select>

						<?php echo form_error('location_perfecture', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_perfecture_error"></div>

						<?php echo form_error('location_city', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_city_error"></div>

					</td>
					<td class="td_label">Land Area</td>
					<td>
						<input type="text" name="land_area" value="<?php echo $land_area; ?>" class="land_area text-field"/>
						<?php echo form_error('land_area', '<div class="error">', '</div>'); ?>
						<div class="error" id="land_area"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Gross Yield</td>
					<td class="tb_input">

						<select name="gross_yield" class="select-field gross_yield" id="select-gross_yield" placeholder="Select or type...">
							<option value=""></option>
							<?php foreach($select_option_gross_yield_tool_properties as $select_option_gross_yield_tool_propertie){ ?>
								<option <?php echo ($gross_yield == $select_option_gross_yield_tool_propertie->gross_yield ? "selected" : ""); ?> value="<?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?>"><?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('gross_yield', '<div class="error">', '</div>'); ?>
						<div class="error" id="gross_yield_error"></div>
					</td>
					<td class="td_label">Occupancy</td>
					<td>
						<input type="text" name="occupancy" value="<?php echo $occupancy; ?>" class="occupancy text-field"/>
						<?php echo form_error('occupancy', '<div class="error">', '</div>'); ?>
						<div class="error" id="occupancy_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Nearest Station<span class="required_field">*</span</td>
					<td class="tb_input">
						<input type="text" name="nearest_station" value="<?php echo $nearest_station; ?>" class="buy_nearest_station text-field"/>
						<select class="text-field distance_to_statition" name="distance_to_station" placeholder="Distance to Station" >
							<option  value="">Distance to Station</option>
							<option <?php echo (($distance_to_station==3) ? "selected" : set_select('distance_to_station',"3", False)); ?>  value="3">Within 3 minutes</option>
							<option <?php echo (($distance_to_station==5) ? "selected"  : set_select('distance_to_station',"5", False)) ; ?>  value="5">Within 5 minutes</option>
							<option <?php echo (($distance_to_station==7) ? "selected"  : set_select('distance_to_station',"7", False)); ?>  value="7">Within 7 minutes</option>
							<option <?php echo (($distance_to_station==10) ? "selected" : set_select('distance_to_station',"10", False)); ?> value="10">Within 10 minutes</option>
							<option <?php echo (($distance_to_station==15) ? "selected" : set_select('distance_to_station',"15", False)); ?> value="15">Within 15 minutes</option>
							<option <?php echo (($distance_to_station==20) ? "selected" : set_select('distance_to_station',"20", False)); ?> value="20">Within 20 minutes</option>
						</select>
						<div class="error" id="distance_to_station_error"></div>
						<?php echo form_error('nearest_station', '<div class="error">', '</div>'); ?>
						<div class="error" id="nearest_station_error"></div>
					</td>
					<td class="td_label">Price <span class="required_field">*</span></td>
					<td>
						<input type="text" name="price" value="<?php echo $price; ?>" class="buy_price text-field" required/>
						<select name="currency" class="select-field currency">
							<option <?php echo set_select('currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('price', '<div class="error">', '</div>'); ?>
						<div class="error" id="price_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">
						Feature
					</td>
					<td colspan="3">
							<table class="feature">
							<tr>
								<?php 
									$i = 0;
									foreach($select_features as $select_feature){ 
										$checkeds="";
										if(count($arr_features) > 0){
											foreach($arr_features as $arr_feature){
												if($select_feature->feature_value == $arr_feature){
													$checkeds = "checked";
												} 
											}
										}
								?>
									<td>
										<input type="checkbox" name="feature[]" <?php echo set_checkbox('feature[]', $select_feature->feature_value, FALSE); ?> class="description_1" value="<?php echo $select_feature->feature_value; ?>" <?php echo $checkeds; ?>><label class="lbl_description"><?php echo $select_feature->feature_name; ?></label>
									</td>
								<?php 
										$i++;
										if($i%4 == 0){
											echo "</tr><tr>";
										}	
									} 
								?>
							</tr>
						</table>

					</td>
				</tr>

				<tr>
					<td class="td_label">
						Description
					</td>
					<td colspan="3">
						<textarea name="description" class="ckeditor" id="description_buy" rows="5" cols="96"><?php echo $description; ?></textarea>
					</td>
				</tr>

				<tr>
					<td class="td_label">
						Transportation
					</td>
					<td colspan="3">
						<textarea name="transportation" class="ckeditor" id="transportation_buy" rows="5" cols="96"><?php echo $transportation; ?></textarea>
					</td>
				</tr>

				<tr>
					<td class="td_label">Images</td>
					<td colspan="3" class="td_choose">
						<div class="upload_img_left">
							<input type="button" class="choose_image" name="choose" value="Choose a file to upload">

							<div class="delete_select">
								<input type="checkbox" name="check_all_image" class="check_all_image" id="check_all_image" value="check"><label class="lbl_sellect_all">Select All</label>
								<input type="button" name="delete_image" class="delete_image" value="Delete Selected">
							</div>
							<div class="clear"></div>
							<input name="file_image[]" type="file" id="file_image1" accept="image/*" multiple class="file-image" />
							<input type="hidden" id="image_name_lists" class="image_name_lists" value="<?php echo $image_name_lists; ?>" name="image_name_lists">
							<input type="hidden" id="primary_image" class="primary_image" name="primary_image">
						</div>
						<div class="content_floor_plan">
							<p class="floor_label">Floor Plan Image</p>
							<div class="content_floor_img">
								<input type="file" name="floor_plan_image" class="upload_floor_img" accept="image/*" id="upload_floor_img">
								<input type="hidden" class="planfloor_image" value="<?php echo $floorplan; ?>" id="planfloor_image">
							</div>
							<input type="button" class="choose_img_floor" name="choose_floor" value="Change">
						</div>
					</td>
				</tr>

				<tr>
					<td class="td_label">
						360<sup>&deg;</sup> Images
					</td>
					<td colspan="3">
						<div class="field_video_image">
							<input type="text" class="video_id" id="video_id_buy" name="video_id" value="<?php echo $video_id; ?>" >
							<input type="button" class="upload_video_id" id="upload_video_id_buy" name="upload_video_id" value="Show">
						</div>
						<iframe name="someFrame" <?php if($video_id == ""){ ?>style="display:none;"<?php } ?> src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" id="get_video_buy" width="410" height="260"></iframe>
					</td>
				</tr>

				<tr>
					<td class="td_label">
						Map
					</td>
					<td colspan="3">
						
						<div style="height: 270px; width: 791px;">
							<div class="latitude">Latitude: <label  id="label_lat" class="lbl_latitude"></label> </div>
							<div class="longitude">Longitude: <label id="label_lng" class="lbl_longitude"></label></div>
							<input type="hidden" id="getlat" name="latitude" value="">
							<input type="hidden" id="getlng" name="longitude" value="">
							<!-- <a class="reset" href="#">Reset</a><div class="clear"></div> -->
							<div style="height: 235px; width: 791px;  margin-top: 16px;" id="map"></div>
						</div>
						

					</td>
				</tr>

			</table>
			<div class="additional_details">
				<h4 class="labal_additional">ADDITIONAL DETAILS<a class="btn_hide btn_arrow">
						<img src="/assets/img/icon/arrow-down.png">
						<img src="/assets/img/icon/arrow-up.png" class="img_arrow">
					</a>
				</h4>
				<div class="additional_details_tool">
					<table class="table_properties">
						<tr>
							<td class="td_label">Layout</td>
							<td class="tb_input_add">
								<!-- <input type="text" name="layout" value="<?php echo $layout; ?>" class="layout text-field"/> -->
								<select name="layout" class="select-field layout" id="select-layout" placeholder="Select or type...">
									<!-- <option value="">Layout</option>
									<?php foreach($select_option_layout_tools_properties as $select_option_layout_tools_propertie){ ?>
										<option <?php echo ($layout == $select_option_layout_tools_propertie->layout ? "selected" : set_select('layout',$select_option_layout_tools_propertie->layout, False)); ?> value="<?php echo $select_option_layout_tools_propertie->layout; ?>"><?php echo $select_option_layout_tools_propertie->layout; ?></option>
									<?php } ?> -->
									<option value="">Layout</option>
									<?php foreach($select_layout as $key => $value){ ?>
										<option <?php echo (($layout == $key) ? "selected" : set_select('layout',$key, False)); ?> value="<?php echo  $key ; ?>"><?php echo $value; ?></option>
									<?php } ?>
								</select>
								<?php echo form_error('layout', '<div class="error">', '</div>'); ?>
								<div class="error" id="layout_error"></div>
							</td>
							<td class="td_label">Zoning</td>
							<td>
								<input type="text" name="zoning" value="<?php echo $zoning; ?>" class="zoning text-field"/>
								<!-- <?php echo form_error('zoning', '<div class="error">', '</div>'); ?>
								<div class="error" id="zoning_error"></div> -->
							</td>
						</tr>

						<tr>
							<td class="td_label">Year Build</td>
							<td class="tb_input_add">
								<select name="year_build" id="building_year" class="select-field year_build">
									<option value="">Year Build</option>
									<?php for($i=date('Y'); $i>1899; $i--) { ?>
									<option <?php echo ($year_build == $i ? "selected" : set_select('year_build', $i, False)); ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
								<?php echo form_error('year_build', '<div class="error">', '</div>'); ?>
								<div class="error" id="year_build_error"></div>
							</td>
							<td class="td_label">Structure</td>
							<td>
								<input type="text" name="structure" value="<?php echo $structure; ?>" class="structure text-field"/>
								
							</td>
						</tr>

						<tr>
							<td class="td_label">Maintainance Fee</td>
							<td class="tb_input_add">
								<input type="text" name="maintainance_fee" value="<?php echo $maintainance_fee; ?>" class="maintainace_fee text-field"/>
								<select name="maintainance_fee_per_month" class="select-field per_month">
									<option value="">Per Month</option>
									<?php for($i=1; $i<=12; $i++) { ?>
									<option <?php echo ($maintainance_fee_per_month == $i ? "selected" : set_select('maintainance_fee_per_month', $i, False)); ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
								
							</td>
							<td class="td_label">Available From</td>
							<td>
								<input type="text" name="available_from" value="<?php echo $available_from; ?>" class="available_from text-field"/>
								
							</td>
						</tr>

						<tr>
							<td class="td_label">Potential Annual Rent</td>
							<td class="tb_input_add">
								<select name="potential_annual_rent" class="select-field Potential" id="select-Potential" placeholder="Select or type...">
									<option value="">Potential Annual Rent</option>
									<?php foreach($select_option_potential_annual_rent_tools_properties as $select_option_potential_annual_rent_tools_propertie){ ?>
										<option <?php echo ($potential_annual_rent == $select_option_potential_annual_rent_tools_propertie->potential_annual_rent ? "selected" : set_select('potential_annual_rent',$select_option_potential_annual_rent_tools_propertie->potential_annual_rent, False)); ?> value="<?php echo $select_option_potential_annual_rent_tools_propertie->potential_annual_rent; ?>"><?php echo $select_option_potential_annual_rent_tools_propertie->potential_annual_rent; ?></option>
									<?php } ?>
								</select>
								
							</td>
							<td class="td_label">Unit Summary</td>
							<td>
								<input type="text" name="unit_summary" value="<?php echo $unit_summary; ?>" class="unit_summary text-field"/>
								
							</td>
						</tr>

						<tr>
							<td class="td_label">Transaction Type</td>
							<td class="tb_input_add">
								<select name="transaction_type" class="select-field transaction_type" id="select-transaction_type" placeholder="Select or type...">
									<option value="">Transaction Type</option>
									<?php foreach($select_option_transaction_type_tools_properties as $select_option_transaction_type_tools_propertie){ ?>
										<option <?php echo ($transaction_type == $select_option_transaction_type_tools_propertie->transaction_type ? "selected" : set_select('transaction_type',$select_option_transaction_type_tools_propertie->transaction_type, False)); ?> value="<?php echo $select_option_transaction_type_tools_propertie->transaction_type; ?>"><?php echo $select_option_transaction_type_tools_propertie->transaction_type; ?></option>
									<?php } ?>
								</select>
								
							</td>
							<td class="td_label">Parking</td>
							<td>
								<select name="parking" class="select-field parking" id="select-parking" placeholder="Select or type...">
									<option value="">Parking</option>
									<?php foreach($select_option_parking_tools_properties as $select_option_parking_tools_propertie){ ?>
										<option <?php echo ($parking == $select_option_parking_tools_propertie->parking ? "selected" : set_select('parking',$select_option_parking_tools_propertie->parking, False)) ; ?> value="<?php echo $select_option_parking_tools_propertie->parking; ?>"><?php echo $select_option_parking_tools_propertie->parking; ?></option>
									<?php } ?>
								</select>
								
							</td>
						</tr>

						<tr>
							<td class="td_label">Road Width</td>
							<td class="tb_input_add">
								<input type="text" name="road_width" value="<?php echo $road_width; ?>" class="road_width text-field width_144"/>
							</td>
							<td class="td_label">Balcony Size</td>
							<td>
								<input type="text" name="balcony_size" value="<?php echo $balcony_size; ?>" class="balcony_size text-field width_144">
							</td>
						</tr>

						<tr>
							<td class="td_label">Building Area Ratio</td>
							<td class="tb_input_add">
								<input type="text" name="building_area_ratio" value="<?php echo $building_area_ratio; ?>" class="building_area_ratio text-field"/>
								
							</td>
							<td class="td_label">Date Updated</td>
							<td>
								<input type="text" name="date_updated" value="<?php echo $date_updated; ?>" id="date_updated" class="date_updated text-field">
								
							</td>
						</tr>

						<tr>
							<td class="td_label">Floor Area Ratio</td>
							<td class="tb_input_add">
								<input type="text" name="floor_area_ratio" value="<?php echo $floor_area_ratio; ?>" class="floor_area_ratio text-field"/>
								
							</td>
							<td class="td_label">Next Update Schedule</td>
							<td>
								<input type="text" name="next_update_schedule" value="<?php echo $next_update_schedule; ?>" id="next_update_schedule" class="next_update_schedule text-field">
								
							</td>
						</tr>

						<tr>
							<td class="td_label">Landmarks</td>
							<td class="tb_input_add">
								<textarea name="landmarks" id="Landmarks" rows="3" cols="36"><?php echo $landmarks; ?></textarea>
						
							</td>
							<td class="td_label">Other Expenses</td>
							<td class="tb_input_add">
								<textarea name="other_expenses" id="other_expenses" rows="3" cols="36"><?php echo $other_expenses; ?></textarea>
						
							</td>
							
							
						</tr>

						<tr>
							<td class="td_label">Building Description</td>
							<td class="tb_input_add">
								<textarea name="building_description" id="building_description" rows="3" cols="36"><?php echo $building_description; ?></textarea>
							</td>
							
							<td class="td_label"></td>
							<td class="tb_input_add">
								
						
							</td>
						</tr>

					</table>
				</div>
			</div>

			<div class="tools_save_cancel">
				<input type="hidden" id="edit_lat" name="edit_latitude" value="<?php echo $latitude;?>">
				<input type="hidden" id="edit_lng" name="edit_longitude" value="<?php echo $longitude;?>">
				<input type="hidden" value="<?php echo $id;?>" id="edit_id">
				<input type="hidden" name="id" value="<?php echo $id;?>">
				<input type="hidden" name="status" value="1">
				<input type="submit" name="save_update" class="btn_save" value="Save">
				<input type="submit" name="cancel" class="btn_cancel" value="Cancel">
			</div>



		</form>
		</div>