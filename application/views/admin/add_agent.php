

	<div class="container_agent" id="wrap_content">
		<?php $this->load->view('/templates/sidebar_admin');?>
		<div class="content">
			<div class="tool_search_select">
				<div class="text_myinfo">
					ADD NEW AGENT
				</div>
			</div>	

			<div class="show_content_info">
				<div class="content_info">
				<?php echo form_open_multipart($action_form,array('class' => 'form_add_agent','id' => 'form_add_agent')); ?>
					<table class="table_info">
						<tr>
							<td>Logo</td>
							<td class="logo_info">
								<input type="file" name="file_image" class="upload_logo" id="upload_logo">
								<!-- <img src="/assets/img/company/company_logo_info.png" alt=""> -->
								<input type="button" class="choose_image" name="choose" value="choose logo upload">
							</td>
						</tr>
						<tr>
							<td>Company Name<span class="require">*</span></td>
							<td>
								<input type="text" class="company text-field" name="company_name" placeholder="Company name" value="<?php echo set_value('company_name'); ?>"/>	
								<div class="error" id="company_name_error"></div>		
								<?php echo form_error('company_name', '<div class="error">', '</div>'); ?>
							</td>
						</tr>
						<tr>
							<td>Email<span class="require">*</span></td>
							<td>
								<input type="email" class="email text-field" id="email" name="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
								<div class="error" id="email_error"></div>
								<?php echo form_error('email', '<div class="error">', '</div>'); ?>
							</td>
						</tr>
						<tr>
							<td>Password<span class="require">*</span></td>
							<td>
								<input type="password" class="password text-field" name="password" value="<?php echo set_value('password'); ?>">
								<div class="error" id="password_error"></div>
								<?php echo form_error('password', '<div class="error">', '</div>'); ?>
							</td>
						</tr>
						<tr>
							<td>Comfirm Password<span class="require">*</span></td>
							<td>
								<input type="password" class="confirm_password text-field" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>">
								<div class="error" id="confirm_password_error"></div>
								<?php echo form_error('confirm_password', '<div class="error">', '</div>'); ?>
							</td>
						</tr>
						<tr>
							<td>Social Media</td>
							<td>
								<div class="social_media">
									<span class="add_sociel">
										<!-- <img src="/assets/img/icon/skype.png" alt="Phone"> -->
										
										<input type="text" class="skype_id text-field" name="skype_id" placeholder="Skype id" value="<?php echo set_value('skype_id'); ?>">
									</span>
									<span class="add_sociel">
										<!-- <img src="/assets/img/icon/viber.png" alt="Email"> -->
										<input type="text" class="viber_id text-field" name="viber_id" placeholder="Viber id" value="<?php echo set_value('viber_id'); ?>">
									</span>
									<span class="add_sociel">
										<!-- <img src="/assets/img/icon/line.png" alt="Email"> -->
										<input type="text" class="line_id text-field" name="line_id" placeholder="Line id" value="<?php echo set_value('line_id'); ?>">
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>Phone Number</td>
							<td><input type="text" class="phone1 text-field" name="phone1" placeholder="Phone number 1" value="<?php echo set_value('phone1'); ?>"> 
							<input type="text" class="phone2 text-field" name="phone2"  placeholder="Phone number 2" value="<?php echo set_value('phone2'); ?>"></td>
						</tr>
						<tr>
							<td>Office Location</td>
							<td>
								<input type="text" class="location text-field" name="office_location" placeholder="Office location" value="<?php echo set_value('office_location'); ?>">
								
							</td>
						</tr>
						<tr>
							<td class="text_des">Description</td>
							<td class="detail_des">
								<textarea cols="58" rows="5" name="description" class="description"><?php echo set_value('description'); ?></textarea>
								
							</td>
						</tr>
						<tr>
							<td class="text_des">
								
							</td>
							<td class="detail_des">
								<input type="submit" name="add_agent" class="add_agent" id="add_agent" value="OK">
							</td>
						</tr>
					</table>	
					</form>
				</div>

			</div>


		</div><div class="clear"></div>
	</div>
