
	<div id="wrap_content">

		<!-- <form method="post" action="<?php echo $action_form; ?>" class="form_upload_properties"> -->
		<input type="hidden" id="image_name_lists" class="image_name_lists" value="<?php echo $image_name_lists; ?>" >
		<input type="hidden" id="edit_lat" name="edit_latitude" value="<?php echo $latitude;?>">
		<input type="hidden" id="edit_lng" name="edit_longitude" value="<?php echo $longitude;?>">
		<input type="hidden" value="<?php echo $id;?>" id="edit_id">
			<div class="block_list_properties_type">
				<a class="btn_back" id="btn_back" href="/admin/properties"><i class="fa fa-angle-double-left custom_icon" aria-hidden="true"></i> BACK</a>
				<ul class="list_properties_type">
					<li id="buy" data-tab="tabs-buy" class="<?php if($status == '1'){ echo "active"; } ?>" disable><a class="properties_type"><img src="/assets/img/icon/status_buy.png" class="icon_status" /> Buy</a><input type="radio" class="properties_type_radio" id="properties_type_radio_buy" name="properties_type" value="buy" <?php echo set_radio('properties_type', 'buy', FALSE); ?> checked=""></li>
					<li id="rent" data-tab="tabs-rent" class="<?php if($status == '2'){ echo "active"; } ?>"><a class="properties_type"><img src="/assets/img/icon/status_rent.png" class="icon_status" /> Rent</a><input type="radio" class="properties_type_radio" id="properties_type_radio_rent" name="properties_type" value="rent" <?php echo set_radio('properties_type', 'rent', FALSE); ?>></li>
					<li id="short_stay" data-tab="tabs-shortstay" class="<?php if($status == '3'){ echo "active"; } ?>"><a class="properties_type"><img src="/assets/img/icon/status_short.png" class="icon_status" /> Short Stay</a><input type="radio" class="properties_type_radio" id="properties_type_radio_short_stay" name="properties_type" value="short_stay" <?php echo set_radio('properties_type', 'short_stay', FALSE); ?>></li>
				</ul>
				<input type="hidden" class="hide_status" value="<?php echo $status_name;?>" />
			</div>
			<!-- Block Buy Content -->
			<?php if($status =="1"){ ?>
			<div class="tabs-content-all <?php if($status !="1"){ ?> tab-content <?php } ?>" id="tabs-buy">
			<?php echo form_open_multipart($action_form,array('id' => 'upload_properties_buy')); ?>


			<table class="table_properties">
				<tr>
					<td class="td_label">Price <span class="required_field">*</span></td>
					<td>
						<input type="text" name="price" value="<?php echo $price; ?>" class="buy_price text-field" required/>
						<select name="currency" class="select-field currency">
							<option <?php echo set_select('currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('price', '<div class="error">', '</div>'); ?>
						<div class="error" id="price_error"></div>
					</td>
					<td class="td_label">Floor</td>
					<td>
						<select name="floor" class="select-field floor" id="select-floor" placeholder="Select or type...">
							<option value="">Floor</option>
							<?php foreach($select_option_floor_tool_properties as $select_option_floor_tool_propertie){ ?>
							<option <?php echo ($floor == $select_option_floor_tool_propertie->floor ? "selected " : ""); ?> value="<?php echo $select_option_floor_tool_propertie->floor; ?>"><?php echo $select_option_floor_tool_propertie->floor; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('floor', '<div class="error">', '</div>'); ?>
						<div class="error" id="floor_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Management fee ect</td>
					<td>
						<input type="text" name="management_fee" value="<?php echo $management_fee; ?>" class="buy_price text-field"/>
						<select name="management_currency" class="select-field currency">
							<!-- <option <?php echo set_select('management_currency',"USD", False); ?> value="USD">USD($)</option> -->
							<option <?php echo set_select('management_currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('management_fee', '<div class="error">', '</div>'); ?>
						<div class="error" id="price_error1"></div>
					</td>
					<td class="td_label">Size</td>
					<td>
						<input type="text" name="size" value="<?php echo $size; ?>" class="size text-field"/><label class="lbl_size">m&sup2;</label>
						<?php echo form_error('size', '<div class="error">', '</div>'); ?>
						<div class="error" id="size_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Maintenance fee</td>
					<td>
						<input type="text" name="maintainance_fee" value="<?php echo $maintainance_fee; ?>" class="buy_price text-field"/>
						<select name="maintenance_currency" class="select-field currency">
							<!-- <option <?php echo set_select('maintenance_currency',"USD", False); ?> value="USD">USD($)</option> -->
							<option <?php echo set_select('maintenance_currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('maintainance_fee', '<div class="error">', '</div>'); ?>
						<div class="error" id="price_error2"></div>
					</td>
					<td class="td_label">Land Rights</td>
					<td>
						<input type="text" value="<?php echo $land_right; ?>" name="land_right" class="land_rights text-field"/>
						<?php echo form_error('land_right', '<div class="error">', '</div>'); ?>
						<div class="error" id="land_right_error"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Maked<span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="text"  id="iron2" name="maked" value="<?php echo $maked; ?>"  class="status text-field"/>
						<?php echo form_error('maked', '<div class="error">', '</div>'); ?>
						<div class="error" id="maked_error"></div>
					</td>
					<td class="td_label">Type</td>
					<td class="tb_input">
						<input type="radio" name="type" value="house" <?php echo ($type == "house" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">House</label>
						<input type="radio" name="type" value="apartment" <?php echo ($type == "apartment" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">Apartment</label>
					</td>
				</tr>

				<tr>
					<td class="td_label">Location <span class="required_field">*</span></td>
					<td class="tb_input">

						<select id="location_perfecture" name="location_perfecture" class="select-field perfecture">
							<option value="">Perfecture</option>
							<?php foreach($select_perfectures as $select_perfecture){ ?>
								<option <?php echo ($location_perfecture == $select_perfecture->id ? "selected" : ""); ?> value="<?php echo $select_perfecture->id?>"><?php echo $select_perfecture->perfecture_name; ?></option>
							<?php } ?>
						</select>

						<select id="location_city" name="location_city" class="select-field city">
							<option value="">City</option>
							<?php foreach($select_perfectures_citys as $select_perfectures_city){ ?>
								<option <?php echo ($location_city == $select_perfectures_city->id ? "selected" : ""); ?> class="<?php echo $select_perfectures_city->perfecture_id_fk; ?>" value="<?php echo $select_perfectures_city->id?>"><?php echo $select_perfectures_city->city_name; ?></option>
							<?php } ?>
						</select>

						<?php echo form_error('location_perfecture', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_perfecture_error"></div>

						<?php echo form_error('location_city', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_city_error"></div>

					</td>
					<td class="td_label">Land Area</td>
					<td>
						<input type="text" name="land_area" value="<?php echo $land_area; ?>" class="land_area text-field"/>
					</td>
				</tr>

				<tr>
					<td class="td_label">Gross Yield</td>
					<td class="tb_input">

						<select name="gross_yield" class="select-field gross_yield" id="select-gross_yield" placeholder="Select or type...">
							<option value=""></option>
							<?php foreach($select_option_gross_yield_tool_properties as $select_option_gross_yield_tool_propertie){ ?>
								<option <?php echo ($gross_yield == $select_option_gross_yield_tool_propertie->gross_yield ? "selected" : ""); ?> value="<?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?>"><?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="td_label">Occupancy</td>
					<td>
						<input type="text" name="occupancy" value="<?php echo $occupancy; ?>" class="occupancy text-field"/>
					</td>
				</tr>

				<tr>
					<td class="td_label">Nearest Station<span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="text" name="nearest_station" value="<?php echo $nearest_station; ?>" class="buy_nearest_station text-field"/>
						<select class="text-field distance_to_statition" name="distance_to_station" placeholder="Distance to Station" >
							<option  value="">Distance to Station</option>
							<option <?php echo (($distance_to_station==3) ? "selected" : set_select('distance_to_station',"3", False)); ?>  value="3">Within 3 minutes</option>
							<option <?php echo (($distance_to_station==5) ? "selected"  : set_select('distance_to_station',"5", False)) ; ?>  value="5">Within 5 minutes</option>
							<option <?php echo (($distance_to_station==7) ? "selected"  : set_select('distance_to_station',"7", False)); ?>  value="7">Within 7 minutes</option>
							<option <?php echo (($distance_to_station==10) ? "selected" : set_select('distance_to_station',"10", False)); ?> value="10">Within 10 minutes</option>
							<option <?php echo (($distance_to_station==15) ? "selected" : set_select('distance_to_station',"15", False)); ?> value="15">Within 15 minutes</option>
							<option <?php echo (($distance_to_station==20) ? "selected" : set_select('distance_to_station',"20", False)); ?> value="20">Within 20 minutes</option>
						</select>
						<div class="error" id="distance_to_station_error"></div>
						<?php echo form_error('distance_to_station', '<div class="error">', '</div>'); ?>

					</td>
				</tr>
			</table>

			<table class="table_properties_info ">
			 	<tr>
					<td class="td_label">Layout</td>
					<td class="tb_input_add">
						<select name="layout" class="select-field layout" id="select-layout" placeholder="Select or type...">
							<option value="">Layout</option>
							<?php foreach($select_layout as $key => $value){ ?>
								<option <?php echo (($layout == $key) ? "selected" : set_select('layout',$key, False)); ?> value="<?php echo  $key ; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					    <?php echo form_error('layout', '<div class="error">', '</div>'); ?>
						<div class="error" id="layout_error"></div>
					</td>
					<td class="td_label" rowspan="4">Images</td>
					 <td  class="tb_input_add" rowspan="4">
						<input type="button" class="choose_image choose_image_upload" name="choose" value="Choose a file to upload">
						<div class="content_select">
						<input type="checkbox" name="check_all_image" class="check_all_image" id="check_all_image" value="check"><label class="lbl_sellect_all">Select All</label>
						<input type="button" name="delete_image" class="delete_image" value="Delete Selected">
						</div>
						<div class="clear"></div>
						<input name="file_image[]" type="file" id="file_image1" accept="image/*" multiple class="file-image" />
						<input type="hidden" class="image_name_lists" value="<?php echo $image_name_lists; ?>" name="image_name_lists">
						<input type="hidden" id="primary_image" class="primary_image" name="primary_image">
						<div class="floor_content">
					        <div class="text_floor_plan">Floor Plan Image</div>
					        <span>
								<input type="button" class="choose_img_floor img_floor_upload" name="choose_floor" value="Change">
							</span>
						<div class="content_floor_plan">
						<div class="content_floor_img">
							<input type="file" name="floor_plan_image" class="upload_floor_img" accept="image/*" id="upload_floor_img">
							<input type="hidden" class="planfloor_image" value="<?php echo $floorplan; ?>" id="planfloor_image">
						</div>
						</div>
					</td>
			    </tr>
			    <tr>
					<td class="td_label">Year Build</td>
					<td class="tb_input_add">
						<select name="year_build" id="building_year" class="select-field year_build">
							<option value="">Year Build</option>
							<?php for($i=date('Y'); $i>1899; $i--) { ?>
							<option <?php echo ($year_build == $i ? "selected" : set_select('year_build', $i, False)); ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('year_build', '<div class="error">', '</div>'); ?>
						<div class="error" id="year_build_error"></div>
					</td>
				</tr>
				<tr>
					<td class="td_label">Zoning</td>
					<td class="tb_input_add">
						<input type="text" name="zoning" value="<?php echo $zoning; ?>" class="zoning text-field"/>
					</td>
				</tr>
				<tr>
					<td class="td_label">Parking</td>
					<td class="tb_input_add">
						<select name="parking" class="select-field parking" id="select-parking" placeholder="Select or type...">
							<option value="">Parking</option>
							<?php foreach($select_option_parking_tools_properties as $select_option_parking_tools_propertie){ ?>
								<option <?php echo ($parking == $select_option_parking_tools_propertie->parking ? "selected" : set_select('parking',$select_option_parking_tools_propertie->parking, False)) ; ?> value="<?php echo $select_option_parking_tools_propertie->parking; ?>"><?php echo $select_option_parking_tools_propertie->parking; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label">Balcony Size</td>
					<td class="tb_input_add">
						<input type="text" name="balcony_size" value="<?php echo $balcony_size; ?>" class="zoning text-field"/>
					</td>
					<td class="td_label">
						360<sup>&deg;</sup> Images
					</td>
					<td class="tb_input_add" colspan="3">
						<div class="field_video_image">
							<input type="text" class="video_id" id="video_id_buy" name="video_id" value="<?php echo $video_id; ?>" >
							<input type="button" class="upload_video_id" id="upload_video_id_buy" name="upload_video_id" value="Show">
						</div>
						<iframe name="someFrame" <?php if($video_id == ""){ ?>style="display:none;"<?php } ?> src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" id="get_video_buy" width="308" height="260"></iframe>
					</td>
				</tr>
				<tr>
					<td class="td_label">Date Updated</td>
					<td class="tb_input_add">
						<input type="text" name="date_updated" value="<?php echo $date_updated; ?>" id="buy_date_updated" class="date_updated text-field">
					</td>
					<td class="td_label" rowspan="3">Map</td>
					<td class="tb_input_add"  rowspan="3">
						<div style="height: 270px;">
							<div class="latitude">Latitude: <label  id="label_lat" class="lbl_latitude"></label> </div>
							<div class="longitude">Longitude: <label id="label_lng" class="lbl_longitude"></label></div>
							<input type="hidden" id="getlat" name="latitude" value="">
							<input type="hidden" id="getlng" name="longitude" value="">
							<!-- <a class="reset" href="#">Reset</a><div class="clear"></div> -->
							<div style="height: 235px; width:308px;  margin-top: 16px;" id="map"></div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="td_label">Next Update Schedule</td>
					<td class="tb_input_add">
						<input type="text" name="next_update_schedule" value="<?php echo $next_update_schedule; ?>" id="buy_next_update_schedule" class="next_update_schedule text-field">
					</td>
				</tr>
				<tr>
					<td class="td_label">Other Expenses</td>
					<td class="tb_input_add">
						<textarea name="other_expenses" rows="3" cols="36"><?php echo $other_expenses; ?></textarea>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Feature
					</td>
					<td colspan="4">
						<table class="feature">
							<tr>
								<?php 
									$i = 0;
									foreach($select_features as $select_feature){ 
										$checkeds="";
										if(count($arr_features) > 0){
											foreach($arr_features as $arr_feature){
												if($select_feature->feature_value == $arr_feature){
													$checkeds = "checked";
												} 
											}
										}
								?>
									<td>
										<input type="checkbox" name="feature[]" <?php echo set_checkbox('feature[]', $select_feature->feature_value, FALSE); ?> class="description_1" value="<?php echo $select_feature->feature_value; ?>" <?php echo $checkeds; ?>><label class="lbl_description"><?php echo $select_feature->feature_name; ?></label>
									</td>
								<?php 
										$i++;
										if($i%4 == 0){
											echo "</tr><tr>";
										}	
									} 
								?>
							</tr>
						</table>

					</td>
				</tr>

			</table>


			<table class="table_properties">
				<tr>
					<td class="td_label">
						Description
					</td>
					<td colspan="3">
						<textarea name="description" class="ckeditor" id="description_buy" rows="5" cols="96"><?php echo $description; ?></textarea>
					</td>
				</tr>

				<tr>
					<td class="td_label">
						Transportation
					</td>
					<td colspan="3">
						<textarea name="transportation" class="ckeditor" id="transportation_buy" rows="5" cols="96"><?php echo $transportation; ?></textarea>
					</td>
				</tr>
			</table>


			<div class="tools_save_cancel">
				<input type="hidden" name="id" value="<?php echo $id;?>">
				<input type="hidden" name="status" value="1">
				<input type="submit" name="save_update" class="btn_save" value="Save">
				<input type="submit" name="cancel" class="btn_cancel" value="Cancel">
			</div>
		</form>
		</div>
		<?php }else if($status == "2"){ ?>
		<!-- Block Rent Content -->
		<div class="tabs-content-all <?php if($status !="2"){ ?> tab-content <?php } ?>" id="tabs-rent">
			<?php echo form_open_multipart($action_form,array('id' => 'upload_properties_rent')); ?>
			<table class="table_properties">
				<tr>
					<td class="td_label">Price <span class="required_field">*</span></td>
					<td>
						<input type="text" name="price" value="<?php echo $price; ?>" class="buy_price text-field" required/>
						<select name="currency" class="select-field currency">
							<option <?php echo set_select('currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('price', '<div class="error">', '</div>'); ?>
						<div class="error" id="price_error3"></div>
					</td>
					<td class="td_label">Floor</td>
					<td>
						<select name="floor" class="select-field floor" id="select-floor1" placeholder="Select or type...">
							<option value="">Floor</option>
							<?php foreach($select_option_floor_tool_properties as $select_option_floor_tool_propertie){ ?>
							<option <?php echo ($floor == $select_option_floor_tool_propertie->floor ? "selected " : ""); ?> value="<?php echo $select_option_floor_tool_propertie->floor; ?>"><?php echo $select_option_floor_tool_propertie->floor; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('floor', '<div class="error">', '</div>'); ?>
						<div class="error" id="floor_error1"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Deposit</td>
					<td>
						<input type="text" name="deposit" value="<?php echo $deposit; ?>" class="buy_price text-field"/>
						<select name="management_currency" class="select-field currency">
							<!-- <option <?php echo set_select('management_currency',"USD", False); ?> value="USD">USD($)</option> -->
							<option <?php echo set_select('management_currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('deposit', '<div class="error">', '</div>'); ?>
						<div class="error" id="deposit_error"></div>
					</td>
					<td class="td_label">Size</td>
					<td>
						<input type="text" name="size" value="<?php echo $size; ?>" class="size text-field"/><label class="lbl_size">m&sup2;</label>
						<?php echo form_error('size', '<div class="error">', '</div>'); ?>
						<div class="error" id="size_error1"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Landlord fee</td>
					<td>
						<input type="text" name="landlord_fee" value="<?php echo $landlord_fee; ?>" class="buy_price text-field"/>
						<select name="maintenance_currency" class="select-field currency">
							<!-- <option <?php echo set_select('maintenance_currency',"USD", False); ?> value="USD">USD($)</option> -->
							<option <?php echo set_select('maintenance_currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('landlord_fee', '<div class="error">', '</div>'); ?>
						<div class="error" id="landlord_fee_error"></div>
					</td>
					<td class="td_label">Land Rights</td>
					<td>
						<input type="text" value="<?php echo $land_right; ?>" name="land_right" class="land_rights text-field"/>
						<?php echo form_error('land_right', '<div class="error">', '</div>'); ?>
						<div class="error" id="land_right_error1"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Maked<span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="text"  id="iron1" name="maked" value="<?php echo $maked; ?>"  class="status text-field"/>
						<?php echo form_error('maked', '<div class="error">', '</div>'); ?>
						<div class="error" id="maked_error1"></div>
					</td>
					<td class="td_label">Type</td>
					<td class="tb_input">
						<input type="radio" name="type" value="house" <?php echo ($type == "house" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">House</label>
						<input type="radio" name="type" value="apartment" <?php echo ($type == "apartment" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">Apartment</label>
					</td>
				</tr>

				<tr>
					<td class="td_label">Location <span class="required_field">*</span></td>
					<td class="tb_input">

						<select id="rent_location_perfecture" name="location_perfecture" class="select-field perfecture">
							<option value="">Perfecture</option>
							<?php foreach($select_perfectures as $select_perfecture){ ?>
								<option <?php echo ($location_perfecture == $select_perfecture->id ? "selected" : ""); ?> value="<?php echo $select_perfecture->id?>"><?php echo $select_perfecture->perfecture_name; ?></option>
							<?php } ?>
						</select>

						<select id="rent_location_city" name="location_city" class="select-field city">
							<option value="">City</option>
							<?php foreach($select_perfectures_citys as $select_perfectures_city){ ?>
								<option <?php echo ($location_city == $select_perfectures_city->id ? "selected" : ""); ?> class="<?php echo $select_perfectures_city->perfecture_id_fk; ?>" value="<?php echo $select_perfectures_city->id?>"><?php echo $select_perfectures_city->city_name; ?></option>
							<?php } ?>
						</select>

						<?php echo form_error('location_perfecture', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_perfecture_error_rent"></div>

						<?php echo form_error('location_city', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_city_error_rent"></div>

					</td>
					<td class="td_label">Land Area</td>
					<td>
						<input type="text" name="land_area" value="<?php echo $land_area; ?>" class="land_area text-field"/>
					</td>
				</tr>

				<tr>
					<td class="td_label">Gross Yield</td>
					<td class="tb_input">

						<select name="gross_yield" class="select-field gross_yield" id="select-gross_yield1" placeholder="Select or type...">
							<option value=""></option>
							<?php foreach($select_option_gross_yield_tool_properties as $select_option_gross_yield_tool_propertie){ ?>
								<option <?php echo ($gross_yield == $select_option_gross_yield_tool_propertie->gross_yield ? "selected" : ""); ?> value="<?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?>"><?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="td_label">Occupancy</td>
					<td>
						<input type="text" name="occupancy" value="<?php echo $occupancy; ?>" class="occupancy text-field"/>
					</td>
				</tr>

				<tr>
					<td class="td_label">Nearest Station<span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="text" name="nearest_station" value="<?php echo $nearest_station; ?>" class="buy_nearest_station text-field"/>
						<select class="text-field distance_to_statition" name="distance_to_station" placeholder="Distance to Station" >
							<option  value="">Distance to Station</option>
							<option <?php echo (($distance_to_station==3) ? "selected" : set_select('distance_to_station',"3", False)); ?>  value="3">Within 3 minutes</option>
							<option <?php echo (($distance_to_station==5) ? "selected"  : set_select('distance_to_station',"5", False)) ; ?>  value="5">Within 5 minutes</option>
							<option <?php echo (($distance_to_station==7) ? "selected"  : set_select('distance_to_station',"7", False)); ?>  value="7">Within 7 minutes</option>
							<option <?php echo (($distance_to_station==10) ? "selected" : set_select('distance_to_station',"10", False)); ?> value="10">Within 10 minutes</option>
							<option <?php echo (($distance_to_station==15) ? "selected" : set_select('distance_to_station',"15", False)); ?> value="15">Within 15 minutes</option>
							<option <?php echo (($distance_to_station==20) ? "selected" : set_select('distance_to_station',"20", False)); ?> value="20">Within 20 minutes</option>
						</select>
						<div class="error" id="distance_to_station_error1"></div>
						<?php echo form_error('distance_to_station', '<div class="error">', '</div>'); ?>

					</td>
				</tr>
			</table>

			<table class="table_properties_info ">
			 	<tr>
					<td class="td_label">Layout</td>
					<td class="tb_input_add">
						<select name="layout" class="select-field layout" id="select-layout1" placeholder="Select or type...">
							<option value="">Layout</option>
							<?php foreach($select_layout as $key => $value){ ?>
								<option <?php echo (($layout == $key) ? "selected" : set_select('layout',$key, False)); ?> value="<?php echo  $key ; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					    <?php echo form_error('layout', '<div class="error">', '</div>'); ?>
						<div class="error" id="layout_error1"></div>
					</td>
					<td class="td_label" rowspan="4">Images</td>
					 <td  class="tb_input_add" rowspan="4">
						<input type="button" class="choose_image choose_image_upload" name="choose" value="Choose a file to upload">
						<div class="content_select">
						<input type="checkbox" name="check_all_image" class="check_all_image" id="rent_check_all_image" value="check"><label class="lbl_sellect_all">Select All</label>
						<input type="button" name="delete_image" class="delete_image" value="Delete Selected">
						</div>
						<div class="clear"></div>
						<input name="file_image[]" type="file" id="rent_file_image1" accept="image/*" multiple class="file-image" />
						<input type="hidden" class="image_name_lists" value="<?php echo $image_name_lists; ?>" name="image_name_lists">
						<input type="hidden" id="rent_primary_image" class="primary_image" name="primary_image">
						<div class="floor_content">
					        <div class="text_floor_plan">Floor Plan Image</div>
					        <span>
								<input type="button" class="choose_img_floor img_floor_upload" name="choose_floor" value="Change">
							</span>
						<div class="content_floor_plan">
						<div class="content_floor_img">
							<input type="file" name="floor_plan_image" class="upload_floor_img" accept="image/*" id="rent_upload_floor_img">
							<input type="hidden" class="planfloor_image" value="<?php echo $floorplan; ?>" id="rent_planfloor_image">
						</div>
						</div>
					</td>
			    </tr>
			    <tr>
					<td class="td_label">Year Build</td>
					<td class="tb_input_add">
						<select name="year_build" id="rent_building_year" class="select-field year_build">
							<option value="">Year Build</option>
							<?php for($i=date('Y'); $i>1899; $i--) { ?>
							<option <?php echo ($year_build == $i ? "selected" : set_select('year_build', $i, False)); ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('year_build', '<div class="error">', '</div>'); ?>
						<div class="error" id="rent_year_build_error"></div>
					</td>
				</tr>
				<tr>
					<td class="td_label">Zoning</td>
					<td class="tb_input_add">
						<input type="text" name="zoning" value="<?php echo $zoning; ?>" class="zoning text-field"/>
					</td>
				</tr>
				<tr>
					<td class="td_label">Parking</td>
					<td class="tb_input_add">
						<select name="parking" class="select-field parking" id="select-parking-rent" placeholder="Select or type...">
							<option value="">Parking</option>
							<?php foreach($select_option_parking_tools_properties as $select_option_parking_tools_propertie){ ?>
								<option <?php echo ($parking == $select_option_parking_tools_propertie->parking ? "selected" : set_select('parking',$select_option_parking_tools_propertie->parking, False)) ; ?> value="<?php echo $select_option_parking_tools_propertie->parking; ?>"><?php echo $select_option_parking_tools_propertie->parking; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label">Balcony Size</td>
					<td class="tb_input_add">
						<input type="text" name="balcony_size" value="<?php echo $balcony_size; ?>" class="zoning text-field"/>
					</td>
					<td class="td_label">
						360<sup>&deg;</sup> Images
					</td>
					<td class="tb_input_add" colspan="3">
						<div class="field_video_image">
							<input type="text" class="video_id" id="video_id_rent" name="video_id" value="<?php echo $video_id; ?>" >
							<input type="button" class="upload_video_id" id="upload_video_id_rent" name="upload_video_id" value="Show">
						</div>
						<iframe name="someFrame" <?php if($video_id == ""){ ?>style="display:none;"<?php } ?> src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" id="get_video_rent" width="308" height="260"></iframe>
					</td>
				</tr>
				<tr>
					<td class="td_label">Date Updated</td>
					<td class="tb_input_add">
						<input type="text" name="date_updated" value="<?php echo $date_updated; ?>" id="rent_date_updated" class="date_updated text-field">
					</td>
					<td class="td_label" rowspan="3">Map</td>
					<td class="tb_input_add"  rowspan="3">
						<div style="height: 270px;">
							<div class="latitude">Latitude: <label  id="rent_label_lat" class="lbl_latitude"></label> </div>
							<div class="longitude">Longitude: <label id="rent_label_lng" class="lbl_longitude"></label></div>
							<input type="hidden" id="rent_getlat" name="latitude" value="">
							<input type="hidden" id="rent_getlng" name="longitude" value="">
							<!-- <a class="reset" href="#">Reset</a><div class="clear"></div> -->
							<div style="height: 235px; width:308px;  margin-top: 16px;" id="rent_map"></div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="td_label">Next Update Schedule</td>
					<td class="tb_input_add">
						<input type="text" name="next_update_schedule" value="<?php echo $next_update_schedule; ?>" id="rent_next_update_schedule" class="next_update_schedule text-field">
					</td>
				</tr>
				<tr>
					<td class="td_label">Other Expenses</td>
					<td class="tb_input_add">
						<textarea name="other_expenses" rows="3" cols="36"><?php echo $other_expenses; ?></textarea>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Feature
					</td>
					<td colspan="4">
						<table class="feature">
							<tr>
								<?php 
									$i = 0;
									foreach($select_features as $select_feature){ 
										$checkeds="";
										if(count($arr_features) > 0){
											foreach($arr_features as $arr_feature){
												if($select_feature->feature_value == $arr_feature){
													$checkeds = "checked";
												} 
											}
										}
								?>
									<td>
										<input type="checkbox" name="feature[]" <?php echo set_checkbox('feature[]', $select_feature->feature_value, FALSE); ?> class="description_1" value="<?php echo $select_feature->feature_value; ?>" <?php echo $checkeds; ?>><label class="lbl_description"><?php echo $select_feature->feature_name; ?></label>
									</td>
								<?php 
										$i++;
										if($i%4 == 0){
											echo "</tr><tr>";
										}	
									} 
								?>
							</tr>
						</table>

					</td>
				</tr>

			</table>


			<table class="table_properties">
				<tr>
					<td class="td_label">
						Description
					</td>
					<td colspan="3">
						<textarea name="description" class="ckeditor" id="description_rent" rows="5" cols="96"><?php echo $description; ?></textarea>
					</td>
				</tr>

				<tr>
					<td class="td_label">
						Transportation
					</td>
					<td colspan="3">
						<textarea name="transportation" class="ckeditor" id="transportation_rent" rows="5" cols="96"><?php echo $transportation; ?></textarea>
					</td>
				</tr>
			</table>

			<div class="tools_save_cancel">
				<input type="hidden" name="id" value="<?php echo $id;?>">
				<input type="hidden" name="status" value="2">
				<input type="submit" name="save_update" class="btn_save" value="Save">
				<input type="submit" name="cancel" class="btn_cancel" value="Cancel">
			</div>
		</form>
		</div>
		<?php }else{ ?>
		<div class="tabs-content-all <?php if($status !="3"){ ?> tab-content <?php } ?>" id="tabs-shortstay">
			<?php echo form_open_multipart($action_form,array('id' => 'upload_properties_shortstay')); ?>
			<table class="table_properties">
				<tr>
					<td class="td_label">Price <span class="required_field">*</span></td>
					<td>
						<input type="text" name="price" value="<?php echo $price; ?>" class="buy_price text-field" required/>
						<select name="currency" class="select-field currency">
							<option <?php echo set_select('currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('price', '<div class="error">', '</div>'); ?>
						<div class="error" id="price_error4"></div>
					</td>
					<td class="td_label">Floor</td>
					<td>
						<select name="floor" class="select-field floor" id="select-floor1" placeholder="Select or type...">
							<option value="">Floor</option>
							<?php foreach($select_option_floor_tool_properties as $select_option_floor_tool_propertie){ ?>
							<option <?php echo ($floor == $select_option_floor_tool_propertie->floor ? "selected " : ""); ?> value="<?php echo $select_option_floor_tool_propertie->floor; ?>"><?php echo $select_option_floor_tool_propertie->floor; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('floor', '<div class="error">', '</div>'); ?>
						<div class="error" id="floor_error2"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Deposit</td>
					<td>
						<input type="text" name="deposit" value="<?php echo $deposit; ?>" class="buy_price text-field"/>
						<select name="management_currency" class="select-field currency">
							<!-- <option <?php echo set_select('management_currency',"USD", False); ?> value="USD">USD($)</option> -->
							<option <?php echo set_select('management_currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('deposit', '<div class="error">', '</div>'); ?>
						<div class="error" id="deposit_error1"></div>
					</td>
					<td class="td_label">Size</td>
					<td>
						<input type="text" name="size" value="<?php echo $size; ?>" class="size text-field"/><label class="lbl_size">m&sup2;</label>
						<?php echo form_error('size', '<div class="error">', '</div>'); ?>
						<div class="error" id="size_error2"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Landlord fee</td>
					<td>
						<input type="text" name="landlord_fee" value="<?php echo $landlord_fee; ?>" class="buy_price text-field"/>
						<select name="maintenance_currency" class="select-field currency">
							<!-- <option <?php echo set_select('maintenance_currency',"USD", False); ?> value="USD">USD($)</option> -->
							<option <?php echo set_select('maintenance_currency',"JPY", False); ?> value="JPY">JPY(¥)</option>
						</select>
						<?php echo form_error('landlord_fee', '<div class="error">', '</div>'); ?>
						<div class="error" id="landlord_fee_error1"></div>
					</td>
					<td class="td_label">Land Rights</td>
					<td>
						<input type="text" value="<?php echo $land_right; ?>" name="land_right" class="land_rights text-field"/>
						<?php echo form_error('land_right', '<div class="error">', '</div>'); ?>
						<div class="error" id="land_right_error2"></div>
					</td>
				</tr>

				<tr>
					<td class="td_label">Maked<span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="text"  id="iron1" name="maked" value="<?php echo $maked; ?>"  class="status text-field"/>
						<?php echo form_error('maked', '<div class="error">', '</div>'); ?>
						<div class="error" id="maked_error1"></div>
					</td>
					<td class="td_label">Type</td>
					<td class="tb_input">
						<input type="radio" name="type" value="house" <?php echo ($type == "house" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">House</label>
						<input type="radio" name="type" value="apartment" <?php echo ($type == "apartment" ? "checked" : ""); ?> class="status radio-field"/><label class="lbl_status">Apartment</label>
					</td>
				</tr>

				<tr>
					<td class="td_label">Location <span class="required_field">*</span></td>
					<td class="tb_input">

						<select id="short_location_perfecture" name="location_perfecture" class="select-field perfecture">
							<option value="">Perfecture</option>
							<?php foreach($select_perfectures as $select_perfecture){ ?>
								<option <?php echo ($location_perfecture == $select_perfecture->id ? "selected" : ""); ?> value="<?php echo $select_perfecture->id?>"><?php echo $select_perfecture->perfecture_name; ?></option>
							<?php } ?>
						</select>

						<select id="short_location_city" name="location_city" class="select-field city">
							<option value="">City</option>
							<?php foreach($select_perfectures_citys as $select_perfectures_city){ ?>
								<option <?php echo ($location_city == $select_perfectures_city->id ? "selected" : ""); ?> class="<?php echo $select_perfectures_city->perfecture_id_fk; ?>" value="<?php echo $select_perfectures_city->id?>"><?php echo $select_perfectures_city->city_name; ?></option>
							<?php } ?>
						</select>

						<?php echo form_error('location_perfecture', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_perfecture_error_short_stay"></div>

						<?php echo form_error('location_city', '<div class="error">', '</div>'); ?>
						<div class="error" id="location_city_error_short_stay"></div>

					</td>
					<td class="td_label">Land Area</td>
					<td>
						<input type="text" name="land_area" value="<?php echo $land_area; ?>" class="land_area text-field"/>
					</td>
				</tr>

				<tr>
					<td class="td_label">Gross Yield</td>
					<td class="tb_input">

						<select name="gross_yield" class="select-field gross_yield" id="select-gross_yield2" placeholder="Select or type...">
							<option value=""></option>
							<?php foreach($select_option_gross_yield_tool_properties as $select_option_gross_yield_tool_propertie){ ?>
								<option <?php echo ($gross_yield == $select_option_gross_yield_tool_propertie->gross_yield ? "selected" : ""); ?> value="<?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?>"><?php echo $select_option_gross_yield_tool_propertie->gross_yield; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="td_label">Occupancy</td>
					<td>
						<input type="text" name="occupancy" value="<?php echo $occupancy; ?>" class="occupancy text-field"/>
					</td>
				</tr>

				<tr>
					<td class="td_label">Nearest Station<span class="required_field">*</span></td>
					<td class="tb_input">
						<input type="text" name="nearest_station" value="<?php echo $nearest_station; ?>" class="buy_nearest_station text-field"/>
						<select class="text-field distance_to_statition" name="distance_to_station" placeholder="Distance to Station" >
							<option  value="">Distance to Station</option>
							<option <?php echo (($distance_to_station==3) ? "selected" : set_select('distance_to_station',"3", False)); ?>  value="3">Within 3 minutes</option>
							<option <?php echo (($distance_to_station==5) ? "selected"  : set_select('distance_to_station',"5", False)) ; ?>  value="5">Within 5 minutes</option>
							<option <?php echo (($distance_to_station==7) ? "selected"  : set_select('distance_to_station',"7", False)); ?>  value="7">Within 7 minutes</option>
							<option <?php echo (($distance_to_station==10) ? "selected" : set_select('distance_to_station',"10", False)); ?> value="10">Within 10 minutes</option>
							<option <?php echo (($distance_to_station==15) ? "selected" : set_select('distance_to_station',"15", False)); ?> value="15">Within 15 minutes</option>
							<option <?php echo (($distance_to_station==20) ? "selected" : set_select('distance_to_station',"20", False)); ?> value="20">Within 20 minutes</option>
						</select>
						<div class="error" id="distance_to_station_error2"></div>
						<?php echo form_error('distance_to_station', '<div class="error">', '</div>'); ?>

					</td>
				</tr>
			</table>

			<table class="table_properties_info ">
			 	<tr>
					<td class="td_label">Layout</td>
					<td class="tb_input_add">
						<select name="layout" class="select-field layout" id="select-layout2" placeholder="Select or type...">
							<option value="">Layout</option>
							<?php foreach($select_layout as $key => $value){ ?>
								<option <?php echo (($layout == $key) ? "selected" : set_select('layout',$key, False)); ?> value="<?php echo  $key ; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					    <?php echo form_error('layout', '<div class="error">', '</div>'); ?>
						<div class="error" id="layout_error2"></div>
					</td>
					<td class="td_label" rowspan="4">Images</td>
					 <td  class="tb_input_add" rowspan="4">
						<input type="button" class="choose_image choose_image_upload" name="choose" value="Choose a file to upload">
						<div class="content_select">
						<input type="checkbox" name="check_all_image" class="check_all_image" id="short_check_all_image" value="check"><label class="lbl_sellect_all">Select All</label>
						<input type="button" name="delete_image" class="delete_image" value="Delete Selected">
						</div>
						<div class="clear"></div>
						<input name="file_image[]" type="file" id="short_file_image1" accept="image/*" multiple class="file-image" />
						<input type="hidden" class="image_name_lists" value="<?php echo $image_name_lists; ?>" name="image_name_lists">
						<input type="hidden" id="short_primary_image" class="primary_image" name="primary_image">
						<div class="floor_content">
					        <div class="text_floor_plan">Floor Plan Image</div>
					        <span>
								<input type="button" class="choose_img_floor img_floor_upload" name="choose_floor" value="Change">
							</span>
						<div class="content_floor_plan">
						<div class="content_floor_img">
							<input type="file" name="floor_plan_image" class="upload_floor_img" accept="image/*" id="short_upload_floor_img">
							<input type="hidden" class="planfloor_image" value="<?php echo $floorplan; ?>" id="short_planfloor_image">
						</div>
						</div>
					</td>
			    </tr>
			    <tr>
					<td class="td_label">Year Build</td>
					<td class="tb_input_add">
						<select name="year_build" id="building_year_short_stay" class="select-field year_build">
							<option value="">Year Build</option>
							<?php for($i=date('Y'); $i>1899; $i--) { ?>
							<option <?php echo ($year_build == $i ? "selected" : set_select('year_build', $i, False)); ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
						<?php echo form_error('year_build', '<div class="error">', '</div>'); ?>
						<div class="error" id="year_build_error_short_stay"></div>
					</td>
				</tr>
				<tr>
					<td class="td_label">Zoning</td>
					<td class="tb_input_add">
						<input type="text" name="zoning" value="<?php echo $zoning; ?>" class="zoning text-field"/>
					</td>
				</tr>
				<tr>
					<td class="td_label">Parking</td>
					<td class="tb_input_add">
						<select name="parking" class="select-field parking" id="select-parking_short_stay" placeholder="Select or type...">
							<option value="">Parking</option>
							<?php foreach($select_option_parking_tools_properties as $select_option_parking_tools_propertie){ ?>
								<option <?php echo ($parking == $select_option_parking_tools_propertie->parking ? "selected" : set_select('parking',$select_option_parking_tools_propertie->parking, False)) ; ?> value="<?php echo $select_option_parking_tools_propertie->parking; ?>"><?php echo $select_option_parking_tools_propertie->parking; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label">Balcony Size</td>
					<td class="tb_input_add">
						<input type="text" name="balcony_size" value="<?php echo $balcony_size; ?>" class="zoning text-field"/>
					</td>
					<td class="td_label">
						360<sup>&deg;</sup> Images
					</td>
					<td class="tb_input_add" colspan="3">
						<div class="field_video_image">
							<input type="text" class="video_id" id="video_id_shortstay" name="video_id" value="<?php echo $video_id; ?>" >
							<input type="button" class="upload_video_id" id="upload_video_id_shortstay" name="upload_video_id" value="Show">
						</div>
						<iframe name="someFrame" <?php if($video_id == ""){ ?>style="display:none;"<?php } ?> src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" id="get_video_rent" width="308" height="260"></iframe>
					</td>
				</tr>
				<tr>
					<td class="td_label">Date Updated</td>
					<td class="tb_input_add">
						<input type="text" name="date_updated" value="<?php echo $date_updated; ?>" id="shortstay_date_updated" class="date_updated text-field">
					</td>
					<td class="td_label" rowspan="3">Map</td>
					<td class="tb_input_add"  rowspan="3">
						<div style="height: 270px;">
							<div class="latitude">Latitude: <label  id="short_label_lat" class="lbl_latitude"></label> </div>
							<div class="longitude">Longitude: <label id="short_label_lng" class="lbl_longitude"></label></div>
							<input type="hidden" id="short_getlat" name="latitude" value="">
							<input type="hidden" id="short_getlng" name="longitude" value="">
							<!-- <a class="reset" href="#">Reset</a><div class="clear"></div> -->
							<div style="height: 235px; width:308px;  margin-top: 16px;" id="short_map"></div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="td_label">Next Update Schedule</td>
					<td class="tb_input_add">
						<input type="text" name="next_update_schedule" value="<?php echo $next_update_schedule; ?>" id="shortstay_next_update_schedule" class="next_update_schedule text-field">
					</td>
				</tr>
				<tr>
					<td class="td_label">Other Expenses</td>
					<td class="tb_input_add">
						<textarea name="other_expenses" rows="3" cols="36"><?php echo $other_expenses; ?></textarea>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Feature
					</td>
					<td colspan="4">
						<table class="feature">
							<tr>
								<?php 
									$i = 0;
									foreach($select_features as $select_feature){ 
										$checkeds="";
										if(count($arr_features) > 0){
											foreach($arr_features as $arr_feature){
												if($select_feature->feature_value == $arr_feature){
													$checkeds = "checked";
												} 
											}
										}
								?>
									<td>
										<input type="checkbox" name="feature[]" <?php echo set_checkbox('feature[]', $select_feature->feature_value, FALSE); ?> class="description_1" value="<?php echo $select_feature->feature_value; ?>" <?php echo $checkeds; ?>><label class="lbl_description"><?php echo $select_feature->feature_name; ?></label>
									</td>
								<?php 
										$i++;
										if($i%4 == 0){
											echo "</tr><tr>";
										}	
									} 
								?>
							</tr>
						</table>

					</td>
				</tr>

			</table>


			<table class="table_properties">
				<tr>
					<td class="td_label">
						Description
					</td>
					<td colspan="3">
						<textarea name="description" class="ckeditor" id="description_shortstay" rows="5" cols="96"><?php echo $description; ?></textarea>
					</td>
				</tr>

				<tr>
					<td class="td_label">
						Transportation
					</td>
					<td colspan="3">
						<textarea name="transportation" class="ckeditor" id="transportation_shortstay" rows="5" cols="96"><?php echo $transportation; ?></textarea>
					</td>
				</tr>
			</table>

			<div class="tools_save_cancel">
				<input type="hidden" name="id" value="<?php echo $id;?>">
				<input type="hidden" name="status" value="3">
				<input type="submit" name="save_update" class="btn_save" value="Save">
				<input type="submit" name="cancel" class="btn_cancel" value="Cancel">
			</div>
		</form>
		</div>
		<?php } ?>
	</div>
