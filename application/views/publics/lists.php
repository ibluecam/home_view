<div class="content_list">
	<input type="hidden" value="<?php echo $link_status; ?>" class="hide_link_status"/>
	<div class="content_menu_list">
		<span class="text_list">LIST SEARCH</span>&nbsp;
		<span><a href="<?php if($link_to_map) echo $link_to_map; ?>" class="link_list">SEARCH BY MAP</a></span>
	</div>
	<div class="data_top">
		<div class="data_top_left">
			<span class="text_sort">SORT BY:</span> 
			<span class="content_link_sort">
			    <span class="span_sort">
			    	<span>Price</span>
				    <a href="<?php echo $link_to_list; ?>stp&ced=desc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
				    <a href="<?php echo $link_to_list; ?>stp&ced=asc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
			    </span>
			    <span class="span_sort">
			    	<span>Size</span>
				    <a href="<?php echo $link_to_list; ?>stb&ced=desc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
				    <a href="<?php echo $link_to_list; ?>stb&ced=asc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
			    </span>
			    <span class="span_sort">
			    	<span>Age</span>
				    <a href="<?php echo $link_to_list; ?>sta&ced=asc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
				    <a href="<?php echo $link_to_list; ?>sta&ced=desc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
			    </span>
			    <span class="span_sort">
			    	<span>Distance to Station</span>
				    <a href="<?php echo $link_to_list; ?>std&ced=desc"><img src="/assets/img/icon/sort_up.png" alt=""></a>
				    <a href="<?php echo $link_to_list; ?>std&ced=asc"><img src="/assets/img/icon/sort_down.png" alt=""></a>
			    </span>
			</span>
		</div>
		<?php if(isset($get_perfecture_name)){ ?>
		<div class="data_top_right">
			<i class="fa fa-map-marker map_icon" aria-hidden="true"></i>
			<span class="text_perfecture"><?php echo $get_perfecture_name; ?> PERFECTURE <?php if(!isset($get_city_name)){ ?>(<span class="total_number"><?php echo $count_city; ?></span>)<?php }else{ ?>: <?php } ?></span>
			<span class="text_city"><?php if(isset($get_city_name)){ echo $get_city_name; ?> 
				(<span class="total_number"><?php echo $count_city; ?></span>)
				<?php } ?>
			</span>
		</div>
		<?php } ?>
		<div class="clear"></div>
	</div>
	<div class="content_list_data">

		<div class="content_filter">
			<div class="text_filter">FILTER</div>
			<form action="/lists" method="get">
				<div class="title_text">Type</div>
				<div class="frm_filter">
					<table>
						<tr>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="1k" id="1k" <?php if(isset($typecheck) && in_array('1k', $typecheck)){ echo "checked";} ?>><label for="1k"> 1K</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="1dk" id="1dk" <?php if(isset($typecheck) && in_array('1dk', $typecheck)){ echo "checked";} ?> ><label for="1dk"> 1DK</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="1ldk" id="1ldk" <?php if(isset($typecheck) && in_array('1ldk', $typecheck)){ echo "checked";} ?> ><label for="1ldk"> 1LDK</label></td>
						</tr>
						<tr>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="2k" id="2k" <?php if(isset($typecheck) && in_array('2k', $typecheck)){ echo "checked";} ?> ><label for="2k"> 2K</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="2dk" id="2dk" <?php if(isset($typecheck) && in_array('2dk', $typecheck)){ echo "checked";} ?> ><label for="2dk"> 2DK</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="2ldk" id="2ldk" <?php if(isset($typecheck) && in_array('2ldk', $typecheck)){ echo "checked";} ?> ><label for="2ldk"> 2LDK</label></td>
						</tr>
						<tr>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="3k" id="3k" <?php if(isset($typecheck) && in_array('3k', $typecheck)){ echo "checked";} ?> ><label for="3k"> 3K</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="3dk" id="3dk" <?php if(isset($typecheck) && in_array('3dk', $typecheck)){ echo "checked";} ?> ><label for="3dk"> 3DK</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="3ldk" id="3ldk" <?php if(isset($typecheck) && in_array('3ldk', $typecheck)){ echo "checked";} ?> ><label for="3ldk"> 3LDK</label></td>
						</tr>
						<tr>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="4k" id="4k" <?php if(isset($typecheck) && in_array('4k', $typecheck)){ echo "checked";} ?> ><label for="4k"> 4K</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="4dk" id="4dk" <?php if(isset($typecheck) && in_array('4dk', $typecheck)){ echo "checked";} ?> ><label for="4dk"> 4DK</label></td>
							<td><input type="checkbox" class="typecheck" name="typecheck[]" value="4ldk" id="4ldk" <?php if(isset($typecheck) && in_array('4ldk', $typecheck)){ echo "checked";} ?> ><label for="4ldk"> 4LDK</label></td>
						</tr>
					</table>
					<span class="custom_td"><input type="checkbox" class="typecheck" name="typecheck[]" value="1room" id="1room" <?php if(isset($typecheck) && in_array('1room', $typecheck)){ echo "checked";} ?> ><label for="1room"> 1 ROOM</label></span>
					<span class="custom_td"><input type="checkbox" class="typecheck" name="typecheck[]" value="more5k" id="more5k" <?php if(isset($typecheck) && in_array('more5k', $typecheck)){ echo "checked";} ?> ><label for="more5k"> More 5K</label></span>
				</div>
				<div class="title_text">Maked</div>
				<div class="frm_filter">
					<span class="maked"><input type="checkbox" class="makedcheck" name="makedcheck[]" value="iron" id="iron" <?php if(isset($makedcheck) && in_array('iron', $makedcheck)){ echo "checked";} ?> ><label for="iron">Iron</label></span>
					<span class="maked1"><input type="checkbox" class="makedcheck" name="makedcheck[]" value="wood" id="wood" <?php if(isset($makedcheck) && in_array('wood', $makedcheck)){ echo "checked";} ?> ><label for="wood">Wood</label></span>
					<span class="maked2"><input type="checkbox" class="makedcheck" name="makedcheck[]" value="other" id="other" <?php if(isset($makedcheck) && in_array('other', $makedcheck)){ echo "checked";} ?> ><label for="other">Other</label></span>
				</div>
				<div class="title_text">Position</div>
				<div class="frm_filter">
					<table class="position_td">
						<tr>
							<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="1f" id="1f" <?php if(isset($positioncheck) && in_array('1f', $positioncheck)){ echo "checked";} ?> ><label for="1f"> 1F</label></td>
							<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="more2f" id="more2f" <?php if(isset($positioncheck) && in_array('more2f', $positioncheck)){ echo "checked";} ?> ><label for="more2f"> More 2F</label></td>
						</tr>
						<tr>
							<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="topfloor" id="topfloor" <?php if(isset($positioncheck) && in_array('topfloor', $positioncheck)){ echo "checked";} ?> ><label for="topfloor"> Top Floor</label></td>
							<td><input type="checkbox" class="positioncheck" name="positioncheck[]" value="cornorroom" id="cornorroom" <?php if(isset($positioncheck) && in_array('cornorroom', $positioncheck)){ echo "checked";} ?> ><label for="cornorroom"> Cornor Room</label></td>
						</tr>
					</table>
				</div>
				<div class="title_text">Price</div>
				<div class="frm_filter center">
					<select class="min_price"  name="min_price">
					    <option value="">MIN</option>
					    <?php for($i = 0; $i < count($get_price); $i++) { ?>
							<option value="<?php echo $get_price[$i] ?>" <?php if(isset($get_min_price) && $get_min_price == $get_price[$i]) echo "selected"; ?>><?php echo number_format($get_price[$i]); ?></option>
						<?php } ?>
					</select>
					<span class="space">~</span>
					<select class="max_price" name="min_price">
					    <option value="">MAX</option>
					    <?php for($i = 0; $i < count($get_price); $i++) { ?>
							<option value="<?php echo $get_price[$i] ?>" <?php if(isset($get_max_price) && $get_max_price == $get_price[$i]) echo "selected"; ?>><?php echo number_format($get_price[$i]); ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="title_text">Size</div>
				<div class="frm_filter center">
					<select name="min_breadth" class="min_breadth">
						<option value="">MIN</option>
						<?php for($i = 0; $i < count($get_breadth); $i++) { ?>
							<option value="<?php echo $get_breadth[$i] ?>" <?php if(isset($get_min_breadth) && $get_min_breadth == $get_breadth[$i]) echo "selected"; ?>><?php echo $get_breadth[$i]; ?></option>
						<?php } ?>
					</select>
					<span class="space">~</span>
					<select name="max_breadth" class="max_breadth">
						<option value="">MAX</option>
						<?php for($i = 0; $i < count($get_breadth); $i++) { ?>
							<option value="<?php echo $get_breadth[$i] ?>" <?php if(isset($get_max_breadth) && $get_max_breadth == $get_breadth[$i]) echo "selected"; ?>><?php echo $get_breadth[$i]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="title_text">Age</div>
				<div class="frm_filter center">
					<select name="min_age" class="min_age">
						<option value="">MIN</option>
						<?php for($i = 0; $i < count($get_age); $i++) { ?>
							<option value="<?php echo $get_age[$i] ?>" <?php if(isset($get_min_age) && $get_min_age == $get_age[$i]) echo "selected"; ?>><?php if($get_age[$i] == 0){echo "New";}else if($get_age[$i] == 1){echo $get_age[$i]." Year";}else{echo $get_age[$i]." Years";} ?></option>
						<?php } ?>
					</select>
					<span class="space">~</span>
					<select name="max_age" class="max_age">
						<option value="">MAX</option>
						<?php for($i = 0; $i < count($get_age); $i++) { ?>
							<option value="<?php echo $get_age[$i] ?>" <?php if(isset($get_max_age) && $get_max_age == $get_age[$i]) echo "selected"; ?>><?php if($get_age[$i] == 0){echo "New";}else if($get_age[$i] == 1){echo $get_age[$i]." Year";}else{echo $get_age[$i]." Years";} ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="title_text">Distance to Station</div>
				<div class="frm_filter center">
					<select name="min_distance_to_station" class="min_distance_to_station">
						<option value="">MIN</option>
						<?php for($i = 0; $i < count($get_distance); $i++) { ?>
							<option value="<?php echo $get_distance[$i] ?>" <?php if(isset($get_min_distance_to_station) && $get_min_distance_to_station == $get_distance[$i]) echo "selected"; ?>><?php echo $get_distance[$i]." minutes"; ?></option>
						<?php } ?>
					</select>
					<span class="space">~</span>
					<select name="max_distance_to_station" class="max_distance_to_station">
						<option value="">MAX</option>
						<?php for($i = 0; $i < count($get_distance); $i++) { ?>
							<option value="<?php echo $get_distance[$i] ?>" <?php if(isset($get_max_distance_to_station) && $get_max_distance_to_station == $get_distance[$i]) echo "selected"; ?>><?php echo $get_distance[$i]." minutes"; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="title_text">Other</div>
				<div class="frm_filter">
					<div class="content_other">
						<span class="span_left"><input type="checkbox" class="othercheck" name="othercheck[]" value="separate_toilet" id="separate_toilet" <?php if(isset($othercheck) && in_array('separate_toilet', $othercheck)){ echo "checked";} ?> ><label for="separate_toilet">Separate Toilet</label></span>
						<span class="span_right"><input type="checkbox" class="othercheck" name="othercheck[]" value="flooring" id="flooring" <?php if(isset($othercheck) && in_array('flooring', $othercheck)){ echo "checked";} ?> ><label for="flooring">Flooring</label></span>
					</div>
					<div class="content_other">
						<span class="span_left"><input type="checkbox" class="othercheck" name="othercheck[]" value="elevator" id="elevator" <?php if(isset($othercheck) && in_array('elevator', $othercheck)){ echo "checked";} ?> ><label for="elevator">Elevator</label></span>
						<span class="span_right"><input type="checkbox" class="othercheck" name="othercheck[]" value="air_conditioner" id="air_conditioner" <?php if(isset($othercheck) && in_array('air_conditioner', $othercheck)){ echo "checked";} ?> ><label for="air_conditioner">Air Conditioner</label></span>
					</div>
					<div class="content_other">
						<span class="span_left"><input type="checkbox" class="othercheck" name="othercheck[]" value="parking" id="parking" <?php if(isset($othercheck) && in_array('parking', $othercheck)){ echo "checked";} ?> ><label for="parking">Parking</label></span>
						<span class="span_right"><input type="checkbox" class="othercheck" name="othercheck[]" value="infrared_heater" id="infrared_heater" <?php if(isset($othercheck) && in_array('infrared_heater', $othercheck)){ echo "checked";} ?> ><label for="infrared_heater">Infrared Heater</label></span>
					</div>
				</div>
				<div class="frm_filter center_submit">
					<input type="button" value="GO" name="btn_go" class="btn_go" id="btn_go">
					<input type="button" value="Reset" class="btn_reset" id="btn_reset">
				</div>
			</form>
		</div>
		<div class="list_data">
			<?php if($get_lists && count($get_lists) >0 ){
				$ii = 0;
				foreach ($get_lists as $row) {
			?>
			<div class="data_home_list">
				<div class="data_description">
					<div class="content_company">
						<div class="company_name"><a href="/detail/<?php echo $row->id; ?>"><?php echo $row->building_name; ?></a></div>
						<div class="company_logo">
							<a href="/agent/<?php echo $row->user_id_fk; ?>">
								<?php if($row->logo != ""){ ?>
									<img src="/assets/img/uploads/company/crop/<?php echo $row->logo; ?>">
								<?php }else{ ?>
									<img src="/assets/img/uploads/company/crop/company_logo_default.jpg">
								<?php } ?>
							</a>
						</div>
						<div class="clear"></div>
					</div>
					<div class="detail_home">
						<div class="content_price">
							<p class="text_price"><?php echo ($row->status == 1) ? "PRICE" : "RENT"; ?></p>
							<p class="num_price"><?php if($row->price > 0){ echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price);}else{ echo "Ask";} ?></p>
							<?php if($is_logged_in){
								if($me->user_group != 3){/* Not Show Favorite Link */}
								else{ if(in_array($row->id, $fav_pro_id)){
							?>
								<a home_id='<?php echo $row->id; ?>' class='link_fav update_fav link_update<?php echo "_".$row->id; ?>' onclick="delete_fav('<?php echo $row->id; ?>')"><i class="fa fa-heart heart_icon update_fav<?php echo "_".$row->id; ?>" aria-hidden="true"></i> <span class="text_fav">FAVORITE</span></a>
							<?php }else{ ?>
								<a home_id="<?php echo $row->id; ?>" class="link_fav add_fav link_add<?php echo "_".$row->id; ?>" onclick="add_fav('<?php echo $row->id; ?>')"><i class="fa fa-heart-o heart_icon add_fav<?php echo "_".$row->id; ?>" aria-hidden="true"></i> <span class="text_fav">FAVORITE</span></a>
							<?php }}}else{ ?>
								<a href="/login" class="link_fav"><i class="fa fa-heart-o heart_icon" aria-hidden="true"></i> <span class="text_fav">FAVORITE</span></a>
							<?php } ?>
						</div>
						<div class="content_des">
							<table>
								<?php if($row->status != ""){ ?>
								<tr>
									<td>Status:</td>
									<td>
										<?php if($row->status == 1){ ?>
											Sale <span class="status_icon">(¥)</span>
										<?php } ?>
										<?php if($row->status == 2){ ?>
											Rent <span class="status_icon">(R)</span>
										<?php } ?>
										<?php if($row->status == 3){ ?>
											Short Stay <span class="status_icon">(S)</span>
										<?php } ?>
									</td>
								</tr>
								<?php } ?>
								<tr>
									<td>Perfecture:</td>
									<td><?php echo $row->perfecture_name; ?></td>
								</tr>
								<tr>
									<td>City:</td>
									<td><?php echo $row->city_name; ?></td>
								</tr>
								<?php if($row->size != ""){ ?>
								<tr>
									<td>Size:</td>
									<td><?php echo $row->size; ?> m<sup>2</sup></td>
								</tr>
								<?php } ?>
								<?php if($row->floor != ""){ ?>
								<tr>
									<td>Floor:</td>
									<td><?php echo $row->floor; ?></td>
								</tr>
								<?php } ?>
								<tr>
									<td><?php echo ($row->status == 1) ? "Price:" : "Rent:"; ?></td>
									<td class="td_price"><?php  if($row->price > 0){ echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price); }else{ echo "Ask";} ?></td>
								</tr>
							</table>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="data_img">
					<div class="big_img_list">
						<a href="/detail/<?php echo $row->id; ?>">
							<?php if($row->images_path != ""){ ?>
								<img src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>" alt="Image of <?php echo $row->building_name; ?>"/>
							<?php }else{ ?>
								<img src="/assets/img/uploads/default_image_properties.jpg" alt="No Image"/>
							<?php } ?>
						</a>
						<?php if($row->views != ""){ ?>
							<div class="count_view">
								<span class="num_count"><?php echo $row->views; ?></span>
								Views
							</div>
						<?php } ?>	
					</div>
					<div class="thumb_img_list">
					<?php 
						if(count($get_image_lists) > 0){    
							for($i = 0; $i < count($get_image_lists[$ii]); $i++) {
								 if($get_image_lists[$ii][$i]->properties_id_fk == $row->id){
						?>
						<div class="c_thumb"><a href="/detail/<?php echo $row->id ?>">
							<img src="/assets/img/uploads/properties/crop/<?php echo $get_image_lists[$ii][$i]->images_path; ?>" alt="Image Thumb" />
						</a></div>
					<?php 
								}
							}
						} 
					?>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<?php $ii++;}
			}else{ ?>
				<div class="not_found">
					<h2 style="margin-bottom: 10px;">Page Not Found</h2>
					<p>The page you're looking for cannot be found.</p>
				</div>
			<?php } ?>

			<div class="pagination">
				<?php echo $pagination; ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>

</div>