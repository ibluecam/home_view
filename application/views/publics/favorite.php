<div class="container_favorite">
	<?php $this->load->view('/templates/sidebar_admin');?>
	<div class="content_right">
	  <form  method="get">
		<div class="top_bar">
				<input class="vertical_middle" id="selectall" type="checkbox" name="checkall"/>
				<label for="selectall">Selecte All</label>
				<span class="count_chk">0</span> <span>Files Selected</span>
				<a href="#popup_delete" id="pop_delete">
					<button type="button" value="1" id="btn_delete" class="btn_delete">
						<i class="fa fa-trash" aria-hidden="true"></i>
						DELETE
				    </button>
				</a>
				<input class="btnsubmit" type="submit" value="DELETE"  name ="btndelete" style="display:none"/>
				<div class="btn_search">
			    	<input class="text_keyword" type="text" value="<?php echo (isset($_GET['keyword'])) ? $_GET["keyword"] : set_value('keyword'); ?>" placeholder="Keyword" name="keyword"/>
			    	<input type="submit" name="btnsearch" class="search" value="search"/>
				</div>
		</div>


		<div class="wrap_data_feature">
			<?php foreach ($favorites as $row) { ?>
			<div class="wrap_data">
				<div class="content_data">
					<div class="data_image">
				        <input class="data_check check_one_<?php echo $row->properties_id_fk; ?>" 
				        value="<?php echo  $row->properties_id_fk .','. $me->user_id;  ?>"
				        attr_id="<?php echo $row->properties_id_fk; ?>" type="checkbox" name="chk_favordite[]"/>

				        <?php  if($row->images_path !=""){ ?>
							<a href="/detail/<?php echo $row->properties_id; ?>"><img class="thumb_image" src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>"/></a>
						<?php }else{  ?>
							<a href="/detail/<?php echo $row->properties_id; ?>"><img class="thumb_image" src="/assets/img/uploads/properties/resize/default_image_properties.jpg"/></a>
						<?php } ?>
					</div>
					<div class="data_currency">
						<?php 
							if($row->properties_status==1){
									echo "¥";
								}else if($row->properties_status==2){
									echo "R";
								}else{
									echo "S";
						    }
						?>
					</div>
					<div class="data_title">
						<a href="/detail/<?php echo $row->properties_id; ?> "><?php echo strtoupper($row->building_name); ?></a>
					</div>
					<div class="data_favorite">
						<i class="fa fa-heart" aria-hidden="true"></i>
						FAVORITE 
					</div>
				</div>
				<div class="ruler_bottom"></div>
			</div>
			<?php } ?>
		</div>
		<div class="clear"></div>
	   <div class="pagination">
			<?php echo $pagination; ?>
	   </div>

	   <!-- popup confrim message -->
	   <div id="popup_delete">
	   		<div class="popup_header"></div>
	   		<div class="popup_footer">
	   			<input class="popup_btn popup_submit" type="button" value="DELETE"  name ="btndelete"/>
	   			<input class="popup_btn popup_cancel" type="submit" value="CANCEL"  name ="btncancel"/>
	   		</div>
	   </div>
	   <!-- end popup confrim message -->
	  </form>
	</div>
</div>
<div class="clear"></div>