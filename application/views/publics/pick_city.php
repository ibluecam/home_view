<div class="content_title"> 
	<div class="pick_search">
		<span>
			<i class="fa fa-search" aria-hidden="true"></i>
			FIND FROM AREA
		</span>
	</div>
	<div class="count_list">
		<span>
			<?php echo number_format($count_properties[0]->count_properties); ?>
			<span>List</span>
			<img src="/assets/img/icon/wifi.png"/>
		</span>
	</div>
	<div class="clear"></div>
</div>
<?php 
	if($link_status != ""){ 
		$status = "?sta=".$link_status;
	}else{
		$status = "";
	} 
?>
<div class="content_left_city">
	<a href="/city/tochigi<?php echo $status; ?>"><button type="button" class="btn_touchigi">SELECT CITIES<img class="image_right" src="/assets/img/icon/s_arrow_right.png"></button></a>
	<a href="/city/gunma<?php echo $status; ?>"><button type="button" class="btn_gunma">SELECT CITIES<img class="image_right" src="/assets/img/icon/s_arrow_right.png"></button></a>
	<a href="/city/ibaraki<?php echo $status; ?>"><button type="button" class="btn_ibaraki">SELECT CITIES<img class="image_right" src="/assets/img/icon/s_arrow_right.png"></button></a>
	<a href="/city/saitama<?php echo $status; ?>"><button type="button" class="btn_saitama">SELECT CITIES<img class="image_right" src="/assets/img/icon/s_arrow_right.png"></button></a>
	<a href="/city/tokyo<?php echo $status; ?>"><button type="button" class="btn_tokyo">SELECT CITIES<img class="image_right" src="/assets/img/icon/s_arrow_right.png"></button></a>
	<a href="/city/kanagawa<?php echo $status; ?>"><button type="button" class="btn_kanagawa">SELECT CITIES<img class="image_right" src="/assets/img/icon/s_arrow_right.png"></button></a>
	<a href="/city/chiba<?php echo $status; ?>"><button type="button" class="btn_chiba">SELECT CITIES<img class="image_right" src="/assets/img/icon/s_arrow_right.png"></button></a>
	
</div>
<div class="content_right">
	<div class="content_right_top">
		<div class="map_market_short">
			<a href="/map">
				 <img class="img" src="/assets/img/icon/map.png"/>
				 <span class="margin_left_7">MAP</span>
			</a>
		</div>
		<div class="map_market_short">
			<a href="/market-price">	
				<img class="img" src="/assets/img/icon/market_price.png"/>
				<span class="margin_left_7">NEW LISTING</span></a>
		</div>

		<?php if($this->uri->segment(2)=='buy'){ ?>
		<div class="map_market_short">
			<img class="img" src="/assets/img/icon/investment.png"/>
			<span class="margin_left_7">INVESTMENT</span>
		</div>
		<?php }else{ ?>

		<div class="map_market_short">
			<img class="img" src="/assets/img/icon/short_term.png"/>
			<span class="margin_left_7">SHORT TERM</span>
		</div>
	   <?php } ?>
		<div class="couple_top">
			<div class="data_couple">
				<div class="couple">
				<a href="skype://homeview?call">
					<img src="/assets/img/icon/m-skype.png"/>
					<p class="text_couple">ID:home view</p>
				</a>
				</div>
			</div>
			<div class="data_couple_no">
				<div class="couple">
				  <a href="viber://+818064501135">
					<img src="/assets/img/icon/m-viber.png"/>
					<p class="text_school">+818094462100</p>
				  </a>
				</div>
			</div>
		</div>

		<div class="couple_bottom">
			<div class="data_couple">
				<div class="couple">
					<img src="/assets/img/icon/m-line.png"/>
					<p class="text_new_constr">ID:home-view</p>
				</div>
		   </div>
		   	<div class="data_couple_no">
			<div class="couple">
				<img src="/assets/img/icon/m-mail.png"/>
				<p class="text_company">info@homeview.jp</p>
			</div>
		</div>
		</div>
	</div>
	<div class="company_info">
		<div class="data_info">
			<img class="img" src="/assets/img/icon/m-phone.png"/>
			<span>+82-32-264-5500</span>
		</div>
	</div>
</div>
<div class="clear"></div>

