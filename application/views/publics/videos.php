<div class="content_videos">

	<div class="content_videos_data">
		<div class="videos_data">
			<?php if(count($get_video_list) > 0){
				$i = 0;
					foreach ($get_video_list as $row) {
			?>
			<div class="cover_videos">
				<div class="content_image_videos">
					<a href="#popup_vdo<?php echo $i ?>" class="popup_vdo">
						<?php if($row->images_path != ""){ ?>
							<img src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>" alt="">
						<?php }else{ ?>
							<img src="/assets/img/uploads/properties/resize/default_image_properties.jpg" alt="">
						<?php } ?>
					</a>
				</div>
				<div class="title_videos">
					<a href="#popup_vdo<?php echo $i ?>" class="popup_vdo">360<sup>&deg;</sup> <?php echo $row->building_name; ?></a>
				</div>
			</div>

			<div class="content_popup<?php echo $i ?>" style="display:none;">
				<div id="popup_vdo<?php echo $i ?>">
						<div class="content_video">
							<iframe width="854" height="480" src="https://www.youtube.com/embed/<?php echo $row->video_id; ?>" frameborder="0" allowfullscreen></iframe>	
						</div>
				</div>
			</div>
			<?php $i++; }}else{ ?>
				<div class="not_found">
					<h2 style="margin-bottom: 10px;">Page Not Found</h2>
					<p>The page you're looking for cannot be found.</p>
				</div>
			<?php } ?>
			<div class="clear"></div>
			<div class="pagination">
				<?php echo $pagination; ?>
			</div>
		</div>
		<div class="list_adv">
			<div class="our_service">
				<div class="text_service">OUR SERVICES</div>
				<div class="short_stay">
						<img src="/assets/img/icon/sale.png" alt="">
						<br/>
						SHORT STAY
				</div>
				<div class="buy_service">
						<img src="/assets/img/icon/yen.png" alt="">
						<br/>
						BUY
				</div>
				<div class="rent_service">
						<img src="/assets/img/icon/rent.png" alt="">
						<br/>
						RENT
				</div>
				<div class="clear"></div>
			</div>
			<div class="advertise">
				<img src="/assets/img/advertise/adv.jpg" alt="Advertisment">
				<img src="/assets/img/advertise/adv1.jpg" alt="Advertisment">
				<img src="/assets/img/advertise/adv2.jpg" alt="Advertisment">
				<img src="/assets/img/advertise/adv3.jpg" alt="Advertisment">
			</div>
		</div>
		<div class="clear"></div>
	</div>

</div>