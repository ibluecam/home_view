

	<div class="container_favorite" id="wrap_content">
	<?php $this->load->view('/templates/sidebar_admin');?>
		<div class="content">
			<div class="tool_search_select">
				<div class="text_myinfo">
					MY INFO
				</div>
			</div>	
			<?php if(count($user_info) > 0){ ?>
			<div class="show_content_info">
				<div class="content_info">
				<form action="/favorite/info/edit/<?php echo $user_id; ?>" method="POST">
					<table class="table_info">
						<tr>
							<td>First Name</td>
							<td><?php echo $user_info->first_name; ?></td>
						</tr>
						<tr>
							<td>Last Name</td>
							<td><?php echo $user_info->last_name; ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?php echo $user_info->email; ?></td>
						</tr>
						<tr>
							<td>Passwords</td>
							<td>*******</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input type="submit" name="edit" class="button_edit_info" value="Edit Info">
							</td>
						</tr>
					</table>
					</form>		
				</div>

			</div>
		<?php }else{ ?>
			<h4>Data not found</h4>
		<?php } ?>

		</div><div class="clear"></div>
	</div>
