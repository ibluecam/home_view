
<div class="content_select_city">
	<div class="content_menu_city">
		<a data-tab="tabs-all" class="tab-link all_tabs <?php if($name_city == "all") echo 'current'; ?>">ALL</a>
		<?php if($perfecture_lists && count($perfecture_lists) > 0){
			foreach ($perfecture_lists as $row) {
		?>
		<a data-tab="tabs-<?php echo $row->perfecture_name; ?>" class="tab-link <?php if($name_city == strtolower($row->perfecture_name)) echo 'current'; ?>"><?php echo $row->perfecture_name; ?></a>
		<?php }} ?>
	</div>

	<div class="content_check_city">
	<form action="/lists" method="POST">
		<input type="hidden" value="<?php echo $status; ?>" name="citystatus">
		<?php if($perfecture_lists && count($perfecture_lists) > 0){
			foreach ($perfecture_lists as $row) {
		?>
		<div class="c_<?php echo strtolower($row->perfecture_name);  ?> content_full_check <?php if($name_city !== strtolower($row->perfecture_name) && $name_city !== "all"){ echo 'tab-content'; } ?>" id="tabs-<?php echo $row->perfecture_name; ?>">
			<div class="content_img_map img_map_<?php echo strtolower($row->perfecture_name);  ?>">
				<div class="map_<?php echo strtolower($row->perfecture_name);  ?>">
					<img src="<?php echo $row->perfecture_icon; ?>" alt="<?php echo $row->perfecture_name; ?>">
					<p><?php echo $row->perfecture_name; ?></p>
					<p>Prefecture</p>
				</div>
			</div>
			<div class="content_check chk_<?php echo strtolower($row->perfecture_name);  ?>">
				<table>
					<tr>
						<?php 
							if(count($get_citys) > 0){
								$i = 0;
								foreach ($get_citys as $city) {
								if($city->perfecture_id == $row->id){
								$i++;
						?>
							<td><input type="checkbox" name="checkcity[]" value="<?php echo $city->id ?>" id="<?php echo $city->city_name.$city->id; ?>" />
							<label for="<?php echo $city->city_name.$city->id; ?>"> <?php echo $city->city_name ?> (<span><?php echo $city->count_city; ?></span>)</label></td>
						<?php if($i%3 == 0){ ?>
							</tr>
							<tr>
						<?php		
									}
								}
							}
						} 
						?>
					</tr>
				</table>
			</div>
			<div class="clear"></div>
		</div>
		<?php }} ?>
		<div class="content_btn_city">
			<input type="submit" name="btn_go_city" class="btn_go_city" value="GO" />
		</div>
	</form>
	</div>

</div>