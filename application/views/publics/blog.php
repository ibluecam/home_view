	<div class="content_blog">
		<div class="blog_data page_size">
			<div class="blog_link">
				<span><a href="/blog/all" class="<?php if($blog_link_active == 'all'){echo 'blog_link_active';} ?>">ALL</a></span>
				<span><a href="/blog/news" class="<?php if($blog_link_active == 'news'){echo 'blog_link_active';} ?>">NEWS</a></span>
				<span><a href="/blog/buy" class="<?php if($blog_link_active == 'buy'){echo 'blog_link_active';} ?>">BUY</a></span>
				<span><a href="/blog/rent" class="<?php if($blog_link_active == 'rent'){echo 'blog_link_active';} ?>">RENT</a></span>
				<span><a href="/blog/living" class="<?php if($blog_link_active == 'living'){echo 'blog_link_active';} ?>">LIVING</a></span>
			</div>
			<div class="blog_data_left">
				<?php if(isset($get_blogs) && count($get_blogs) > 0){
					foreach ($get_blogs as $row) {
				?>
				<div class="content_data_blog">
					<div class="data_image_blog">
						<a href="/blog-detail/<?php echo $row->id; ?>">
							<?php if($row->feature_image !== ""){ ?>
								<img src="/assets/img/uploads/blog/thumb/<?php echo $row->feature_image; ?>">
							<?php }else{ ?>
								<img src="/assets/img/uploads/blog/thumb/default_feature_image.jpg">
							<?php } ?>
						</a>
					</div>
					<div class="data_description_blog">
						<p class="blog_title">
							<?php 
								$count_str_title = strlen($row->title);
								if($count_str_title > 58){
									echo substr($row->title, 0,58)."...";
								}else{
									echo $row->title;
								}
							?>
						</p>
						<p class="blog_date">
							<?php 
								$time = strtotime($row->created_dt);
								$newformat = date('Y m d',$time);
								echo $newformat;
							?>
						</p>
						<div class="blog_description">
							<?php 
								$count_str_body = strlen(strip_tags($row->body));
								if($count_str_body > 150){
									echo substr(strip_tags($row->body), 0,150);
								?>
								<span><a href="/blog-detail/<?php echo $row->id; ?>">learn more...</a></span>
								<?php }else{
									echo strip_tags($row->body);
								}
							?>
						<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<?php }}else{ ?>
				<div class="not_found">
					<h2 style="margin-bottom: 10px;">Page Not Found</h2>
					<p>The page you're looking for cannot be found.</p>
				</div>
				<?php } ?>
				<div class="pagination">
					<?php echo $pagination; ?>
				</div>
			</div>
			<div class="blog_data_right">
				<div class="our_service">
					<div class="text_service">OUR SERVICES</div>
					<div class="short_stay">
							<img src="/assets/img/icon/sale.png" alt="">
							<br/>
							SHORT STAY
					</div>
					<div class="buy_service">
							<img src="/assets/img/icon/yen.png" alt="">
							<br/>
							BUY
					</div>
					<div class="rent_service">
							<img src="/assets/img/icon/rent.png" alt="">
							<br/>
							RENT
					</div>
					<div class="clear"></div>
				</div>
				<div class="data_right_blog">
					<img src="/assets/img/advertise/adv.jpg" alt="Advertisment">
					<img src="/assets/img/advertise/adv1.jpg" alt="Advertising">
					<img src="/assets/img/advertise/adv2.jpg" alt="Advertising">
					<img src="/assets/img/advertise/adv3.jpg" alt="Advertising">
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>