<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="description" content="" />

		

	<title>Home View</title>

	

	<link rel="shortcut icon" type="image/png" href="/assets/img/icon/favo16.png"/>



	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

	<link href="/assets/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/publics/home.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css"/>

</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1030846260272871";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div class="wrapper">



	<!-- Block Header -->



	<div class="contain_header">

		<div class="content_logo page_size">

			<div class="logo"><a href="/"><img src="/assets/img/icon/home_logo.svg" alt="Home View Logo"></a></div>

		</div>

		<div class="content_menu page_size">

			<div class="nav_menu">
<!-- <a href="http://line.me/R/msg/text/?LINE%20it%21%0d%0ahttp%3a%2f%2fline%2enaver%2ejp%2f"><img src="[URL of Button image]" width="[width of Button]" height="[height of Button]" alt="LINE it!" /></a> -->
				<div class="menu">

					<ul>

						<li><a href="/" class="list_menu">HOME</a></li>

						<li><a href="/lists/buy" class="list_menu">BUY</a></li>

						<li><a href="/lists/rent" class="list_menu">RENT</a></li>

						<!-- <li><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" class="list_menu" target="blank">FACE BOOK</a></li> -->

						<li><a href="/videos" class="list_menu">360<sup>&deg;</sup> IMAGES</a></li>

					</ul>

				</div>

				<div class="login_register">

					<ul>

						<?php if(!$is_logged_in){ ?>

						<li><a href="/login" class="list_menu">LOGIN</a></li>

						<li>|</li>

						<li><a href="/register" class="list_menu">REGISTER</a></li>

						<?php }else{ ?>

						<li>

							<a href="/<?php echo $chk_group_user; ?><?php echo $me->user_id;  ?>" class="list_menu account_name">

								<i class="fa fa-user account_icon" aria-hidden="true"></i> 

								Hello! <?php echo $chk_user_group_name;  ?>

							</a>

						</li>

						<li><a href="/login/logout" class="list_menu">Log out <i class="fa fa-power-off" aria-hidden="true"></i></a></li>

						<?php } ?>

					</ul>

				</div>

				<div class="clear"></div>

			</div>

			<div class="clear"></div>

		</div>

		<div class="page_size">

			<div class="top_text">

				FIND A HOME IN JAPAN

			</div>

			<div class="menu_buy_rent">

				<a href="/pick-city/buy">BUY</a>

				<a href="/pick-city/rent">RENT</a>

				<a href="/pick-city/short-stay">SHORT STAY</a>

			</div>

			<div class="tooltip"><p><span class="spacing">ENGLISH</span> <span class="spacing2">SPEAKERS</span> <span class="spacing3">AVAILABLE</span></p></div>

			<div class="contact">

				<div class="contact_left">

					<span class="phone">

						<img src="/assets/img/icon/phone_b.png" alt="Phone">

						<span>+81 (0)3 6277 7223</span>

					</span>

					<span class="email">

						<img src="/assets/img/icon/email.png" alt="Email">

						<span>INFO@HOMEVIEW.JP</span>

					</span>

				</div>

				<div class="contact_right">

					<span class="facebook">

						<img src="/assets/img/icon/face.png" alt="Skype">

						<span><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" target="blank">HOME VIEW</a></span>

					</span>

					<span class="skype">

						<img src="/assets/img/icon/skype.png" alt="Skype">

						<span><a href="skype://live:shimanekennow?call">HOME VIEW</a></span>

					</span>

					<span class="viber">

						<img src="/assets/img/icon/viber.png" alt="Viber">

						<span><a href="viber://+8108094462100">+81 080 9446 2100</a></span>

					</span>

					<span class="line">

						<img src="/assets/img/icon/line.png" alt="Line">

						<span><a line-id="home-view">home-view</a></span>

					</span>

				</div>

				<div class="clear"></div>

			</div>

		</div>

	</div>

	<!-- End Block Header -->



			<div class="content_body">

				<div class="content_feature">

					<div class="feature_data page_size">

						<div class="feature_text">

							MOST POPULAR HOMES

						</div>

						<div class="feature_data_left">

							<div class="wrap_data_feature">

								<?php if($get_home && count($get_home) > 0){

									foreach ($get_home as $row) {

								?>

									<div class="wrap_data">

										<div class="content_data">

											<div class="data_image">

												<a href="/detail/<?php echo $row->id; ?>">

													<?php if($row->images_path != ""){ ?>

														<img src="/assets/img/uploads/properties/thumb/<?php echo $row->images_path; ?>" alt="Image of <?php echo $row->building_name; ?>">

													<?php }else{ ?>

														<img src="/assets/img/uploads/default_image_properties.jpg" alt="No Image">

													<?php } ?>

												</a>

												<?php if($row->views != ""){ ?>

													<div class="count_view">

														<span class="num_count"><?php echo $row->views; ?></span>

														Views

													</div>

												<?php } ?>

											</div>

											<div class="data_currency">

												<?php if($row->status == 1){ ?>

													&yen;

												<?php } ?>

												<?php if($row->status == 2){ ?>

													R

												<?php } ?>

												<?php if($row->status == 3){ ?>

													S

												<?php } ?>

											</div>

											<div class="data_title">

												<a href="/detail/<?php echo $row->id; ?>"><?php echo $row->building_name; ?></a>

											</div>

											<div class="data_description">

												<?php echo $row->city_name." ".$row->perfecture_name; ?>

											</div>

											<div class="data_price">

												<?php echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price); ?>

											</div>

										</div>

										<div class="ruler_bottom"></div>

									</div>

								<?php }}else{ ?>

									<div class="not_found">

										This property could not be found

									</div>

								<?php } ?>

								<div class="clear"></div>

							</div>

						</div>

						<div class="feature_data_right">

							<div class="data_right_top">

								<div class="text_service">OUR SERVICES</div>

								<div class="short_stay">

										<img src="/assets/img/icon/sale.png" alt="">

										<br/>

										SHORT STAY

								</div>

								<div class="buy_service">

										<img src="/assets/img/icon/yen.png" alt="">

										<br/>

										BUY

								</div>

								<div class="rent_service">

										<img src="/assets/img/icon/rent.png" alt="">

										<br/>

										RENT

								</div>

								<div class="clear"></div>

							</div>

							<div class="data_right_bottom">

								<img src="/assets/img/advertise/adv.jpg" alt="Advertising">

							</div>

						</div>

						<div class="clear"></div>

					</div>

				</div>

				<div class="content_place">

					<div class="page_size get_a_place">

						<div class="text_get_place">

							HOW TO GET A PLACE IN JAPAN

						</div>

						<div class="content_step">

							<div class="rent_step in_step">

								<div class="text_step">

									R

								</div>

								<div class="step">

									<div class="step_title">HOW TO RENT</div>

								</div>

								<div class="step link-step">

									<a href="/step/rent">DETAILS</a>

								</div>

							</div>

							<div class="buy_step in_step">

								<div class="text_step">

								&yen;

								</div>

								<div class="step">

									<div class="step_title">HOW TO BUY</div>

								</div>

								<div class="step link-step">

									<a href="/step/buy">DETAILS</a>

								</div>

							</div>

							<div class="short_stay_step in_step">

								<div class="text_step">

									S

								</div>

								<div class="step">

									<div class="step_title">HOW TO MAKE SHORT STAY</div>

								</div>

								<div class="step link-step">

									<a href="/step/short-stay">DETAILS</a>

								</div>

							</div>

							<div class="clear"></div>

						</div>

					</div>

				</div>

				<div class="content_blog">

					<div class="blog_data page_size">
						<!-- <div class="fb-page" data-href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" data-tabs="timeline" data-width="500" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
			    		<blockquote cite="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts">HOME VIEW</a></blockquote></div> -->


						<!-- <div class="blog_text">

							BLOG

						</div>

						<div class="blog_link">

							<span><a data_blog="all" class="blog_link_active link_blog">ALL</a></span>

							<span><a data_blog="news" class="link_blog">NEWS</a></span>

							<span><a data_blog="buy" class="link_blog">BUY</a></span>

							<span><a data_blog="rent" class="link_blog">RENT</a></span>

							<span><a data_blog="living" class="link_blog">LIVING</a></span>

						</div>

						<div class="blog_data_left">

							<?php if(isset($get_blogs) && count($get_blogs) > 0){

								foreach ($get_blogs as $row) {

							?>

							<div class="content_data_blog">

								<div class="data_image_blog">

									<a href="/blog-detail/<?php echo $row->id; ?>">

										<?php if($row->feature_image !== ""){ ?>

											<img src="/assets/img/uploads/blog/thumb/<?php echo $row->feature_image; ?>">

										<?php }else{ ?>

											<img src="/assets/img/uploads/blog/thumb/default_feature_image.jpg">

										<?php } ?>

									</a>

								</div>

								<div class="data_description_blog">

									<p class="blog_title">

										<?php 

											$count_str_title = strlen($row->title);

											if($count_str_title > 58){

												echo substr($row->title, 0,58).'...';

											}else{

												echo $row->title;

											}

										?>

									</p>

									<p class="blog_date">

										<?php 

											$time = strtotime($row->created_dt);

											$newformat = date('Y m d',$time);

											echo $newformat;

										?>

									</p>

									<div class="blog_description">

										<?php 

											$count_str_body = strlen(strip_tags($row->body));

											if($count_str_body > 150){

												echo substr(strip_tags($row->body), 0,150);

											?>

											<span><a href="/blog-detail/<?php echo $row->id; ?>">learn more...</a></span>

											<?php }else{

												echo strip_tags($row->body);

											}

										?>

									<div class="clear"></div>

									</div>

								</div>

								<div class="clear"></div>

							</div>

							<?php } ?>

							<?php if(count($get_blogs) >= 4){ ?>

								<div class="more_blog">

									<a href="/blog">MORE</a>

								</div>

						    <?php }}else{ ?>

								<div class="not_found">

									<h3 style="margin-bottom: 10px;">Page Not Found</h3>

									<p>The page you're looking for cannot be found.</p>

								</div>

							<?php } ?>

						</div>

						<div class="blog_data_right">

							<div class="data_right_blog">

								<img src="/assets/img/advertise/adv1.jpg" alt="Advertising">

								<img src="/assets/img/advertise/adv2.jpg" alt="Advertising">

								<img src="/assets/img/advertise/adv3.jpg" alt="Advertising">

							</div>

						</div>

						<div class="clear"></div>
 -->
					</div>

				</div>

			</div>

			<!-- Block Footer -->

		

			<div class="content_footer">

				<div class="content_contact">

					<div class="page_size contac_info">
						<div class="warp_text_contact">
						<div class="text_contact">

							CONTACT INFORMATION

						</div>

						<div class="txet_des_contact">

							Get the lastest news and properties in your inbox

						</div>

						<div class="form_email">

							<form>

								<input type="text" class="con_email" name="email" placeholder="Your Email">

								<br/>

								<input type="submit" name="subscribe" value="SUBSCRIBE" class="btn_sub">

							</form>

						</div>
						</div>

						<div class="content_fb_page">
							<div class="fb-page" data-href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" data-tabs="timeline" data-width="500" data-height="230" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
				    		<blockquote cite="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HOME-VIEW-632748560218900/?fref=ts">HOME VIEW</a></blockquote></div>
						</div>
						<div class="clear"></div>

					</div>

				</div>

				<div class="content_footer_bottom">

					<div class="page_size">

						<div class="footer_left">&copy;2016 Copyright Home View. All Rights Reserved. </div>

						<div class="footer_right">Designed by Softboom Co., LTD.</div>

						<div class="clear"></div>

					</div>

				</div>

			</div>

			<!-- End Block Footer -->

		</div>

		<script type="text/javascript" src="/assets/js/jquery.js"></script>

		<script type="text/javascript" src="/assets/js/publics/home.js?v=<?php echo time(); ?>"></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-88691267-1', 'auto');
		  ga('send', 'pageview');
		</script>
	</body>

</html>