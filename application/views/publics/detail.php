
<?php if(count($properties)>0){  ?>
<div class="title_detail">
	<?php 
		foreach ($properties as $rows) {
	?>
	<a id="back_link"><img class="arrow_img" src="/assets/img/icon/arrow-left.png"/>
		<span class="back_text">Back</span>
	</a>
	<div class="title_rigth">
		<span class="title"><?php echo strtoupper($rows->building_name ." ". $rows->properties_type." ". $rows->city_name); ?></span>
	</div>
</div>
<div class="container_content">
   <div class="container_left">
		<div class="content_left_detail">
			<div class="container_main">
				<?php  if($rows->images_path!=""){ ?>
		       <a class="fancyboxLauncher" rel="gallery1" href="/assets/img/uploads/properties/resize/<?php echo $rows->images_path;?>">
					<img class="main_img" src="/assets/img/uploads/properties/resize/<?php echo $rows->images_path; ?>"/>
				</a>
				<?php }else{ ?>
					<img class="main_img" src="/assets/img/uploads/properties/resize/default_image_properties.jpg"/>
				<?php } ?>
				<div class="title_map">
					<span class="contact_text">360<sup>&deg;</sup> IMAGE / MAP / FEATURE / DESCRIPTION</span>
				</div>
				<?php if($rows->video_id != ""){ ?>
					<div class="content_video">
						<iframe width="477" height="283" src="https://www.youtube.com/embed/<?php echo $rows->video_id ?>" frameborder="0" allowfullscreen></iframe>	
					</div>
				<?php } ?>
			     <span class="get_latlng" lat="<?php echo $rows->latitude; ?>" lng="<?php echo $rows->longitude; ?>"></span>
				<div id="map">

				</div>
				<div class="contain_feature">
					<ul>
					 <?php 
				        $i = 0;
				        foreach($select_features as $select_feature){ 
				          $checkeds="";
				          if(count($arr_features) > 0){
					          foreach($arr_features as $arr_feature){
					           if($select_feature->feature_value == $arr_feature){
					            $checkeds = "checked";
					           } 
					          }
					      }
				      ?>
				         <li>
				          <input type="checkbox"  <?php echo $checkeds; ?> class="description_1"/>&nbsp;
				          <label for="Elevator" class="text_feature"><?php echo $select_feature->feature_name ?></label>
				         </li>
				      <?php 
				          $i++;
				          if($i%4 == 0){
				           echo "</ul><ul>";
				          } 
				        } 
				    ?>
				</div>
				<div class="clear"></div>
				<?php if($rows->properties_description!=""){ ?>
				<div class="left_description">
					<?php echo preg_replace("/<img[^>]+\>/i", " ", $rows->properties_description); ?>
				</div>
				<?php } ?>
				<?php if($rows->transportation!="" || $rows->floorplan!=""){ ?>
				<div class="title_map">
					<span class="contact_text">FLOORPLAN / TRANSPORTATION</span>
				</div>
				<?php } ?>
				<?php if($rows->floorplan!=""){ ?>
				<div class="floorplan">
					<img src="/assets/img/uploads/properties/resize/<?php echo $rows->floorplan;?>">
				</div>
				<?php } ?>
				<?php if($rows->transportation!=""){ ?>
				<div class="transportation">
					<?php echo $rows->transportation;?>
				</div>
				<?php } ?>
			</div>
		</div>

		<div class="container_thumb">
			<div class="inner_thumb">
				<?php foreach ($properties_image as $image) { ?>
					<span class="thumb">
						<a class="fancyboxDetail" rel="gallery1" href="/assets/img/uploads/properties/resize/<?php echo $image->images_path;?>">
							<img class="smallimg" src="/assets/img/uploads/properties/crop/<?php echo $image->images_path; ?>"/>
						</a>
				    </span>
				<?php } ?>
			</div>
			<div class="contain_info_detail">
				<div class="header_detail">
					<span>DETAILS</span>
				</div>
				<div class="info_detail">
					<table>
						<tr>
							<td class="td_left">Price:</td>
							<td>&nbsp;</td>
							<td class="td_right color_red"><?php echo ($rows->currency=="USD")? "$" : "¥"; echo number_format($rows->price);?></td>
						</tr>
						<tr>
							<td class="td_left">Unit Number:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo $rows->unit_number;?></td>
						</tr>
						<tr>
							<td class="td_left">Floor:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo $rows->floor;?></td>
						</tr>
						<tr>
							<td class="td_left">Type:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo $rows->type;?></td>
						</tr>
						<tr>
							<td class="td_left">Size:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo $rows->size;?> m²</td>
						</tr>

						 <!-- block buy -->
						<?php if($rows->properties_status=="1"){ ?>
							<tr>
								<td class="td_left">Cross Yield:</td>
								<td>&nbsp;</td>
								<td class="td_right"><?php echo $rows->gross_yield;?>%</td>
							</tr>
							<tr>
								<td class="td_left">Land Rights:</td>
								<td>&nbsp;</td>
								<td class="td_right"><?php echo $rows->land_right;?></td>
							</tr>
							<tr>
								<td class="td_left">Occupancy:</td>
								<td>&nbsp;</td>
								<td class="td_right"><?php echo $rows->occupancy;?></td>
							</tr>
							<tr>
								<td class="td_left">Land Area:</td>
								<td>&nbsp;</td>
								<td class="td_right"><?php echo ($rows->land_area!="") ? $rows->land_area : "Ask";?></td>
							</tr>
						<?php } ?>

						<!-- block rent -->
						<?php if($rows->properties_status=="2" || $rows->properties_status=="3"){ ?>
						<tr>
							<td class="td_left">Key Money:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo ($rows->key_money!="") ? $rows->key_money : "Ask" ;?></td>
						</tr>
						<tr>
							<td class="td_left">Agency Fee:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo ($rows->agency_fee!="") ? $rows->agency_fee : "Ask";?></td>
						</tr>
						<tr>
							<td class="td_left">Deposit:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo ($rows->deposit!="") ? number_format($rows->deposit).'¥' : "Ask";?></td>
						</tr>
						<?php } ?>

						<tr>
							<td class="td_left">Location:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo $rows->city_name ." ".$rows->perfecture_name;?></td>
						</tr>
						<tr>
							<td class="td_left">Nearest Station:</td>
							<td>&nbsp;</td>
							<td class="td_right"><?php echo $rows->nearest_station;?></td>
						</tr>
					</table>
				</div>

				<div class="additional_detail">
					<span>ADDITIONAL DETAILS</span>
					<img class="arrow_img" src="/assets/img/icon/arrow-down.png"/>
				</div>
				<div class="info_additional_detail">
					<table>
						<tr>
							 <td class="add_td_left">Layout:</td>
							 <td class="add_td_right"><?php echo ($rows->layout!="") ? strtoupper($rows->layout) : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Year Built:</td>
							 <td class="add_td_right"><?php echo ($rows->year_build!="") ? $rows->year_build : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Maintainance Fee:</td>
							 <td class="add_td_right"><?php echo ($rows->maintainance_fee!="") ? "¥".$rows->maintainance_fee.'/mth' : 'Ask';?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Potential Annual Rent:</td>
							 <td class="add_td_right"><?php echo ($rows->potential_annual_rent!="") ? "¥".$rows->potential_annual_rent.'/year' : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Transaction Type:</td>
							 <td class="add_td_right"><?php echo ($rows->transaction_type!="") ? $rows->transaction_type : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Building Area Ratio:</td>
							 <td class="add_td_right"><?php echo ($rows->building_area_ratio!="") ? $rows->building_area_ratio : "Ask";?></td>
						</tr>

						<!-- block buy -->
						<?php if($rows->properties_status=="1"){ ?>
						<tr>
							 <td class="add_td_left">Floor Area Ration:</td>
							 <td class="add_td_right"><?php echo ($rows->floor_area_ratio!="") ? $rows->floor_area_ratio.'%' : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Zoning:</td>
							 <td class="add_td_right"><?php echo ($rows->zoning!="") ? $rows->zoning : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Structure:</td>
							 <td class="add_td_right"><?php echo ($rows->structure!="") ? $rows->structure : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Road Width:</td>
							 <td class="add_td_right"><?php echo ($rows->road_width!="") ? $rows->road_width : "Ask";?></td>
						</tr>
						<?php } ?>
						<!-- end block buy -->

					    <!-- block rent -->
					    <?php if($rows->properties_status=="2" || $rows->properties_status=="3"){ ?>
					    <tr>
							 <td class="add_td_left">Guarantor Company:</td>
							 <td class="add_td_right"><?php echo ($rows->guarantor_company!="") ? $rows->guarantor_company : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Guarantor Agency:</td>
							 <td class="add_td_right"><?php echo ($rows->guarantor_agency_name!="") ? $rows->guarantor_agency_name : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Building Style:</td>
							 <td class="add_td_right"><?php echo ($rows->building_style!="") ? $rows->building_style : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Lease Term:</td>
							 <td class="add_td_right"><?php echo ($rows->lease_term!="") ? $rows->lease_term : "Ask";?></td>
						</tr>
						<?php } ?>

						<!-- end block rent -->

						<!-- block short stay -->
						<?php if($rows->properties_status=="3"){ ?>
						<tr>
							 <td class="add_td_left">Credit Card Payment:</td>
							 <td class="add_td_right"><?php echo ($rows->credit_card_payment!="") ? $rows->credit_card_payment : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Short Term Stay:</td>
							 <td class="add_td_right"><?php echo ($rows->short_term_stay!="") ? $rows->short_term_stay : "Ask";?></td>
						</tr>
						<?php } ?>
						<!-- end block short stay -->

						<tr>
							 <td class="add_td_left">Available From:</td>
							 <td class="add_td_right"><?php echo ($rows->available_from!="") ? $rows->available_from : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Unit Summary:</td>
							 <td class="add_td_right"><?php echo ($rows->unit_summary!="") ? $rows->unit_summary : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Balcony Size:</td>
							 <td class="add_td_right"><?php echo ($rows->balcony_size!="") ? $rows->balcony_size : "Ask";?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Parking:</td>
							 <td class="add_td_right"><?php echo ($rows->parking!="") ? $rows->parking : "Ask";?></td>
						</tr>
						<?php if($rows->building_description!=""){ ?>
						<tr>
							 <td class="add_td_left">Building Description:</td>
						</tr>
						<tr>
							<td colspan="2" class="td_single"><?php echo ($rows->building_description!="") ? $rows->building_description : "Ask";?></td>
						</tr>
						<?php } ?>
						<?php if($rows->other_expenses!="" && $rows->properties_status==2){ ?>
						<tr>
							 <td class="add_td_left">Other Expenses:</td>
						</tr>
						<tr>
							<td colspan="2"  class="td_single"><?php echo ($rows->other_expenses!="") ? $rows->other_expenses : "Ask";?></td>
						</tr>
						<?php } ?>
						<?php if($rows->landmarks!=""){ ?>
						<tr>
							 <td class="add_td_left">Landmarks:</td>
						</tr>
						<tr>
							<td colspan="2"  class="td_single"><?php echo ($rows->landmarks!="") ? $rows->landmarks : "Ask";?></td>
						</tr>
						<?php } ?>

						<tr>
							 <td class="add_td_left">Date Updated:</td>
							 <td class="add_td_right"><?php echo date("Y-m-d",strtotime($rows->date_updated));?></td>
						</tr>
						<tr>
							 <td class="add_td_left">Next Update Schedule:</td>
							 <td class="add_td_right"><?php echo date("Y-m-d",strtotime($rows->next_update_schedule));?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<?php if(count($similar_location)>0){ ?>
		<div class="container_similar">
			<div class="similar_place_bar">
				<span class="similar_place_title_left">SIMILAR PLACES</span>
				<span class="similar_place_title_right"><a href="/lists?pf=<?php echo $similar_location[0]->location_perfecture ?>">MORE <img class="arrow_img" src="/assets/img/icon/arrow-right.png"/></span>
			</div>
			<div class="wrap_data_feature">
			  <?php foreach ($similar_location as $location) { ?>
				<div class="wrap_data">
					<div class="content_data">
						<div class="data_image">
							<?php  if($location->images_path !=""){ ?>
								<a href="/detail/<?php echo $location->properties_id; ?>"><img class="image_similar" src="/assets/img/uploads/properties/thumb/<?php echo $location->images_path; ?>"/></a>
							<?php }else{ ?>
								<a href="/detail/<?php echo $location->properties_id; ?>"><img class="image_similar" src="/assets/img/uploads/properties/thumb/default_image_properties.jpg"/></a>
							<?php } ?>
						</div>
						<div class="data_currency">
							<?php
								if($location->status==1){
									echo "¥";
								}else if($location->status==2){
									echo "R";
								}else{
									echo "S";
								}
							 ?>
						</div>
						<div class="data_title">
							<a href="/detail/<?php echo $location->properties_id; ?>"><?php echo strtoupper($location->building_name); ?></a>
						</div>
						<div class="data_description">
							<?php echo $location->city_name ." ". $location->perfecture_name; ?>
						</div>
						<div class="data_price">
							<?php echo ($location->currency=="USD")? "$" : "¥"; echo number_format($location->price);?>
						</div>
					</div>
					<div class="ruler_bottom"></div>
				</div>
			   <?php } ?>
			</div>
		</div>
		<?php  } ?>
	</div>

	<div class="content_right">

		<div class="company_info">
			<a href="/agent/<?php echo $rows->user_id_fk; ?>"><div class="logo_company">
				<?php if($rows->logo!=""){ ?>
					<img src="/assets/img/uploads/company/resize/<?php echo $rows->logo; ?>"/>
				<?php 
					}else{
						echo '<img src="/assets/img/uploads/company/resize/company_logo_default.jpg"/>';
					} 
				?>
			</a>
			</div>
			<div class="contact_info">
				<span class="contact_img"><a href="callto://+<?php echo $rows->skype_id; ?>"><img alt="skype" src="/assets/img/icon/skype.png"/></a></span>
				<span class="contact_img"><a href="viber://add?number=<?php echo $rows->viber_id; ?>"><img alt="viber" src="/assets/img/icon/viber.png"/></a></span>
				<span class="contact_img"><a href=""><img alt="line" src="/assets/img/icon/line.png"/></a></span>
			</div>
			<div class="company_name">
				<a class="agent_link" href="/agent/<?php echo $rows->user_id_fk; ?>">
					<span><?php echo $rows->company_name; ?></span>
				</a>
			</div>
			<div class="contact_detail">
				<?php if($rows->phone1!=""){ ?>
					<div class="contact_other">
						<img class="arrow_img" src="/assets/img/icon/phone.png"/>
						<span class="contact_text"><?php echo $rows->phone1; ?></span>
					</div>
				<?php } ?>
				<?php if($rows->phone2!=""){ ?>
					<div class="contact_other">
						<img class="arrow_img" src="/assets/img/icon/tel.png"/>
						<span class="contact_text"><?php echo $rows->phone2; ?></span>
					</div>
				<?php } ?>
				<?php if($rows->email!=""){ ?>
					<div class="contact_other">
						<img class="arrow_img" src="/assets/img/icon/message.png"/>
						<span class="contact_text"><?php echo $rows->email; ?></span>
					</div>
				<?php } ?>
			</div>
			<div class="description">
				<div id="show_description">
					<span class="title_descr">DESCRIPTION</span>
					<img class="arrow_img" src="/assets/img/icon/arrow-down.png"/>
				</div>
				<div class="text_description">
				<?php echo $rows->description; ?> 
				</div>
			</div>
		</div>

		<div class="contain_inquire">
			<span class="inquire_title">INQUIRE</span>
			<form id="frm_inquire" method="post">
				<input class="form_controll" type="text" name="name" placeholder="Name" />
				<input class="form_controll" type="text" name="email" placeholder="Email" />
				<input class="form_controll" type="text" name="phone" placeholder="Phone" />
				<textarea class="textarea_controll" name="message" rows="30" placeholder="Message" ></textarea>
				<input type="hidden" name="email_owner" value="<?php echo $rows->email; ?>"/>
				<div class="contain_agree">
					<input type="checkbox" name="agree" id="agree" />&nbsp;
					<label for="agree" class="text_agree">I agree and understand the terms<span> stated in the disclaimer</span></label>
					<span id="error_check"></span>
				</div>
				
				<input type="submit" class="contain_submit" value="SEND MESSAGE" name="submit"/>
			</form>
		</div>
		<?php
			}
		 ?>

		<div class="data_right_top">
			<div class="text_our_service">
				OUR SERVICES
			</div>
			<div class="short_stay">
				<div class="service_img">S</div>
				<div class="service_text">SHORT STAY</div>
			</div>
			<div class="buy_rent">
				<div class="buy_rent_left">
					<div class="service_img">&#165;</div>
					<div class="service_text">BUY</div>
				</div>
				<div class="buy_rent_right">
					<div class="service_img">R</div>
					<div class="service_text">RENT</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="data_right_bottom">
			<img src="/assets/img/advertise/adv.jpg" alt="Advertising">
		</div>
		<div class="data_right_bottom">
			<img src="/assets/img/advertise/adv1.jpg" alt="Advertising">
		</div>

	</div>
 <div class="clear"></div>
</div>
<?php }else{ ?>
	<div class="not_found">
		<h2>This property could not be found</h2>
		<p class="des_not_found">
			Unfortunately the property you are looking for is unable to be found. This could 
			mean the property has been removed from our system, or there was an error in the  
			URL. Please check and try again, or make a new search.
		</p>
	</div>
	<?php if(count($most_recent) > 0){ ?>
	<div class="container_data_last">
		<div class="similar_place_bar">
			<span class="similar_place_title_left">MOST RECENT HOMES</span>
			<span class="similar_place_title_right"><a href="/lists">MORE <img class="arrow_img" src="/assets/img/icon/arrow-right.png"/></span>
		</div>
	<div class="wrap_data_feature">
			  <?php foreach ($most_recent as $recent) { ?>
				<div class="wrap_data">
					<div class="content_data">
						<div class="data_image">
							<a href="/detail/<?php echo $recent->id; ?>">
							<?php  if($recent->images_path !=""){ ?>
								<img class="image_similar" src="/assets/img/uploads/properties/resize/<?php echo $recent->images_path; ?>"/>
							<?php }else{ ?>
								<img class="image_similar" src="/assets/img/uploads/properties/resize/default_image_properties.jpg"/>
							<?php } ?>
							</a>
							<?php if($recent->views != ""){ ?>
								<div class="count_view">
									<span class="num_count"><?php echo $recent->views; ?></span>
									Views
								</div>
							<?php } ?>
						</div>
						<div class="data_currency">
							<?php
								if($recent->status==1){
									echo "¥";
								}else if($recent->status==2){
									echo "R";
								}else{
									echo "S";
								}
							 ?>
						</div>
						<div class="data_title">
							<a href="/detail/<?php echo $recent->id; ?>"><?php echo strtoupper($recent->building_name); ?></a>
						</div>
						<div class="data_description">
							<?php echo $recent->city_name ." ". $recent->perfecture_name; ?>
						</div>
						<div class="data_price">
							<?php echo ($recent->currency=="USD")? "$" : "¥"; echo number_format($recent->price);?>
						</div>
					</div>
					<div class="ruler_bottom"></div>
				</div>
			   <?php } ?>
			</div>
			<div class="clear"></div>
		</div>
		<?php } ?>
<?php } ?>