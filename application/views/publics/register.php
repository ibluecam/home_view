

<div class="content_register">

	<div class="content_menu_register">
		<span><a href="/login" class="link_login">Login</a></span>&nbsp;
		<span class="text_register">Register</span>
	</div>
	<div class="content_frm_register">
		<p class="text_create">Create your account</p>
		<div class="frm_content">
			<?php echo form_open('register',array('id' => 'frm_register')); ?>
				<div class="content_form">
					<div class="first_name">
						<input type="text" name="firstname" class="username" value="<?php echo set_value('firstname') ?>" placeholder="First Name" required/>
					</div>&nbsp;
					<div class="last_name">
						<input type="text" name="lastname" class="username" value="<?php echo set_value('lastname') ?>" placeholder="Last Name" required/>
					</div>
					<div class="error_firstname"><?php echo form_error('firstname'); ?></div>
					<div class="error_lastname"><?php echo form_error('lastname'); ?></div>
					<div class="clear"></div>
				</div>
				<div class="content_form">
					<input type="text" name="email" id="email" class="frm_reg" value="<?php echo set_value('email') ?>" placeholder="Email Address" required/>
					<div class="error_prefix">
						<?php echo form_error('email'); ?>
					</div>
				</div>
				<div class="content_form">
					<input type="password" name="password" class="frm_reg" value="<?php echo set_value('password') ?>" placeholder="Password" required/>
					<div class="error_prefix">
						<?php echo form_error('password'); ?>
					</div>
				</div>
				<div class="content_btn_reg">
					<input type="submit" name="btn_register" class="btn_register" value="Register" />
				</div>
				<div class="agree_privacy">
					<input type="checkbox" name="agree" id="ch_agree" required />&nbsp;
					<label for="ch_agree" class="text_agree">I Agree to the Terms of Use and Privacy</label>
					<div class="error_prefix_check" id="error_check">
						<?php echo form_error('agree'); ?>
					</div>
				</div>
			</form>
		</div>
		<div class="already_acc">
			<span class="text_agree">Already Have An Account?</span>
			<a href="/login" class="go_login">LOGIN</a>
		</div>
	</div>

</div>