	<div class="content_blog">
		<div class="blog_data page_size">
			<div class="blog_data_left">
				<?php if(count($get_blog_detail) > 0) {?>
				<p class="blog_title"><?php echo $get_blog_detail->title; ?></p>
				<p class="blog_date">
					<?php 
						$time = strtotime($get_blog_detail->created_dt);
						$newformat = date('Y m d',$time);
						echo $newformat;
					?>			
				</p>
				<div class="data_image_blog">
					<?php if($get_blog_detail->feature_image !== ""){ ?>
						<img src="/assets/img/uploads/blog/resize/<?php echo $get_blog_detail->feature_image; ?>">
					<?php }else{ ?>
						<img src="/assets/img/uploads/blog/resize/default_feature_image.jpg">
					<?php } ?>
				</div>
				<div class="blog_description">
					<?php echo $get_blog_detail->body; ?>
				</div>
				<div class="more_blog">
					<a href="/blog/<?php echo $get_blog_detail->category; ?>"><?php echo $get_blog_detail->category; ?></a>
				</div>
				<div class="source">
					<span class="text-source">Source: </span>
					<span><?php echo $get_blog_detail->source; ?></span>
				</div>
				<?php }else{ ?>
					<div class="blog_link">
						<span><a href="/blog/all">ALL</a></span>
						<span><a href="/blog/news">NEWS</a></span>
						<span><a href="/blog/buy">BUY</a></span>
						<span><a href="/blog/rent">RENT</a></span>
						<span><a href="/blog/living">LIVING</a></span>
					</div>
					<div class="not_found">
						<h2 style="margin-bottom: 10px;">Page Not Found</h2>
						<p>The page you're looking for cannot be found.</p>
					</div>
				<?php } ?>
			</div>
			<div class="blog_data_right">
				<div class="our_service">
					<div class="text_service">OUR SERVICES</div>
					<div class="short_stay">
							<img src="/assets/img/icon/sale.png" alt="">
							<br/>
							SHORT STAY
					</div>
					<div class="buy_service">
							<img src="/assets/img/icon/yen.png" alt="">
							<br/>
							BUY
					</div>
					<div class="rent_service">
							<img src="/assets/img/icon/rent.png" alt="">
							<br/>
							RENT
					</div>
					<div class="clear"></div>
				</div>
				<div class="data_right_blog">
					<a href="/"><img src="/assets/img/advertise/adv.jpg" alt="Advertisment"></a>
					<a href="/"><img src="/assets/img/advertise/adv1.jpg" alt="Advertising"></a>
					<a href="/"><img src="/assets/img/advertise/adv2.jpg" alt="Advertising"></a>
					<a href="/"><img src="/assets/img/advertise/adv3.jpg" alt="Advertising"></a>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>