

<div class="content_step">
	<div class="menu_step">
        <div class="tab-link list_menu <?php if($step == 'rent') echo 'active'; ?>" data-tab="tab-1">RENT</div>
		<div class="tab-link list_menu <?php if($step == 'buy') echo 'active'; ?>" data-tab="tab-2">BUY</div>
		<div class="tab-link list_menu <?php if($step == 'short-stay') echo 'active'; ?>" data-tab="tab-3">SHORT STAY</div>
		<div class="clear"></div>
	</div>
	<div class="content_step_detail <?php if($step != 'buy') echo 'tab-content'; ?>" id="tab-2">
        <h3 class="how_to_buy">HOW TO GET A PLACE IN JAPAN</h3>
    <p class="text_title">The process of purchasing real estate in japan</p>
        <p class="text_description">Let us tell you about the process for purchasing Japanese real estate.
                                    It might be possible to gather property information from outside Japan via the Internet, but it will be necessary to visit Japan at least three times in order to inspect prospective property, sign contracts, and conduct the property handover.</p><br/>
        <p class="text_title">1: Consultation Service and Financial Plan.</p>
        <p class="text_description">We are prepared to know buyer's specific desired purchase conditions and provide information of the property which we believe it the best and to meet the needs of each of buyers.
                                    When purchasing a property, buyer must pay the fee charges such as agent commission for agency transaction and registration fee as well as the purchase price. Additionally, you must 
                                    prepare for the expenses of moving house and others. The sum of such expenses is estimated to be 5-10% of the purchase price. We recommend to make a financial planning of the payment amount totalizing the purchase price and all the charges.</p><br/>
        <p class="text_title">2: Property visit(Visiting Japan)</p>
        <p class="text_description">When buyer has fixed aim of the location, type and budget of real property, we will get started on finding a property for. Once the customer finds properties that fulfill their requirements, we show the property and have them check it out for themselves.</p>
        <p class="text_title">3: Purchase application</p>
        <p class="text_description">Once you have decided on the property you would like to purchase, you can fill out the form, and our sales staff will coordinate the conditions of the sale between you and the property seller.</p><br/>
        <p class="text_title">4: Purchase application</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Explanation of Important Matters</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Conclusion of Agreement</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Payment of contract-related fees</p>
        <p class="text_description">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Once the parties have agreed to the transaction conditions, dedicated personnel from the real estate agent will explain all important details related to the contract, including property rights, legal limitations, and all transaction-related agreements. The buyer and seller then sign a sales contract containing the agreed-upon details. When that happens, the seller must be paid a deposit (5–10%), the agent must be paid a brokerage fee (half the amount), and a revenue stamp fee must be paid.</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Seal/ signature (Please ensure to bring both parties‘ seal in case of joint ownership)</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Passport</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Affidavit</p><br/>
        <p class="text_title">5: Settlement</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Application for registration of property rights transfer</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Payment of remaining money and other costs</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Key delivery</p>
        <p class="text_description">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You must get a judicial scrivener to carry out the process of registering the property rights transfer for the real estate being purchased.
                                        This requires the payment of the balance as well as other fees (see below for details).
                                        Once that has been done, you are given the keys and the property is yours.
                                        Fees required during settlement
                                        For more details about fees, please contact us. Documents required during settlement</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Seal / signature (Please ensure to bring both parties‘ seal in case of joint ownership)</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Seal registration certificate / signature registration certificate</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Passport</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Residential registration certificate</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Affidavit</p><br/>
        <p class="text_title">Basic information about real estate transactions in Japan</p>
        <p class="text_description">Real estate systems and customs can vary quite a lot by country. There is some basic information you should know before purchasing real estate in Japan.</p><br/>
        <p class="text_title">Foreigners are allowed to obtain Japanese real estate</p>
        <p class="text_description">Nowadays, there are no legal limitations or tax-related differences when foreigners obtain Japanese real estate. You may purchase real estate regardless of what your nationality is, where you live, and whether you have residence status. However, there are differences in the documents needed by residents and non-residents (mainly documents for confirming the person's identity) and in the requirements for an account at a Japanese bank with regard to receiving purchase funds and paying taxes on real estate after purchase.</p><br/>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;How are residents and non-residents defined?</p><br/>
        <p class="text_title">6: A copy of recent pay slips or bank statement</p>
        <p class="text_description">A resident is an individual whose residence is in Japan or who has been in Japan for at least one year continuously up to the present. If a person resides in two or more countries, that person's "assumed residence" is based on such details as his/her work situation and contract.
                                    A non-resident is an individual who resides outside of Japan.</p><br/>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Receiving funds</p><br/>
        <p class="text_description">Essentially, if a non-resident is purchasing investment property, it will be difficult to do so with a loan from a Japanese bank.
                                    For more details, see "Purchase funds and post-purchase fees".</p><br/>
        <p class="text_title">How to obtain information on purchasing real estate</p>
        <p class="text_description">Information on purchasing Japanese real estate is available online and can be obtained even by people living outside Japan. However, there are also properties with no information available online, so it is better to actually go to Japan and obtain up-to-date information from real estate agents based in specific areas. We at KEN Corporation have a solid track record of conducting real estate transactions within the Tokyo-Yokohama area and have a great deal of up-to-date information available, making us an ideal partner for helping you to find property.</p><br/>
        <p class="text_title">About land prices being included in purchase costs</p>
        <p class="text_description">Usually, in Japan, the purchase price of a condominium or detached house includes not only the cost of the building but the cost of the land, and the purchaser obtains ownership rights to the land. However, condominiums and houses are also sometimes purchased with "leasehold rights" under which the purchaser owns the building itself but can only lease the land. Such property has the advantage of being cheaper to purchase than property that includes land ownership rights. Property with "fixed term land lease rights", for which the period that the land can be leased is determined in advance, is particularly common with high-rise condominiums and similar property in downtown Tokyo. Most such cases involve long-term leases with a contract period of 50 years or longer, and this is regarded as being reasonable because it permits the establishment of housing in favorable locations with a minimal burden at time of purchase.</p><br/>
    </div>
    <div class="content_step_detail <?php if($step != 'short-stay') echo 'tab-content'; ?>" id="tab-3">
        <p class="text_title">The process of purchasing real estate in japan</p>
        <p class="text_description">Let us tell you about the process for purchasing Japanese real estate.
                                    It might be possible to gather property information from outside Japan via the Internet, but it will be necessary to visit Japan at least three times in order to inspect prospective property, sign contracts, and conduct the property handover.</p><br/>
        <p class="text_title">1: Consultation Service and Financial Plan.</p>
        <p class="text_description">We are prepared to know buyer's specific desired purchase conditions and provide information of the property which we believe it the best and to meet the needs of each of buyers.
                                    When purchasing a property, buyer must pay the fee charges such as agent commission for agency transaction and registration fee as well as the purchase price. Additionally, you must 
                                    prepare for the expenses of moving house and others. The sum of such expenses is estimated to be 5-10% of the purchase price. We recommend to make a financial planning of the payment amount totalizing the purchase price and all the charges.</p><br/>
        <p class="text_title">2: Property visit(Visiting Japan)</p>
        <p class="text_description">When buyer has fixed aim of the location, type and budget of real property, we will get started on finding a property for. Once the customer finds properties that fulfill their requirements, we show the property and have them check it out for themselves.</p>
        <p class="text_title">3: Purchase application</p>
        <p class="text_description">Once you have decided on the property you would like to purchase, you can fill out the form, and our sales staff will coordinate the conditions of the sale between you and the property seller.</p><br/>
        <p class="text_title">4: Purchase application</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Explanation of Important Matters</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Conclusion of Agreement</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Payment of contract-related fees</p>
        <p class="text_description">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Once the parties have agreed to the transaction conditions, dedicated personnel from the real estate agent will explain all important details related to the contract, including property rights, legal limitations, and all transaction-related agreements. The buyer and seller then sign a sales contract containing the agreed-upon details. When that happens, the seller must be paid a deposit (5–10%), the agent must be paid a brokerage fee (half the amount), and a revenue stamp fee must be paid.</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Seal/ signature (Please ensure to bring both parties‘ seal in case of joint ownership)</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Passport</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Affidavit</p><br/>
        <p class="text_title">5: Settlement</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Application for registration of property rights transfer</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Payment of remaining money and other costs</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Key delivery</p>
        <p class="text_description">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You must get a judicial scrivener to carry out the process of registering the property rights transfer for the real estate being purchased.
                                        This requires the payment of the balance as well as other fees (see below for details).
                                        Once that has been done, you are given the keys and the property is yours.
                                        Fees required during settlement
                                        For more details about fees, please contact us. Documents required during settlement</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Seal / signature (Please ensure to bring both parties‘ seal in case of joint ownership)</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Seal registration certificate / signature registration certificate</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Passport</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Residential registration certificate</p>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Affidavit</p><br/>
        <p class="text_title">Basic information about real estate transactions in Japan</p>
        <p class="text_description">Real estate systems and customs can vary quite a lot by country. There is some basic information you should know before purchasing real estate in Japan.</p><br/>
        <p class="text_title">Foreigners are allowed to obtain Japanese real estate</p>
        <p class="text_description">Nowadays, there are no legal limitations or tax-related differences when foreigners obtain Japanese real estate. You may purchase real estate regardless of what your nationality is, where you live, and whether you have residence status. However, there are differences in the documents needed by residents and non-residents (mainly documents for confirming the person's identity) and in the requirements for an account at a Japanese bank with regard to receiving purchase funds and paying taxes on real estate after purchase.</p><br/>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;How are residents and non-residents defined?</p><br/>
        <p class="text_title">6: A copy of recent pay slips or bank statement</p>
        <p class="text_description">A resident is an individual whose residence is in Japan or who has been in Japan for at least one year continuously up to the present. If a person resides in two or more countries, that person's "assumed residence" is based on such details as his/her work situation and contract.
                                    A non-resident is an individual who resides outside of Japan.</p><br/>
        <p class="text_description">o &nbsp;&nbsp;&nbsp;&nbsp;Receiving funds</p><br/>
        <p class="text_description">Essentially, if a non-resident is purchasing investment property, it will be difficult to do so with a loan from a Japanese bank.
                                    For more details, see "Purchase funds and post-purchase fees".</p><br/>
        <p class="text_title">How to obtain information on purchasing real estate</p>
        <p class="text_description">Information on purchasing Japanese real estate is available online and can be obtained even by people living outside Japan. However, there are also properties with no information available online, so it is better to actually go to Japan and obtain up-to-date information from real estate agents based in specific areas. We at KEN Corporation have a solid track record of conducting real estate transactions within the Tokyo-Yokohama area and have a great deal of up-to-date information available, making us an ideal partner for helping you to find property.</p><br/>
        <p class="text_title">About land prices being included in purchase costs</p>
        <p class="text_description">Usually, in Japan, the purchase price of a condominium or detached house includes not only the cost of the building but the cost of the land, and the purchaser obtains ownership rights to the land. However, condominiums and houses are also sometimes purchased with "leasehold rights" under which the purchaser owns the building itself but can only lease the land. Such property has the advantage of being cheaper to purchase than property that includes land ownership rights. Property with "fixed term land lease rights", for which the period that the land can be leased is determined in advance, is particularly common with high-rise condominiums and similar property in downtown Tokyo. Most such cases involve long-term leases with a contract period of 50 years or longer, and this is regarded as being reasonable because it permits the establishment of housing in favorable locations with a minimal burden at time of purchase.</p><br/>
	</div>
	<div class="content_step_detail <?php if($step != 'rent') echo 'tab-content'; ?>" id="tab-1">
    <h3 class="how_to_buy">HOW TO GET A PLACE IN JAPAN</h3>
        <p class="text_title">1: Cash</p>
        <p class="text_description">When you try rent Japanese apartments, you have to get Japanese cash,</p><br/>
        <p class="text_description">Basic fees to consider are the deposit, the real estate agency commission fee, plus the first month rent, as well as a small fee for property insurance, an annual maintenance fee and a key exchange fee.</p><br/>
        <p class="text_description">You might have to pay key money too, though not all landlords will ask for it and some agents may waive the fee</p>
        <p class="text_description">So for a standard apartment for 80,000 yen a month, you can expect to pay around 400,000 yen in upfront costs before any moving services and the initial costs for setting up your utilities.</p><br/>
        <p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Deposit (one month’s rent) = 80,000</p>
        <p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Key money (one month’s rent) = 80,000</p>
        <p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Agent fee (up to one and a half months rent) = 120,000</p>
        <p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;First month’s rent = 80,000</p>
        <p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Property insurance = 15,000</p>
        <p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Maintenance fee = 10,000</p>
        <p class="text_description">• &nbsp;&nbsp;&nbsp;&nbsp;Key exchange fee = 12,000</p><br/>
        <p class="text_description">Total: 397,000 yen</p><br/>
        <p class="text_title">2: Passport and Visa</p>
        <p class="text_description">If apply for an apartment or housing contract in Japan you need to provide two forms of official identification:</p><br/>
        <p class="text_description">1. Your passport</p>
        <p class="text_description">2. visa, residence card or student ID.</p><br/>
        <p class="text_description">If you’re on a tourist visa, you’ll only be able to rent short-term contracts that specifically don’t require a guarantor. For long-term rentals, a 90-day tourist visa won’t be accepted.</p><br/>
        <p class="text_title">3: Japanese phone number</p>
        <div class="step_img">
            <img src="/assets/img/step_phone_img.jpg">
        </div>
        <div class="step_block">
            <p class="text_description">You need to have a working phone number where agent can contact you</p>
            <p class="text_description">directly If applying from overseas, an international number is fine.</p>
        </div>
        <div class="clear"></div><br/>
        <p class="text_title">4: Japanese Bank Account</p>
        <p class="text_description">When you start look at Apartment, you don't need to have Japanese bank account, you’ll need one eventually to pay the rent via bank transfer. For the upfront costs like the deposit you can wire transfer 
            from an international bank and some agents will accept credit card. Cash payment is rare, though possible at some places. It’s best to check with the agent which method they prefer. For overseas applications, your home account will 
            work but you’ll have to cover any transfer costs.
        </p><br/>
        <p class="text_title">5: Employer letter or certficate of eligibility (If you’re a student)</p>
        <p class="text_description">
            These are the same documents you likely used in your visa application; any papers that demonstrate your activities in Japan such as a letter of employment, invitation letter or certificate of eligibility from the immigration bureau. Often your letter of employment will show your salary information but you should also prepare number 7 below…
        </p><br/>
                <p class="text_title">6: A copy of recent pay slips or bank statement</p>
        <p class="text_description">You’ll need to prove that you can pay the rent each month so agents will ask for a copy of the past few months’ pay slips (usually 3 months), your yearly income slip or a copy of your latest bank statement or bank book if you’re unemployed.</p><br/>
        <p class="text_description">Japanese agents will set the rent at around 30% of your income so you need to prove that you consistently make or will make more than 3 times the rent. Some agents may consider only the income you earn from within the country so you won’t be allowed to rent a place that’s more than a third of your Japan income, even if you plan to subsidize that with earnings from back home.</p><br/>
        <p class="text_title">7: Domestic Emergency Contact</p>
        <p class="text_description">In case you suddenly abandon ship and leave the country, the emergency contact deals with the hot mess you left behind so it’s sometimes difficult to find a Japanese person who is willing. Your best bet is to nominate your employer if you can; culturally they view you as their responsibility and as an organization they are better prepared to deal with any problems that might arise. Nevertheless, some agents will let you nominate a non-Japanese resident as an emergency contact.</p><br/>
        <p class="text_title">8: Guarantor</p>
        <p class="text_description">Even if you can show that you are employed and earning enough salary, you will still need a guarantor who will be liable for the rent if you can’t make the payments. Some Japanese people who need a guarantor will ask their parents, some companies might also cover their employees. If you can find someone to be your guarantor they’ll need to prepare several documents including proof of residence and an income statement which they will have to get from the local government office. They will also have to prove that the rent is around 30% of what they earn or else they cannot be your guarantor.</p><br/>
        <p class="text_description">If you don’t have somebody you can ask, you can use a guarantor company which the agent will recommend to you. They act as a kind of third party insurance company – you won’t have to deal with them directly but you will have to pay them a month’s rent or more for the service, plus an annual renewal fee of around 10,000 yen. In fact, it’s becoming more and more common for Japanese people to use a guarantor company either at the specific request of the agent or by choice (to avoid burdening someone with the responsibility of being a guarantor).</p><br/>
        <p class="text_description">Finally, make sure to have several copies of all of the documents and keep everything in one place as you’ll need it all again if you renew your contract or move elsewhere. You should start to look for apartments around 1 month before you intend to move and processing the contract should take about two weeks. Happy hunting!</p><br/>
        <p class="text_title">Useful words to know:</p>
        <div class="content_table_step">
            <table>
                <tr>
                    <td>Passport:....................................................................................</td>
                    <td><span>パスポート</span> pasupo-to</td>
                </tr>
                <tr>
                    <td>Visa:............................................................................................</td>
                    <td><span>ビザ</span> bi-zah</td>
                </tr>
                <tr>
                    <td>Residence Card:.......................................................................</td>
                    <td><span>在留</span> zairyu card</td>
                </tr>
                <tr>
                    <td>Letter of employment (with salary information):...............</td>
                    <td><span>在籍証明書</span> zaisekishomeisho</td>
                </tr>
                <tr>
                    <td>Pay slip/Tax withholding slip:................................................</td>
                    <td><span>源泉徴収票</span> gensenchoshuhyo</td>
                </tr>
                <tr>
                    <td>Certificate of eligibility:...........................................................</td>
                    <td><span>在留資格認定証明書</span> zairyushikaku nintei shomeisho</td>
                </tr>
                <tr>
                    <td>Guarantor:..................................................................................</td>
                    <td><span>保証人</span> hoshonin</td>
                </tr>
            </table>
        </div>
	</div>
</div>