<?php if(isset($get_blogs) && count($get_blogs) > 0){
		foreach ($get_blogs as $row) {
	?>
	<div class="content_data_blog">
		<div class="data_image_blog">
			<a href="/blog-detail/<?php echo $row->id; ?>">
				<?php if($row->feature_image !== ""){ ?>
					<img src="/assets/img/uploads/blog/thumb/<?php echo $row->feature_image; ?>">
				<?php }else{ ?>
					<img src="/assets/img/uploads/blog/thumb/default_feature_image.jpg">
				<?php } ?>
			</a>
		</div>
		<div class="data_description_blog">
			<p class="blog_title">
				<?php 
					$count_str_title = strlen($row->title);
					if($count_str_title > 58){
						echo substr($row->title, 0,58).'...';
					}else{
						echo $row->title;
					}
				?>
			</p>
			<p class="blog_date">
				<?php 
					$time = strtotime($row->created_dt);
					$newformat = date('Y m d',$time);
					echo $newformat;
				?>
			</p>
			<div class="blog_description">
				<?php 
					$count_str_body = strlen(strip_tags($row->body));
					if($count_str_body > 150){
						echo substr(strip_tags($row->body), 0,150);
					?>
					<span><a href="/blog-detail/<?php echo $row->id; ?>">learn more...</a></span>
					<?php }else{
						echo strip_tags($row->body);
					}
				?>
			<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<?php if(count($get_blogs) >= 4){ ?>
		<div class="more_blog">
			<a href="/blog">MORE</a>
		</div>
    <?php }}else{ ?>
		<div class="not_found">
			<h3 style="margin-bottom: 10px;">Page Not Found</h3>
			<p>The page you're looking for cannot be found.</p>
		</div>
	<?php } ?>