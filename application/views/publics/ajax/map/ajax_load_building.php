	
	<?php if($get_building_detail && count($get_building_detail)){
		foreach ($get_building_detail as $building) {
	 ?>
	<div class="in_detail_map">
	 	<div class="map_detail_text">
	 		<a href="/detail/<?php echo $building->id; ?>"><?php echo $building->building_name; ?></a>
		<p><?php echo ($building->currency == "USD") ? "$" : "¥"; echo number_format($building->price); ?></p>
		</div>
		<div class="map_detail_img">
			<div class="icon_type">
			<?php if($building->status == 1){ ?>
				&yen;
			<?php } ?>
			<?php if($building->status == 2){ ?>
				R
			<?php } ?>
			<?php if($building->status == 3){ ?>
				S
			<?php } ?></div>
			<a href="/detail/<?php echo $building->id; ?>">
				<?php if($building->images_path != ""){ ?>
					<img src="/assets/img/uploads/properties/thumb/<?php echo $building->images_path; ?>" alt="" />
				<?php }else{ ?>
					<img src="/assets/img/uploads/default_image_properties.jpg" alt="" />
				<?php } ?>
			</a>
		</div>
		<div class="clear"></div>
	 </div>
	 <?php }} ?>
	<input type="hidden" id="get_building_name" value="<?php echo $get_building_detail[0]->building_name; ?>">
	<div class="pagination">
		<?php echo $pagination; ?>
	</div>