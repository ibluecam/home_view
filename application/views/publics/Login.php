<div class="inner_container">
	<div class="content_menu_register">
		<span class="text_register">Login</span></span>&nbsp;
		<span><a href="/register" class="link_register">Register</a>
		
	</div>
	<div class="form">
		<div class="title">SIGN IN</div>
		<form id="login" method="post" >
			<div class="contain_input">
				<?php if(!empty($error_message)){ ?>
					<div class="isa_error">
					   <i class="fa fa-times-circle"></i>
					   <?php echo $error_message; ?>
					</div>
				<?php }	?> 
				<input class="form_controll" type="email" name="email" value="<?php echo set_value('email') ?>" placeholder="Email Addresss" required="required"/>
				<div class="php_error"><?php echo form_error('email'); ?></div>
				<input class="form_controll" type="password" name="password" value="<?php echo set_value('password') ?>" placeholder="Password" required ="required"/>
				<div class="php_error"><?php echo form_error('password'); ?></div>
			</div>
			<input class="submit_controll" type="submit" name="btngo" value="Go"/>
			
		</form>
		<a href=""><p class="forget">Forgot your password?</p></a>
		<span class="dont">Don't Have An Account Yet?</span><a href="/register"><span class="not_register">Register Here</span></a>
	</div>
</div>