<div class="container_market_price">
	<div class="bar_title">
		<span class="market_title">
			<i class="fa fa-search" aria-hidden="true"></i>
			MARKET PRICE IN PREFECTURE NAME
	    </span>
	</div>

	<div class="container_left">
		<p class="normal_p">TYPE</p>
		<div class="data_radio">
			<input type="radio" class="rdo_search" id="Apartment" name="type" />
			<label for="Apartment" class="text_radio">Apartment</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="House" name="type" />
			<label for="House" class="text_radio">House</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="Short" name="type" />
			<label for="Short" class="text_radio">Short term</label>
		</div>
		<p class="p_top">FLOOR PLAN</p>
		<div class="data_radio">
			<input type="radio" class="rdo_search" id="flat" name="floor" />
			<label for="flat" class="text_radio">Flat</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="1K" name="floor" />
			<label for="1K" class="text_radio">1K/1DK</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="2K" name="floor" />
			<label for="2K" class="text_radio">1LDK/2K/2DK</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="3K" name="floor" />
			<label for="3K" class="text_radio">1LDK/3K/3DK</label>
		</div>
		<div class="data_radio">
			<input type="radio" id="4K" name="floor" />
			<label for="4K" class="text_radio">3LDK/4K ~</label>
		</div>

	</div>
	<div class="container_middle">
		<div class="city_title">
			<p class="normal_p">CITY</p>
		</div>
	</div>
	<div class="container_right">
		<div class="maket_price_bar">
			<p class="normal_p">MARKET PRICE</p>
		</div>
	</div>

	<div class="data_side_bar">
		<table>
			<tr>
				<td class="city" >Yokohama</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:30px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Kawasaki</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:50px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Yokosuka</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:100px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Odawara</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:70px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Atsugi</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:90px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Ayase</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:150px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Chigasaki</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:200px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Ebina</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:130px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Fujisawa</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:230px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Hadano</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:122px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Hiratsuka</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:130px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Isehara</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:197px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Kamakura</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:200px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Minamiashigara</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:134px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Sagamihara</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:155px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Yamato</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:123px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Yokohama(capital)</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:230px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Yokosuka</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:220px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Zama</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:80px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>
			<tr>
				<td class="city" >Zushi</td>
				<td class="city_value" >7.9 万円</td>
				<td class="city_slide_bar">
					<div class="prograss_bar" style="width:300px;">
					</div>
				</td>
				<td><input class="search" type="submit" value="SEARCH"/></td>
			</tr>

		</table>
	</div>
</div>
<div class="clear"></div>