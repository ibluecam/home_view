
<!DOCTYPE html>
<html>
  <head>
    <title>Styling the Base Map</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">  
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <link href="/assets/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/noUiSlider/nouislider.min.css" rel="stylesheet">
    <style>
      html, body, #map {
        margin: 0;
        padding: 0;
        height: 100%;
      }
      .map-card {
          left: 15px;
          position: absolute;
          top: 15px;
          z-index: 4;
          background: red;
          width: 320px;
          height: 400px;
      }
    #map_canvas{height:500px; width:100%;}
    .content_icon_right{
      position: absolute;
      right: 15px;
      top: 50px;
    }
    .icon_right{
      background: #FFF;
      width: 60px;
      height: 45px;
      text-align: center;
      padding-top: 15px;
      border-radius: 50px;
      color: red;
      cursor: pointer;
      font-size: 24px;
      margin-bottom: 10px;
      box-shadow: 0px 2px 2px #888; 
    }
    /*#drawpoly{;
      top: 60px;
    }*/
    #price{
          position: absolute;
          z-index: 1;
    }
    #area{
      margin-top: 70px;
      z-index: 1;
      position: absolute;
    }
    .hide_show{
      display: none;
    }
    #price_slider{
      height: 180px;
      display: inline-block;
      padding: 15px 0;
    }
    .slider_price{
      height: 0px;
      background: #FFF;
      text-align: center;
      margin-top: 40px;
      padding-top: 40px;
      position: relative;
      border-bottom-right-radius: 50px;
      border-bottom-left-radius: 50px;
      margin-bottom: 10px;
      box-shadow: 0px 2px 2px #888; 
      display: none;
    }
    .animate_box{
      width: 100px;
      height: 34px;
      background: #FFF;
      position: absolute;
      right: 36px;
      padding-top: 7px;
      padding-left: 8px;
      border-top-left-radius: 20px;
      border-bottom-left-radius: 20px;
      color: #F00;
      display: none;
    }
    #area_slider{
      height: 180px;
      display: inline-block;
      padding: 15px 0;
    }
    .slider_area{
      height: 0px;
      background: #FFF;
      text-align: center;
      margin-top: 120px;
      padding-top: 40px;
      position: relative;
      border-bottom-right-radius: 50px;
      border-bottom-left-radius: 50px;
      margin-bottom: 10px;
      box-shadow: 0px 2px 2px #888; 
      display: none;
    }
    </style>
    </head>
  <body>

    <div id="map"></div>
    <div class="map-card hide_show">
      <div id="get_val_slide"></div>
    </div>
    <div class="content_icon_right">
      <div id="drawpoly" class="icon_right"><i class="fa fa-hand-o-up" aria-hidden="true"></i></div>
      <div id="price" class="icon_right"><i class="fa fa-jpy" aria-hidden="true"></i></div>
      <div class="slider_price">
        <input type="hidden" value="1000000000" class="price_val" />
        <div id="price_slider">
          <div class="price_box animate_box"></div>
        </div>
      </div>
      <div id="area" class="icon_right">M<sup style="font-size:13px;">2</sup></div>
      <div class="slider_area">
        <input type="hidden" value="1000000000" class="area_val" />
        <div id="area_slider">
          <div class="area_box animate_box"></div>
        </div>
      </div>
    </div>
    <input type="hidden" name="" class="all_featurs" value='<?php echo $latlng; ?>'>

  <script type="text/javascript" async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD84u3y2zY_qHrC7gB-pU_DCZh_XGf2yr4&callback=initialize"> </script>
  <script type="text/javascript" src="/assets/noUiSlider/nouislider.min.js"></script>
  <script type="text/javascript" src="/assets/noUiSlider/wNumb.js"></script> 
  <script type="text/javascript" src="/assets/js/publics/test_map.js"></script> 

    <script>
          var features = <?php echo $latlng; ?>;
          var page_type = <?php echo '"'.$page_type.'"'; ?>;
          var start_price = <?php echo $start_price; ?>;
          var start_area = <?php echo $start_area; ?>;
    </script>
  </body>
</html>


