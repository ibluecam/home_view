	
	<div class="content_agent">
		<div class="info_agent">
			<div class="info_detail">
				<?php if(count($agent_info) > 0){ ?>
				<div class="logo_agent">
					<?php if($agent_info->logo != ""){ ?>
						<img src="/assets/img/uploads/company/resize/<?php echo $agent_info->logo; ?>" alt="Logo Agent">
					<?php }else{ ?>
						<img src="/assets/img/uploads/company/resize/company_logo_default.jpg" alt="Logo Agent">
					<?php } ?>
				</div>
				<div class="agent_name">
					<?php if(!empty($agent_info->company_name)){
							echo $agent_info->company_name;
						}else{
							echo $agent_info->last_name." ".$agent_info->first_name;
						}
					?>
				</div>
				<div class="agent_social_media">
					<?php if($agent_info->skype_id != ""){ ?>
						<span class="skype">
							<img src="/assets/img/icon/skype.png" alt="Skype">
							<span><a><?php echo $agent_info->skype_id; ?></a></span>
						</span>
					<?php } ?>
					<?php if($agent_info->viber_id != ""){ ?>
					<span class="viber">
						<img src="/assets/img/icon/viber.png" alt="Viber">
						<span><a><?php echo $agent_info->viber_id; ?></a></span>
					</span>
					<?php } ?>
					<?php if($agent_info->line_id != ""){ ?>
					<span class="line">
						<img src="/assets/img/icon/line.png" alt="Line">
						<span><a><?php echo $agent_info->line_id; ?></a></span>
					</span>
					<?php } ?>
				</div>
				<?php if($agent_info->email != ""){ ?>
					<div class="agent_email">
						<img class="arrow_img" src="/assets/img/icon/message.png"/>
						<span class="contact_text"><?php echo $agent_info->email; ?></span>
					</div>
				<?php } ?>
				<div class="agent_phone">
					<?php if($agent_info->phone1 != ""){ ?>
						<span class="phone1">
							<img class="arrow_img" src="/assets/img/icon/phone.png"/>
							<span class="contact_text"><?php echo $agent_info->phone1; ?></span>
						</span>
					<?php } ?>
					<?php if($agent_info->phone2 != ""){ ?>	
						<span class="phone2">
							<img class="arrow_img" src="/assets/img/icon/tel.png"/>
							<span class="contact_text"><?php echo $agent_info->phone2; ?></span>
						</span>
					<?php } ?>
				</div>
				<?php if($agent_info->office_location != ""){ ?>
					<div class="agent_office_location">
						<p class="title_red">Office Location</p>
						<p class="office_name"><?php echo $agent_info->office_location; ?></p>
					</div>
				<?php } ?>
				<?php if($agent_info->description != ""){ ?>
					<div class="agent_description">
						<p class="title_red">Description</p>
						<p class="description"><?php echo $agent_info->description; ?></p>
					</div>
				<?php } ?>
				<?php }else{ ?>
					<div class="not_found">
						No agent were found!
					</div>
				<?php } ?>
			</div>
			<div class="red_ruler"></div>
		</div>
		<div class="properties_agent">
			<div class="text_count">
				Product List (<span><?php echo $count_agent_properties; ?></span>)
			</div>
			<div class="agent_wrap_data">
				<?php if(count($agent_properties) > 0){
					foreach ($agent_properties as $row) {
				?>
				<div class="wrap_data">
					<div class="content_data">
						<div class="data_image">
							<a href="/detail/<?php echo $row->id; ?>">
								<?php if($row->images_path != ""){ ?>
									<img src="/assets/img/uploads/properties/resize/<?php echo $row->images_path; ?>" alt="Image of <?php echo $row->building_name; ?>">
								<?php }else{ ?>
									<img src="/assets/img/uploads/default_image_properties.jpg" alt="No Image">
								<?php } ?>
							</a>
							<?php if($row->views != ""){ ?>
								<div class="count_view">
									<span class="num_count"><?php echo $row->views; ?></span>
									Views
								</div>
							<?php } ?>
						</div>
						<div class="data_currency">
							<?php if($row->status == 1){ ?>
								&yen;
							<?php } ?>
							<?php if($row->status == 2){ ?>
								R
							<?php } ?>
							<?php if($row->status == 3){ ?>
								S
							<?php } ?>
						</div>
						<div class="data_title">
							<a href="/detail/<?php echo $row->id; ?>"><?php echo $row->building_name; ?></a>
						</div>
						<div class="data_description">
							<?php echo $row->city_name." ".$row->perfecture_name; ?>
						</div>
						<div class="data_price">
							<?php echo ($row->currency == "USD") ? "$" : "¥"; echo number_format($row->price); ?>
						</div>
					</div>
					<div class="ruler_bottom"></div>
				</div>
				<?php }}else{ ?>
					<div class="not_found">
						No data were found!
					</div>
				<?php } ?>
			<div class="clear"></div>
			<div class="pagination">
				<?php echo $pagination; ?>
			</div>
			</div>
		</div>
		<div class="clear"></div>

	</div>