<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_home extends CI_Model {

	private $tbl_properties = 'properties';
	private $tbl_properties_images = 'properties_images';
	private $tbl_perfectures = 'perfectures';
	private $tbl_citys = 'citys';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->tbl_properties = $ci->config->item('db_table_prefix').$this->tbl_properties;
		$this->tbl_properties_images = $ci->config->item('db_table_prefix').$this->tbl_properties_images;
		$this->tbl_perfectures = $ci->config->item('db_table_prefix').$this->tbl_perfectures;
		$this->tbl_citys = $ci->config->item('db_table_prefix').$this->tbl_citys;
	}

	public function get_properties($limit,$offset){
		$this->db->select(
			$this->tbl_properties.'.id,'.
			$this->tbl_properties.'.building_name,'.
			$this->tbl_properties.'.status,'.
			$this->tbl_properties.'.price,'.
			$this->tbl_properties.'.currency,'.
			$this->tbl_properties.'.views,'.
			$this->tbl_perfectures.'.perfecture_name,'.
			$this->tbl_citys.'.city_name,'.
			$this->tbl_properties_images.'.images_path'
		);
		$this->db->from($this->tbl_properties);
        $this->db->join($this->tbl_properties_images, $this->tbl_properties_images.'.properties_id_fk = '.$this->tbl_properties.'.id and primary = 1','LEFT');
        $this->db->join($this->tbl_perfectures, $this->tbl_perfectures.'.id = '.$this->tbl_properties.'.location_perfecture','LEFT');
        $this->db->join($this->tbl_citys, $this->tbl_citys.'.id = '.$this->tbl_properties.'.location_city','LEFT');
        $this->db->order_by('date_created','DESC');
        $this->db->limit($limit,$offset);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
}