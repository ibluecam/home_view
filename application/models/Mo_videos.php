<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_videos extends CI_Model {

    private $table_properties = 'properties';
    private $table_properties_image = 'properties_images';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
        $this->table_properties = $ci->config->item('db_table_prefix').$this->table_properties;
        $this->table_properties_image = $ci->config->item('db_table_prefix').$this->table_properties_image;
	}
	public function get_video_list($limit,$offset){
		$this->db->select(
							$this->table_properties.'.id,'.
							$this->table_properties.'.video_id,'.
							$this->table_properties.'.building_name,'.
							$this->table_properties_image.'.images_path'
						);
		$this->db->from($this->table_properties);
		$this->db->join($this->table_properties_image, $this->table_properties_image.'.properties_id_fk = '.$this->table_properties.'.id and primary = 1','left');
		$this->db->where('video_id !=','');
		$this->db->order_by($this->table_properties.'.date_created','DESC');
        $this->db->limit($limit,$offset);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_video_count(){
		$this->db->select('count('.$this->table_properties.'.id) as count_video');
		$this->db->from($this->table_properties);
		$this->db->join($this->table_properties_image, $this->table_properties_image.'.properties_id_fk = '.$this->table_properties.'.id and primary = 1','left');
		$this->db->where('video_id !=','');
		$query = $this->db->get();
        $result = $query->result();
	    if(count($result)>0){
	    	return $result;
	    }else{
	    	return array();
	    }
	}


}