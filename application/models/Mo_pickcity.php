<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_pickcity extends CI_Model {

	private $tbl_properties = 'properties';
	private $tbl_users = 'users';
	private $tbl_users_group = 'users_groups';
	private $tbl_users_profile = 'users_profile';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->tbl_properties = $ci->config->item('db_table_prefix').$this->tbl_properties;
		$this->tbl_users = $ci->config->item('db_table_prefix').$this->tbl_users;
		$this->tbl_users_group = $ci->config->item('db_table_prefix').$this->tbl_users_group;
		$this->tbl_users_profile = $ci->config->item('db_table_prefix').$this->tbl_users_profile;
	}

	public function count_properties(){
		$this->db->select("count(".$this->tbl_properties.'.id) as count_properties');
		$this->db->from($this->tbl_properties);
		$query=$this->db->get();
		return $result=$query->result();
	}

	public function get_admin_info(){
		$this->db->select($this->tbl_users.'.*,'.$this->tbl_users_profile.'.*,'.$this->tbl_users_group.'.*');
		$this->db->from($this->tbl_users);
		$this->db->join($this->tbl_users_group,$this->tbl_users_group.'.id='.$this->tbl_users.'.user_group');
		$this->db->join($this->tbl_users_profile,$this->tbl_users_profile.'.user_id_fk='.$this->tbl_users.'.id');
		$this->db->where($this->tbl_users_group.'.group_name','superadmin');
		$this->db->limit(1);
		$query=$this->db->get();
		return $result=$query->result();
	}

}