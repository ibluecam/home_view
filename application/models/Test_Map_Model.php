<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Test_Map_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
	} 
	public function testData($id){
		$this->db->select('*');
		$this->db->from('tbl_latlng');
		$this->db->where_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function getAllLatLng(){
		$this->db->select('*');
		$this->db->from('tbl_latlng');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}