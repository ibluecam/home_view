<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_detail extends CI_Model {

	private $table_name					= 'properties';
	private $table_properties_images	= 'properties_images';
	private $table_perfectures			= 'perfectures';
	private $table_citys				= 'citys';
	private $table_users				= 'users';
	private $table_features             ='features';
	private $table_user_profile         ='users_profile';
	
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->table_name				= $ci->config->item('db_table_prefix').$this->table_name;
		$this->table_perfectures		= $ci->config->item('db_table_prefix').$this->table_perfectures;
		$this->table_citys		= $ci->config->item('db_table_prefix').$this->table_citys;
		$this->table_properties_images		= $ci->config->item('db_table_prefix').$this->table_properties_images;
		$this->table_users		= $ci->config->item('db_table_prefix').$this->table_users;
		$this->table_features		= $ci->config->item('db_table_prefix').$this->table_features;
		$this->table_user_profile	= $ci->config->item('db_table_prefix').$this->table_user_profile;
	}

	function select($properties_id){
		$this->db->select($this->table_name.'.*,'.$this->table_name.'.description as properties_description,'.$this->table_properties_images.'.*,'.$this->table_users.'.*,'.$this->table_perfectures.'.*,'.$this->table_citys.'.*,'.$this->table_user_profile.'.*');
		$this->db->select($this->table_name.'.status as properties_status');
        $this->db->from($this->table_name);
        $this->db->join( $this->table_properties_images, $this->table_properties_images.'.properties_id_fk = '.$this->table_name.'.id and primary = 1','left');
        $this->db->join($this->table_users, $this->table_users.'.id = '.$this->table_name.'.user_id_fk');
        $this->db->join($this->table_perfectures, $this->table_perfectures.'.id = '.$this->table_name.'.location_perfecture');
        $this->db->join($this->table_citys, $this->table_perfectures.'.id = '.$this->table_citys.'.perfecture_id_fk');
		$this->db->join($this->table_user_profile,$this->table_user_profile.'.user_id_fk ='.$this->table_users.'.id');        
        $this->db->where($this->table_name.'.id',$properties_id);
        $this->db->group_by($this->table_name.'.id'); 
        $query = $this->db->get();
        return $result = $query->result();
	}

	function select_image($properties_id){
		$this->db->select($this->table_properties_images.'.*');
		$this->db->from($this->table_properties_images);
		$this->db->where($this->table_properties_images.".properties_id_fk",$properties_id);
		$this->db->order_by('primary',"DESC");
		$query=$this->db->get();
		return $query->result();
	}
	function select_features(){
		$this->db->select($this->table_features.".*");
		$this->db->from($this->table_features);
		$query=$this->db->get();
		return $query->result();
	}
	function update_viewer($properties_id){
		$this->db->set('views', 'IFNULL(views+ 1,1)', FALSE);
		$this->db->where($this->table_name.'.id', $properties_id);
		$this->db->update($this->table_name);
	}
	function similer_location($location_perfecture,$properties_id){
		$this->db->select($this->table_name.".id as properties_id,".$this->table_name.".*,".$this->table_properties_images.'.*,'.$this->table_perfectures.'.*,'.$this->table_citys.'.*');
		$this->db->from($this->table_name);
		$this->db->join($this->table_properties_images,$this->table_properties_images.'.properties_id_fk='.$this->table_name.'.id and primary = 1','left');
		$this->db->join($this->table_perfectures, $this->table_perfectures.'.id = '.$this->table_name.'.location_perfecture');
        $this->db->join($this->table_citys, $this->table_perfectures.'.id = '.$this->table_citys.'.perfecture_id_fk');
		$this->db->where($this->table_name.'.location_perfecture',$location_perfecture);
		$this->db->where($this->table_name.'.id !=',$properties_id);
		$this->db->limit(4);
		$this->db->group_by($this->table_name.".id");
		$query=$this->db->get();
		return $query->result();
	}
}