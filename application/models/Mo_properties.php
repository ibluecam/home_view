<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_properties extends CI_Model {

	private $table_name					= 'properties';
	private $table_properties_images	= 'properties_images';
	private $table_perfectures			= 'perfectures';
	private $table_citys				= 'citys';
	private $table_users				= 'users';
	private $table_features				= 'features';
	private $table_favorite				= 'user_favorite_properties';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->table_name				= $ci->config->item('db_table_prefix').$this->table_name;
		$this->table_perfectures		= $ci->config->item('db_table_prefix').$this->table_perfectures;
		$this->table_citys		= $ci->config->item('db_table_prefix').$this->table_citys;
		$this->table_properties_images		= $ci->config->item('db_table_prefix').$this->table_properties_images;
		$this->table_users		= $ci->config->item('db_table_prefix').$this->table_users;
		$this->table_features		= $ci->config->item('db_table_prefix').$this->table_features;
		$this->table_favorite		= $ci->config->item('db_table_prefix').$this->table_favorite;
	}

	public function admin_get_properties_lists($where,$limit,$offset,$keyword){
		$this->db->select($this->table_name.'.*, '.$this->table_name.'.id as properties_id,'.$this->table_properties_images.'.*');
        $this->db->from($this->table_name);
        $this->db->join( $this->table_properties_images, $this->table_properties_images.'.properties_id_fk = '.$this->table_name.'.id and primary = 1','left');
       	$this->db->where($where);
       	if($keyword!=""){
       		$this->db->like('CONCAT('.$this->table_name.'.building_name,'.' '.$this->table_name.'.properties_type)',$keyword);
        }
       	$this->db->order_by($this->table_name.'.date_created','DESC');
       	$this->db->group_by($this->table_name.'.id');	
       	$this->db->limit($limit,$offset);
        $query = $this->db->get();
       // echo $this->db->last_query();exit
        return $result = $query->result();
	}
	public function admin_count_properties_lists($where,$keyword){
		$this->db->select('count('.$this->table_name.'.id) as count_properties');
        $this->db->from($this->table_name);
        $this->db->join( $this->table_properties_images, $this->table_properties_images.'.properties_id_fk = '.$this->table_name.'.id and primary = 1','left');
       	$this->db->where($where);
       	if($keyword!=""){
       		$this->db->like('CONCAT('.$this->table_name.'.building_name,'.' '.$this->table_name.'.properties_type)',$keyword);
        }	
        $query = $this->db->get();
        $result = $query->result();
        if(count($result) > 0){
    		return $result[0];
    	}
	}
	public function admin_get_properties_images($where){
		$this->db->select($this->table_properties_images.'.*,');
        $this->db->from($this->table_properties_images);
        $this->db->where($where);
        $this->db->order_by($this->table_properties_images.".primary","ASC");
        $query = $this->db->get();
        return $result = $query->result();
	}

	/*==== SELECT DATA FOR SELECT OPTION ===*/
	public function select_option_floor_tool_properties(){
		$this->db->select($this->table_name.'.floor');
        $this->db->from($this->table_name);
         $this->db->group_by('floor'); 
 		$this->db->order_by('floor', 'ASC'); 
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_option_type_tool_properties(){
		$this->db->select($this->table_name.'.type');
        $this->db->from($this->table_name);
         $this->db->group_by('type'); 
 		$this->db->order_by('type', 'ASC'); 
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_option_gross_yield_tool_properties(){
		$this->db->select($this->table_name.'.gross_yield');
        $this->db->from($this->table_name);
         $this->db->group_by('gross_yield'); 
 		$this->db->order_by('gross_yield', 'ASC'); 
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_option_potential_annual_rent_tools_properties(){
		$this->db->select($this->table_name.'.potential_annual_rent');
        $this->db->from($this->table_name);
         $this->db->group_by('potential_annual_rent'); 
 		$this->db->order_by('potential_annual_rent', 'ASC'); 
        $query = $this->db->get();
        return $result = $query->result();
	}

	public function select_option_layout_tools_properties(){
		$this->db->select($this->table_name.'.layout');
        $this->db->from($this->table_name);
         $this->db->group_by('layout'); 
 		$this->db->order_by('layout', 'ASC'); 
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_option_transaction_type_tools_properties(){
		$this->db->select($this->table_name.'.transaction_type');
        $this->db->from($this->table_name);
         $this->db->group_by('transaction_type'); 
 		$this->db->order_by('transaction_type', 'ASC'); 
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_option_parking_tools_properties(){
		$this->db->select($this->table_name.'.parking');
        $this->db->from($this->table_name);
         $this->db->group_by('parking'); 
 		$this->db->order_by('parking', 'ASC'); 
        $query = $this->db->get();
        return $result = $query->result();
	}
	/*==== END SELECT DATA FOR SELECT OPTION ===*/

	public function add_properties($data){
		$data["date_created"]	= date("Y-m-d H:i:s");
		$this->db->insert($this->table_name, $data);
   		$insert_id = $this->db->insert_id();
   		return $insert_id;
	}
	public function add_images_properties($data){
		$this->db->insert($this->table_properties_images, $data);
   		$insert_id = $this->db->insert_id();
   		return $insert_id;
	}
	public function update_properties($data,$id){
		//$data["date_updated"] = date("Y-m-d H:i:s");

		$this->db->where('id', $id);
    	$this->db->update($this->table_name, $data);
	}
	public function update_images_properties($data,$id){
		$this->db->where('properties_id_fk', $id);
    	$this->db->update($this->table_properties_images, $data);
	}
	public function update_primary_images($where){

		$data["primary"] = 1;
		$this->db->where($where);
    	$this->db->update($this->table_properties_images, $data);
	}
	public function update_primary_images_limit($where){
		$data["primary"] = 1;
		$this->db->where($where);
		$this->db->limit(1);
    	$this->db->update($this->table_properties_images, $data);
	}
	public function reset_new_primary_images($where){
		$data["primary"] = 0;
		$this->db->where($where);
    	$this->db->update($this->table_properties_images, $data);
	}
	public function remove_images($id){
		$this->db->where('id', $id);
  		$this->db->delete($this->table_properties_images);
	}
	public function remove_images_by_name($image_name){
		$this->db->where('images_path', $image_name);
  		$this->db->delete($this->table_properties_images);
	}
	public function delete_properties($id){
		$this->db->where('id', $id);
  		$this->db->delete($this->table_name);
	}
	public function delete_favorite_by_properties($properties_id){
		$this->db->where('properties_id_fk', $properties_id);
  		$this->db->delete($this->table_favorite);
	}
	public function delete_all_properties_images($id){
		$this->db->where('properties_id_fk', $id);
  		$this->db->delete($this->table_properties_images);
	}
	public function select_properties_by_id($id){
		$this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->where($this->table_name.'.id', $id); 
        $query = $this->db->get();
      	$result = $query->result();
      	if(count($result) > 0)
      		return $result[0];
      	return array();
	}
	
	public function select_perfectures(){
		$this->db->select($this->table_perfectures.'.*');
        $this->db->from($this->table_perfectures);
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_perfectures_city(){
		$this->db->select($this->table_citys.'.*');
        $this->db->from($this->table_citys);
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_features(){
		$this->db->select($this->table_features.'.*');
        $this->db->from($this->table_features);
        $query = $this->db->get();
        return $result = $query->result();
	}

}