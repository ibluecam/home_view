<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Model {

	private $table_name			 = 'users';
	private $table_users_profile = 'users_profile';
	private $table_users_group		 = 'users_groups';
	private $table_properties		 = 'properties';
	private $table_properties_images ='properties_images';
	var $key_encrypt='$2a$07$usII$9oodadasdsadesillystringfors';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->table_name		   = $ci->config->item('db_table_prefix').$this->table_name;
		$this->table_users_profile = $ci->config->item('db_table_prefix').$this->table_users_profile;
		$this->table_users_group = $ci->config->item('db_table_prefix').$this->table_users_group;
		$this->table_properties = $ci->config->item('db_table_prefix').$this->table_properties;
		$this->table_properties_images = $ci->config->item('db_table_prefix').$this->table_properties_images;
	}
	public function select_users_list($where){
		$this->db->select('count('.$this->table_properties.'.id) as count_properties');
		$this->db->select($this->table_name.'.*,'.$this->table_users_profile.".*");
		$this->db->select($this->table_name.'.id as user_id');
	 	$this->db->from($this->table_name);
	 	$this->db->join($this->table_users_profile,$this->table_users_profile.'.user_id_fk='.$this->table_name.'.id');
	 	$this->db->join($this->table_properties,$this->table_properties.'.user_id_fk='.$this->table_name.'.id','left');
	 	$this->db->where($where);
	 	$this->db->order_by($this->table_name.'.created_dt',"desc");
	 	$this->db->group_by($this->table_name.'.id');
	    $query = $this->db->get();
	    $result = $query->result();
	    if(count($result)>0){
	    	return $result;
	    }else{
	    	return array();
	    }
	}

	public function count_all_properties($where){
		$this->db->select('count('.$this->table_properties.'.id) as count_all_properties');
		$this->db->select($this->table_name.'.*,'.$this->table_users_profile.".*");
		$this->db->select($this->table_name.'.id as user_id');
	 	$this->db->from($this->table_name);
	 	$this->db->join($this->table_users_profile,$this->table_users_profile.'.user_id_fk='.$this->table_name.'.id');
	 	$this->db->join($this->table_properties,$this->table_properties.'.user_id_fk='.$this->table_name.'.id','left');
	 	$this->db->where($where);
	    $query = $this->db->get();
	    $result = $query->result();
	    if(count($result)>0){
	    	return $result[0];
	    }else{
	    	return array();
	    }
	}
	public function select_user_by_id($id){
		$this->db->select($this->table_name.'.*,'.$this->table_users_profile.".*");
		$this->db->select($this->table_name.'.id as user_id');
	 	$this->db->from($this->table_name);
	 	$this->db->join($this->table_users_profile,$this->table_users_profile.'.user_id_fk='.$this->table_name.'.id');
	 	$this->db->where($this->table_name.".id",$id);
	    $query = $this->db->get();
	    $result = $query->result();
	    if(count($result)>0){
	    	return $result[0];
	    }else{
	    	return array();
	    }
	}
	public function insertUser($data){
		$data["created_dt"]	= date("Y-m-d H:i:s");
		$this->db->insert($this->table_name, $data);
   		$insert_id = $this->db->insert_id();
   		return $insert_id;
	}
	public function insertUserProfile($data){
		$this->db->insert($this->table_users_profile, $data);
	}
	public function update_users($data,$id){
		$this->db->where('id', $id);
    	$this->db->update($this->table_name, $data);
	}
	public function update_user_profiles($data,$user_id_fk){
		$this->db->where('user_id_fk', $user_id_fk);
    	$this->db->update($this->table_users_profile, $data);
	}
	public function delete_users($id){
		$this->db->where('id', $id);
  		$this->db->delete($this->table_name);
	}
	public function delete_user_profiles($user_id_fk){
		$this->db->where('user_id_fk', $user_id_fk);
  		$this->db->delete($this->table_users_profile);
	}
	public function encrypt_password($password){
		$enc_password=crypt($password, $this->key_encrypt);
		return $enc_password;
	}
	public function getUserDetailByUsername($email){
		$this->db->select($this->table_name.'.*,'.$this->table_users_profile.'.*,'.$this->table_users_group.'.*');
		$this->db->select($this->table_name.'.id as user_id');
	 	$this->db->from($this->table_name);
	 	$this->db->join($this->table_users_profile,$this->table_users_profile.'.user_id_fk='.$this->table_name.'.id');
	 	$this->db->join($this->table_users_group,$this->table_users_group.'.id='.$this->table_name.'.user_group');
	 	$this->db->where('email',$email);
	    $query = $this->db->get();
	    $result = $query->result();
	    if(count($result)>0){
	    	return $result[0];
	    }
	}
	public function getMyDetail(){
		$id = $this->session->userdata('user_id');
		$token = $this->session->userdata('token_login');
		$this->db->select($this->table_name.'.*,'.$this->table_users_profile.'.*,'.$this->table_users_group.'.*');
	 	$this->db->select($this->table_name.'.id as user_id');
	 	$this->db->from($this->table_name);
	 	$this->db->join($this->table_users_profile,$this->table_users_profile.'.user_id_fk='.$this->table_name.'.id');
	 	$this->db->join($this->table_users_group,$this->table_users_group.'.id='.$this->table_name.'.user_group');
	 	$this->db->where($this->table_name.'.id',$id);
	 	$this->db->where($this->table_name.'.user_token', $token);
	    $query = $this->db->get();
	    $result = $query->result();
	    if(count($result)>0){
	    	return $result[0];
	    }
	}
	public function saveLoginSession($user_id, $token){	
		$this->session->set_userdata('user_id', $user_id);
		$this->session->set_userdata('token_login', $token);
	}

	public function login($email, $password, &$message=''){
    	$user_detail = $this->getUserDetailByUsername($email);
    	if($user_detail){
    		$enc_password = $this->encrypt_password($password);
    		if($user_detail->password!=$enc_password){
    			$message = 'Password is incorrect';
    		}else{
    			$this->saveLoginSession($user_detail->user_id, $user_detail->user_token);
    			return true;
    		}
    	}else{
    		$message = 'Account does not exist';
    	}
    }
    public function getUserDetailregisterSuccess($id){
		$this->db->select($this->table_name.'.*,'.$this->table_users_profile.'.*,'.$this->table_users_group.'.*');
		$this->db->select($this->table_name.'.id as user_id');
	 	$this->db->from($this->table_name);
	 	$this->db->join($this->table_users_profile,$this->table_users_profile.'.user_id_fk='.$this->table_name.'.id');
	 	$this->db->join($this->table_users_group,$this->table_users_group.'.id='.$this->table_name.'.user_group');
	 	$this->db->where($this->table_name.'.id',$id);
	    $query = $this->db->get();
	    $result = $query->result();
	    if(count($result)>0){
	    	return $result[0];
	    }
	}
    public function registerSuccess($id){
    	$user_info = $this->getUserDetailregisterSuccess($id);
    
    	$this->saveLoginSession($user_info->user_id, $user_info->user_token);
    }
    public function logout(){
    	$this->session->unset_userdata('user_id');
    	$this->session->unset_userdata('token_login');
    }
    public function isEmailExist($email){
		$this->db->select('email');
	 	$this->db->from($this->table_name);
	 	$this->db->where('email',$email);
	    $query = $this->db->get();
	    $result = $query->result();
	    return $result;
	}
	 public function isEmailExistEidt($email,$user_id){
		$this->db->select('email');
	 	$this->db->from($this->table_name);
	 	$this->db->where('email',$email);
	 	$this->db->where('id !=',$user_id);
	    $query = $this->db->get();
	   // echo $this->db->last_query();exit;
	    $result = $query->result();
	    return $result;
	}
	public function group_by_user_id($user_id){
		$this->db->where('id=', $user_id);
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row()->user_group;
		return NULL;
	}

	public function select_properties_by_user($user_id){
		$this->db->select($this->table_properties.'.*');
        $this->db->from($this->table_properties);
        $this->db->where($this->table_properties.'.user_id_fk',$user_id);
        $query=$this->db->get();
        return $result=$query->result();
    }

    public function select_image_by_properties_id($properties_id){
    	$this->db->select($this->table_properties_images.'.*');
        $this->db->from($this->table_properties_images);
        $this->db->where('properties_id_fk',$properties_id);
        $query=$this->db->get();
        return $result=$query->result();
    }

    public function delete_row_image($image_id){
    	 $this->db-> where('id', $image_id);
  		 $this->db->delete($this->table_properties_images);
    }
    public function delete_properties_by_properties_id($properties_id){
    	 $this->db-> where('id', $properties_id);
  		 $this->db->delete($this->table_properties);
    }
    public function select_all_agent(){
    	$this->db->select('count('.$this->table_name.'.id) as count_agent');
    	$this->db->from($this->table_name);
    	$this->db->where('user_group',2);
    	$query = $this->db->get();
    	$result = $query->result();
    	if(count($result) > 0){
    		return $result[0];
    	}
    }


}