<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_map extends CI_Model {

	private $tbl_properties = 'properties';
	private $tbl_properties_images = 'properties_images';
	private $tbl_perfectures = 'perfectures';
	private $tbl_citys = 'citys';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->tbl_properties = $ci->config->item('db_table_prefix').$this->tbl_properties;
		$this->tbl_properties_images = $ci->config->item('db_table_prefix').$this->tbl_properties_images;
		$this->tbl_perfectures = $ci->config->item('db_table_prefix').$this->tbl_perfectures;
		$this->tbl_citys = $ci->config->item('db_table_prefix').$this->tbl_citys;
	}

	public function get_properties_marker($where){
		$this->db->select(
						'count('.$this->tbl_properties.'.id) as count_home,'.
						$this->tbl_properties.'.id as properties_id,'.
						$this->tbl_properties.'.location_perfecture,'.
						$this->tbl_properties.'.location_city,'.
						$this->tbl_properties.'.properties_type,'.
						$this->tbl_properties.'.status,'.
						$this->tbl_properties.'.price,'.
						$this->tbl_properties.'.currency,'.
						$this->tbl_properties.'.latitude,'.
						$this->tbl_properties.'.longitude,'.
						$this->tbl_properties.'.building_name,'.
						$this->tbl_perfectures.'.perfecture_name,'.
						$this->tbl_citys.'.city_name'
					);
		$this->db->from($this->tbl_properties);
        $this->db->join($this->tbl_properties_images, $this->tbl_properties_images.'.properties_id_fk = '.$this->tbl_properties.'.id and primary = 1','left');
        $this->db->join($this->tbl_perfectures, $this->tbl_perfectures.'.id = '.$this->tbl_properties.'.location_perfecture','left');
        $this->db->join($this->tbl_citys, $this->tbl_citys.'.id = '.$this->tbl_properties.'.location_city','left');
        $this->db->where($where,NULL,false);
        $this->db->group_by($this->tbl_properties.'.building_name');
		$query = $this->db->get();
		return $result = $query->result();
	}

	public function get_perfectures(){
		$this->db->select('*');
		$this->db->from($this->tbl_perfectures);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_citys($perfecture_id){
		$this->db->select('*');
		$this->db->from($this->tbl_citys);
		$this->db->where('perfecture_id_fk',$perfecture_id);
		$this->db->order_by($this->tbl_citys.'.city_name','ASC');
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_count_home_city($where){
		$this->db->select('count(id) as count_home');
		$this->db->from($this->tbl_properties);
		$this->db->where($where,NULL,false);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_detail_building($where,$limit,$offset){
		$this->db->select($this->tbl_properties.'.*,'.$this->tbl_properties_images.'.images_path');
		$this->db->from($this->tbl_properties);
		$this->db->join($this->tbl_properties_images, $this->tbl_properties_images.'.properties_id_fk = '.$this->tbl_properties.'.id and primary = 1','LEFT');
		$this->db->where($where,NULL,false);
		$this->db->limit($limit,$offset);
		$query = $this->db->get();

		return $result = $query->result();
	}
	public function get_building_count($where){
		$this->db->select('count('.$this->tbl_properties.'.id) as count_building');
		$this->db->from($this->tbl_properties);
		$this->db->join($this->tbl_properties_images, $this->tbl_properties_images.'.properties_id_fk = '.$this->tbl_properties.'.id and primary = 1','LEFT');
		$this->db->where($where,NULL,false);
		$query = $this->db->get();
		return $result = $query->result();
	}

	public function get_perfecutre_name($perfecture_id){
		$this->db->select('perfecture_name');
		$this->db->from($this->tbl_perfectures);
		$this->db->where('id',$perfecture_id);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_city_name($city_id){
		$this->db->select('city_name');
		$this->db->from($this->tbl_citys);
		$this->db->where('id',$city_id);
		$query = $this->db->get();
		return $result = $query->result();
	}
}