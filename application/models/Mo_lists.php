<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_lists extends CI_Model {

	private $tbl_properties = 'properties';
	private $tbl_properties_images = 'properties_images';
	private $tbl_perfectures = 'perfectures';
	private $tbl_citys = 'citys';
	private $tbl_users_profile = 'users_profile';
	private $tbl_user_favorites = 'user_favorite_properties';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->tbl_properties = $ci->config->item('db_table_prefix').$this->tbl_properties;
		$this->tbl_properties_images = $ci->config->item('db_table_prefix').$this->tbl_properties_images;
		$this->tbl_perfectures = $ci->config->item('db_table_prefix').$this->tbl_perfectures;
		$this->tbl_citys = $ci->config->item('db_table_prefix').$this->tbl_citys;
		$this->tbl_users_profile = $ci->config->item('db_table_prefix').$this->tbl_users_profile;
		$this->tbl_user_favorites = $ci->config->item('db_table_prefix').$this->tbl_user_favorites;
	}

	public function get_properties($where,$limit,$offset,$order,$cending){
		$this->db->select(
			$this->tbl_properties.'.id,'.
			$this->tbl_properties.'.building_name,'.
			$this->tbl_properties.'.status,'.
			$this->tbl_properties.'.price,'.
			$this->tbl_properties.'.currency,'.
			$this->tbl_properties.'.size,'.
			$this->tbl_properties.'.floor,'.
			$this->tbl_properties.'.views,'.
			$this->tbl_properties.'.key_money,'.
			$this->tbl_properties.'.deposit,'.
			$this->tbl_properties.'.maintainance_fee,'.
			$this->tbl_perfectures.'.perfecture_name,'.
			$this->tbl_citys.'.city_name,'.
			$this->tbl_properties_images.'.images_path,'.
			$this->tbl_users_profile.'.logo,'.
			$this->tbl_users_profile.'.user_id_fk'
		);
		$this->db->from($this->tbl_properties);
        $this->db->join($this->tbl_properties_images, $this->tbl_properties_images.'.properties_id_fk = '.$this->tbl_properties.'.id and primary = 1','left');
        $this->db->join($this->tbl_perfectures, $this->tbl_perfectures.'.id = '.$this->tbl_properties.'.location_perfecture');
        $this->db->join($this->tbl_citys, $this->tbl_citys.'.id = '.$this->tbl_properties.'.location_city');
        $this->db->join($this->tbl_users_profile, $this->tbl_users_profile.'.user_id_fk = '.$this->tbl_properties.'.user_id_fk','left');
        $this->db->where($where,NULL,false);
        $this->db->order_by($this->tbl_properties.'.'.$order,$cending);
        $this->db->limit($limit,$offset);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_count_properties($where){
		$this->db->select('count('.$this->tbl_properties.'.id) as count_properties');
		$this->db->from($this->tbl_properties);
        $this->db->join($this->tbl_properties_images, $this->tbl_properties_images.'.properties_id_fk = '.$this->tbl_properties.'.id and primary = 1','left');
        $this->db->join($this->tbl_perfectures, $this->tbl_perfectures.'.id = '.$this->tbl_properties.'.location_perfecture');
        $this->db->join($this->tbl_citys, $this->tbl_citys.'.id = '.$this->tbl_properties.'.location_city');
        $this->db->join($this->tbl_users_profile, $this->tbl_users_profile.'.user_id_fk = '.$this->tbl_properties.'.user_id_fk','left');
        $this->db->where($where,NULL,false);
        $this->db->order_by($this->tbl_properties.'.date_created','DESC');
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_properties_images($id){
		$this->db->select('*');
		$this->db->from($this->tbl_properties_images);
        $this->db->where('properties_id_fk',$id);
        $this->db->limit(3,0);
		$query = $this->db->get();
		return $result = $query->result();
	}

	public function add_favorite($data){
		$this->db->insert($this->tbl_user_favorites,$data);
	}
	
	public function get_favorite_lists($user_id){
		$this->db->select('*');
		$this->db->from($this->tbl_user_favorites);
		$this->db->where('user_id_fk',$user_id);
		$query = $this->db->get();
		return $result = $query->result();
	}

	public function delete_favorite($where){
		$this->db->where($where);
		$this->db->delete($this->tbl_user_favorites);
	}
	public function get_perfecutre_name($perfecture_id){
		$this->db->select('perfecture_name');
		$this->db->from($this->tbl_perfectures);
		$this->db->where('id',$perfecture_id);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_city_name($city_id){
		$this->db->select('city_name');
		$this->db->from($this->tbl_citys);
		$this->db->where('id',$city_id);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_count_home($where){
		$this->db->select('count('.$this->tbl_properties.'.id) as count_home');
		$this->db->from($this->tbl_properties);
		$this->db->where($where);
		$query = $this->db->get();
		return $result = $query->result();
	} 
}