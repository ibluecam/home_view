<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_blogs extends CI_Model {

	private $table_name = 'blogs';
    private $table_properties = 'properties';
    private $table_properties_image = 'properties_images';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->table_name = $ci->config->item('db_table_prefix').$this->table_name;
        $this->table_properties = $ci->config->item('db_table_prefix').$this->table_properties;
        $this->table_properties_image = $ci->config->item('db_table_prefix').$this->table_properties_image;
	}
	public function add_blog($data){
		$data["created_dt"]	= date("Y-m-d H:i:s");
		$data["updated_dt"]	= date("Y-m-d H:i:s");
		$this->db->insert($this->table_name, $data);
   		$insert_id = $this->db->insert_id();
   		return $insert_id;
	}
	public function select_blog_user($where,$limit,$offset){
		$this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_blog_user_count($where){
		$this->db->select('count('.$this->table_name.'.id) as count_blog');
        $this->db->from($this->table_name);
        $this->db->where($where);
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_blog_admin($where,$limit,$offset){
		$this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->where($where);
        $this->db->order_by($this->table_name.'.created_dt',"desc");
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function count_blog_admin($where){
		$this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->where($where);
        $query = $this->db->get();
        return $result = $query->result();
	}
	public function select_blog_by_id($id){
		$this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->where("id",$id);
        $query = $this->db->get();
        $result = $query->result();
        if(count($result) > 0){
        	 return $result[0];
        }else{
        	return array();
        }
	}
	public function select_blog_user_home(){
		$this->db->select($this->table_name.'.*');
        $this->db->from($this->table_name);
        $this->db->limit(4,0);
        $query = $this->db->get();
        return $result = $query->result();
    }
	public function update_blog($data,$id){
		$this->db->where('id', $id);
    	$this->db->update($this->table_name, $data);
	}

	public function delete_blog($id){
		$this->db->where('id', $id);
  		$this->db->delete($this->table_name);
	}



}