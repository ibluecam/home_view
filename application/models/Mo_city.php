<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_city extends CI_Model {

	private $tbl_properties = 'properties';
	private $tbl_perfectures = 'perfectures';
	private $tbl_citys = 'citys';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->tbl_properties = $ci->config->item('db_table_prefix').$this->tbl_properties;
		$this->tbl_perfectures = $ci->config->item('db_table_prefix').$this->tbl_perfectures;
		$this->tbl_citys = $ci->config->item('db_table_prefix').$this->tbl_citys;
	}

	public function get_perfectures(){
		$this->db->select('*');
		$this->db->from($this->tbl_perfectures);
		$query = $this->db->get();
		return $result = $query->result();
	}
	public function get_citys($status){
		if($status != ""){
			$status = ' AND '.$this->tbl_properties.'.status = '.$status;
		}else{
			$status = "";
		}
		$this->db->select($this->tbl_citys.'.*, count('.$this->tbl_properties.'.id) as count_city,'.$this->tbl_perfectures.'.id as perfecture_id');
		$this->db->from($this->tbl_citys);
		$this->db->join($this->tbl_properties,$this->tbl_citys.'.id = '.$this->tbl_properties.'.location_city'.$status,'left');
		$this->db->join($this->tbl_perfectures,$this->tbl_citys.'.perfecture_id_fk = '.$this->tbl_perfectures.'.id','left');
		$this->db->group_by($this->tbl_citys.'.id');
		$this->db->order_by($this->tbl_citys.'.city_name', 'ASC');
		$query = $this->db->get();
		return $result = $query->result();
	}
}