<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_favorite extends CI_Model {

	private $table_name					= 'properties';
	private $table_properties_images	= 'properties_images';
	private $table_users				= 'users';
	private $table_user_profile         ='users_profile';
	private $table_favorite             ='user_favorite_properties';
	private $tbl_perfectures = 'perfectures';
	private $tbl_citys = 'citys';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->table_name				= $ci->config->item('db_table_prefix').$this->table_name;
		$this->table_properties_images		= $ci->config->item('db_table_prefix').$this->table_properties_images;
		$this->table_users		= $ci->config->item('db_table_prefix').$this->table_users;
		$this->table_favorite		= $ci->config->item('db_table_prefix').$this->table_favorite;
		$this->tbl_perfectures = $ci->config->item('db_table_prefix').$this->tbl_perfectures;
		$this->tbl_citys = $ci->config->item('db_table_prefix').$this->tbl_citys;
	}

	function select_favorite($user_id,$limit,$offset,$keyword){
		$this->db->select($this->table_favorite.'.properties_id_fk as properties_id');
		$this->db->select($this->table_favorite.'.*,'.$this->table_name.'.*,'.$this->table_properties_images.".*");
		$this->db->select($this->table_users.'.*,'.$this->table_user_profile.'.*,'.$this->table_name.'.status as properties_status');
		$this->db->select($this->tbl_perfectures.'.perfecture_name,'.$this->tbl_citys.'.city_name');
		$this->db->from($this->table_favorite);
		$this->db->join($this->table_name,$this->table_name.'.id='.$this->table_favorite.'.properties_id_fk');
		$this->db->join($this->table_properties_images, $this->table_properties_images.'.properties_id_fk = '.$this->table_name.'.id and primary = 1','left');
		$this->db->join($this->table_users,$this->table_users.'.id='.$this->table_favorite.'.user_id_fk');
		$this->db->join($this->tbl_perfectures, $this->tbl_perfectures.'.id = '.$this->table_name.'.location_perfecture','LEFT');
        $this->db->join($this->tbl_citys, $this->tbl_citys.'.id = '.$this->table_name.'.location_city','LEFT');
		$this->db->join($this->table_user_profile,$this->table_user_profile.'.user_id_fk='.$this->table_users.'.id');
		$this->db->where($this->table_users.'.id',$user_id);
		if($keyword!=""){
			$this->db->like('CONCAT('.$this->table_name.'.building_name,'.' '.$this->table_name.'.properties_type)',$keyword);
		}
		$this->db->order_by($this->table_favorite.'.created_dt','DESC');
        $this->db->limit($limit,$offset);
		$query=$this->db->get();
		return $query->result();
	}
	function select_count_favorite($user_id,$keyword){
		$this->db->select($this->table_favorite.'.properties_id_fk as properties_id');
		$this->db->select($this->table_favorite.'.*,'.$this->table_name.'.*,'.$this->table_properties_images.".*");
		$this->db->select($this->table_users.'.*,'.$this->table_user_profile.'.*,'.$this->table_name.'.status as properties_status');
		$this->db->select($this->tbl_perfectures.'.perfecture_name,'.$this->tbl_citys.'.city_name');
		$this->db->from($this->table_favorite);
		$this->db->join($this->table_name,$this->table_name.'.id='.$this->table_favorite.'.properties_id_fk');
		$this->db->join( $this->table_properties_images, $this->table_properties_images.'.properties_id_fk = '.$this->table_name.'.id and primary = 1','left');
		$this->db->join($this->table_users,$this->table_users.'.id='.$this->table_favorite.'.user_id_fk');
		$this->db->join($this->tbl_perfectures, $this->tbl_perfectures.'.id = '.$this->table_name.'.location_perfecture','LEFT');
        $this->db->join($this->tbl_citys, $this->tbl_citys.'.id = '.$this->table_name.'.location_city','LEFT');
		$this->db->join($this->table_user_profile,$this->table_user_profile.'.user_id_fk='.$this->table_users.'.id');
		$this->db->where($this->table_users.'.id',$user_id);
		if($keyword!=""){
			$this->db->like('CONCAT('.$this->table_name.'.building_name,'.' '.$this->table_name.'.properties_type)',$keyword);
		}
		$query=$this->db->get();
		return $query->result();
	}

	function delete_favorite($properties_id,$user_id){
		$this->db->where("properties_id_fk",$properties_id);
		$this->db->where("user_id_fk",$user_id);
		$this->db->delete($this->table_favorite);
	}
}