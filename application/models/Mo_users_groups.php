<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mo_users_groups extends CI_Model {

	private $table_name			 = 'users_groups';
	private $table_users_profile = 'users_profile';
	private $table_users	 = 'users';
	function __construct()
	{
		parent::__construct();
		$ci =& get_instance();
		$this->table_name		   = $ci->config->item('db_table_prefix').$this->table_name;
		$this->table_users_profile = $ci->config->item('db_table_prefix').$this->table_users_profile;
		$this->table_users = $ci->config->item('db_table_prefix').$this->table_users;
	}
	public function groupdata_by_id($id){
		$this->db->where('id=', $id);
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}



}