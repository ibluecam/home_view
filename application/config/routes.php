<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'publics/Home';

// config rout controller publics
$route['city/(:any)'] = "publics/city/index/$1";
$route['lists/(:any)'] = "publics/lists/index/$1";
$route['lists/(:any)/(:any)'] = "publics/lists/index/$1/$2";
$route['lists/add_favorite'] = "publics/lists/add_favorite";
$route['lists/delete_favorite'] = "publics/lists/delete_favorite";

$route['map/ajax_load_city'] = "publics/map/ajax_load_city";
$route['map/ajax_load_building/(:num)'] = "publics/map/ajax_load_building/index/$1";
$route['map/ajax_load_latlng'] = "publics/map/ajax_load_latlng";

$route['detail/(:any)'] = "publics/detail/index/$1";
$route['favorite/(:any)/(:num)'] = "publics/favorite/$1/$2";

$route['favorite/info/(:num)'] = "publics/favorite/info/$1";
$route['favorite/info/edit/(:num)'] = "publics/favorite/favorite_edit_user/$1";

$route['blog/(:any)'] = "publics/blog/index/$1";
$route['blog/(:any)/(:any)'] = "publics/blog/index/$1/$2";
$route['blog-detail/(:num)'] = "publics/blog_detail/index/$1";

$route['agent/(:any)'] = "publics/agent/index/$1";
$route['agent/(:any)/(:any)'] = "publics/agent/index/$1/$2";

$route['videos/(:any)'] = "publics/videos/index/$1";
$route['videos/(:any)/(:any)'] = "publics/videos/index/$1/$2";

$route['step/(:any)'] = "publics/step/index/$1";

$route['pick-city/(:any)'] = "publics/pick_city/index/$1";

// config rout controller admin

$route['admin/blog/(:any)'] = "admin/blog/index/$1";
$route['admin/blog/add'] = "admin/blog/add";


$route['admin/info/edit/(:num)'] = "admin/my_info/edit/$1";
$route['admin/properties/edit/(:num)'] = "admin/properties/edit/$1";

$route['admin/info/(:num)'] = "admin/my_info/info/$1";
$route['admin/admin_info/info/edit/(:num)'] = "admin/admin_info/edit_info/$1";


$route['admin/(:any)'] = "admin/$1";
$route['admin/(:any)/(:any)'] = "admin/$1/$2";

$route['(:any)'] = "publics/$1";
$route['(:any)/(:any)'] = "publics/$1/$2";

$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
