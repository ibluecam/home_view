<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_blogs','mo_blogs');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
	}
	public function index(){
		$this->data["success"] = $this->session->flashdata('success');
		$this->blog();
	}
	public function blog(){
		if($this->superadmin_permission()){
			if($this->input->post("btndelete")){
				$this->delete();
			}
			$where = array();
			$count_blog=count($this->mo_blogs->count_blog_admin($where));
			$config = array();
	        $config["base_url"] = "/admin/blog";
	        $config["total_rows"] = $count_blog;
	        $config["per_page"] = 20;
	        $config["uri_segment"] = 3;
	        $config['first_link'] = '«';
	        $config['last_link'] = '»';
	        $config['next_link'] = 'NEXT';
	        $config['prev_link'] = 'PREVIOUS';
	        $config['num_links'] = 7;
	        $config['cur_tag_open'] = '<span class="current">';
	        $config['cur_tag_close'] = '</span>';
	        $this->pagination->initialize($config);

	        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $limit = $config["per_page"];
			$pagination = $this->pagination->create_links();
			$select_blog_admin = $this->mo_blogs->select_blog_admin($where,$limit,$offset);

			$this->data["select_blog_admins"] = $select_blog_admin;
			$this->data["pagination"]=$pagination;
			$this->header_page_title = "List blog";
			$this->header_icon = "fa-list-alt";
			$this->array_css=array("admin/list_blog");
			$this->array_js=array("admin/list_blog");
			$this->load_auth_view('admin/lists_blog');
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}	
	}
	public function add(){
		if($this->superadmin_permission()){
			$this->header_page_title = "add blog";
			$this->header_icon = "fa-book";
			$this->array_css=array("admin/add_blog","admin/jquery.filer");
			$this->array_js=array("admin/jquery.filer.min","admin/add_blog","jquery.validate.min");	
			$this->data["action_form"]= "";
			if($this->input->post("save")){
    			if($this->validate_form_blog()){
    				$this->exc_add();
    				redirect(site_url('/admin/blog'));
    			}
    		}
			$this->load_auth_view('admin/add_blog');
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	}
	public function exc_add(){
		if($this->superadmin_permission()){
			$data_blog = array();
	        $title = $this->input->post("title");
	        $category = $this->input->post("category");
	        $source = $this->input->post("source");
	        $body = $this->input->post("body");

	        $return_image_name = $this->upload_logo();

			$file_feature_image = "default_feature_image.jpg";
			if(!empty($return_image_name)){
				$file_feature_image = $return_image_name;
			}
			$user_id_fk = $this->get_session_user_id();
	        $data_blog = array(
		        				"title"				=> $title,
		        				"user_id_fk"		=> $user_id_fk,
		        				"feature_image"		=> $file_feature_image,
		        				"category"			=> $category,
		        				"source"			=> $source,
		        				"body"				=> $body
	        				);
	        $this->mo_blogs->add_blog($data_blog);

	        $this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Blog has been inserted.</div>');
	    }else{
	    	$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));	
	    }
	}
	public function edit($id = null){
		if($this->superadmin_permission()){
			$this->header_page_title = "Super Admin";
			$this->header_icon = "fa-user";
			$this->array_css=array("admin/jquery.filer","admin/edit_blog");
			$this->array_js=array("admin/jquery.filer.min","admin/edit_blog","jquery.validate.min");


			$this->data["action_form"] = "";
			$this->data["id"] = "";
			$this->data["title"] = "";
			$this->data["category"] = "";
			$this->data["feature_image"] = "";
			$this->data["body"] = "";
		
			if(!empty($id)){
				$select_blog = $this->mo_blogs->select_blog_by_id($id);
				if(!empty($select_blog)){
					$this->data["id"] = $select_blog->id;
					$this->data["title"] = $select_blog->title;
					$this->data["category"] = $select_blog->category;
					$this->data["source"] = $select_blog->source;
					$this->data["feature_image"] = $select_blog->feature_image;
					$this->data["body"] = $select_blog->body;

				}else{
					$this->session->set_flashdata('errors', 'You dont have permission to access the part of the site.');
					redirect(site_url('/admin/agent'));
				}
			}else{
					$this->session->set_flashdata('errors', 'You dont have permission to access the part of the site.');
					redirect(site_url('/admin/agent'));
			}

			if($this->input->post("save_update")){
	            if($this->validate_form_blog()){
	               $this->exc_update();
	            }
	        }

			$this->load_auth_view('admin/edit_blog');
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	}
	public function exc_update(){
		if($this->superadmin_permission()){
			$data_blog = array();
			$id = $this->input->post("id");
			$title = $this->input->post("title");
			$category = $this->input->post("category");
			$source = $this->input->post("source");
			$body = $this->input->post("body");

			$file_company_logo = "";
			$return_image_name = $this->upload_logo();
			if(!$this->check_image_when_update($id,$return_image_name)){
				$file_company_logo = $this->validate_logo($return_image_name);
			}

			$data_blog = array(
								"title"	=> $title,
								"category"	=> $category,
								"source"	=> $source,
								"body"		=> $body
							);

			$this->mo_blogs->update_blog($data_blog,$id);

			if(!empty($file_company_logo)){
				$data_blog_feature_image["feature_image"] = $file_company_logo;
				$this->mo_blogs->update_blog($data_blog_feature_image,$id);
			}

			$this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Blog has been update</div>');
	  		redirect("/admin/blog", 'refresh');
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	}
	private function validate_form_blog(){
		$this->form_validation->set_rules('title', 'title', 'trim|required',array('required' => 'title field is required.','trim' => 'title field is required.'));
		$this->form_validation->set_rules('category', 'category', 'trim|required',array('category' => 'category field is required.','trim' => 'category field is required.'));
		$this->form_validation->set_rules('source', 'source', 'trim|required',array('required' => 'source field is required.','trim' => 'source field is required.'));

		if ($this->form_validation->run())
        {
        	return true;	
        }
	}
	public function upload_logo(){
		$files = $_FILES["file_image"];
		$new_image_name = "";
		if(!empty($files['name'])){

	        $path = './assets/img/uploads/blog/original';
	        $config = array(
	            'upload_path'   => $path,
	            'allowed_types' => 'jpg|gif|png|jpeg',
	            'overwrite'     => 1,                       
	        );
	        $this->load->library('upload', $config);
	        $_FILES['file_image']['name']= $files['name'];
	        $_FILES['file_image']['type']= $files['type'];
	        $_FILES['file_image']['tmp_name']= $files['tmp_name'];
	        $_FILES['file_image']['error']= $files['error'];
	        $_FILES['file_image']['size']= $files['size'];
	        
	        $ex_image = explode(".", $files['name']);
		    $new_image_name = "adtezc32s" . uniqid() . date('Y-m-d-H-i-s') . uniqid() .".".$ex_image[1];

	        $config['file_name'] = $new_image_name;
	        $this->upload->initialize($config);
	        if ($this->upload->do_upload('file_image')) {
	            $this->upload->data();
	        } else {
	            return false;
	        }
	        $this->upload_image_resize($new_image_name);
	        $this->upload_image_thumb($new_image_name);
	    }
        return $new_image_name;
	}

	private function upload_image_resize($file_images_name){
        $this->load->library('image_lib');
        $source_path = './assets/img/uploads/blog/original/' . $file_images_name;
        $target_path = './assets/img/uploads/blog/resize/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 750
        );

        $this->image_lib->clear();
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
    }

    private function upload_image_thumb($file_images_name){
        $this->load->library('image_lib');
        $source_path = './assets/img/uploads/blog/original/' . $file_images_name;
        $target_path = './assets/img/uploads/blog/thumb/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 210
        );

        $this->image_lib->clear();
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
    }

    private function check_image_when_update($id,$file_images){
        $get_images = $this->mo_blogs->select_blog_by_id($id);
        if(count($get_images->feature_image) > 0 and empty($file_images)){
        	return true;
        }else{
        	$file_feature_image = "default_feature_image.jpg";
        	$image_path_resize = 'assets/img/uploads/blog/resize/';
        	$image_path_thumb = 'assets/img/uploads/blog/thumb/';
			$image_path_original = 'assets/img/uploads/blog/original/';
			$feature_image = $get_images->feature_image;
		    if (file_exists($image_path_resize.$feature_image) && $feature_image !== $file_feature_image){
				unlink($image_path_resize.$feature_image);
				unlink($image_path_original.$feature_image);
				unlink($image_path_thumb.$feature_image);
			}
        	return false;
        }
    }

    private function validate_logo($image_file){
		$file_company_logo = "default_feature_image.jpg";
		if(!empty($image_file) or ($image_file !=null)){
			$file_company_logo = $image_file;
		}
		return $file_company_logo;
	}

	public function delete(){
    	if($this->superadmin_permission()){
    		$id=$this->input->post("article_id");
    		$image_name=$this->input->post("image_name");
	    	if(!empty($id)){
	    		$this->mo_blogs->delete_blog($id);
	    		$this->unlink_image_blog($image_name);
	    		$this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Blog has been deleted.</div>');
				redirect("/admin/blog");
	    	}else{
	    		$this->session->set_flashdata('error', '<div class="alert alert-success"><strong>Warning!</strong> Blog can not deleted.</div>');
				redirect("/admin/blog");
	    	}
	    }else{
	    	$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
	    }
    }

   	private function unlink_image_blog($image_name){
   		if(!empty($image_name)){
   			if($image_name != "default_feature_image.jpg"){
   				$unlink_img="assets/img/uploads/blog/resize/".$image_name;
		        $unlink_thumb="assets/img/uploads/blog/thumb/".$image_name;
		        $unlink_original="assets/img/uploads/blog/original/".$image_name;
		        @unlink(FCPATH.$unlink_original);
		        @unlink(FCPATH.$unlink_img);
		        @unlink(FCPATH .$unlink_thumb);	
   			}
   		}
   	}

}