<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 ob_start();
class Properties extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_properties','mo_properties');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('session');
        $this->load->library('pagination');
	} 
	public function index()
	{	
        if($this->agent_permission()){
            $this->header_page_title = "List Properties";
            $this->header_icon = "fa-home";
            $this->array_css=array("admin/list_view");
            $this->array_js = array("admin/list_view");
            $user_id = $this->get_session_user_id();
            $where = array("properties.user_id_fk"=>$user_id);
            if($this->input->get("btndelete")){
                $this->delete_properties();
            }
            $keyword="";
            if($this->input->get('keyword')){
                 $keyword = $this->input->get('keyword');
            }
            $count_properties_agent = $this->mo_properties->admin_count_properties_lists($where,$keyword);
            $config = array();
            $config['page_query_string'] = TRUE;
            $config["base_url"] = "/admin/properties?keyword=".$keyword;
            $config["total_rows"] = $count_properties_agent->count_properties;
            $config["per_page"] = 20;
            $config["uri_segment"] = 3;
            $config['first_link'] = '«';
            $config['last_link'] = '»';
            $config['next_link'] = 'NEXT';
            $config['prev_link'] = 'PREVIOUS';
            $config['num_links'] = 7;
            $config['cur_tag_open'] = '<span class="current">';
            $config['cur_tag_close'] = '</span>';
            $this->pagination->initialize($config);
            $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $limit = $config["per_page"];
            $admin_get_properties_lists = $this->mo_properties->admin_get_properties_lists($where,$limit,$offset,$keyword);
            $pagination = $this->pagination->create_links();

            $this->data["pagination"]= $pagination;
            $this->data["count_proerties"]=$count_properties_agent;
            $this->data["admin_get_properties_lists"]  = $admin_get_properties_lists;
            $this->data["success"] = $this->session->flashdata('success');
            $this->load_auth_view('admin/list_view');
        }else{
            $this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
            redirect(site_url('/'));
        }
	}

    public function delete_properties(){
        if($this->agent_permission()){
            $id="";
            $unlink_img="";
            $is_checked= $this->input->get('chk_properties');
            if((int)$is_checked==1){
                foreach ($is_checked as $properties_id) {
                    $id=array('properties_id_fk' => $properties_id );
                    $result=$this->mo_properties->admin_get_properties_images($id);
                    foreach ($result as $rows) {
                        $unlink_original="assets/img/uploads/properties/original/".$rows->images_path;
                        $unlink_img="assets/img/uploads/properties/resize/".$rows->images_path;
                        $unlink_thumb="assets/img/uploads/properties/thumb/".$rows->images_path;
                        $unlink_crop="assets/img/uploads/properties/crop/".$rows->images_path;
                        @unlink(FCPATH.$unlink_original);
                        @unlink(FCPATH.$unlink_img);
                        @unlink(FCPATH .$unlink_thumb);
                        @unlink(FCPATH .$unlink_crop);
                    }
                   $this->mo_properties->delete_properties($properties_id);
                   $this->mo_properties->delete_all_properties_images($properties_id);
                   $this->mo_properties->delete_favorite_by_properties($properties_id); 
                }
                $this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Data has been deleted</div>');
                redirect('/admin/properties', 'refresh');
            }
        }else{
            $this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
            redirect(site_url('/'));
        }
    }


	public function add(){
		// include 'application/libraries/Resizeimage.php';
		// $image = new ImageResize('assets/img/map.png');
		// $image->resize(200, 200);
		// $image->save('assets/img/map2.png');
		// exit;
        if($this->agent_permission()){
    		$this->header_page_title = "UPLOAD PROPERTIES";
    		$this->header_icon = "fa-upload";
    		$this->array_css=array("admin/properties","admin/selectize","admin/jquery.filer");
            $this->array_js = array("admin/jquery.chained.min","admin/map","admin/selectize","admin/jquery.filer.min","jquery.validate.min","admin/properties");
    		$this->data["action_form"]	= "";
    		
    		$this->data["select_perfectures"]	                                = $this->mo_properties->select_perfectures();
    		$this->data["select_perfectures_citys"]	                            = $this->mo_properties->select_perfectures_city();
    		$this->data["select_option_floor_tool_properties"]                  = $this->mo_properties->select_option_floor_tool_properties();
            $this->data["select_option_type_tool_properties"]                   = $this->mo_properties->select_option_type_tool_properties();
            $this->data["select_option_gross_yield_tool_properties"]            = $this->mo_properties->select_option_gross_yield_tool_properties();
            $this->data["select_option_potential_annual_rent_tools_properties"] = $this->mo_properties->select_option_potential_annual_rent_tools_properties();
            $this->data["select_option_transaction_type_tools_properties"]      = $this->mo_properties->select_option_transaction_type_tools_properties();
            $this->data["select_option_parking_tools_properties"]               = $this->mo_properties->select_option_parking_tools_properties();
           //$this->data["select_option_layout_tools_properties"]                = $this->mo_properties->select_option_layout_tools_properties();
            $this->data["select_features"]                                      = $this->mo_properties->select_features();

            $this->data["select_layout"]=array("1room"=>"1 Room","1k"=>"1K","1dk"=>"1DK","1ldk"=>"1LDK","2k"=>"2K","2dk"=>"2DK","2LDk"=>"2LDK","3k"=>"3K","3dk"=>"3DK","3ldk"=>"3LDK","4k"=>"4K","4dk"=>"4DK","4ldk"=>"4LDK","more5k"=>"More 5K");

    		if($this->input->post("buy_save")){
    			if($this->validation_form_properties()){
    				$this->exc_add();
                  
                    redirect('/admin/properties', 'refresh');
    			}
    		}

            if($this->input->post("rent_save")){
                if($this->validation_form_properties()){
                    $this->exc_add();
                    redirect('/admin/properties', 'refresh');
                }
            }
    		$this->data["success"] = $this->session->flashdata('success');
            $this->map_js = true;
            $this->data['map_js'] = $this->map_js;
    		$this->load_auth_view('admin/add_properties');
        }else{
            $this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
            redirect(site_url('/'));
        }
	}
	public function exc_add(){
        if($this->agent_permission()){
    		$data_properties = array();
    		$data_image_properties = array();

        	$building_name = $this->input->post("building_name");
        	$floor = $this->input->post("floor");
            $management_fee = $this->input->post("management_fee");
            $management_currency = $this->input->post("management_currency");
        	$unit_number = $this->input->post("unit_number");
        	$type = $this->input->post("type");
        	$status = $this->input->post("status");
        	$size = $this->input->post("size");
            $maked = $this->input->post("maked");
            $position = $this->input->post("position");
        	$location_perfecture = $this->input->post("location_perfecture");
        	$location_city = $this->input->post("location_city");
        	$land_right = $this->input->post("land_right");
        	$gross_yield = $this->input->post("gross_yield");
        	$occupancy = $this->input->post("occupancy");
        	$nearest_station = $this->input->post("nearest_station");
            $distance_to_station = $this->input->post("distance_to_station");


        	$price = $this->input->post("price");

        	$currency = $this->input->post("currency");
        	$feature = $this->input->post("feature");
        	$description = $this->input->post("description");
        	$latitude = $this->input->post("latitude");
        	$longitude = $this->input->post("longitude");

            //- new field for buy
            $land_area = $this->input->post("land_area");
            $transportation = $this->input->post("transportation");
            $floorplan = $this->input->post("floorplan");
            //- new field for rent
            $deposit = $this->input->post("deposit");
            $key_money = $this->input->post("key_money");
            $agency_fee = $this->input->post("agency_fee");



        	/**===== Additional Detail Field =====*/
        	$layout = $this->input->post("layout");
        	$zoning = $this->input->post("zoning");
        	$year_build = $this->input->post("year_build");
        	$structure = $this->input->post("structure");
        	$maintainance_fee = $this->input->post("maintainance_fee");
            $maintenance_currency = $this->input->post("maintenance_currency");
        	$maintainance_fee_per_month = $this->input->post("maintainance_fee_per_month");
        	$available_from = $this->input->post("available_from");
        	$potential_annual_rent = $this->input->post("potential_annual_rent");
        	$unit_summary = $this->input->post("unit_summary");
        	$transaction_type = $this->input->post("transaction_type");
        	$parking = $this->input->post("parking");
        	$floor_area_ratio = $this->input->post("floor_area_ratio");
            //new field for buy
            $road_width = $this->input->post("road_width");
            $building_description = $this->input->post("building_description");
            $landmarks = $this->input->post("landmarks");
            $balcony_size = $this->input->post("balcony_size"); 
            
            //new field for rent
            $building_style = $this->input->post("building_style");
            $landlord_fee=$this->input->post("landlord_fee");
            $guarantor_company = $this->input->post("guarantor_company");
            $guarantor_agency_name = $this->input->post("guarantor_agency_name");
            $lease_term = $this->input->post("lease_term");
            $other_expenses = $this->input->post("other_expenses");
            $credit_card_payment = $this->input->post("credit_card_payment");
            $short_term_stay = $this->input->post("short_term_stay");

            $video_id = $this->input->post("video_id");

            $date_updated = date("Y-m-d H:i:s");

            if(strtotime($this->input->post("date_updated")) != ''){
                $date_updated = $this->input->post("date_updated");
                $date_updated = date("Y-m-d H:i:s",strtotime($date_updated));
            }

        	$building_area_ratio = $this->input->post("building_area_ratio");

            $next_update_schedule = date("Y-m-d H:i:s");
            if(strtotime($this->input->post("next_update_schedule")) != ''){
                $next_update_schedule = $this->input->post("next_update_schedule");
                $next_update_schedule = date("Y-m-d H:i:s",strtotime($next_update_schedule));
            }

        	//$properties_type = $this->input->post("properties_type");
        	
            $user_id_fk = $this->get_session_user_id();

            $return_plane_floor_image_name = $this->upload_image_plane_floor();

        	$feature = json_encode($feature);
            $data_properties = array(
            						"building_name" 				=> $building_name,
            						"user_id_fk" 					=> $user_id_fk,
            						"properties_type"				=> $type,
            						"floor" 						=> $floor,
                                    "management_fee"                => $management_fee,
                                    "management_currency"           => $management_currency,
                                    "unit_number"                   => $unit_number,
            						"landlord_fee" 					=> $landlord_fee,
            						"type" 							=> $type,
            						"status" 						=> $status,
            						"size" 							=> $size,
                                    "maked"                         => $maked,
                                    "position"                      => $position,
            						"location_perfecture" 			=> $location_perfecture,
            						"location_city"				 	=> $location_city,
            						"land_right" 					=> $land_right,
            						"gross_yield" 					=> $gross_yield,
            						"occupancy" 					=> $occupancy,
            						"nearest_station" 				=> $nearest_station,
                                    "distance_to_station"           => $distance_to_station,
            						"price" 						=> $price,
            						"currency" 						=> $currency,
            						"feature" 						=> $feature,
            						"description" 					=> $description,
            						"latitude" 						=> $latitude,
            						"longitude" 					=> $longitude,
                                    "land_area"                     => $land_area,
                                    "transportation"                => $transportation,
                                    "floorplan"                     => $floorplan,
                                    "deposit"                       => $deposit, // for rent
                                    "key_money"                     => $key_money, // for rent 
                                    "agency_fee"                    => $agency_fee, // for rent 
                                    //additional detail fields
            						"layout" 						=> $layout,
            						"zoning"			 			=> $zoning,
            						"year_build" 					=> $year_build,
            						"structure" 					=> $structure,
            						"maintainance_fee" 				=> $maintainance_fee,
                                    "maintenance_currency"          => $maintenance_currency,
            						"maintainance_fee_per_month" 	=> $maintainance_fee_per_month,
            						"available_from" 				=> $available_from,
            						"potential_annual_rent" 		=> $potential_annual_rent,
            						"unit_summary" 					=> $unit_summary,
            						"transaction_type" 				=> $transaction_type,
            						"parking" 						=> $parking,
                                    "road_width"                    => $road_width,
                                    "building_description"          => $building_description,
                                    "landmarks"                     => $landmarks,
                                    "balcony_size"                  => $balcony_size,
                                    "video_id"                      => $video_id,
                                    "building_style"                => $building_style,// for rent
                                    "guarantor_company"             => $guarantor_company,// for rent
                                    "guarantor_agency_name"         => $guarantor_agency_name,// for rent
                                    "lease_term"                    => $lease_term,// for rent
                                    "other_expenses"                => $other_expenses,// for rent
                                    "credit_card_payment"           => $credit_card_payment,// for rent
                                    "short_term_stay"               => $short_term_stay,// for rent
            						"floor_area_ratio" 				=> $floor_area_ratio,
                                    "floorplan"                     => $return_plane_floor_image_name,
            						"date_updated" 					=> $date_updated,
            						"building_area_ratio" 			=> $building_area_ratio,
            						"next_update_schedule" 			=> $next_update_schedule
            						); 

              //  var_dump($data_properties);

                $file_images = $this->input->post("file_images");
               //  var_dump($file_images);exit();
                $primary_image[] = $this->input->get_post("primary_image");
               
                
                
                $files = array();
                if($this->input->post("rent_save")){
                    $files = $_FILES["file_image"];
                }else{
                    $files = $_FILES["file_image"];
                }   
               

                $images = array();

                foreach ($files['name'] as $key => $image) {
                    $images[] = $image; 
                }
                $images = array_filter($images);
                 
                $file_images_name = array();
                if(!empty($file_images) and !empty($images)){
                    $file_images_name = array_intersect($images,$file_images);
                }
                $file_images_name_primary = array();
                if(!empty($file_images) and !empty($primary_image)){
                    $file_images_name_primary = array_intersect($primary_image,$file_images);
                }

                $return_image_name = $this->upload_file($file_images_name,$file_images_name_primary);

        		$properties_id = $this->mo_properties->add_properties($data_properties);
        	   
                if(!empty($return_image_name)){

                    foreach($return_image_name as $image_list){
                       
                        $data_image_properties = array(
                                                        "properties_id_fk"  => $properties_id,
                                                        "images_path"   => $image_list["images_list"],
                                                        "primary"       => $image_list["primary"],
                                                        "created_dt"    => date("Y-m-d H:i:s"),
                                                        "updated_dt"    => date("Y-m-d H:i:s")
                                                    );

                        $this->mo_properties->add_images_properties($data_image_properties);
                    }
                }
                // }else{
                //     $primary = 1;
                //     $image_list = "";//default_image_properties.jpg
                //     $data_image_properties = array(
                //                                     "properties_id_fk"  => $properties_id,
                //                                     "images_path"   => $image_list,
                //                                     "primary"       => $primary,
                //                                     "created_dt"    => date("Y-m-d H:i:s"),
                //                                     "updated_dt"    => date("Y-m-d H:i:s")
                //                                     );

                //     $this->mo_properties->add_images_properties($data_image_properties);
                // }
		        /*========= INSERT IMAGES PROPERTIES =============*/
		      
		    	$this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Data has been insert.</div>');
        }else{
            $this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
            redirect(site_url('/'));
        }
      
	}

    public function edit($id=null){
        if($this->agent_permission()){
            $this->header_page_title = "update properties";
            $this->header_icon = "fa-pencil-square-o";
            $this->array_css=array("admin/properties","admin/selectize","admin/jquery.filer");
            $this->array_js = array("admin/jquery.chained.min","admin/map","admin/selectize","admin/jquery.filer.min","jquery.validate.min","admin/properties");
            $this->data["action_form"]  = "/admin/properties/edit/";
            
            $this->data["select_perfectures"]                                   = $this->mo_properties->select_perfectures();
            $this->data["select_perfectures_citys"]                             = $this->mo_properties->select_perfectures_city();
            $this->data["select_option_floor_tool_properties"]                  = $this->mo_properties->select_option_floor_tool_properties();
            $this->data["select_option_type_tool_properties"]                   = $this->mo_properties->select_option_type_tool_properties();
            $this->data["select_option_gross_yield_tool_properties"]            = $this->mo_properties->select_option_gross_yield_tool_properties();
            $this->data["select_option_potential_annual_rent_tools_properties"] = $this->mo_properties->select_option_potential_annual_rent_tools_properties();
            $this->data["select_option_transaction_type_tools_properties"]      = $this->mo_properties->select_option_transaction_type_tools_properties();
            $this->data["select_option_parking_tools_properties"]               = $this->mo_properties->select_option_parking_tools_properties();
           // $this->data["select_option_layout_tools_properties"]                = $this->mo_properties->select_option_layout_tools_properties();
            $this->data["select_features"]                                      = $this->mo_properties->select_features();
            $this->data["select_layout"]=array("1room"=>"1 Room","1k"=>"1K","1dk"=>"1DK","1ldk"=>"1LDK","2k"=>"2K","2dk"=>"2DK","2LDk"=>"2LDK","3k"=>"3K","3dk"=>"3DK","3ldk"=>"3LDK","4k"=>"4K","4dk"=>"4DK","4ldk"=>"4LDK","more5k"=>"More 5K");

            $this->data["select_properties"] = $select_properties = $this->mo_properties->select_properties_by_id($id);
            $this->data["id"] = "";
            $this->data["building_name"] = "";
            $this->data["floor"] = "";
            $this->data["unit_number"] = "";
            $this->data["size"] = "";
            $this->data["status"] = "";
            $this->data["location_city"] = "";
            $this->data["zoning"] = "";
            $this->data["structure"] = "";
            $this->data["latitude"] = "";
            $this->data["longitude"] = "";
            $this->data["description"] = "";
            $this->data["land_right"] = "";
            $this->data["occupancy"] = "";
            $this->data["nearest_station"] = "";
            $this->data["price"] = "";
            $this->data["description"] = "";
            $this->data["maintainance_fee"] = "";
            $this->data["available_from"] = "";
            $this->data["unit_summary"] = "";
            $this->data["floor_area_ratio"] = "";
            $this->data["date_updated"] = "";
            $this->data["building_area_ratio"] = "";
            $this->data["next_update_schedule"] = "";
            $this->data["type"] = "";
            $this->data["location_perfecture"] = "";
            $this->data["location_city"] = "";
            $this->data["gross_yield"] = "";
            $this->data["currency"] = "";
            $this->data["year_build"] = "";
            $this->data["maintainance_fee_per_month"] = "";
            $this->data["potential_annual_rent"] = "";
            $this->data["transaction_type"] = "";
            $this->data["layout"] = "";
            $this->data["parking"] = "";
            $this->data["properties_type"] = "";
            $this->data["building_description"] = "";
            $this->data["road_width"] = "";
            $this->data["balcony_size"] = "";
            $this->data["other_expenses"] = "";
            $this->data["landmarks"] = "";
            $this->data["land_area"] = "";
            $this->data["arr_features"] = array();
            $this->data["image_name_lists"] = "";
            $this->data["building_style"] = "";
            $this->data["credit_card_payment"] = "";
            $this->data["short_term_stay"] = "";
            $this->data["video_id"] = "";
            $this->data["maked"] = "";
            $this->data["position"] = "";
            $this->data["distance_to_station"] = "";
            $where_img = array();
            $image_name_lists = "";
            if(!empty($select_properties)){
                //var_dump($select_properties);exit;
                /* SELECTED DATA TEXTBOX*/
                $this->data["id"] = $properties_id = $select_properties->id;

                $this->data["action_form"]  = "/admin/properties/edit/".$select_properties->id;
              //  $this->data["building_name"] = set_value('building_name') == false ? $select_properties->building_name : set_value('building_name');
               // $this->data["unit_number"] = set_value('unit_number') == false ? $select_properties->unit_number : set_value('unit_number');
                $this->data["size"] = set_value('size') == false ? $select_properties->size : set_value('size');
                $this->data["land_right"] = set_value('land_right') == false ? $select_properties->land_right : set_value('land_right');
                $this->data["occupancy"] = set_value('occupancy') == false ? $select_properties->occupancy : set_value('occupancy');
                $this->data["nearest_station"] = set_value('nearest_station') == false ? $select_properties->nearest_station : set_value('nearest_station');
                $this->data["price"] = set_value('price') == false ? $select_properties->price : set_value('price');
                $this->data["description"] = set_value('description') == false ? $select_properties->description : set_value('description');
                $this->data["latitude"] = set_value('latitude') == false ? $select_properties->latitude : set_value('latitude');
                $this->data["longitude"] = set_value('longitude') == false ? $select_properties->longitude : set_value('longitude');
                $this->data["zoning"] = set_value('zoning') == false ? $select_properties->zoning : set_value('zoning');
              //  $this->data["structure"] = set_value('structure') == false ? $select_properties->structure : set_value('structure');
                $this->data["maintainance_fee"] = set_value('maintainance_fee') == false ? $select_properties->maintainance_fee : set_value('maintainance_fee');
               // $this->data["available_from"] = set_value('available_from') == false ? $select_properties->available_from : set_value('available_from');
              //  $this->data["unit_summary"] = set_value('unit_summary') == false ? $select_properties->unit_summary : set_value('unit_summary');
               // $this->data["floor_area_ratio"] = set_value('floor_area_ratio') == false ? $select_properties->floor_area_ratio : set_value('floor_area_ratio');
                $this->data["date_updated"] = set_value('date_updated') == false ? date("Y-m-d",strtotime($select_properties->date_updated)) : set_value('date_updated');
                // $this->data["building_area_ratio"] = set_value('building_area_ratio') == false ? $select_properties->building_area_ratio : set_value('building_area_ratio');
                $this->data["next_update_schedule"] = set_value('next_update_schedule') == false ? date("Y-m-d",strtotime($select_properties->next_update_schedule)) : set_value('next_update_schedule');
                $this->data["management_fee"] = set_value('management_fee') == false ? $select_properties->management_fee : set_value('management_fee');
                /* END SELECTED DATA TEXTBOX*/

                $this->data["floorplan"] = $select_properties->floorplan;

                $this->data["video_id"] =  $select_properties->video_id;
                $this->data["floor"] = $select_properties->floor;
                $this->data["type"] = $select_properties->type;
                $this->data["location_perfecture"] = $select_properties->location_perfecture;
                $this->data["location_city"] = $select_properties->location_city;
                $this->data["gross_yield"] = $select_properties->gross_yield;
                $this->data["currency"] = $select_properties->currency;
                $this->data["maintenance_currency"] = $select_properties->maintenance_currency;
                $this->data["management_currency"] = $select_properties->management_currency;
                $this->data["year_build"] = $select_properties->year_build;
               // $this->data["maintainance_fee_per_month"] = set_value('maintainance_fee_per_month') == false ? $select_properties->maintainance_fee_per_month : set_value('maintainance_fee_per_month');
                //$this->data["potential_annual_rent"] = $select_properties->potential_annual_rent;
                $this->data["layout"] = $select_properties->layout;
              //  $this->data["transaction_type"] = $select_properties->transaction_type;
                $this->data["parking"] = $select_properties->parking;
                $this->data["transportation"]   = $select_properties->transportation;
                $this->data["landlord_fee"] = $select_properties->landlord_fee;
                $this->data["deposit"] = $select_properties->deposit;
                // $this->data["agency_fee"] = $select_properties->agency_fee;
                // $this->data["key_money"] = $select_properties->key_money;
                //$this->data["guarantor_company"] = $select_properties->guarantor_company;
                //$this->data["guarantor_agency_name"] = $select_properties->guarantor_agency_name;
                //$this->data["lease_term"] = $select_properties->lease_term;
                $this->data["balcony_size"] = $select_properties->balcony_size;
                $this->data["other_expenses"] = $select_properties->other_expenses;
                //$this->data["landmarks"] = $select_properties->landmarks;
                $this->data["land_area"] = $select_properties->land_area;
              //  $this->data["road_width"] = $select_properties->road_width;
               // $this->data["building_description"] = $select_properties->building_description;
               // $this->data["building_style"] = $select_properties->building_style;
               // $this->data["credit_card_payment"] = $select_properties->credit_card_payment;
              //  $this->data["short_term_stay"] = $select_properties->short_term_stay;
                $this->data["maked"] =  $select_properties->maked;
              //  $this->data["position"] =  $select_properties->position;
                $this->data["distance_to_station"] = $select_properties->distance_to_station;
                $status_name="";
                if($select_properties->status == "1"){
                    $status_name = "buy";
                }else if($select_properties->status == "2"){
                    $status_name = "rent";
                }else{
                    $status_name = "short_stay";
                }
                $this->data["status_name"] = $status_name;
                /* SELECTED RADIO*/
                $this->data["status"] = $select_properties->status;
                $this->data["properties_type"] = $select_properties->properties_type;
                /* END SELECTED RADIO*/

                /* SELECTED CHECKED BOX*/
                $feature = $select_properties->feature;
                $this->data["arr_features"] = json_decode($feature);
                $where_img["properties_id_fk"] = $properties_id;
                $images_lists = $this->mo_properties->admin_get_properties_images($where_img);
                $primary_image = "";
                $image_name_lists = "";
                foreach($images_lists as $images_list){
                    if($images_list->primary==1){
                        $primary_image = $images_list->images_path;
                    }
                    if(!empty($images_list->images_path)){
                        $image_name_lists .= $images_list->id."#_#".$images_list->images_path.",";
                    }
                    
                }
                $image_name_lists = rtrim($image_name_lists,",");
                $this->data["image_name_lists"] = $image_name_lists;
                $this->data["primary_image"] = $primary_image;
            }
            
            if($this->input->post("save_update")){
                if($this->validation_form_properties()){
                    $this->exc_update();
                    
                }
            }
            $this->data["success"] = $this->session->flashdata('success');
            $this->map_js = true;
             $this->data['map_js'] = $this->map_js;
            $this->load_auth_view('admin/edit_properties');
        }else{
            $this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
            redirect(site_url('/'));
        }
    }

    public function exc_update(){
        if($this->agent_permission()){
            $data_properties = array();
            $data_image_properties = array();
            $id = $this->input->post("id");

           // $building_name = $this->input->post("building_name");
            $floor = $this->input->post("floor");
            $management_fee = $this->input->post("management_fee");
            $management_currency = $this->input->post("management_currency");
          //  $unit_number = $this->input->post("unit_number");
            $type = $this->input->post("type");
            $status = $this->input->post("status");
            $size = $this->input->post("size");
            $maked = $this->input->post("maked");
            //$position = $this->input->post("position");
            $location_perfecture = $this->input->post("location_perfecture");
            $location_city = $this->input->post("location_city");
            $land_right = $this->input->post("land_right");
            $land_area = $this->input->post("land_area");
            $gross_yield = $this->input->post("gross_yield");
            $occupancy = $this->input->post("occupancy");
            $nearest_station = $this->input->post("nearest_station");
            $distance_to_station = $this->input->post("distance_to_station");
            $price = $this->input->post("price");
            $currency = $this->input->post("currency");
            $feature = $this->input->post("feature");
            $description = $this->input->post("description");
            $transportation = $this->input->post("transportation");
            $latitude = $this->input->post("latitude");
            $longitude = $this->input->post("longitude");
            /**===== Additional Detail Field =====*/
            $layout = $this->input->post("layout");
            $zoning = $this->input->post("zoning");
            $year_build = $this->input->post("year_build");
           // $structure = $this->input->post("structure");
            $maintainance_fee = $this->input->post("maintainance_fee");
            $maintenance_currency = $this->input->post("maintenance_currency");
           // $maintainance_fee_per_month = $this->input->post("maintainance_fee_per_month");
           // $available_from = $this->input->post("available_from");
           // $potential_annual_rent = $this->input->post("potential_annual_rent");
         //   $unit_summary = $this->input->post("unit_summary");
          //  $transaction_type = $this->input->post("transaction_type");
            $parking = $this->input->post("parking");
        //    $road_width = $this->input->post("road_width");
            $balcony_size = $this->input->post("balcony_size");
        //    $landmarks = $this->input->post("landmarks");

         //   $lease_term = $this->input->post("lease_term");
            $other_expenses = $this->input->post("other_expenses");
          //  $guarantor_company = $this->input->post("guarantor_company");
         //   $guarantor_agency_name = $this->input->post("guarantor_agency_name");

         //   $floor_area_ratio = $this->input->post("floor_area_ratio");

            $video_id = $this->input->post("video_id");

          //  $credit_card_payment = $this->input->post("credit_card_payment");
          //  $short_term_stay = $this->input->post("short_term_stay");
          //  $building_style = $this->input->post("building_style");
           // $building_description = $this->input->post("building_description");

         //   $agency_fee = $this->input->post("agency_fee");
            $deposit = $this->input->post("deposit");
            $landlord_fee=$this->input->post("landlord_fee");
          //  $key_money = $this->input->post("key_money");

            $date_updated = date("Y-m-d H:i:s");

            if(strtotime($this->input->post("date_updated")) != ''){
                $date_updated = $this->input->post("date_updated");
                $date_updated = date("Y-m-d H:i:s",strtotime($date_updated));
            }

            $building_area_ratio = $this->input->post("building_area_ratio");

            $next_update_schedule = date("Y-m-d H:i:s");
            if(strtotime($this->input->post("next_update_schedule")) != ''){
                $next_update_schedule = $this->input->post("next_update_schedule");
                $next_update_schedule = date("Y-m-d H:i:s",strtotime($next_update_schedule));
            }
            
            //$properties_type = $this->input->post("properties_type");
            
            $user_id_fk = $this->get_session_user_id();
      
            $feature = json_encode($feature);
            $data_properties = array(
                                 //   "building_name"                 => $building_name,
                                    "user_id_fk"                    => $user_id_fk,
                                    "properties_type"               => $type,
                                    "floor"                         => $floor,
                                    "management_fee"                => $management_fee,
                                    "management_currency"           => $management_currency,
                                    //"unit_number"                   => $unit_number,
                                    "type"                          => $type,
                                    "status"                        => $status,
                                    "size"                          => $size,
                                    "maked"                         => $maked,
                                   // "position"                      => $position,
                                    "location_perfecture"           => intval($location_perfecture),
                                    "location_city"                 => intval($location_city),
                                    "land_right"                    => $land_right,
                                    "land_area"                     => $land_area,
                                    "gross_yield"                   => $gross_yield,
                                    "occupancy"                     => $occupancy,
                                    "nearest_station"               => $nearest_station,
                                    "distance_to_station"           => $distance_to_station,
                                    "price"                         => $price,
                                    "currency"                      => $currency,
                                    "feature"                       => $feature,
                                    "description"                   => $description,
                                    "transportation"                => $transportation,
                                    "latitude"                      => $latitude,
                                    "longitude"                     => $longitude,
                                    "layout"                        => $layout,
                                    "zoning"                        => $zoning,
                                    "year_build"                    => $year_build,
                                   // "agency_fee"                    => $agency_fee,
                                    "deposit"                       => $deposit,
                                   // "key_money"                     => $key_money,
                                   // "structure"                     => $structure,
                                    "maintainance_fee"              => $maintainance_fee,
                                    "maintenance_currency"          => $maintenance_currency,
                                  //  "maintainance_fee_per_month"    => $maintainance_fee_per_month,
                                  //  "available_from"                => $available_from,
                                 //   "potential_annual_rent"         => $potential_annual_rent,
                                 //   "unit_summary"                  => $unit_summary,
                                  //  "transaction_type"              => $transaction_type,
                                    "parking"                       => $parking,
                                 //   "road_width"                    => $road_width,
                                    "video_id"                      => $video_id,
                                    "balcony_size"                  => $balcony_size,
                                 //   "landmarks"                     => $landmarks,
                                 //   "lease_term"                    => $lease_term,
                                    "other_expenses"                => $other_expenses,
                                    "landlord_fee"                  => $landlord_fee,
                                   // "guarantor_company"             => $guarantor_company,
                                  //  "guarantor_agency_name"         => $guarantor_agency_name,
                                 //   "floor_area_ratio"              => $floor_area_ratio,
                                  //  "credit_card_payment"           => $credit_card_payment,
                                 //   "short_term_stay"               => $short_term_stay,
                                 //   "building_style"                => $building_style,
                                 //   "building_description"          => $building_description,
                                 //   "building_area_ratio"           => $building_area_ratio,
                                    "next_update_schedule"          => $next_update_schedule,
                                    "date_updated"                  => $date_updated
                                    );
                // var_dump($data_properties);exit;
                $return_plane_floor_image_name = $this->upload_image_plane_floor();
                if(!empty($return_plane_floor_image_name)){
                    $floorplane_properties["floorplan"] = $return_plane_floor_image_name;
                    $this->mo_properties->update_properties($floorplane_properties,$id);
                }

                $file_images = $this->input->post("file_images");
                $files = $_FILES["file_image"];  
                $primary_image[] = $this->input->get_post("primary_image");
                $images = array();
                foreach ($files['name'] as $key => $image) {
                    $images[] = $image; 
                }
                $images = array_filter($images);

                $file_images_name = array();
                if(!empty($file_images) and !empty($images)){
                    $file_images_name = array_intersect($images,$file_images);
                }
                $file_images_name_primary = array();
                if(!empty($file_images) and !empty($primary_image)){
                    $file_images_name_primary = array_intersect($primary_image,$file_images);
                }

                $return_image_name = $this->upload_file($file_images_name,$file_images_name_primary);

                //var_dump($file_images_name_primary);exit;

                $this->mo_properties->update_properties($data_properties,$id);
                
                /*========= INSERT IMAGES PROPERTIES =============*/
                //$file_images = $this->input->post("file_images");
                $primary_image_update = "";
                if($this->check_image_when_update($id,$return_image_name)){
                    if(!empty($return_image_name)){
                        foreach($return_image_name as $image_list){
                            if($image_list["primary"] == 1){
                                $primary_image_update = $image_list["images_list"];
                            }
                            $data_image_properties = array(
                                                            "properties_id_fk"  => $id,
                                                            "images_path"   => $image_list["images_list"],
                                                            "created_dt"    => date("Y-m-d H:i:s"),
                                                            "updated_dt"    => date("Y-m-d H:i:s")
                                                        );
                            $this->mo_properties->add_images_properties($data_image_properties);
                        }
                    }
                }
                $where_update_image_primary = array();
                $where_reset_image_primary = array();
                $primary_images_id = $this->input->post("primary_image");
                if(!empty($primary_images_id)){
                    if(strlen($primary_images_id) >= 9){
                        $sub_string = substr($primary_images_id, 0,9);
                        if($sub_string != "adtezc32s"){
                            $primary_images_id = $primary_image_update;
                        }   
                    }else{
                            $primary_images_id = $primary_image_update;
                    }
                    
                }
                
                if(!empty($primary_images_id)){
                    //var_dump($primary_images_id);exit;
                    $where_update_image_primary["images_path"] = $primary_images_id;
                    $where_update_image_primary["properties_id_fk"] = $id;
                    $where_reset_image_primary["properties_id_fk"] = $id;

                    $this->mo_properties->reset_new_primary_images($where_reset_image_primary);
                    $this->mo_properties->update_primary_images($where_update_image_primary);
                }
                else{
                    if($this->check_image_primary($id)){
                        $where_update_image_primary["properties_id_fk"] = $id;
                        $where_reset_image_primary["properties_id_fk"] = $id;
                        $this->mo_properties->reset_new_primary_images($where_reset_image_primary);
                        $this->mo_properties->update_primary_images_limit($where_update_image_primary);
                    }
                }
                $this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Data has been update</div>');
                redirect(site_url("/admin/properties"));    
        }else{
            $this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
            redirect(site_url('/'));
        }
    }

    private function check_image_when_update($properties_id,$file_images){
        $where_get_images = array("properties_id_fk"=>$properties_id);
        $get_images = $this->mo_properties->admin_get_properties_images($where_get_images);
        if(count($get_images) == 0 and empty($file_images)){
            // $primary = 1;
            // $image_list = "default_image_properties.jpg";
            // $data_image_properties = array(
            //                                 "properties_id_fk"  => $properties_id,
            //                                 "images_path"   => $image_list,
            //                                 "primary"       => $primary,
            //                                 "created_dt"    => date("Y-m-d H:i:s"),
            //                                 "updated_dt"    => date("Y-m-d H:i:s")
            //                                 );
            // $this->mo_properties->add_images_properties($data_image_properties);
            return false;
        }else{
            return true;
        }
    }
    private function check_image_primary($properties_id){
        $where_get_images = array("properties_id_fk"=>$properties_id,"primary"=>"1");
        $get_images = $this->mo_properties->admin_get_properties_images($where_get_images);
        if(count($get_images) == 0){
            // $primary = 1;
            // $image_list = "default_image_properties.jpg";
            // $data_image_properties = array(
            //                                 "properties_id_fk"  => $properties_id,
            //                                 "images_path"   => $image_list,
            //                                 "primary"       => $primary,
            //                                 "created_dt"    => date("Y-m-d H:i:s"),
            //                                 "updated_dt"    => date("Y-m-d H:i:s")
            //                                 );
            // $this->mo_properties->add_images_properties($data_image_properties);
            return true;
        }else{
            return false;
        }
    }


    private function validation_form_properties(){
      //  $this->form_validation->set_rules('building_name', 'building_name', 'trim|required',array('required' => 'building name field is required.','trim' => 'building name field is required.'));
      //  $this->form_validation->set_rules('unit_number', 'unit_number', 'trim|required',array('required' => 'unit number field is required.','trim' => 'unit number field is required.'));
        // $this->form_validation->set_rules('floor', 'floor', 'trim|required',array('required' => 'floor field is required.','trim' => 'floor field is required.'));
        // $this->form_validation->set_rules('type', 'type', 'trim|required',array('required' => 'type field is required.','trim' => 'type field is required.'));
        // $this->form_validation->set_rules('size', 'size', 'trim|required',array('required' => 'size field is required.','trim' => 'size field is required.'));
        $this->form_validation->set_rules('location_perfecture', 'location_perfecture', 'trim|required',array('required' => 'location perfecture field is required.','trim' => 'location perfecture field is required.'));
        $this->form_validation->set_rules('location_city', 'location_city', 'trim|required',array('required' => 'location city field is required.','trim' => 'location city field is required.'));
        // $this->form_validation->set_rules('land_right', 'land_right', 'trim|required',array('required' => 'land right field is required.','trim' => 'land right field is required.'));
        // $this->form_validation->set_rules('gross_yield', 'gross_yield', 'trim|required',array('required' => 'gross yield field is required.','trim' => 'gross yield field is required.'));
        // $this->form_validation->set_rules('occupancy', 'occupancy', 'trim|required',array('required' => 'occupancy field is required.','trim' => 'occupancy field is required.'));
        // $this->form_validation->set_rules('nearest_station', 'nearest_station', 'trim|required',array('required' => 'nearest station field is required.','trim' => 'nearest station field is required.'));
        $this->form_validation->set_rules('price', 'price', 'trim|required|numeric',array('required' => 'price field is required.','trim' => 'price field is required.'));
        $this->form_validation->set_rules('year_build', 'year_build', 'trim|required',array('required' => 'building year field is required.','trim' => 'building yea field is required.'));

        $this->form_validation->set_rules('maked', 'maked', 'trim|required',array('required' => 'maked field is required.','trim' => 'maked field is required.'));
      //  $this->form_validation->set_rules('position', 'position', 'trim|required',array('required' => 'position perfecture field is required.','trim' => 'position field is required.'));
        $this->form_validation->set_rules('layout', 'layout', 'trim|required',array('required' => 'layout field is required.','trim' => 'layout field is required.'));
        $this->form_validation->set_rules('distance_to_station', 'distance_to_station', 'trim|required',array('required' => 'distance to station field is required.','trim' => 'distance to station field is required.'));

        if ($this->form_validation->run())
        {
            return true;    
        }
    }

    public function upload_file($file_images_name,$file_images_name_primary){
        $files = $_FILES["file_image"];
        $path = './assets/img/uploads/properties/original';
        
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'overwrite'     => 1,  
            // 'encrypt_name'  => TRUE                    
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=1;
        //var_dump($files['name']);exit;
        foreach ($file_images_name as $key => $image) {
            $_FILES['file_image[]']['name']= $files['name'][$key];
            $_FILES['file_image[]']['type']= $files['type'][$key];
            $_FILES['file_image[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['file_image[]']['error']= $files['error'][$key];
            $_FILES['file_image[]']['size']= $files['size'][$key];
            
            
            $ex_image = explode(".", $image);
            $new_image_name = "adtezc32s" . uniqid() . date('Y-m-d-H-i-s') . uniqid() .".".$ex_image[1];
            $fileName = $new_image_name;
            //$images["images"][] = $fileName;
            $config['file_name'] = $fileName;
            //copy($fileName,'./thumb/'.$fileName);
            if(!empty($file_images_name_primary[0])){

                if($file_images_name_primary[0] == $image){
                    $primary  = 1;
                }else{
                    $primary  = 0;
                }
            }else{
                if($i == 1){
                    $primary   = 1;  
                    
                }else{
                    $primary   = 0;  
                }

               
            }

            $images[] = array(
                            "primary"  => $primary,
                            "images_list"   => $fileName
                            );


            $this->upload->initialize($config);
            if ($this->upload->do_upload('file_image[]')) {
                $this->upload->data();
            } else {
                return false;
            }
             $i++;  
        }

        $this->upload_image_resize($images);
        $this->upload_image_thumbnail($images);
        $this->upload_image_crop($images);
        return $images;
    }

    public function upload_image_plane_floor(){

        $files = $_FILES["floor_plan_image"];
        $new_image_name = "";
        if(!empty($files['name'])){

            $path = './assets/img/uploads/properties/original';
            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'jpg|gif|png|jpeg',
                'overwrite'     => 1,                       
            );
            $this->load->library('upload', $config);
            $_FILES['floor_plan_image']['name']= $files['name'];
            $_FILES['floor_plan_image']['type']= $files['type'];
            $_FILES['floor_plan_image']['tmp_name']= $files['tmp_name'];
            $_FILES['floor_plan_image']['error']= $files['error'];
            $_FILES['floor_plan_image']['size']= $files['size'];
            
            $ex_image = explode(".", $files['name']);
            $new_image_name = "adtezc32s" . uniqid() . date('Y-m-d-H-i-s') . uniqid() .".".$ex_image[1];

            $config['file_name'] = $new_image_name;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('floor_plan_image')) {
                $this->upload->data();
            } else {
                return false;
            }
            $this->upload_plane_floor_image_resize($new_image_name);
        }
        return $new_image_name;
    }


    private function upload_image_thumbnail($file_images_name){
        $this->load->library('image_lib');
        foreach($file_images_name as $images_name){
            $source_path =  './assets/img/uploads/properties/resize/' . $images_name["images_list"];
            $target_path =  './assets/img/uploads/properties/thumb/';
            $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'allowed_types' => 'jpg|gif|png|jpeg',
                'maintain_ratio' => TRUE,
                'create_thumb' => TRUE,
                'thumb_marker' => '',
                'width' => 170,
                'height' => 170
            );

            $this->image_lib->clear();
            $this->image_lib->initialize($config_manip);
            //$this->image_lib->crop();
            $this->image_lib->resize();
        }
    }

    private function upload_image_crop($file_images_name){
        $this->load->library('image_lib');
        foreach($file_images_name as $images_name){
            $source_path =  './assets/img/uploads/properties/thumb/' . $images_name["images_list"];
            $target_path =  './assets/img/uploads/properties/crop/';
            $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'allowed_types' => 'jpg|gif|png|jpeg',
                'maintain_ratio' => TRUE,
                'create_thumb' => TRUE,
                'thumb_marker' => '',
                'x_axis'       => 40,
                'y_axis'        => 13,
                'width' => 100,
                'height' => 100
            );

            $this->image_lib->clear();
            $this->image_lib->initialize($config_manip);
            $this->image_lib->crop();
            //$this->image_lib->resize();
        }
    }

    private function upload_plane_floor_image_resize($file_images_name){
        $this->load->library('image_lib');
        $source_path = './assets/img/uploads/properties/original/' . $file_images_name;
        $target_path = './assets/img/uploads/properties/resize/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 477,
            'height' => 358
        );

        $this->image_lib->clear();
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
    }

    private function upload_image_resize($file_images_name){
        $this->load->library('image_lib');
        foreach($file_images_name as $images_name){
            $source_path = './assets/img/uploads/properties/original/' . $images_name["images_list"];
            $target_path = './assets/img/uploads/properties/resize/';
            $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'allowed_types' => 'jpg|gif|png|jpeg',
                'maintain_ratio' => TRUE,
                'create_thumb' => TRUE,
                'thumb_marker' => '',
                'width' => 477,
                'height' => 358
            );

            $this->image_lib->clear();
            $this->image_lib->initialize($config_manip);
            $this->image_lib->resize();
        }
    }

    public function remove_file_update(){
        if(isset($_POST['file']) && isset($_POST['img_id'])){
            $this->mo_properties->remove_images($_POST['img_id']);
            if($_POST['file'] != "default_image_properties.jpg"){
                $original = './assets/img/uploads/properties/original/'. $_POST['file'];
                $resize = './assets/img/uploads/properties/resize/'. $_POST['file'];
                $thumb = './assets/img/uploads/properties/thumb/'. $_POST['file'];
                $crop = './assets/img/uploads/properties/crop/'. $_POST['file'];
                if(file_exists($original) || file_exists($resize) || file_exists($thumb) && file_exists($crop)){
                    unlink($original);
                    unlink($resize);
                    unlink($thumb);
                    unlink($crop);
                }
            }
        }
    }
    public function remove_file_insert(){
        if(isset($_POST['file'])){
            //$ex_image = explode(".", $image);
            // $new_image_name = $ex_image[0]."_". date('Y-m-d-H-i-s') . uniqid() .".".$ex_image[1];
            // var_dump($_POST['file']);exit;
            $original = './assets/img/uploads/properties/original/'. $_POST['file'];
            $resize = './assets/img/uploads/properties/resize/'. $_POST['file'];
            $thumb = './assets/img/uploads/properties/thumb/'. $_POST['file'];
            $crop = './assets/img/uploads/properties/crop/'. $_POST['file'];
            if(file_exists($original) && file_exists($resize) && file_exists($thumb) && file_exists($crop)){
                unlink($original);
                unlink($resize);
                unlink($thumb);
                unlink($crop);
            }
        }
    }
    // public function remove_multi_file_insert(){
    //     $images_arr = explode(",", $_POST['check_multi_image']);
    //     foreach($images_arr as $image_arr){
    //         $file = './assets/img/uploads/'. $image_arr;
    //         if(file_exists($file)){
    //             unlink($file);
    //         }
    //     }
    // }
    // public function remove_multi_file_update(){
    //     $images_arr = explode(",", $_POST['check_multi_image']);
    //     foreach($images_arr as $image_arr){
    //         $this->mo_properties->remove_images_by_name($image_arr);
    //         $file = './assets/img/uploads/'. $image_arr;
    //         if(file_exists($file)){
    //             unlink($file);
    //         }
    //     }
    // }

	// private function upload_files($path,$title, $files)
 //    {
 //        $config = array(
 //            'upload_path'   => $path,
 //            'allowed_types' => 'jpg|gif|png',
 //            'overwrite'     => 1,                       
 //        );

 //        $this->load->library('upload', $config);

 //        $images = array();
 //        $title = str_replace(' ', '_', $title);
 //        //var_dump($files['name']);exit;
 //        foreach ($files['name'] as $key => $image) {
 //            $_FILES['file_image[]']['name']= $files['name'][$key];
 //            $_FILES['file_image[]']['type']= $files['type'][$key];
 //            $_FILES['file_image[]']['tmp_name']= $files['tmp_name'][$key];
 //            $_FILES['file_image[]']['error']= $files['error'][$key];
 //            $_FILES['file_image[]']['size']= $files['size'][$key];

 //            $fileName = $title .'_'. $image;
 //            $images[] = $fileName;
 //            $config['file_name'] = $fileName;

 //            $this->upload->initialize($config);
 //            if ($this->upload->do_upload('file_image[]')) {
 //                $this->upload->data();
 //            } else {
 //                return false;
 //            }
 //        }

 //        return $images;
 //    }
}
