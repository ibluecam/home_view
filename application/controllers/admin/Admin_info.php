<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_info extends MY_Controller { 

	public function __construct(){
		parent::__construct();
	} 
	public function index()
	{	
		if($this->superadmin_permission()){
			$this->info();
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	}
	public function info($id=null){
		if($this->superadmin_permission()){
			$this->header_page_title = "My Info";
			$this->header_icon = "fa-info";
			$this->array_css=array("admin/list_view","admin/admin_info");
			$user_info = $this->mo_user->select_user_by_id($id);
			$this->data["user_info"] = $user_info;
			$this->data["success"] = $this->session->flashdata('success');

			$this->load_auth_view('admin/admin_info');
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	} 
	public function edit_info($id=null){
		if($this->superadmin_permission()){
			$btn_edit = $this->input->post('btn_edit');
			$password = $this->input->post('password');
			$conf_pwd = $this->input->post('conf_pwd');
			if(isset($btn_edit)){
				$firstname = $this->input->post('firstname');
				$lastname  = $this->input->post('lastname'); 
				$email     = $this->input->post('email');
				$this->mo_user->update_users(array('email' => $email),$id);
				$data = array(
							'first_name' => $firstname,
							'last_name' => $lastname
						);
				$this->mo_user->update_user_profiles($data,$id);
				$this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Your information has been update</div>');
				redirect('/admin/admin_info/info/'.$id);
			}
			if(isset($password)){
				if($password == "" || $conf_pwd == ""){
					echo "<div class='alert alert-error'><strong>Warning!</strong> Password or Confirm password is required.</div>";exit();
				}else if($password != $conf_pwd){
					echo "<div class='alert alert-error'><strong>Warning!</strong> The confirm password field does not match the password field.</div>";exit();
				}else{
					$password = $this->mo_user->encrypt_password($password);
					$this->mo_user->update_users(array('password' => $password),$id);
					echo "<div class='alert alert-success'><strong>Success!</strong> Password has been updated.</div>";exit();
				}
			}

			$this->header_page_title = "Update my info";
			$this->header_icon = "fa-pencil-square-o";
			$this->array_css=array("admin/list_view","admin/admin_info");
			$this->array_js=array("admin/admin_edit_info");
			$user_info = $this->mo_user->select_user_by_id($id);
			$this->data["user_info"] = $user_info;
			$this->load_auth_view('admin/admin_edit_info');
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	}

	 public function validEmailEdit(){             
	  if(isset($_POST)){
            $email = $this->input->post('email');
            $user_id=$this->input->post("userId");
            if($this->mo_user->isEmailExistEidt($email,$user_id)){
             	echo json_encode(false);
            }else{
             	echo json_encode(true);
            }
        }
	}
	
}
   