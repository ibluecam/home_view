<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_info extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('User','mo_user');
	} 
	public function index()
	{	
		$this->info();
	}
	public function info($id=null){
		if($this->agent_permission()){
			if(!empty($id)){
				$user_info = $this->mo_user->select_user_by_id($id);
				if(count($user_info) > 0){

					$this->header_page_title = "My info";
					$this->header_icon = "fa-info";
					$this->array_css=array("admin/list_view","admin/my_info");
					$this->data["user_info"] = $user_info;
					$this->data["success"] = $this->session->flashdata('success');
					$this->load_auth_view('admin/my_info');	

				}else{
					redirect(site_url('/'));
				}
			}else{
				redirect(site_url('/'));
			}
		}else{
			redirect(site_url('/'));
		}
	}

	public function edit($id = null){
		if($this->agent_permission()){
			
			$this->header_page_title = "update my info";
			$this->header_icon = "fa-pencil-square-o";
			$this->array_css=array("admin/list_view","admin/jquery.filer","admin/edit_my_info");
			$this->array_js=array("admin/jquery.filer.min","admin/edit_my_info");

			$this->data["action_form"] = "";
			$this->data["id"] = "";
			$this->data["company_name"] = "";
			$this->data["email"] = "";
			$this->data["password"] = "";
			$this->data["confirm_password"] = "";
			$this->data["skype_id"] = "";
			$this->data["viber_id"] = "";
			$this->data["line_id"] = "";
			$this->data["phone1"] = "";
			$this->data["phone2"] = "";
			$this->data["office_location"] = "";
			$this->data["description"] = "";
			$this->data["logo"] = "";
			if(!empty($id)){
				$select_agent = $this->mo_user->select_user_by_id($id);
				if(!empty($select_agent)){
					$this->data["id"] = $select_agent->user_id;
					$this->data["company_name"] = $select_agent->company_name;
					$this->data["logo"] = $select_agent->logo;
					$this->data["email"] = $select_agent->email;
					//$this->data["password"] = $this->mo_user->encrypt_password($select_agent->password);
					//$this->data["confirm_password"] = $this->mo_user->encrypt_password($select_agent->password);

					$this->data["skype_id"] = $select_agent->skype_id;
					$this->data["viber_id"] = $select_agent->viber_id;
					$this->data["line_id"] = $select_agent->line_id;
					$this->data["phone1"] = $select_agent->phone1;
					$this->data["phone2"] = $select_agent->phone2;
					$this->data["office_location"] = $select_agent->office_location;
					$this->data["description"] = $select_agent->description;
				}else{
					$this->session->set_flashdata('errors', 'You dont have permission to access the part of the site.');
					redirect(site_url('/admin/agent'));
				}
			}else{
				$this->session->set_flashdata('errors', 'You dont have permission to access the part of the site.');
				redirect(site_url('/admin/agent'));
			}

			if($this->input->post("save_update")){
	            if($this->validation_form_agent_update()){
	               $this->exc_update();
	            }
	        }

			$this->load_auth_view('admin/edit_my_info');
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	}
	private function exc_update(){
		if($this->agent_permission()){
			$data_user = array();
			$data_user_profile = array();
			$data_user_image_profile = array();
			$id = $this->input->post("id");
			$company_name = $this->input->post("company_name");
			$email = $this->input->post("email");
			
			$skype_id = $this->input->post("skype_id");
			$viber_id = $this->input->post("viber_id");
			$line_id = $this->input->post("line_id");
			$phone1 = $this->input->post("phone1");
			$phone2 = $this->input->post("phone2");
			$office_location = $this->input->post("office_location");
			$description = $this->input->post("description");
			
			// $file_company_logo = $this->input->post("file_image");
			//
			$file_company_logo = "";
			$return_image_name = $this->upload_logo();
			if(!$this->check_image_when_update($id,$return_image_name)){
				$file_company_logo = $this->validate_logo($return_image_name);
			}	

			
			//$file_company_logo = $this->validate_logo($return_image_name);
			// if($this->validate_password($password,$confirm_password)){
			// 	$password = $this->mo_user->encrypt_password($password);
			// }

			$data_user = array(
								"email"			=> $email,
								//"password"		=> $password,
								"user_group"	=> 2
							);


			$data_user_profile = array(
									"company_name"			=> $company_name,
									"phone1"				=> $phone1,
									"phone2"				=> $phone2,
									"skype_id"				=> $skype_id,
									"viber_id"				=> $viber_id,
									"line_id"				=> $line_id,
									"office_location"		=> $office_location,
									"description"			=> $description
									);
			if(!empty($file_company_logo)){
				$data_user_image_profile["logo"] = $file_company_logo;
				$this->mo_user->update_user_profiles($data_user_image_profile,$id);
			}
			$this->mo_user->update_users($data_user,$id);
			$this->mo_user->update_user_profiles($data_user_profile,$id);
			$this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Your information has been update</div>');
	  		redirect("/admin/info/".$id, 'refresh');
	  	}else{
	  		$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
	  	}
	}
	public function change_password(){
		if($this->agent_permission()){
			$password_data_user = array();
			$password = $this->input->post("password");
			$confirm_password = $this->input->post("confirm_password");
			$id = $this->input->post("id");

			if($this->validate_password_update($password,$confirm_password) == 1){
				$password = $this->mo_user->encrypt_password($password);
				$password_data_user = array(
											"password" => $password
											);
				$this->mo_user->update_users($password_data_user,$id);
				$message["msg"] = "Password has been updated.";
			}else{
				$message["msg_error"] = $this->validate_password_update($password,$confirm_password);
			}

			echo json_encode($message);
		}else{
			$this->session->set_flashdata('errors', 'You dont have permission to access the admin part of the site.');
			redirect(site_url('/'));
		}
	}

	private function check_image_when_update($user_profile_id,$file_images){
        $get_images = $this->mo_user->select_user_by_id($user_profile_id);

        if(count($get_images) > 0 and empty($file_images)){
        	return true;
        }else{
			$file_company_logo = "company_logo_default.jpg";
        	$image_path_resize = 'assets/img/uploads/company/resize/';
			$image_path_original = 'assets/img/uploads/company/original/';
			$logo = $get_images->logo;
		    if (file_exists($image_path_resize.$logo) && $logo != $file_company_logo){
	    		unlink($image_path_resize.$logo);
				unlink($image_path_original.$logo);
			}
        	return false;
            // $image_list = "company_logo_default.jpg";
            // $data_image_properties = array(
            //                                 "user_id_fk"  => $user_id_fk,
            //                                 "logo"   => $image_list,
            //                                 "created_dt"    => date("Y-m-d H:i:s"),
            //                                 "updated_dt"    => date("Y-m-d H:i:s")
            //                                 );
            // $this->mo_properties->add_images_properties($data_image_properties);
        }
    }

	private function validate_password_update($password,$comfirm_password){
		if(!empty($password) or !empty($comfirm_password)){
			if($password == $comfirm_password){
				return true;
			}else{
				return "The confirm password field does not match the password field.";
			}
		}else{
				return "Password or Confirm password is required.";
		}		
	}
	private function validate_logo($image_file){
		$file_company_logo = "company_logo_default.jpg";
		if(!empty($image_file) or ($image_file !=null)){
			$file_company_logo = $image_file;
		}
		return $file_company_logo;
	}	
	private function validation_form_agent_update(){
		$this->form_validation->set_rules('company_name', 'company_name', 'trim|required',array('required' => 'company name is required.','trim' => 'company is required.'));
		$this->form_validation->set_rules('email', 'email', 'trim|required',array('required' => 'email is required.','trim' => 'email is required.'));
		
		if ($this->form_validation->run())
        {
        	return true;	
        }
	}

	public function upload_logo(){

		$files = $_FILES["file_image"];
		$new_image_name = "";
		if(!empty($files['name'])){
	        $path = './assets/img/uploads/company/original';
	        $config = array(
	            'upload_path'   => $path,
	            'allowed_types' => 'jpg|gif|png|jpeg',
	            'overwrite'     => 1,                       
	        );
	        $this->load->library('upload', $config);
	        $_FILES['file_image']['name']= $files['name'];
	        $_FILES['file_image']['type']= $files['type'];
	        $_FILES['file_image']['tmp_name']= $files['tmp_name'];
	        $_FILES['file_image']['error']= $files['error'];
	        $_FILES['file_image']['size']= $files['size'];

	        $ex_image = explode(".", $files['name']);
	        $new_image_name = "adtezc32s" . uniqid() . date('Y-m-d-H-i-s') . uniqid() .".".$ex_image[1];
	        $config['file_name'] = $new_image_name;
	        $this->upload->initialize($config);
	        if ($this->upload->do_upload('file_image')) {
	            $this->upload->data();
	        } else {
	            return false;
	        }
	        $this->upload_image_resize($new_image_name);
	        $this->upload_image_crop($new_image_name);
	    }
        return $new_image_name;
	}

	private function upload_image_resize($file_images_name){
        $this->load->library('image_lib');
        $source_path = './assets/img/uploads/company/original/' . $file_images_name;
        $target_path =  './assets/img/uploads/company/resize/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 135,
            'height' => 100
        );

        $this->image_lib->clear();
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
    }
    private function upload_image_crop($file_images_name){
        $this->load->library('image_lib');
            $source_path =  './assets/img/uploads/company/resize/' . $file_images_name;
            $target_path =  './assets/img/uploads/company/crop/';
            $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'allowed_types' => 'jpg|gif|png|jpeg',
                'maintain_ratio' => TRUE,
                'create_thumb' => TRUE,
                'thumb_marker' => '',
                'x_axis'       => 40,
                'y_axis'        => 13,
                'width' => 70,
                'height' => 70
            );

            $this->image_lib->clear();
            $this->image_lib->initialize($config_manip);
            $this->image_lib->crop();
    }


	public function remove_file_insert(){
        if(isset($_POST['file'])){
        	if($_POST['file'] != "company_logo_default.jpg"){
        		$original = './assets/img/uploads/company/original/'. $_POST['file'];
        		$resize = './assets/img/uploads/company/resize/'. $_POST['file'];
	            if(file_exists($original) || file_exists($resize)){
	                unlink($original);
	                unlink($resize);
	            }
        	} 
        }
    }

    public function validEmailEdit(){             
	  if(isset($_POST)){
            $email = $this->input->post('email');
            $user_id=$this->input->post("userId");
            if($this->mo_user->isEmailExistEidt($email,$user_id)){
             	echo json_encode(false);
            }else{
             	echo json_encode(true);
            }
        }
	}



}
