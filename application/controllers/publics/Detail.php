<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Detail extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('PHPMailer'); 
		$this->load->library('SMTP');
		$this->load->model("Mo_detail","mo_detail");
		$this->load->model('Mo_home','mo_home');
	} 
	public function index()
	{		
		$this->detail();
	}
	public function detail(){
		if($this->uri->segment(2)){
			$properties_id=$this->uri->segment(2);
			$this->mo_detail->update_viewer($properties_id);
			$properties=$this->mo_detail->select($properties_id);
			$properties_image=$this->mo_detail->select_image($properties_id);
			$select_features=$this->mo_detail->select_features();
			if($properties){
				$similar_location=$this->mo_detail->similer_location($properties[0]->location_perfecture,$properties_id);
				$this->data["arr_features"]=json_decode($properties[0]->feature);
				$this->data["properties_image"]=$properties_image;
				$this->data["select_features"]=$select_features;
				$this->data["similar_location"]=$similar_location;
			}
			$this->data["properties"]=$properties;
		}else{
			$this->data["properties"]=array();
			$this->data["similar_location"]=array();
		}
		$get_most_recent = $this->mo_home->get_properties(5,0);
		$this->data['most_recent'] = $get_most_recent;
		$this->map_js=true;
		$this->data["map_js"]=$this->map_js;
		$this->header_page_title = "Detail";
		$this->header_icon = "fa-search";
	    if($this->is_mobile==true){
			$this->array_css=array("mobile/detail");
			$this->array_js=array("mobile/detail");
			$this->load_view_mobile('mobile/detail');
		}else{
			$this->array_js=array("publics/detail");
			$this->array_css=array("publics/detail");
			$this->load_view('publics/detail');
		}
		
	}

	public function send_inquire(){
		$name=$this->input->post('name');
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		$message=$this->input->post('message');
		$email_owner=$this->input->post('email_owner');

		$to= $email_owner;
		$user_name="support@homeview.jp";
		$message="";
		$message.='';
		$this->phpmailer->CharSet = "UTF-8";
        $this->phpmailer->IsSMTP();
        $this->phpmailer->Host = "ssl://homeview.jp";
        $this->phpmailer->Port = "465";
        $this->phpmailer->SMTPAuth = "yes";
        $this->phpmailer->Username = $user_name;
        $this->phpmailer->Password = "b#s_yMDfB={?";
        $this->phpmailer->FromName = $name;
        $this->phpmailer->From     = $email;
        $this->phpmailer->AddAddress($to);      
        $this->phpmailer->Subject  = "INQUIRE";        
        $this->phpmailer->MsgHTML($message);
        $this->phpmailer->IsHTML(true);   
        $valid = $this->phpmailer->Send(); 
		if($valid !=1) {
            echo "Error sending: " . $this->phpmailer->ErrorInfo;                                         
        }
	}
	
}
