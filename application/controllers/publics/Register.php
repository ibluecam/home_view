<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('User','mo_user');
	} 
	public function index()
	{	
		$btn_register = $this->input->post('btn_register');
		if($btn_register){
			if($this->validation_register()){
				$this->register_exec();
			}
		}
		$this->register();	
	}
	public function register(){
		$this->header_page_title = "account";
		$this->header_icon = "fa-user";

		if($this->is_mobile==true){
			$this->array_css=array("mobile/register");
			$this->array_js = array("publics/register");
			$this->load_view_mobile('mobile/register');
		}else{
			$this->array_css = array("publics/register");
			$this->array_js = array("publics/register");
			$this->load_view('publics/register');
		}
	}
	public function register_exec(){

		//Get value from view 
		$first_name = $this->input->post('firstname');
		$last_name = $this->input->post('lastname');
		$email = $this->input->post('email');
		$pass_word = $this->input->post('password');
		$password = $this->mo_user->encrypt_password($pass_word);
		$today = date("Y-m-d H:i:s");
		$user_token = $this->mo_user->encrypt_password($pass_word.$today);

		$data = array(
					"email" 	 => $email,
					"password"   => $password,
					"user_group" => 3,
					"user_token" => $user_token,
					"created_dt" => $today
				);
		$last_id = $this->mo_user->insertUser($data);
		$data_name = array(
					"first_name" => $first_name,
					"last_name"  => $last_name,
					"user_id_fk" => $last_id
				);
		$this->mo_user->insertUserProfile($data_name);
		$this->mo_user->registerSuccess($last_id);
		redirect('/favorite');
	}

	public function validation_register(){
		$config = array(
		        array(
		                'field' => 'firstname',
		                'label' => 'First Name',
		                'rules' => 'required'
		        ),
		        array(
		                'field' => 'lastname',
		                'label' => 'Last Name',
		                'rules' => 'required'
		        ),
		        array(
		                'field' => 'password',
		                'label' => 'Password',
		                'rules' => 'required|min_length[3]|max_length[20]',
		                'errors' => array('required' => 'You must provide a %s.'),
		        ),
		        array(
		                'field' => 'email',
		                'label' => 'Email',
		                'rules' => 'trim|required|valid_email|min_length[3]|max_length[50]|is_unique[users.email]',
		                'errors' => array('is_unique' => '%s already exists.')
		        ),
		        array(
		                'field' => 'agree',
		                'label' => 'Please indicate that you accept the Terms of use and Privacy.',
		                'rules' => 'required',
		                'errors' => array('required' => '%s')
		        )
		);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run())
        {
        	return true;	
        }
	}
	public function validEmail(){             
	  if(isset($_POST)){
            $email = $this->input->post('email');
            if($this->mo_user->isEmailExist($email)){
             	echo json_encode(false);
            }else{
             	echo json_encode(true);
            }
        }
	}
}
