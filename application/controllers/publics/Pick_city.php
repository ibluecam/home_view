<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pick_city extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Mo_pickcity",'mo_pickcity');
	} 
	public function index()
	{		
		$this->header_page_title = "SEARCH";
		$this->header_icon = "fa-search";
		$count_properties=$this->mo_pickcity->count_properties();
		$get_admin_info=$this->mo_pickcity->get_admin_info();

		$link_status = $this->uri->segment(2);
        if($link_status == 'buy'){
            $status = $link_status;
        }else if($link_status == 'short-stay'){
            $status = $link_status;
        }else if($link_status == 'rent'){
            $status = 'rent';
        }else{
        	$status = '';
        }
        $this->data['link_status'] = $status;
	
		$this->data["get_admin_info"]=$get_admin_info;
		$this->data["count_properties"]=$count_properties;
		if($this->is_mobile == true){
			$this->array_css=array("mobile/pick_city");
			$this->load_view_mobile('mobile/pick_city');
		}else{
			$this->array_css=array("publics/pick_city");
			$this->load_view('publics/pick_city');
		}
	}
}
