<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Favorite extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Mo_favorite",'mo_favorite');
		$this->load->model("User",'mo_user');
		$this->load->library('pagination');
		
		//check is logged_in
		if(!$this->user_permission()){
			redirect("/");
		}
	} 
	public function index()
	{		
		$this->favorite();
	}

	public function favorite(){

		$user_id=$this->me->user_id;
		$keyword="";
		if($this->input->get('btndelete')){
			$this->delete_favorite();
			redirect("/favorite");
		}
		 if($this->input->get('keyword')){
			$keyword = $this->input->get('keyword');
		}
		$count_favarite=count($this->mo_favorite->select_count_favorite($user_id,$keyword));
		$config = array();
		$config['page_query_string'] = TRUE;
        $config["base_url"] = "/favorite?keyword=".$keyword;
        $config["total_rows"] = $count_favarite;
        $config["uri_segment"] = 2;
        $config['first_link'] = '«';
        $config['last_link'] = '»';
        $config['next_link'] = 'NEXT';
        $config['prev_link'] = 'PREVIOUS';
        if($this->is_mobile==true){
        	$config['num_links'] = 3;
        	$config["per_page"] = 6;
    	}else{
    		$config['num_links'] = 7;
        	$config["per_page"] = 16;
    	}
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';
        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $limit = $config["per_page"];
		$favorites= $this->mo_favorite->select_favorite($user_id,$limit,$offset,$keyword);
		$pagination = $this->pagination->create_links();
		//var_dump($favorites);
		$this->data["favorites"]= $favorites;
		$this->data["pagination"]= $pagination;
		$this->data["count_favarite"]=$count_favarite;
		$this->data["user_id"]	= $user_id;
		$this->header_page_title = "favorite";
		$this->header_icon = "fa-heart";

		if($this->is_mobile==true){
			$this->array_css=array("mobile/favorite");
			$this->array_js=array("mobile/favorite");
			$this->load_view_mobile('mobile/favorite');
		 }else{
			$this->array_css=array("publics/favorite");
			$this->array_js=array("publics/favorite");
			$this->load_auth_view('publics/favorite');
		}
		$this->data["success"] = $this->session->flashdata('success');
	

	}

	public function delete_favorite(){
		$is_checked= $this->input->get('chk_favordite');
		if((int)$is_checked==1){
			foreach ($is_checked as $value) {
				$arr_value=explode(",",$value);
				$properties_id=$arr_value[0];
				$user_id=$arr_value[1];
				$this->mo_favorite->delete_favorite($properties_id,$user_id);
			}
			$this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Your favorite is deleted.</div>');
		}
	}

	public function info($id = null){

		//$user_id=$this->me->user_id;
		$keyword="";
		$this->header_page_title = "favorite";
		$this->header_icon = "fa-heart";
		$count_favarite=count($this->mo_favorite->select_count_favorite($id,$keyword));
		$this->data["count_favarite"]=$count_favarite;
		$user_info = $this->mo_user->select_user_by_id($id);
		$this->data["user_info"] = $user_info;
		$this->data["success"] = $this->session->flashdata('success');
		if($this->is_mobile==true){
			$this->array_css=array("mobile/favorite_info");
			$this->load_view_mobile('mobile/favorite_info');
		}else{
			$this->array_css=array("publics/favorite_user_info");
			$this->load_auth_view('publics/favorite_user_info');
		}
	}

	public function favorite_edit_user($id = null){

		$btn_edit = $this->input->post('btn_edit');
		$password = $this->input->post('password');
		$conf_pwd = $this->input->post('conf_pwd');
		if(isset($btn_edit)){
			$firstname = $this->input->post('firstname');
			$lastname  = $this->input->post('lastname'); 
			$email     = $this->input->post('email');
			$this->mo_user->update_users(array('email' => $email),$id);
			$data = array(
						'first_name' => $firstname,
						'last_name' => $lastname
					);
			$this->mo_user->update_user_profiles($data,$id);
			$this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Success!</strong> Your information has been update</div>');
			redirect('/favorite/info/'.$id);
		}
		if(isset($password)){
			if($password == "" || $conf_pwd == ""){
				echo "<div class='alert alert-error'><strong>Warning!</strong> Password or Confirm password is required.</div>";exit();
			}else if($password != $conf_pwd){
				echo "<div class='alert alert-error'><strong>Warning!</strong> The confirm password field does not match the password field.</div>";exit();
			}else{
				$password = $this->mo_user->encrypt_password($password);
				$this->mo_user->update_users(array('password' => $password),$id);
				echo "<div class='alert alert-success'><strong>Success!</strong> Password has been updated.</div>";exit();
			}
		}
		$keyword="";
		$this->header_page_title = "favorite";
		$this->header_icon = "fa-heart";
		$count_favarite=count($this->mo_favorite->select_count_favorite($id,$keyword));
		$this->data["count_favarite"]=$count_favarite;
		$user_info = $this->mo_user->select_user_by_id($id);
		$this->data["user_info"] = $user_info;
		if($this->is_mobile==true){
			$this->array_css=array("mobile/favorite_edit_info");
			$this->array_js=array("publics/favorite_edit_user");
			$this->load_view_mobile('mobile/favorite_edit_info');
		  }else{
		 	$this->array_css=array("publics/favorite_edit_user");
		 	$this->array_js=array("publics/favorite_edit_user");
		 	$this->load_auth_view('publics/favorite_edit_user');
		}
	}

	public function validEmailEdit(){             
	  if(isset($_POST)){
            $email = $this->input->post('email');
            $user_id=$this->input->post("userId");
            if($this->mo_user->isEmailExistEidt($email,$user_id)){
             	echo json_encode(false);
            }else{
             	echo json_encode(true);
            }
        }
	}

}
