<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Blog extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_blogs','mo_blogs');
		$this->load->library('pagination');
	} 
	public function index()
	{		
		$this->laod_blog();
	}
	public function laod_blog(){
		$category = $this->uri->segment(2);
		if(isset($category) && $category != 'all'){
			$where = array(
				'category' => $category,
				);
		}else{
			$where = array();
			$category = 'all';
		}

		$count_blog = $this->mo_blogs->select_blog_user_count($where);
		$total_rows = $count_blog[0]->count_blog;
		$config = array();
        $config["base_url"] = "/blog/".$category;
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['next_link'] = 'NEXT';
        $config['prev_link'] = 'PREVIOUS';
        $config['num_links'] = 3;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';
        $this->pagination->initialize($config);
        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $limit = $config["per_page"];
        $pagination = $this->pagination->create_links();
        $this->data['pagination'] = $pagination;
		$get_blogs = $this->mo_blogs->select_blog_user($where,$limit,$offset);
		$this->data['get_blogs'] = $get_blogs;
		$this->data['blog_link_active'] = $category;
		$this->header_page_title = "blog";
		$this->header_icon = "fa-book";
		if($this->is_mobile==true){
			$this->array_css=array("mobile/blog");
			$this->load_view_mobile('mobile/blog');
		}else{
			$this->array_css = array("publics/blog");
			$this->load_view('publics/blog');
		}
	}

}
