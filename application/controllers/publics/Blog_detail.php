<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Blog_detail extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_blogs','mo_blogs');
	} 
	public function index()
	{		
		$this->laod_blog_detail();
	}
	public function laod_blog_detail(){
		$id = $this->uri->segment(2);
		
		$get_blog_detail = $this->mo_blogs->select_blog_by_id($id);
		$this->data['get_blog_detail'] = $get_blog_detail;
		$this->header_page_title = "blog";
		$this->header_icon = "fa-book";
		if($this->is_mobile==true){
			$this->array_css=array("mobile/blog_detail");
			$this->load_view_mobile('mobile/blog_detail');
		}else{
			$this->array_css = array("publics/blog_detail");
			$this->array_js = array("publics/blog_detail");
			$this->load_view('publics/blog_detail');
		}
	}

}
