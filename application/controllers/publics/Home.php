<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_home','mo_home');
		$this->load->model('Mo_blogs','mo_blogs');
	} 
	public function index()
	{	
		$data['get_blogs'] = $this->mo_blogs->select_blog_user_home();
		$get_home = $this->mo_home->get_properties(8,0);
		$data['get_home'] = $get_home;
		$data['is_logged_in'] = $this->is_logged_in;
		$data['chk_group_user'] = $this->chk_group_user;
		$data['chk_user_group_name'] = $this->chk_user_group_name;
		$data['me'] = $this->me;
		$data["mobile_login_msg"] = $this->session->flashdata('mobile_login_msg');
		if($this->is_mobile == true){
			$this->load->view('mobile/home',$data);
		}else{
			$this->load->view('publics/home',$data);
		}
	}
	public function ajax_load_blog(){
		$category = $this->input->post('category');
		if(isset($category) && $category != 'all'){
			$where = array(
				'category' => $category,
				);
		}else{
			$where = array();
			$category = 'all';
		}
		$get_blogs = $this->mo_blogs->select_blog_user($where,4,0);
		$data['get_blogs'] = $get_blogs;
		if($this->is_mobile == true){
			$this->load->view('mobile/ajax/home/ajax_load_blog',$data);
		}else{
			$this->load->view('publics/ajax/home/ajax_load_blog',$data);
		}
	}
}
