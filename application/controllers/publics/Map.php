<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Map extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_map','mo_map');
		$this->load->library('pagination');
	} 
	public function index()
	{		

		$get_perfecture_id 				= $this->input->get('pf');
		$get_city_id 	   				= $this->input->get('ct');
		$get_apartment    				= $this->input->get('apmt');
		$get_house         				= $this->input->get('hou');
		$get_buy           				= $this->input->get('by');
		$get_rent          				= $this->input->get('rt');
		$get_short         				= $this->input->get('sht');
		$get_min_price     				= $this->input->get('mnp');
		$get_max_price     				= $this->input->get('mxp');
		$city_group        				= $this->input->get('ctg');
		$get_cending	   				= $this->input->get('ced');
		$get_layout        				= $this->input->get('lo');
		$get_maked		   				= $this->input->get('mk');
		$get_position	   				= $this->input->get('po');
		$get_min_breadth   				= $this->input->get('mnb');
		$get_max_breadth   				= $this->input->get('mxb');
		$get_min_age   				    = $this->input->get('mna');
		$get_max_age   				    = $this->input->get('mxa');
		$get_min_distance_to_station    = $this->input->get('mnd');
		$get_max_distance_to_station    = $this->input->get('mxd');
		$get_other        				= $this->input->get('ot');
		$sort_price              		= $this->input->get('stp');
		$sort_breadth              		= $this->input->get('stb');
		$sort_age              			= $this->input->get('sta');
		$sort_distance              	= $this->input->get('std');

		//Get price data 
		$data_price_rent = array(
							10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,110000,120000,
							130000,140000,150000,160000,170000,180000,190000,200000,300000,400000,500000,
							600000,700000,800000,900000,1000000,1200000,1400000,1600000,1800000,2000000
					);
		$data_price_sell = array(
							2000000,4000000,6000000,8000000,10000000,12000000,14000000,16000000,18000000,20000000,25000000,30000000,
							40000000,50000000,60000000,70000000,80000000,90000000,100000000,120000000,140000000,160000000,180000000,200000000
					);
		$data_price = $data_price_sell;

		$where  = "properties.id != ''";
		$link_pagin = "";


		if($city_group != ""){
			$where  .= " AND properties.location_city in (".$city_group.")";
			$link_pagin .= "ctg=".$city_group."&";
		}

		if($get_perfecture_id != ""){
			$where .= " AND location_perfecture = ".$get_perfecture_id;
			$perfecture_name_get = $this->mo_map->get_perfecutre_name($get_perfecture_id);
			$this->data['get_perfecture_name'] = $perfecture_name_get[0]->perfecture_name;
			$this->data['perfecture_id_get'] = $get_perfecture_id;
			$link_pagin .= "pf=".$get_perfecture_id."&";
		}

		if($get_city_id != ""){
			$where .= " AND location_city = ".$get_city_id;
			$city_name_get = $this->mo_map->get_city_name($get_city_id);
			$this->data['get_city_name'] = $city_name_get[0]->city_name;
			$this->data['city_id_get'] = $get_city_id;
			$link_pagin .= "ct=".$get_city_id."&";
		}

		if($get_apartment != "" && $get_house != ""){
			$where .= "";
			$this->data['get_apartment'] = "yes";
			$this->data['get_house'] = "yes";
			$link_pagin .= "apmt=".$get_apartment."&hou=".$get_house."&";
		}else if($get_apartment != ""){
			$where .= " AND properties_type = "."'".$get_apartment."'";
			$this->data['get_apartment'] = "yes";
			$link_pagin .= "apmt=".$get_apartment."&";
		}else if($get_house != ""){
			$where .= " AND properties_type = "."'".$get_house."'";
			$this->data['get_house'] = "yes";
			$link_pagin .= "hou=".$get_house."&";
		}

		if($get_buy != "" && $get_rent != "" && $get_short != ""){
			$where .= "";
			$this->data['get_buy'] = "yes";
			$this->data['get_rent'] = "yes";
			$this->data['get_short'] = "yes";
			$link_pagin .= "by=".$get_buy."&rt=".$get_rent."&sht=".$get_short."&";
		}else if($get_buy != "" && $get_rent != ""){
			$where .= " AND status != 3";
			$this->data['get_buy'] = "yes";
			$this->data['get_rent'] = "yes";
			$link_pagin .= "by=".$get_buy."&rt=".$get_rent."&";
		}else if($get_buy != "" && $get_short != ""){
			$where .= " AND status != 2";
			$this->data['get_buy'] = "yes";
			$this->data['get_short'] = "yes";
			$link_pagin .= "by=".$get_buy."&sht=".$get_short."&";
		}else if($get_rent != "" && $get_short != ""){
			$where .= " AND status != 1";
			$this->data['get_rent'] = "yes";
			$this->data['get_short'] = "yes";
			$link_pagin .= "&rt=".$get_rent."&sht=".$get_short."&";
			$data_price = $data_price_rent;
		}else if($get_buy != ""){
			$where .= " AND status = 1";
			$this->data['get_buy'] = "yes";
			$link_pagin .= "by=".$get_buy."&";
		}else if($get_rent != ""){
			$where .= " AND status = 2";
			$this->data['get_rent'] = "yes";
			$link_pagin .= "rt=".$get_rent."&";
			$data_price = $data_price_rent;
		}else if($get_short != ""){
			$where .= " AND status = 3";
			$this->data['get_short'] = "yes";
			$link_pagin .= "sht=".$get_short."&";
			$data_price = $data_price_rent;
		}

		if($get_min_price != ""){
			$where .= " AND price >= ".$get_min_price;
			$this->data['get_min_price'] = $get_min_price;
			$link_pagin .= "mnp=".$get_min_price."&";
		}
		if($get_max_price != ""){
			$where .= " AND price <= ".$get_max_price;
			$this->data['get_max_price'] = $get_max_price;
			$link_pagin .= "mxp=".$get_max_price."&";
		}

		$link_status = "";
		if($get_layout != ""){
			$layout = explode(" ", $get_layout);
			$my_layout = "";
			for ($i=0; $i < count($layout) ; $i++) { 
				$my_layout .= "'".$layout[$i]."',";
			}
			$trim_layout = rtrim($my_layout, ',');
			$where .= " AND properties.layout in (".$trim_layout.")";
			$this->data['typecheck'] = $layout;
			$link_pagin .= "lo=".str_replace(' ', '+', $get_layout)."&";
			$link_status .= "lo=".str_replace(' ', '+', $get_layout)."&";
		}
		if($get_maked != ""){
			$maked = explode(" ", $get_maked);
			$my_maked = "";
			for ($i=0; $i < count($maked) ; $i++) { 
				$my_maked .= "'".$maked[$i]."',";
			}
			$trim_maked = rtrim($my_maked, ',');
			$where .= " AND properties.maked in (".$trim_maked.")";
			$this->data['makedcheck'] = $maked;
			$link_pagin .= "mk=".str_replace(' ', '+', $get_maked)."&";
			$link_status .= "mk=".str_replace(' ', '+', $get_maked)."&";
		}

		if($get_position != ""){
			$position = explode(" ", $get_position);
			$my_position = "";
			for ($i=0; $i < count($position) ; $i++) { 
				$my_position .= "'".$position[$i]."',";
			}
			$trim_position = rtrim($my_position, ',');
			$where .= " AND properties.position in (".$trim_position.")";
			$this->data['positioncheck'] = $position;
			$link_pagin .= "po=".str_replace(' ', '+', $get_position)."&";
			$link_status .= "po=".str_replace(' ', '+', $get_position)."&";
		}

		if($get_min_breadth != ""){
			$where .= " AND size >= ".$get_min_breadth;
			$link_pagin .= "mnb=".$get_min_breadth."&";
			$this->data['get_min_breadth'] = $get_min_breadth;
			$link_status .= "mnb=".$get_min_breadth."&";
		}
		if($get_max_breadth != ""){
			$where .= " AND size <= ".$get_max_breadth;
			$link_pagin .= "mxb=".$get_max_breadth."&";
			$this->data['get_max_breadth'] = $get_max_breadth;
			$link_status .= "mxb=".$get_max_breadth."&";
		}

		$year_now = date("Y");
		$cal_min_age = $year_now - $get_min_age;
		$cal_max_age = $year_now - $get_max_age;
		if($get_min_age != ""){
			$where .= " AND year_build <= ".$cal_min_age;
			$link_pagin .= "mna=".$get_min_age."&";
			$this->data['get_min_age'] = $get_min_age;
			$link_status .= "mna=".$get_min_age."&";
		}
		if($get_max_age != ""){
			$where .= " AND year_build >= ".$cal_max_age;
			$link_pagin .= "mxa=".$get_max_age."&";
			$this->data['get_max_age'] = $get_max_age;
			$link_status .= "mxa=".$get_max_age."&";
		}

		if($get_min_distance_to_station != ""){
			$where .= " AND distance_to_station >= ".$get_min_distance_to_station;
			$link_pagin .= "mnd=".$get_min_distance_to_station."&";
			$this->data['get_min_distance_to_station'] = $get_min_distance_to_station;
			$link_status .= "mnd=".$get_min_distance_to_station."&";
		}
		if($get_max_distance_to_station != ""){
			$where .= " AND distance_to_station <= ".$get_max_distance_to_station;
			$link_pagin .= "mxd=".$get_max_distance_to_station."&";
			$this->data['get_max_distance_to_station'] = $get_max_distance_to_station;
			$link_status .= "mxd=".$get_max_distance_to_station."&";
		}

		if($get_other != ""){
			$other = explode(" ", $get_other);
			$my_other = "";
			$where .= " AND (";
			for ($i=0; $i < count($other) ; $i++) { 
				if($i > 0){
					$where .= " OR properties.feature like '%".$other[$i]."%'";
				}else{
					$where .= "properties.feature like '%".$other[$i]."%'";
				}
			}
			$where .= ")";
			$this->data['othercheck'] = $other;
			$link_pagin .= "ot=".str_replace(' ', '+', $get_other);
			$link_status .= "ot=".str_replace(' ', '+', $get_other);
		}

		if(isset($sort_price)){
			$link_pagin .= "stp&ced=".$get_cending."&";
			$link_status .= "stp&ced=".$get_cending."&";
		}else if(isset($sort_breadth)){
			$link_pagin .= "stb&ced=".$get_cending."&";
			$link_status .= "stb&ced=".$get_cending."&";
		}else if(isset($sort_age)){
			$link_pagin .= "stb&ced=".$get_cending."&";
			$link_status .= "stb&ced=".$get_cending."&";
		}else if(isset($sort_distance)){
			$link_pagin .= "stb&ced=".$get_cending."&";
			$link_status .= "stb&ced=".$get_cending."&";
		}else{
			$link_pagin .= "";
			$link_status .= "";
		}

		$get_marker = $this->mo_map->get_properties_marker($where);
		$marker = json_encode($get_marker);
		$link_pagin = substr($link_pagin, 0,-1);
		$this->data['marker'] = $marker;
		$this->data['get_perfectures'] = $this->mo_map->get_perfectures();
		$this->data['link_to_lists'] = "/lists?".$link_pagin;
		$this->data['link_status'] = "/lists?".$link_status;
		$this->data['link_lists_ajax'] = "?".$link_status;
		$this->map_js = true;
		$this->data['map_js'] = $this->map_js;
		$this->header_page_title = "search";
		$this->header_icon = "fa-search";

		$this->data['get_price'] = $data_price;
		if($this->is_mobile == true){
			$this->array_css = array("mobile/map");
			$this->array_js = array("publics/map");
			$this->load_view_mobile('mobile/map');
		}else{
			$this->array_css = array("publics/map");
			$this->array_js = array("publics/map");
			$this->load_view('publics/map');
		}
	}
	public function ajax_load_city(){
		$get_perfecture_id 				= $this->input->post('perfecture_id');
		$get_apartment     				= $this->input->post('apartment');
		$get_house         				= $this->input->post('house');
		$get_buy           				= $this->input->post('buy');
		$get_rent          				= $this->input->post('rent');
		$get_short         				= $this->input->post('short_stay');
		$get_min_price     				= $this->input->post('min_price');
		$get_max_price     				= $this->input->post('max_price');
		$get_citys         				= $this->mo_map->get_citys($get_perfecture_id);
		$get_layout        				= $this->input->get('lo');
		$get_maked		   				= $this->input->get('mk');
		$get_position	   				= $this->input->get('po');
		$get_min_breadth   				= $this->input->get('mnb');
		$get_max_breadth   				= $this->input->get('mxb');
		$get_min_age   				    = $this->input->get('mna');
		$get_max_age   				    = $this->input->get('mxa');
		$get_min_distance_to_station    = $this->input->get('mnd');
		$get_max_distance_to_station    = $this->input->get('mxd');
		$get_other        				= $this->input->get('ot');
		$where = "";

		if($get_apartment != "" && $get_house != ""){
			$where .= "";
		}else if($get_apartment != ""){
			$where .= " AND properties_type = "."'".$get_apartment."'";
		}else if($get_house != ""){
			$where .= " AND properties_type = "."'".$get_house."'";
		}

		if($get_buy != "" && $get_rent != "" && $get_short != ""){
			$where .= "";
		}else if($get_buy != "" && $get_rent != ""){
			$where .= " AND status != 3";
		}else if($get_buy != "" && $get_short != ""){
			$where .= " AND status != 2";
		}else if($get_rent != "" && $get_short != ""){
			$where .= " AND status != 1";
		}else if($get_buy != ""){
			$where .= " AND status = 1";
		}else if($get_rent != ""){
			$where .= " AND status = 2";
		}else if($get_short != ""){
			$where .= " AND status = 3";
		}

		if($get_min_price != ""){
			$where .= " AND price >= ".$get_min_price;
		}
		if($get_max_price != ""){
			$where .= " AND price <= ".$get_max_price;
		}
		$link_status = "";
		if($get_layout != ""){
			$layout = explode(" ", $get_layout);
			$my_layout = "";
			for ($i=0; $i < count($layout) ; $i++) { 
				$my_layout .= "'".$layout[$i]."',";
			}
			$trim_layout = rtrim($my_layout, ',');
			$where .= " AND properties.layout in (".$trim_layout.")";
		}
		if($get_maked != ""){
			$maked = explode(" ", $get_maked);
			$my_maked = "";
			for ($i=0; $i < count($maked) ; $i++) { 
				$my_maked .= "'".$maked[$i]."',";
			}
			$trim_maked = rtrim($my_maked, ',');
			$where .= " AND properties.maked in (".$trim_maked.")";
		}

		if($get_position != ""){
			$position = explode(" ", $get_position);
			$my_position = "";
			for ($i=0; $i < count($position) ; $i++) { 
				$my_position .= "'".$position[$i]."',";
			}
			$trim_position = rtrim($my_position, ',');
			$where .= " AND properties.position in (".$trim_position.")";
		}

		if($get_min_breadth != ""){
			$where .= " AND size >= ".$get_min_breadth;
		}
		if($get_max_breadth != ""){
			$where .= " AND size <= ".$get_max_breadth;
		}

		$year_now = date("Y");
		$cal_min_age = $year_now - $get_min_age;
		$cal_max_age = $year_now - $get_max_age;
		if($get_min_age != ""){
			$where .= " AND year_build <= ".$cal_min_age;
		}
		if($get_max_age != ""){
			$where .= " AND year_build >= ".$cal_max_age;
		}

		if($get_min_distance_to_station != ""){
			$where .= " AND distance_to_station >= ".$get_min_distance_to_station;
		}
		if($get_max_distance_to_station != ""){
			$where .= " AND distance_to_station <= ".$get_max_distance_to_station;
		}

		if($get_other != ""){
			$other = explode(" ", $get_other);
			$my_other = "";
			$where .= " AND (";
			for ($i=0; $i < count($other) ; $i++) { 
				if($i > 0){
					$where .= " OR properties.feature like '%".$other[$i]."%'";
				}else{
					$where .= "properties.feature like '%".$other[$i]."%'";
				}
			}
			$where .= ")";
		}
		// echo $where;exit();
		$data['where'] = $where;
		$data['get_citys'] = $get_citys;
		if($this->is_mobile == true){
			$this->load->view('mobile/ajax/map/ajax_load_city',$data);
		}else{
			$this->load->view('publics/ajax/map/ajax_load_city',$data);
		}
	}

	public function ajax_load_building(){

		$get_perfecture_id = $this->input->get('perfecture_id');
		$get_city_id 	   = $this->input->get('city_id');
		$get_apartment     = $this->input->get('apartment');
		$get_house         = $this->input->get('house');
		$get_buy           = $this->input->get('buy');
		$get_rent          = $this->input->get('rent');
		$get_short         = $this->input->get('short_stay');
		$get_min_price     = $this->input->get('min_price');
		$get_max_price     = $this->input->get('max_price');

		$get_building_name = $this->input->get('building_name');

		$where = " properties.building_name = "."'".$get_building_name."'";

		if($get_perfecture_id != ""){
			$where .= " AND location_perfecture = ".$get_perfecture_id;
		}

		if($get_city_id != ""){
			$where .= " AND location_city = ".$get_city_id;
		}

		if($get_apartment != "" && $get_house != ""){
			$where .= "";
		}else if($get_apartment != ""){
			$where .= " AND properties_type = "."'".$get_apartment."'";
		}else if($get_house != ""){
			$where .= " AND properties_type = "."'".$get_house."'";
		}

		if($get_buy != "" && $get_rent != "" && $get_short != ""){
			$where .= "";
		}else if($get_buy != "" && $get_rent != ""){
			$where .= " AND status != 3";
		}else if($get_buy != "" && $get_short != ""){
			$where .= " AND status != 2";
		}else if($get_rent != "" && $get_short != ""){
			$where .= " AND status != 1";
		}else if($get_buy != ""){
			$where .= " AND status = 1";
		}else if($get_rent != ""){
			$where .= " AND status = 2";
		}else if($get_short != ""){
			$where .= " AND status = 3";
		}

		if($get_min_price != ""){
			$where .= " AND price >= ".$get_min_price;
		}
		if($get_max_price != ""){
			$where .= " AND price <= ".$get_max_price;
		}

		$count_building = $this->mo_map->get_building_count($where);
		$total_rows = $count_building[0]->count_building;
		$config = array();
        $config["base_url"] = "/map/ajax_load_building";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config['first_link'] = '«';
        $config['last_link'] = '»';
        $config['next_link'] = '>>';
        $config['prev_link'] = '<<';
        $config['num_links'] = 3;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';
        $this->pagination->initialize($config);
        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $limit = $config["per_page"];
        $pagination = $this->pagination->create_links();
		$get_building_detail = $this->mo_map->get_detail_building($where,$limit,$offset);
		$data['get_building_detail'] = $get_building_detail;
		$data['pagination'] = $pagination;
		$this->load->view('publics/ajax/map/ajax_load_building',$data);
	}

	public function ajax_load_latlng(){
		$get_perfecture_id 				= $this->input->post('perfecture_id');
		$get_city_id 	   				= $this->input->post('city_id');
		$get_apartment     				= $this->input->post('apartment');
		$get_house         				= $this->input->post('house');
		$get_buy           				= $this->input->post('buy');
		$get_rent          				= $this->input->post('rent');
		$get_short         				= $this->input->post('short_stay');
		$get_min_price     				= $this->input->post('min_price');
		$get_max_price     				= $this->input->post('max_price');
		$get_layout        				= $this->input->get('lo');
		$get_maked		   				= $this->input->get('mk');
		$get_position	   				= $this->input->get('po');
		$get_min_breadth   				= $this->input->get('mnb');
		$get_max_breadth   				= $this->input->get('mxb');
		$get_min_age   				    = $this->input->get('mna');
		$get_max_age   				    = $this->input->get('mxa');
		$get_min_distance_to_station    = $this->input->get('mnd');
		$get_max_distance_to_station    = $this->input->get('mxd');
		$get_other        				= $this->input->get('ot');
		$where = " properties.id != ''";

		if($get_perfecture_id != ""){
			$where .= " AND location_perfecture = ".$get_perfecture_id;
		}

		if($get_city_id != ""){
			$where .= " AND location_city = ".$get_city_id;
		}

		if($get_apartment != "" && $get_house != ""){
			$where .= "";
		}else if($get_apartment != ""){
			$where .= " AND properties_type = "."'".$get_apartment."'";
		}else if($get_house != ""){
			$where .= " AND properties_type = "."'".$get_house."'";
		}

		if($get_buy != "" && $get_rent != "" && $get_short != ""){
			$where .= "";
		}else if($get_buy != "" && $get_rent != ""){
			$where .= " AND status != 3";
		}else if($get_buy != "" && $get_short != ""){
			$where .= " AND status != 2";
		}else if($get_rent != "" && $get_short != ""){
			$where .= " AND status != 1";
		}else if($get_buy != ""){
			$where .= " AND status = 1";
		}else if($get_rent != ""){
			$where .= " AND status = 2";
		}else if($get_short != ""){
			$where .= " AND status = 3";
		}

		if($get_min_price != ""){
			$where .= " AND price >= ".$get_min_price;
		}
		if($get_max_price != ""){
			$where .= " AND price <= ".$get_max_price;
		}
		$link_status = "";
		if($get_layout != ""){
			$layout = explode(" ", $get_layout);
			$my_layout = "";
			for ($i=0; $i < count($layout) ; $i++) { 
				$my_layout .= "'".$layout[$i]."',";
			}
			$trim_layout = rtrim($my_layout, ',');
			$where .= " AND properties.layout in (".$trim_layout.")";
		}
		if($get_maked != ""){
			$maked = explode(" ", $get_maked);
			$my_maked = "";
			for ($i=0; $i < count($maked) ; $i++) { 
				$my_maked .= "'".$maked[$i]."',";
			}
			$trim_maked = rtrim($my_maked, ',');
			$where .= " AND properties.maked in (".$trim_maked.")";
		}

		if($get_position != ""){
			$position = explode(" ", $get_position);
			$my_position = "";
			for ($i=0; $i < count($position) ; $i++) { 
				$my_position .= "'".$position[$i]."',";
			}
			$trim_position = rtrim($my_position, ',');
			$where .= " AND properties.position in (".$trim_position.")";
		}

		if($get_min_breadth != ""){
			$where .= " AND size >= ".$get_min_breadth;
		}
		if($get_max_breadth != ""){
			$where .= " AND size <= ".$get_max_breadth;
		}

		$year_now = date("Y");
		$cal_min_age = $year_now - $get_min_age;
		$cal_max_age = $year_now - $get_max_age;
		if($get_min_age != ""){
			$where .= " AND year_build <= ".$cal_min_age;
		}
		if($get_max_age != ""){
			$where .= " AND year_build >= ".$cal_max_age;
		}

		if($get_min_distance_to_station != ""){
			$where .= " AND distance_to_station >= ".$get_min_distance_to_station;
		}
		if($get_max_distance_to_station != ""){
			$where .= " AND distance_to_station <= ".$get_max_distance_to_station;
		}

		if($get_other != ""){
			$other = explode(" ", $get_other);
			$my_other = "";
			$where .= " AND (";
			for ($i=0; $i < count($other) ; $i++) { 
				if($i > 0){
					$where .= " OR properties.feature like '%".$other[$i]."%'";
				}else{
					$where .= "properties.feature like '%".$other[$i]."%'";
				}
			}
			$where .= ")";
		}

		// echo $where;exit();
		$get_marker = $this->mo_map->get_properties_marker($where);
		$marker = json_encode($get_marker);
		echo $marker;
	}
}
