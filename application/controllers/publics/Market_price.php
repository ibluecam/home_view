<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Market_price extends MY_Controller {

	public function __construct(){
		parent::__construct();
	} 
	public function index()
	{		
		$this->header_page_title = "SEARCH";
		$this->header_icon = "fa-search";

		if($this->is_mobile==true){
			$this->array_css=array("mobile/market_price");
			$this->load_view_mobile('mobile/market_price');
		}else{
			$this->array_css=array("publics/market_price");
			$this->load_view('publics/market_price');
		}
	}
}
