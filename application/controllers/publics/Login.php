<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('User','mo_user');
	} 
	public function index(){	
		 if ($this->is_logged_in) {
		 	redirect("/favorite");
		 }else{
			$this->login();
		}
	}

	public function login(){
		$this->data['error_message'] = '';
		if($this->input->post('btngo')){
			$this->login_exc();
		}
		$this->header_page_title = "Account";
		$this->header_icon = "fa-user";


		if($this->is_mobile==true){
			$this->array_css=array("mobile/login");
			$this->array_js=array("publics/login");
			$this->load_view_mobile('mobile/login');
		 }else{
			 $this->array_js=array("publics/login");
			 $this->array_css=array("publics/login");
			 $this->load_view('publics/login');
		}

	}

 	public function set_login_rules(){
    	$this->form_validation->set_rules('email', 'Email', 'required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[3]|max_length[20]');
    }

    public function login_exc(){
    	$error_message = '';
		$email=$this->input->post("email");
		$password=$this->input->post("password");
		$this->set_login_rules();
		if ($this->form_validation->run() == TRUE){
			$login = $this->mo_user->login($email, $password, $error_message);
			if($login){
				$is_logged_in=$this->mo_user->getMyDetail();
				
				if($this->is_mobile==true){
					if(!empty($is_logged_in) && $is_logged_in->user_group != 3){
						$this->session->set_flashdata('mobile_login_msg', '<div class="alert alert-error login-error"><strong>Warning!</strong> Guest only can login on mobile!<div>X</div></div>');
						$this->logout();
					}
				}
				if(!empty($is_logged_in) && $is_logged_in->user_group==1){
					redirect('/admin/agent');
				}else if(!empty($is_logged_in) && $is_logged_in->user_group==2){
					redirect('/admin/properties');
				}else{
					redirect('/favorite');
				}
			}
		}
		$this->data['error_message'] = $error_message;
    }

    public function logout() {
        $this->mo_user->logout();
        redirect('/');
    }
}
