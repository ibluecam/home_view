<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Step extends MY_Controller {

	public function __construct(){
		parent::__construct();
	} 
	public function index()
	{		
		$this->laod_step();
	}
	public function laod_step(){
        $get_step = $this->uri->segment(2);
        if($get_step == 'buy'){
            $step = $get_step;
        }else if($get_step == 'short-stay'){
            $step = $get_step;
        }else{
            $step = 'rent';
        }
        $this->data['step'] = $step;

		$this->header_page_title = "HOW TO GET A PLACE IN JAPAN";

		if($this->is_mobile == true){
			$this->array_css = array("mobile/step");
			$this->array_js = array("publics/step");
			$this->load_view_mobile('mobile/step');
		}else{
			$this->header_icon = "fa-lightbulb-o";
			$this->array_css = array("publics/step");
			$this->array_js = array("publics/step");
			$this->load_view('publics/step');
		}

		
	}

}
