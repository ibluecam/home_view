<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Videos extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_videos','mo_videos');
		$this->load->library('pagination');
	} 
	public function index()
	{		
		$this->laod_videos();
	}
	public function laod_videos(){

		$category = $this->uri->segment(2);
		$video_count = $this->mo_videos->get_video_count();
		$total_rows = $video_count[0]->count_video;
		$config = array();
        $config["base_url"] = "/videos";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 15;
        $config["uri_segment"] = 2;
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['next_link'] = 'NEXT';
        $config['prev_link'] = 'PREVIOUS';
        $config['num_links'] = 3;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';
        $this->pagination->initialize($config);
        $offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $limit = $config["per_page"];
        $pagination = $this->pagination->create_links();
        $this->data['pagination'] = $pagination;

		$get_vdo_list = $this->mo_videos->get_video_list($limit,$offset);
		$this->data['get_video_list'] = $get_vdo_list;
		$this->header_page_title = "360<sup>&deg;</sup> Images";
		$this->header_icon = "fa-search";
		if($this->is_mobile==true){
			$this->array_js = array("mobile/videos");
			$this->array_css=array("mobile/videos");
			$this->load_view_mobile('mobile/videos');
		}else{
			$this->array_css = array("publics/videos");
			$this->array_js = array("publics/videos");
			$this->load_view('publics/videos');
		}
	}

}
