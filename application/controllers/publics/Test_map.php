<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Test_map extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Test_Map_Model','mo_map');
	} 
	public function index()
	{	
		$page_type = $this->input->get('page_type');
		$start_price = $this->input->get('start_price');
		$start_area = $this->input->get('start_area');
		if(isset($page_type)){
			$page_type = $page_type;
		}else{
			$page_type = "buy";
		}
		if(isset($start_price)){
			$start_price = "[".$start_price."]";
		}else{
			$start_price = "[1e6]";
		}
		if(isset($start_area)){
			$start_area = "[".$start_area."]";
		}else{
			$start_area = "[0]";
		}
		$get_latlng = $this->mo_map->getAllLatLng();
		$latlng = json_encode($get_latlng);
		$data['latlng'] = $latlng;
		$data['page_type'] = $page_type;
		$data['start_price'] = $start_price;
		$data['start_area'] = $start_area;
		$this->load->view('publics/test_map',$data);
	}
	public function testData(){
		$get_id = $this->input->post('id');
		$id = explode(',', $get_id);
		$cut_end_lng = array_pop($id);
		$result = $this->mo_map->testData($id);
		if(count($result) > 0){
			echo json_encode($result);
		}	
	}
}
