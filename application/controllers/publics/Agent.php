<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Agent extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_agent','mo_agent');
		$this->load->model('User','mo_user');
		$this->load->library('pagination');
	} 
	public function index()
	{		
		$this->laod_agent();
	}
	public function laod_agent(){
		$agent_id = $this->uri->segment(2);

		$count_agent_properties = $this->mo_agent->get_agent_count_properties($agent_id);

		$count_properties = $count_agent_properties[0]->count_properties;

		$config = array();
        $config["base_url"] = "/agent/".$agent_id;
        $config["total_rows"] = $count_properties;
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['next_link'] = 'NEXT';
        $config['prev_link'] = 'PREVIOUS';
        $config['num_links'] = 3;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';
        $this->pagination->initialize($config);
        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $limit = $config["per_page"];
        $pagination = $this->pagination->create_links();
        $this->data['pagination'] = $pagination;
        $agent_properties = $this->mo_agent->get_agent_properties($agent_id,$limit,$offset);

        //Get Agent's Properties
		$this->data['agent_properties'] = $agent_properties;
		//Count Agent's Properties
		$this->data['count_agent_properties'] = $count_properties;
		//Get Agent's Info
		$this->data['agent_info'] = $this->mo_user->select_user_by_id($agent_id);
		$this->header_page_title = "agent profile";
		$this->header_icon = "fa-user";

		if($this->is_mobile==true){
			$this->array_css=array("mobile/agent");
			$this->load_view_mobile('mobile/agent');
		}else{
			// $this->array_css=array("mobile/agent");
			// $this->load_view_mobile('mobile/agent');
			$this->array_css = array("publics/agent");
			$this->load_view('publics/agent');
		}
	}

}
