<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class City extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_city','mo_city');
	} 
	public function index()
	{		
		$this->laod_city();
	}
	public function laod_city(){
		$name_city = $this->uri->segment(2);
		if($name_city){
			$name_city = $name_city;
		}else{
			$name_city = "all";
		}
		$get_status = $this->input->get('sta');
		if($get_status != ""){
			if($get_status == 'buy'){
				$status = 1;
			}else if($get_status == 'rent'){
				$status = 2;
			}else if($get_status == 'short-stay'){
				$status = 3;
			}else{
				$status = "";
			}
		}else{
			$status = "";
		}
		$get_perfectrue = $this->mo_city->get_perfectures();
		$this->data['perfecture_lists'] = $get_perfectrue;
		$this->data['name_city'] = $name_city;
		$this->data['get_citys'] = $this->mo_city->get_citys($status);
		$this->data['status'] = $get_status;
		$this->header_page_title = "select city";
		$this->header_icon = "fa-building";
		if($this->is_mobile == true){
			$this->array_css = array("mobile/city");
			$this->array_js = array("publics/city");
			$this->load_view_mobile('mobile/city');
		}else{
			$this->array_css = array("publics/city");
			$this->array_js = array("publics/city");
			$this->load_view('publics/city');
		}
	}

}
