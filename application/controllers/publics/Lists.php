<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lists extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mo_lists','mo_lists');
		$this->load->library('pagination');
	} 
	public function index()
	{		
		$this->header_page_title = "search";
		$this->header_icon = "fa-search";

		$get_perfecture_id 				= $this->input->get('pf');
		$get_city_id 	   				= $this->input->get('ct');
		$get_apartment     				= $this->input->get('apmt');
		$get_house         				= $this->input->get('hou');
		$get_buy           				= $this->input->get('by');
		$get_rent          				= $this->input->get('rt');
		$get_short         				= $this->input->get('sht');
		$get_min_price     				= $this->input->get('mnp');
		$get_max_price     				= $this->input->get('mxp');
		$city_check        				= $this->input->post('checkcity');
		$city_status					= $this->input->post('citystatus');
		$city_group        				= $this->input->get('ctg');
		$get_cending	   				= $this->input->get('ced');
		$get_layout        				= $this->input->get('lo');
		$get_maked		   				= $this->input->get('mk');
		$get_position	   				= $this->input->get('po');
		$get_min_breadth   				= $this->input->get('mnb');
		$get_max_breadth   				= $this->input->get('mxb');
		$get_min_age   				    = $this->input->get('mna');
		$get_max_age   				    = $this->input->get('mxa');
		$get_min_distance_to_station    = $this->input->get('mnd');
		$get_max_distance_to_station    = $this->input->get('mxd');
		$get_other        				= $this->input->get('ot');
		$sort_price              		= $this->input->get('stp');
		$sort_breadth              		= $this->input->get('stb');
		$sort_age              			= $this->input->get('sta');
		$sort_distance              	= $this->input->get('std');

		$status = $this->uri->segment(2);

		if($city_status != ""){
			$status = $city_status;
		}
		$data_price_rent = array(
							10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,110000,120000,
							130000,140000,150000,160000,170000,180000,190000,200000,300000,400000,500000,
							600000,700000,800000,900000,1000000,1200000,1400000,1600000,1800000,2000000
					);
		$data_price_sell = array(
							2000000,4000000,6000000,8000000,10000000,12000000,14000000,16000000,18000000,20000000,25000000,30000000,
							40000000,50000000,60000000,70000000,80000000,90000000,100000000,120000000,140000000,160000000,180000000,200000000
					);

		$data_price = $data_price_sell;

		$link_status = "";

		$where  = "properties.id != '' ";

		$link_pagin = "";
		if(isset($city_check) && $city_check != ""){
			$my_city_id = implode(',', $city_check);
			$where  .= "AND properties.location_city in (".$my_city_id.")";
			$link_pagin .= "ctg=".$my_city_id."&";
		}
		if($city_group && $city_group != ""){
			$where  .= " AND properties.location_city in (".$city_group.")";
			$link_pagin .= "ctg=".$city_group."&";
		}

		if($status){
			if($status == 'buy'){
				$where .= " AND status = 1";
				$link_pagin .= "by=buy&";
			}else if($status == 'rent'){
				$where .= " AND status = 2";
				$link_pagin .= "rt=rent&";
				$data_price = $data_price_rent;
			}else if($status == 'short-stay'){
				$where .= " AND status = 3";
				$link_pagin .= "sht=short-stay&";
				$data_price = $data_price_rent;
			}else{
				$where .= "";
			}
		}else{
			$status = 'all';
		}

		if($get_perfecture_id != ""){
			$where .= " AND location_perfecture = ".$get_perfecture_id;
			$link_pagin .= "pf=".$get_perfecture_id."&";
			$perfecture_name_get = $this->mo_lists->get_perfecutre_name($get_perfecture_id);
			$this->data['get_perfecture_name'] = $perfecture_name_get[0]->perfecture_name;
		}

		if($get_city_id != ""){
			$where .= " AND location_city = ".$get_city_id;
			$link_pagin .= "ct=".$get_city_id."&";
			$city_name_get = $this->mo_lists->get_city_name($get_city_id);
			$this->data['get_city_name'] = $city_name_get[0]->city_name;
		}

		if($get_apartment != "" && $get_house != ""){
			$where .= "";
			$link_pagin .= "apmt=".$get_apartment."&hou=".$get_house."&";
		}else if($get_apartment != ""){
			$where .= " AND properties_type = "."'".$get_apartment."'";
			$link_pagin .= "apmt=".$get_apartment."&";
		}else if($get_house != ""){
			$where .= " AND properties_type = "."'".$get_house."'";
			$link_pagin .= "hou=".$get_house."&";
		}

		if($get_buy != "" && $get_rent != "" && $get_short != ""){
			$where .= "";
			$link_pagin .= "by=".$get_buy."&rt=".$get_rent."&sht=".$get_short."&";
		}else if($get_buy != "" && $get_rent != ""){
			$where .= " AND status != 3";
			$link_pagin .= "by=".$get_buy."&rt=".$get_rent."&";
		}else if($get_buy != "" && $get_short != ""){
			$where .= " AND status != 2";
			$link_pagin .= "by=".$get_buy."&sht=".$get_short."&";
		}else if($get_rent != "" && $get_short != ""){
			$where .= " AND status != 1";
			$link_pagin .= "rt=".$get_rent."&sht=".$get_short."&";
			$data_price = $data_price_rent;
		}else if($get_buy != ""){
			$where .= " AND status = 1";
			$link_pagin .= "by=".$get_buy."&";
		}else if($get_rent != ""){
			$where .= " AND status = 2";
			$link_pagin .= "rt=".$get_rent."&";
			$data_price = $data_price_rent;
		}else if($get_short != ""){
			$where .= " AND status = 3";
			$link_pagin .= "sht=".$get_short."&";
			$data_price = $data_price_rent;
		}

		$this->data['link_status'] = '/lists?'.$link_pagin;

		if($get_min_price != ""){
			$where .= " AND price >= ".$get_min_price;
			$link_pagin .= "mnp=".$get_min_price."&";
			$this->data['get_min_price'] = $get_min_price;
		}
		if($get_max_price != ""){
			$where .= " AND price <= ".$get_max_price;
			$link_pagin .= "mxp=".$get_max_price."&";
			$this->data['get_max_price'] = $get_max_price;
		}

		if($get_layout != ""){
			$layout = explode(" ", $get_layout);
			$my_layout = "";
			for ($i=0; $i < count($layout) ; $i++) { 
				$my_layout .= "'".$layout[$i]."',";
			}
			$trim_layout = rtrim($my_layout, ',');
			$where .= " AND properties.layout in (".$trim_layout.")";
			$this->data['typecheck'] = $layout;
			$link_pagin .= "lo=".str_replace(' ', '+', $get_layout)."&";
		}

		if($get_maked != ""){
			$maked = explode(" ", $get_maked);
			$my_maked = "";
			for ($i=0; $i < count($maked) ; $i++) { 
				$my_maked .= "'".$maked[$i]."',";
			}
			$trim_maked = rtrim($my_maked, ',');
			$where .= " AND properties.maked in (".$trim_maked.")";
			$this->data['makedcheck'] = $maked;
			$link_pagin .= "mk=".str_replace(' ', '+', $get_maked)."&";
		}

		if($get_position != ""){
			$position = explode(" ", $get_position);
			$my_position = "";
			for ($i=0; $i < count($position) ; $i++) { 
				$my_position .= "'".$position[$i]."',";
			}
			$trim_position = rtrim($my_position, ',');
			$where .= " AND properties.position in (".$trim_position.")";
			$this->data['positioncheck'] = $position;
			$link_pagin .= "po=".str_replace(' ', '+', $get_position)."&";
		}

		if($get_min_breadth != ""){
			$where .= " AND size >= ".$get_min_breadth;
			$link_pagin .= "mnb=".$get_min_breadth."&";
			$this->data['get_min_breadth'] = $get_min_breadth;
		}
		if($get_max_breadth != ""){
			$where .= " AND size <= ".$get_max_breadth;
			$link_pagin .= "mxb=".$get_max_breadth."&";
			$this->data['get_max_breadth'] = $get_max_breadth;
		}

		$year_now = date("Y");
		$cal_min_age = $year_now - $get_min_age;
		$cal_max_age = $year_now - $get_max_age;
		if($get_min_age != ""){
			$where .= " AND year_build <= ".$cal_min_age;
			$link_pagin .= "mna=".$get_min_age."&";
			$this->data['get_min_age'] = $get_min_age;
		}
		if($get_max_age != ""){
			$where .= " AND year_build >= ".$cal_max_age;
			$link_pagin .= "mxa=".$get_max_age."&";
			$this->data['get_max_age'] = $get_max_age;
		}

		if($get_min_distance_to_station != ""){
			$where .= " AND distance_to_station >= ".$get_min_distance_to_station;
			$link_pagin .= "mnd=".$get_min_distance_to_station."&";
			$this->data['get_min_distance_to_station'] = $get_min_distance_to_station;
		}
		if($get_max_distance_to_station != ""){
			$where .= " AND distance_to_station <= ".$get_max_distance_to_station;
			$link_pagin .= "mxd=".$get_max_distance_to_station."&";
			$this->data['get_max_distance_to_station'] = $get_max_distance_to_station;
		}

		if($get_other != ""){
			$other = explode(" ", $get_other);
			$my_other = "";
			$where .= " AND (";
			for ($i=0; $i < count($other) ; $i++) { 
				if($i > 0){
					$where .= " OR properties.feature like '%".$other[$i]."%'";
				}else{
					$where .= "properties.feature like '%".$other[$i]."%'";
				}
			}
			$where .= ")";
			$this->data['othercheck'] = $other;
			$link_pagin .= "ot=".str_replace(' ', '+', $get_other)."&";
		}

		$link_list = $link_pagin;
		if(isset($sort_price)){
			$order = 'price';
			$cending = $get_cending;
			$link_pagin .= "stp&ced=".$get_cending."&";
		}else if(isset($sort_breadth)){
			$order = 'size';
			$cending = $get_cending;
			$link_pagin .= "stb&ced=".$get_cending."&";
		}else if(isset($sort_age)){
			$order = 'year_build';
			$cending = $get_cending;
			$link_pagin .= "stb&ced=".$get_cending."&";
		}else if(isset($sort_distance)){
			$order = 'distance_to_station';
			$cending = $get_cending;
			$link_pagin .= "stb&ced=".$get_cending."&";
		}
		else{
			$order = 'date_created';
			$cending = 'DESC';
		}


		$count_city = $this->mo_lists->get_count_home($where);
		$this->data['count_city'] = $count_city[0]->count_home;
		
		$link_pagin = substr($link_pagin, 0,-1);
		
		$link_to_list = "/lists?".$link_list;

		$count_properties = $this->mo_lists->get_count_properties($where);
		$total_rows = $count_properties[0]->count_properties;

		if($this->is_mobile == true){
			$num_links = 3;
		}else{
			$num_links = 7;
		}
		$config = array();
		$config['page_query_string'] = TRUE;
        $config["base_url"] = "/lists?".$link_pagin;
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 6;
        $config["uri_segment"] = 3;
        $config['first_link'] = '«';
        $config['last_link'] = '»';
        $config['next_link'] = 'NEXT';
        $config['prev_link'] = 'PREVIOUS';
        $config['num_links'] = $num_links;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = '</span>';
        $this->pagination->initialize($config);
		$get_thumb_img = array();
        $offset = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $limit = $config["per_page"];
		$get_lists = $this->mo_lists->get_properties($where,$limit,$offset,$order,$cending);
		foreach ($get_lists as $row) {
			$get_image_lists = $this->mo_lists->get_properties_images($row->id);
		    array_push($get_thumb_img,$get_image_lists);
		}
	
		$pagination = $this->pagination->create_links();
		if($this->is_logged_in){
			$user_id = $this->me->user_id;
			$get_fav_lists = $this->mo_lists->get_favorite_lists($user_id);
			$my_fav_pro_id = "";
			foreach ($get_fav_lists as $row) {
				$my_fav_pro_id .= $row->properties_id_fk.",";
			}
			$fav_pro_id = explode(',', $my_fav_pro_id);
			$cut_last_arr = array_pop($fav_pro_id);
			$this->data['fav_pro_id'] = $fav_pro_id;
		}
		//Get breadth data
		$data_breadth = array(
							20,25,30,35,40,50,60,70,80,90,100
							);
		//Get age data
		$data_age = array(
						0,1,2,3,5,10,15,20
						);
		//Get breadth data
		$data_distance = array(
							3,5,7,10,20
							);
		$this->data['get_price'] = $data_price;
		$this->data['get_breadth'] = $data_breadth;
		$this->data['get_age'] = $data_age;
		$this->data['get_distance'] = $data_distance;
		$this->data['get_image_lists'] = $get_thumb_img;
		$this->data['status'] = $status;
		$this->data['get_lists'] = $get_lists;
		$this->data['pagination'] = $pagination;
		$this->data['link_to_map'] = "/map?".$link_pagin;
		$this->data['link_to_list'] = $link_to_list;
		if($this->is_mobile == true){
			$this->array_css = array("mobile/lists","publics/animate_font");
			$this->array_js = array("publics/lists");
			$this->load_view_mobile('mobile/lists');
		}else{
			$this->array_css = array("publics/lists","publics/animate_font");
			$this->array_js = array("publics/lists");
			$this->load_view('publics/lists');
		}
	}
	public function add_favorite(){
		$home_id = $this->input->post('home_id');
		$user_id = $this->me->user_id;
		$today = date('Y-m-d H:i:s');
		$data = array(
			'user_id_fk'       => $user_id,
			'properties_id_fk' => $home_id,
			'created_dt'       => $today,
			'updated_dt'       => $today
			);
		$this->mo_lists->add_favorite($data);
	}
	public function delete_favorite(){
		$home_id = $this->input->post('home_id');
		$user_id = $this->me->user_id;
		$where = array(
			'user_id_fk' 	   => $user_id,
			'properties_id_fk' => $home_id
			);
		$this->mo_lists->delete_favorite($where);
	}
}